import { graphql }          from 'react-apollo'

import queryGenerate        from 'engine/graphql/generate/create/query'

import config               from 'app/feedback/config'


export default (payload) => {
  const { queryMap, queryName } = config

  const query = queryGenerate({ queryMap, name: queryName })

  return graphql(query, {
    props: ({ data }) => {
      const { feedbackTypeList = {} } = data || {}

      if (feedbackTypeList && feedbackTypeList.errors && !feedbackTypeList.errors.length) {
        const { errors, result = {} } = feedbackTypeList
        const { code } = errors

        if (code === 0 && result !== null) {
          const dataFeedbackType = result.reduce((dataFeedbackType, item) => {
            const { id, value } = item

            return { ...dataFeedbackType, [id]: { id, value } }
          }, {})

          const orderFeedbackType = Object.keys(dataFeedbackType)

          return { orderFeedbackType, dataFeedbackType }
        }
      }
    }
  })
}
