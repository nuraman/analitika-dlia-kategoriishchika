import React, { Component } from 'react'

import { compose }          from 'redux'

import styled               from 'styled-components'


import withFeedbackType     from 'app/feedback/graphQl/withFeedbackType'


const Div = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-bottom: 10px;
`

const DivCheck = styled.div`
  display: flex;
  align-items: center;
  padding-right: 20px;
`

const Input= styled.input`
  display: none;
  
  &:checked {
    display: block; 
  }
`

const LabelCheck = styled.label`
  position: relative;
  width: 18px;
  min-width: 18px;
  height: 18px;
  margin-right: 10px;
  border: 1px solid #48b9bf;
  cursor: pointer;
  
  &:before {
    display: ${ props=>props.checked ? 'block' : 'none' };
    content: "";
    position: absolute;
    width: 12px;
    height: 12px;
    top: 3px;
    left: 3px;
    background-color: #48b9bf;
  }
`

const Label = styled.label`
  margin-bottom: -2px;
  font-weight: 300;
  font-size: 14px;
  font-family: HelveticaRegular;
  color: #fff;
  cursor: pointer;
`

class FeedbackType extends Component {
  constructor() {
    super()

    this.state = {
      selected: '',
      dataFeedbackType: {},
      orderFeedbackType: [],
    }
  }

  onChange = ({ id }) => {
    const { onSelect } = this.props

    onSelect({ id })

    this.setState({selected: id})
  }

  componentWillReceiveProps(nextProps) {
    const { dataFeedbackType, orderFeedbackType = [], onSelect } = nextProps

    if (orderFeedbackType.length > 0) {
      const { id } = dataFeedbackType[orderFeedbackType[0]]

      onSelect({ id })

      this.setState({ dataFeedbackType, orderFeedbackType, selected: id })
    }
  }

  render() {
    const { dataFeedbackType, orderFeedbackType = [], selected } = this.state

    return (
      <Div>
        {
          orderFeedbackType.map((item) => {
            const { id, value } = dataFeedbackType[item]

            return (
              <DivCheck key={ id }>
                <Input type='checkbox' />
                <LabelCheck
                  checked={ selected === id  }
                  onClick={ () => this.onChange({ id }) }
                />
                <Label
                  onClick={ () => this.onChange({ id }) }
                >
                  { value }
                </Label>
              </DivCheck>
            )
          })
        }
      </Div>
    )
  }
}

export default compose(
  withFeedbackType()
)(FeedbackType)
