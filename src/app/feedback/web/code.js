const codeMap = {
  CLOSE_FEEDBACK: 6,
  SUCCESS:        0,
  ERROR:          1
}

export default codeMap
