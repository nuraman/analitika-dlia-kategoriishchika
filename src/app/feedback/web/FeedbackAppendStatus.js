import React, { Component } from 'react'

import styled               from 'styled-components'

import codeFeedback         from './code'


const DivContainer = styled.div`
 position: absolute;
 display: ${ props=>props.open ? 'block' : 'none' };
 top: 25%;
 left: 50%;
 width: 368px;
 height: 109px;
 background-color: #484848;
`

const DivHeader = styled.div`
  display: table;
  width: 100%;
  height: 30px;
`

const DivRight = styled.div`
  display: table-cell;
  text-align: right;
  vertical-align: middle;
  height: 100%;
`

const DivInline = styled.div`
  display: inline-flex;
  align-items: center;
  justify-content: space-around;
  height: 100%;
`

const DivClose = styled.div`
  box-sizing: border-box;
  
  height: 17px;
  min-width: 30px;
  width: 30px;
  
  cursor: pointer;
  
  fill: #fff;
  
  background-image: url(assets/discount/closeWight.svg);
  background-size: cover;
`

const DivContent = styled.div`
  margin-bottom: 20px;
`

const DivRow = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  height: 30px;
`

const DivTextThank = styled.div`
  font-family: HelveticaLight;
  font-size: 22px;
  color: #fff;
  margin-left: 10px;
`

const DivIcoThank = styled.div`
  min-width: 50px;
  width: 50px;
  height: 32px;
  
  background-image: url(assets/discount/thank.svg);
  background-size: cover;
`

const DivTextStatus = styled.div`
  font-family: HelveticaLight;
  font-size: 14px;
  color: #fff;
  padding-left: 60px;
`


class FeedbackAppendStatus extends Component {
  constructor() {
    super()

    this.state = {
      open: false,
      code: ''
    }
  }

  onSetState = ({ open, code }) => {

    const { onClose } = this.props

    this.setState({ open, code })

    if (open === true) {
      setTimeout(() => {
        this.setState({ open: !open })
        onClose()
      }, 3000)
    }
  }

  componentWillReceiveProps(nextProps) {
    const { open, code } = nextProps

    this.onSetState({ open, code })
  }

  componentWillMount() {
    const { open, code } = this.props

    this.onSetState({ open, code })
  }

  onHandleClick = () => {
    this.setState({ open: !this.state.open })

    const { onClose } = this.props

    onClose()
  }

  render() {
    const { open, code } = this.state
    let message = ''

    if (code === codeFeedback.SUCCESS) {
      message = 'Ваше сообщение отправлено, спасибо за обратную связь'
    }

    if (code === codeFeedback.ERROR) {
      message = 'Ошибка при отправке сообщения'
    }

    if (code === codeFeedback.CLOSE_FEEDBACK) return null

    return (
      <DivContainer open={ open }>
        <DivHeader>
          <DivRight>
            <DivInline>
              <DivClose
                onClick={ this.onHandleClick }
              />
            </DivInline>
          </DivRight>
        </DivHeader>
        <DivContent>
          <DivRow>
            <DivIcoThank />
            <DivTextThank>
              Спасибо
            </DivTextThank>
          </DivRow>
          <DivRow>
            <DivTextStatus>
              { message }
            </DivTextStatus>
          </DivRow>
        </DivContent>
      </DivContainer>
    )
  }
}

export default FeedbackAppendStatus
