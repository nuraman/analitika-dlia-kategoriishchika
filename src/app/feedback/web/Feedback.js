import styled               from 'styled-components'

import React, { Component } from 'react'

import { compose }          from 'redux'


import withFeedbackAppend   from 'app/feedback/graphQl/withFeedbackAppend'

import FeedbackType         from './FeedbackType'

import codeFeedback         from './code'


const DivContainer = styled.div`
  background-color: #484848;
  position: absolute;
  width: 442px;
  height: 390px;
  top: 60px;
  left: 35%;
`

const DivHeader = styled.div`
  display: flex;
  width: 100%;
  flex-direction: row;
  align-items: center;
  height: 39px;
  justify-content: space-between;
  border-bottom: 1px solid rgb(158, 158, 158); 
`

const Title = styled.div`
  display: flex;
  padding-left: 20px;
  height: 18px;
  font-size: 18px;
  font-family: HelveticaRegular;
  color: #fff;
`

const Ico = styled.div`
  min-width: 20px;
  width: 20px;
  height: 17px;
  fill: #9e9e9e;
  
  padding-right: 15px;
  
  cursor: pointer;
  
  background-image: url(${ props => props.path });
  background-size: cover;
`

const DivContent = styled.div`
  display: block;
  padding: 20px;
  width: calc(100% - 40px);
  height: calc(100% - 40px);
`

const DivText = styled.div`
  padding-left: 10px;
  font-family: HelveticaRegular;
  color: #fff;
`

const Textarea = styled.textarea`
  box-sizing: border-box;

  width: 100%;

  border: 1px solid rgb(158, 158, 158); 
  background-color: transparent;

  font-family: HelveticaRegular;
  font-size: 14px;
  color: #fff;
  outline: none;
  resize: none;
  height: 200px;
  padding: 8px;
`

const Button = styled.a`
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  
  margin-top: 10px;
  
  width: 100%;
  height: 50px;
  
  box-sizing: border-box;
  
  font-weight: 300;
  font-size: 18px;
  font-family: HelveticaRegular;
  
  text-decoration: none;
  border: none;
  outline: none;
  color: #fff;
  background-color: #48b9bf;
  width: 100%;
  cursor: pointer;
`


class Feedback extends Component {
  constructor() {
    super()

    this.state = {
      code: '',
      open: false,
      discountDescription: '',
      typeFeedback: '',
    }
  }

  onClose = (code) => {
    this.setState({ open: !this.state.open, code })

    const { onClose } = this.props

    onClose({ code })
  }

  onChange = ({ currentTarget: { value } }) => this.setState({ discountDescription: value })

  onSelectFeedbackType = ({ id }) => this.setState({ typeFeedback: id })

  onHandleClick = () => {
    const { discountDescription, typeFeedback } = this.state

    this.props.feedbackAppend({
      message: discountDescription,
      messageType: typeFeedback,
    }).then(({ data }) => {
      const { feedbackAppend } = data
      const { errors: { code } } = feedbackAppend

      this.onClose(code)
    })
  }

  componentWillMount() {
    const { open } = this.props

    this.setState({ open })
  }

  render() {
    const { open, discountDescription } = this.state

    if (!open) return null

    return (
      <DivContainer>
        <DivHeader>
          <Title>
            Обратная связь
          </Title>
          <Ico
            path='assets/close.svg'
            onClick={ () => this.onClose(codeFeedback.CLOSE_FEEDBACK) }
          />
        </DivHeader>
        <DivContent>
          <FeedbackType onSelect={ this.onSelectFeedbackType } />
          <DivText>
            Оставьте ваше сообщение
          </DivText>
          <Textarea
            value={ discountDescription }

            onChange={ this.onChange }
          />
          <Button
            onClick={ this.onHandleClick }
          >
            Отправить сообщение
          </Button>
        </DivContent>
      </DivContainer>
    )
  }
}

export default compose(
  withFeedbackAppend()
)(Feedback)
