import React, { Component } from 'react'


import Feedback             from './Feedback'
import FeedbackAppendStatus from './FeedbackAppendStatus'

import codeFeedback         from './code'


class Container extends Component {
  constructor() {
    super()

    this.state = {
      openFeedback: false,

      openFeedbackAppendStatus: false,
      codeFeedbackAppendStatus: '',
    }
  }

  onCloseFeedback = ({ code }) => {
    if (code !== codeFeedback.CLOSE_FEEDBACK) {
      this.setState({
        openFeedback: !this.state.openFeedback,
        openFeedbackAppendStatus: true,
        codeFeedbackAppendStatus: code,
      })
    } else this.setState({ openFeedback: !this.state.openFeedback })

    const { onCloseFeedback } = this.props

    onCloseFeedback()
  }

  onCloseFeedbackAppendStatus = () => {
    this.setState({ openFeedbackAppendStatus: false, codeFeedbackAppendStatus: 6 })
  }

  componentWillReceiveProps(nextProps) {
    const { openFeedback } = nextProps

    this.setState({ openFeedback })
  }

  render() {
    const { openFeedback, openFeedbackAppendStatus, codeFeedbackAppendStatus } = this.state

    if(!openFeedback && !openFeedbackAppendStatus) return null

    if (openFeedback) return <Feedback open={ openFeedback } onClose={ this.onCloseFeedback } />


    if ((!openFeedback && codeFeedbackAppendStatus === 0) || (!openFeedback && codeFeedbackAppendStatus === 1)) {
      return(
        <FeedbackAppendStatus
          open={ openFeedbackAppendStatus }
          code={ codeFeedbackAppendStatus }

          onClose={ this.onCloseFeedbackAppendStatus }
        />
      )
    }

    return null
  }
}


export default Container
