import types                from 'engine/types'


const mutationName = 'feedbackAppend'

const queryName    = 'feedbackTypeList'

const inputNameMap = {
  MESSAGE:           'message',
  MESSAGE_TYPE:      'messageType',
}

const resultNameMap = {
  ID:                'id',
  VALUE:             'value',
  SELECTED:          'selected'
}

const queryMap = [
  {
    queryName: queryName,
    inputList: [],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        }
      ],
      result: [
        {
          name: [resultNameMap.ID],
          type: types.Int,
        },
        {
          name: [resultNameMap.VALUE],
          type: types.String,
        },
        {
          name: [resultNameMap.SELECTED],
          type: types.Boolean,
        }
      ]
    }
  }
]

const mutationMap = [
  {
    mutationName: mutationName,
    inputList: [
      {
        name: [inputNameMap.MESSAGE],
        type: types.String,
      },
      {
        name: [inputNameMap.MESSAGE_TYPE],
        type: types.Int
      }
    ],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        }
      ],
    }
  }
]

export default {
  mutationName,
  mutationMap,
  queryMap,
  queryName
}
