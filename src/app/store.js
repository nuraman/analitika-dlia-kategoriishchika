import './ReactotronConfig'

import Reactotron           from 'reactotron-react-js'

import { applyMiddleware }  from 'redux'
import { compose }          from 'redux'


import createReducer        from './createReducer'


let store

export default (payload) => {
  if (store) return store

  const { client } = payload
  const composeEnhancers = process.env.NODE_ENV !== 'production'
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
    : compose

  store = Reactotron.createStore(
    createReducer({ client }),
    composeEnhancers(
      applyMiddleware(client.middleware())
    )
  )

  return store
}

