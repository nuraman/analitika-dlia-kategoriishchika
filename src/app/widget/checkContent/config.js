import types                from 'engine/types'


const inputNameMap = {
  GOODS_CATEGORY:                'goodsCategory',
  POINT_GROUP:                   'pointGroup',
  POINT_FORMAT:                  'pointFormat',
  POINT:                         'point',
  COLOR:                         'color',
  OTHER_CATEGORY:                'otherCategory'
}

const queryNameMap = {
  GRF14: 'grf14',
  GRF15: 'grf15',
  SEGMENT_TITLE: 'segmentsTitle',
}

const resultNameMap = {
  COLOR:                         'color',

  CATEGORY_ID:                   'categoryId',
  CATEGORY_NAME:                 'categoryName',

  GOODS_NAME:                    'goodsName',

  VALUE:                         'value',

  CAPTION:                       'caption',
  WIDGET:                        'vidget',
  DESCRIPTION:                   'description',

  PROVIDER_ID:                   'providerId',
  PROVIDER_NAME:                 'providerName',
}

const queryMap = [
  {
    queryName: queryNameMap.GRF14,
    inputList: [
      {
        name: inputNameMap.GOODS_CATEGORY,
        type: types.String,
      },
      {
        name: inputNameMap.POINT,
        type: types.String,
      },
      {
        name: inputNameMap.POINT_GROUP,
        type: types.String,
      },
      {
        name: inputNameMap.POINT_FORMAT,
        type: types.String,
      },
      {
        name: inputNameMap.COLOR,
        type: types.String,
      }
    ],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        }
      ],
      result: [
        {
          name: resultNameMap.CATEGORY_ID,
          type: types.Int,
        },
        {
          name: resultNameMap.CATEGORY_NAME,
          type: types.String,
        },
        {
          name: resultNameMap.VALUE,
          type: types.Float,
        },
      ],
      title: [
        {
          name: resultNameMap.CAPTION,
          type: types.String,
        },
        {
          name: resultNameMap.WIDGET,
          type: types.String,
        },
        {
          name: resultNameMap.DESCRIPTION,
          type: types.String,
        }
      ]
    }
  },
  {
    queryName: queryNameMap.GRF15,
    inputList: [
      {
        name: inputNameMap.GOODS_CATEGORY,
        type: types.String,
      },
      {
        name: inputNameMap.POINT,
        type: types.String,
      },
      {
        name: inputNameMap.POINT_GROUP,
        type: types.String,
      },
      {
        name: inputNameMap.POINT_FORMAT,
        type: types.String,
      },
      {
        name: inputNameMap.COLOR,
        type: types.String,
      },
      {
        name: inputNameMap.OTHER_CATEGORY,
        type: types.String
      }
    ],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        }
      ],
      result: [
        {
          name: resultNameMap.GOODS_NAME,
          type: types.String,
        },
        {
          name: resultNameMap.VALUE,
          type: types.Float,
        },
        {
          name: resultNameMap.PROVIDER_ID,
          type: types.Int,
        }
      ],
      pie: [
        {
          name: resultNameMap.PROVIDER_NAME,
          type: types.String,
        },
        {
          name: resultNameMap.PROVIDER_ID,
          type: types.Int
        },
        {
          name: resultNameMap.VALUE,
          type: types.Float
        }
      ],
      title: [
        {
          name: resultNameMap.CAPTION,
          type: types.String,
        },
        {
          name: resultNameMap.WIDGET,
          type: types.String,
        },
        {
          name: resultNameMap.DESCRIPTION,
          type: types.String,
        }
      ]
    }
  },
  {
    queryName: queryNameMap.SEGMENT_TITLE,
    inputList: [
      {
        name: inputNameMap.COLOR,
        type: types.String,
      },
    ],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        }
      ],
      title: [
        {
          name: resultNameMap.CAPTION,
          type: types.String,
        },
        {
          name: resultNameMap.WIDGET,
          type: types.String,
        },
        {
          name: resultNameMap.DESCRIPTION,
          type: types.String,
        }
      ]
    }
  },
]

export default {
  queryMap,
  queryNameMap
}
