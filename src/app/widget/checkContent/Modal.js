import React, { Component } from 'react'

import { compose }          from 'redux'

import styled               from 'styled-components'


import OptionSelect         from './Option'
import CheckContent         from './CheckContentBrand/Container'
import CheckSKU             from './CheckContentSKU/Container'
import CheckSkuRating       from './SkuRating/Container'

import config               from 'app/widget/rfmAnalyseWidget/config'
import reducer              from './reducer'

import withRfmSegmentTitle  from 'app/widget/checkContent/hoc/withDataRfmSegment'


const Modal = styled.div`
  display: ${ props=>props.open ? 'flex' : 'none' };
  position: fixed;
  align-items: center;
  justify-content: space-around;
  position: fixed;
  z-index: 1;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: hidden;
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
  -webkit-animation-name: fadeIn; /* Fade in the background */
  -webkit-animation-duration: 0.4s;
  animation-name: fadeIn;
  animation-duration: 0.4s
`

const WrapperContent = styled.div`
  display: flex;
  flex-direction: row;
  flex-grow: 1;
  justify-content: space-around;
`


class ModalCheck extends Component {
  constructor() {
    super()

    this.state = {
      open: false,
      openSKU: false,
      openCheckContent: false,
      openSkuRating: false,
      openOption: true,

      title: '',
      segmentTitle: '',
      isProvider: false,
    }
  }

  componentWillMount() {
    const { open, isProvider } = this.props

    this.setState({ open, isProvider })

    window.addEventListener('click', this.onHandleClick, false)
  }

  componentWillReceiveProps(nextProps) {
    const { open, isProvider, title = '' } = nextProps

    this.setState({ open, isProvider, title })
  }

  onHandleClick = ({ target: { id } }) => {
    const { onClose } = this.props

    if (id === 'modal') {
      onClose()
      this.setState({ openOption: true, openCheckContent: false, openSKU: false, openSkuRating: false })
    }
  }

  onClose = () => {
    const { onClose } = this.props

    this.setState({ openOption: true, openCheckContent: false, openSKU: false, openSkuRating: false })

    onClose()
  }

  onSelect = ({ id }) => {
    if (id === config.idMap.OPEN_SKU_RATING) {
      this.setState({
        openOption: false,
        openCheckContent: false,
        openSKU: false,
        openSkuRating: true,
      })
    }

    if (id === config.idMap.OPEN_CHECK_CONTENT) {
      this.setState({
        openOption: false,
        openCheckContent: true,
        openSKU: false,
        openSkuRating: false,
      })
    }

    if (id === config.idMap.OPEN_DISCOUNT) {
      const { onDiscountOpen } = this.props

      onDiscountOpen()
    }
  }

  onSelectBrand = ({ id, title }) => {
    this.setState({ openOption: false, openCheckContent: false, openSKU: true, openSkuRating: false, segmentTitle: title })

    const { dispatch } = this.props

    dispatch({
      type: reducer.actionTypes.SELECT_BRAND,
      payload: { id }
    })
  }

  onBack = () => {
    this.setState({ openOption: true, openCheckContent: false, openSKU: false, openSkuRating: false })
  }

  onBackToCheckContent = () => {
    this.setState({ openOption: false, openCheckContent: true, openSKU: false, openSkuRating: false })
  }

  render() {

    const { open, openSKU, openOption, openSkuRating, openCheckContent, isProvider, title = '', segmentTitle } = this.state

    return(
      <Modal
        open={ open }
        id='modal'
      >
        <WrapperContent>
          <OptionSelect
            isProvider={ isProvider }
            open={ openOption }
            title={ title }
            onSelect={ this.onSelect }
            onClose={ this.onClose }
          />
          <CheckContent
            open={ openCheckContent }
            onSelectBrand={ this.onSelectBrand }
            onClose={ this.onClose }
            onBack={ this.onBack }
          />
          <CheckSKU
            open={ openSKU }
            onClose={ this.onClose }
            segmentTitle={ segmentTitle }
            onBack={ this.onBackToCheckContent }
          />
          <CheckSkuRating
            open={ openSkuRating }
            onClose={ this.onClose }
            onBack={ this.onBack }
          />
        </WrapperContent>
      </Modal>
    )
  }
}

export default compose(
  withRfmSegmentTitle,
)(ModalCheck)
