const name='checkContent'

const actionTypes = {
  SELECT_BRAND: `${ name }/SELECT_BRAND`
}

const STATE = {
  brandId: ''
}

const reducer = (state=STATE, action) => {
  const { type, payload } = action

  switch (type) {
    case actionTypes.SELECT_BRAND: {
      const { id } = payload

      return {
        ...state,
        brandId: id
      }
    }
    default: {
      return state
    }
  }
}

export default {
  actionTypes,
  reducer
}
