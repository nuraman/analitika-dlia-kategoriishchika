import React                from 'react'

import styled               from 'styled-components'


const Container = styled.div`
  display: flex;
  flex-direction: column;
  padding-left: 20px;
  width: calc(100% - 20px);
`

const WrapperItem = styled.div`
  display: flex;
  flex-direction: row;
  
  width: 100%;
  
  padding-bottom: 2px;
  height: 25px;
  
  background-color: transparent;
`

const Item = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  
  width: 400px;
  
  height: 100%;
`

const DetailIco = styled.div`
  display: block;
  width: 11px;
  height: 21px;
  fill: #9e9e9e;
  cursor: pointer;

  background-image: url(assets/nextButton.svg);
  background-size: cover;
  
  margin-left: 10px;
`

const Label = styled.label`
  display: block;
  font-family: HelveticaLight;
  font-size: 16px;
  color: #fff;
`

export default (props) => {

  const { data, order } = props

  const onHandleClick = ({ id }) => {
    const { onSelect } = props

    onSelect(id)
  }

  return (
    <Container>
      {
        order.map((item, index) => {
          const { categoryId, categoryName } = data[item]

          return (
            <WrapperItem key={ index }>
              <Item>
                <Label>{ categoryName }</Label>
                <DetailIco display={ true } onClick={ () => onHandleClick({ id: categoryId }) } />
              </Item>
            </WrapperItem>
          )
        })
      }
    </Container>
  )
}
