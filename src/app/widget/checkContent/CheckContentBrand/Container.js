import React, { Component } from 'react'

import { compose }          from 'redux'

import styled               from 'styled-components'

import { CircularProgress } from 'material-ui/Progress'

import withData             from '../hoc/withDataBrand'

import Chart                from '../Chart'

import Header               from '../CheckContentSKU/Header'
import BlockPrevButton      from '../CheckContentSKU/PreviousButton'
import BlockNextButton      from '../CheckContentSKU/NextButton'
import BlockChart           from './Chart'
import BlockCategoryList    from './BlockCategoryList'

import dataIterator         from 'app/widget/checkContent/api/dataIterator'


const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 900px;
  height: 500px;
  
  background-color: #403e3e;
`

const Content = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  
  width: 100%;
  height: calc(100% - 50px);
`

const ContentRight = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  
  width: 50%;
  height: 100%;
`

const ContentLeft = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 50%;
  height: 100%;
  background-color: #484848;
  
  position: relative;
`

const Back  = styled.input`
  position: absolute;
  top: 10px;
  left: 0px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 150px;
  height: 40px;
  text-align: center;
  font-weight: 300;
  font-family: HelveticaRegular;
  font-size: 18px;
  border: none;
  outline: none;
  color: #fff;
  background-color: #48b9bf;
  cursor: pointer;
    
  &:hover {
    background-color: #59cad0;
  }
  
  &:focus, &:active {
    border: none;
    outline: none;
  }
`

const ContainerLoader = styled.div`
  display: flex;
  width: 900px;
  height: 500px;
  justify-content: space-around;
  align-items: center;
  
  background-color: #403e3e;
`

const MAX_ITEMS = 9

class CheckContentBrand extends Component {
  constructor() {
    super()

    this.state = {
      config: {},
      categoryList: [],
      lastIndex: 0,
      firstIndex: 0,

      newData: {},
      newOrder: [],

      canPrev: false,
      canNext: true,

      height: 363,
    }
  }

  componentWillMount() {
    const { data, order = [], title, description } = this.props

    const { height } = this.state

    const chart = new Chart()

    if (order.length > 0) {
      const newOrder = order.slice(0, MAX_ITEMS + 1)

      const newData = newOrder.reduce((newData, id) => {
        return { ...newData, [id]: { ...data[id] } }
      }, {})

      const { config, categoryList } = this.getChartProperty({ newData, newOrder, height, chart })

      this.setState({
        data,
        order,
        config,
        chart,
        categoryList: categoryList.reverse(),
        title,
        description,
        lastIndex: MAX_ITEMS,
        newData,
        newOrder,
        firstIndex: 0,
        canPrev: false,
        canNext: true,
      })
    } else {
      this.setState({ chart })
    }
  }

  componentWillReceiveProps(nextProps) {
    const { data, order = [], title, description } = nextProps

    const { height, chart } = this.state

    if (order.length > 0) {
      const newOrder = order.slice(0, MAX_ITEMS + 1)

      const newData = newOrder.reduce((newData, id) => {
        return { ...newData, [id]: { ...data[id] } }
      }, {})

      const { config, categoryList } = this.getChartProperty({ newData, newOrder, height, chart })

      this.setState({
        data,
        order,
        config,
        categoryList: categoryList.reverse(),
        title, description,
        lastIndex: MAX_ITEMS,
        firstIndex: 0,
        newData,
        newOrder,
        canPrev: false,
        canNext: true,
      })
    }
  }

  getChartProperty = ({ newData, newOrder, height, chart }) => {
    chart.options = { data: newData, order: newOrder, height }

    const config = chart.getConfig(this.onHandleClick)

    const categoryList = [...chart.getCategories()]

    return {
      config,
      categoryList,
    }
  }

  onHandleShiftPrev = () => {
    const { data, order, firstIndex, lastIndex, chart, height } = this.state

    const { newData, newOrder } = dataIterator.getPrevList({ data, order, firstIndex, max: MAX_ITEMS })

    if (newOrder.length > 0) {
      const { config, categoryList } = this.getChartProperty({ newData, newOrder, height, chart })

      this.setState({
        config,
        categoryList: categoryList.reverse(),
        firstIndex: newOrder[newOrder.length - 1],
        lastIndex: +newOrder[0],
        newOrder: newOrder.reverse(),
        newData,
        canNext: order.length - 1 !== lastIndex,
        canPrev: firstIndex > MAX_ITEMS + 1
      })
    }
  }

  onHandleShiftNext = () => {
    const { data, order, lastIndex, chart, height } = this.state

    const { newData, newOrder } = dataIterator.getNextList({ data, order, lastIndex, max: MAX_ITEMS })

    if (newOrder.length > 0) {
      const { config, categoryList } = this.getChartProperty({ newData, newOrder, height, chart })

      this.setState({
        config,
        categoryList: categoryList.reverse(),
        lastIndex: +newOrder[newOrder.length - 1],
        firstIndex: newOrder[0],
        newOrder,
        newData,
        canNext: order.length !== lastIndex,
        canPrev: lastIndex > 0
      })
    }
  }

  onSelectBrand = (id) => {
    const { title } = this.state

    const { onSelectBrand } = this.props

    onSelectBrand({ id, title })
  }

  onHandleClose = () => {
    const { onClose } = this.props

    onClose()
  }

  onHandleBack = () => {
    const { onBack } = this.props

    onBack()
  }

  render() {
    const {  canPrev, canNext, order = [], newData, newOrder, title } = this.state

    const { open, loading } = this.props

    if (!open) return null

    if (loading && open) return (
      <ContainerLoader>
        <CircularProgress />
      </ContainerLoader>
    )

    if (!order.length) return null

    return(
      <Container>
        <Header leftTitle={`СОСТАВ ЧЕКОВ ${ title }`} onClose={ this.onHandleClose } />
        <Content>
          <ContentLeft>
            <Back
              type='button'
              value='Назад'

              onClick={ this.onHandleBack }
            />
            <BlockPrevButton display={ canPrev } onShowPrev={ this.onHandleShiftPrev } />
            <BlockChart data={ newData } order={ newOrder } />
          </ContentLeft>
          <ContentRight>
            <BlockCategoryList data={ newData } order={ newOrder } onSelect={ this.onSelectBrand } />
            <BlockNextButton display={ canNext } onShowNext={ this.onHandleShiftNext } />
          </ContentRight>
        </Content>
      </Container>
    )
  }
}

export default compose(
  withData,
)(CheckContentBrand)
