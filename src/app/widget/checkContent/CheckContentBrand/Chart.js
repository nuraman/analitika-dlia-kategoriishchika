import React                from 'react'

import styled               from 'styled-components'


const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 200px;
`

const WrapperItem = styled.div`
  display: flex;
  flex-direction: row;
  
  width: 100%;
  
  padding-bottom: 2px;
  height: 25px;
  
  background-color: transparent;
`

const Item = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  
  width: 150px;
  
  height: 100%;
`

const ItemChart = styled.div`
  display: block;
  width: ${ props=>props.width }px;
  height: 20px;
  
  background-color: rgb(99, 218, 196);
`

const Label = styled.label`
  display: table-cell;
  vertical-align: middle;
  font-family: HelveticaLight;
  font-size: 16px;
  color: #fff;
  padding-left: 20px;
`

export default (props) => {

  const { data, order } = props

  return(
    <Container>
      {
        order.map((item, index) => {
          const { value } = data[item]

          return (
            <WrapperItem key={ index }>
              <Item>
                <ItemChart width={ value } />
                <Label>{ `${ value }%` }</Label>
              </Item>
            </WrapperItem>
          )
        })
      }
    </Container>
  )
}
