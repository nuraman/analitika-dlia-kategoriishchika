import React                from 'react'

import styled               from 'styled-components'


import config               from 'app/widget/rfmAnalyseWidget/config'


const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 500px;
  height: 400px;
  
  background-color: #403e3e;
`

const Header = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  height: 50px;
  
  border-bottom: 1px solid #f9f9f9
`

const HeaderEmpty = styled.div`
  width: 50px;
  background-color: #484848;
`

const HeaderTitle = styled.div`
  display: table;
  height: 50px;
  width: 400px;
`

const Label = styled.label`
  display: table-cell;
  vertical-align: middle;
  font-family: HelveticaLight;
  font-size: 16px;
  color: #fff;
  padding-left: 20px;
`

const HeaderClose = styled.div`
  position: relative;
  display: block;
  box-sizing: border-box;
  width: 50px;
  height: 50px;
  border-left: 1px solid #7d7d7d;
  cursor: pointer;
  
  &:hover:before, :hover:after {
    opacity: 0.6;
  }
  &:before, &:after {
    content: "";
    position: absolute;
    top: 50%;
    left: 9px;
    width: 30px;
    height: 1px;
    opacity: 0.9;
    background-color: #fff;
    cursor: pointer;
  }
  &:before {
    transform: translateY(-50%) rotate(45deg);
  }
  &:after {
    transform: translateY(-50%) rotate(-45deg);
  }
`

const Body = styled.div`
  display: flex;
  flex-direction: row;
  
  width: 100%;
  height: calc(100% - 50px);
`

const BodyEmpty = styled.div`
  display: block;
  width: 50px;
  background-color: #484848;
`

const BodyContent = styled.div`
  display: block;
  width: calc(100% - 50px);
  padding: 80px 0px 80px 0px;
`

const WrapperLabel = styled.div`
  display: table;
  width: 100%;
  height: 50px;
  
  cursor: pointer;
  
  &:hover {
    background-color: #484848;
  }
`


export default (props) => {
  const { isProvider, title = '', open } = props

  const onHandleClick = ({ id }) => {
    const { onSelect } = props

    onSelect({ id })
  }

  const onHandleClose = () => {
    const { onClose } = props

    onClose()
  }

  if (!open) return null

  return(
    <Container>
      <Header>
        <HeaderEmpty />
        <HeaderTitle>
          <Label>{ title }</Label>
        </HeaderTitle>
        <HeaderClose onClick={ onHandleClose } />
      </Header>
      <Body>
        <BodyEmpty />
        <BodyContent>
          {
            isProvider ?
            <WrapperLabel onClick={() => onHandleClick({id: config.idMap.OPEN_DISCOUNT})}>
              <Label>Создать акцию</Label>
            </WrapperLabel> : null
          }
          <WrapperLabel onClick={ () => onHandleClick({ id: config.idMap.OPEN_CHECK_CONTENT }) }>
            <Label>Показать состав чеков</Label>
          </WrapperLabel>
          <WrapperLabel onClick={ () => onHandleClick({ id: config.idMap.OPEN_SKU_RATING }) }>
            <Label>Посмотреть рейтинг товаров</Label>
          </WrapperLabel>
        </BodyContent>
      </Body>
    </Container>
  )
}

