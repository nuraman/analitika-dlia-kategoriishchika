import React                from 'react'

import { connect }          from 'react-redux'
import { compose }          from 'redux'


import  withGrf15           from 'app/widget/graphql/withGrf15'


function withDataSKU(ComposedComponent) {
  function decorator(props) {
    return (
      <ComposedComponent { ...props } />
    )
  }

  function mapStateToProps(state) {
    const { menu, discount, checkContent } = state

    const { category, pointGroup, pointFormat, point } = menu

    const { param1 } = discount

    const { brandId } = checkContent

    return {
      options: {
        variables: {
          goodsCategory: category.id,
          pointGroup: null,
          pointFormat: pointFormat.id === '' ? null : pointFormat.id,
          point: null,
          color: param1,
          otherCategory: brandId.toString(),
        }
      }
    }
  }

  return compose(
    connect(mapStateToProps),
    withGrf15()
  )(decorator)
}

export default withDataSKU
