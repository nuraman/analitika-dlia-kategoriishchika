import React                from 'react'

import { connect }          from 'react-redux'
import { compose }          from 'redux'

import withGrf14            from 'app/widget/graphql/withGrf14'


function withData(ComposedComponent) {
  function decorator(props) {
    return (<ComposedComponent { ...props } />)
  }

  function mapStateToProps(state) {
    const { menu, discount, user } = state

    const { category, pointGroup, pointFormat, point, period } = menu

    const { param1 } = discount

    const { id, dateBegin, dateEnd } = period

    const { userLogin: { data, order } } = user
    const { isProvider } = data[order[0]] || {}


    return {
      id,
      dateBegin,
      dateEnd,
      isProvider,
      options: {
        variables: {
          goodsCategory: category.id,
          pointGroup: null,
          pointFormat: pointFormat.id === ''? null : pointFormat.id,
          point: null,
          color: param1,
        }
      }
    }
  }

  return compose(
    connect(mapStateToProps),
    withGrf14(),
  )(decorator)
}

export default withData
