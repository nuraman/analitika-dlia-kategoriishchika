import React                from 'react'

import { connect }          from 'react-redux'
import { compose }          from 'redux'


import  withRfmSegmentTitle from 'app/widget/graphql/withRfmSegmentTitle'


function withDataRfmSegment(ComposedComponent) {
  function decorator(props) {
    return (
      <ComposedComponent { ...props } />
    )
  }

  function mapStateToProps(state) {
    const { discount, user } = state

    const { userLogin: { data, order } } = user
    const { isProvider } = data[order[0]] || {}

    const { param1 } = discount

    return {
      isProvider,
      options: {
        variables: {
          color: param1,
        }
      }
    }
  }

  return compose(
    connect(mapStateToProps),
    withRfmSegmentTitle()
  )(decorator)
}

export default withDataRfmSegment
