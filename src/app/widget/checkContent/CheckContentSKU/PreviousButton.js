import React                from 'react'

import styled               from 'styled-components'


const WrapperButton = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;
  
  height: 100%;
  width: 50px;
`

const PrevIco = styled.div`
  display: ${ props=>props.display ? 'table-cell' : 'none' };
  
  width: 11px;
  height: 21px;
  
  fill: black;
  
  vertical-align: middle;
  
  cursor: pointer;
  
  background-image: url(assets/leftButton.svg);
  background-size: cover;
`


export default (props) => {
  const { display } = props

  const onHandleClickNext = () => {
    const { onShowPrev } = props

    onShowPrev()
  }

  return(
    <WrapperButton>
      <PrevIco display={ display } onClick={ onHandleClickNext } />
    </WrapperButton>
  )
}
