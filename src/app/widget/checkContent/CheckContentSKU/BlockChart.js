import React                from 'react'

import styled               from 'styled-components'


const ListItem = styled.div`
  display: flex;
  flex-direction: column;
  width: calc(100% - 50px);
`

const Item = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  
  position: relative;
  
  width: 100%;
  
  padding-bottom: 10px;
  height: 30px;
  
  background-color: transparent;
`

const WrapperChart = styled.div`
  height: 20px;
  width: 40px;
`

const ItemLeftChart = styled.div`
  display: block;
  width: ${ props=>props.width }%;
  height: 100%;
  
  background-color: rgb(99, 218, 196);
`

const Label = styled.label`
  display: table-cell;
  vertical-align: middle;
  font-family: HelveticaLight;
  font-size: 14px;
  color: #fff;
  padding-left: 50px;
  
  position: absolute;
`

const LabelCategory = styled.label`
  display: block;
  font-family: HelveticaLight;
  font-size: 14px;
  color: #fff;
  padding-left: 100px;
  
  position: absolute;
`

export default (props) => {

  const { data, order } = props

  return (
    <ListItem>
      {
        order.map((item, index) => {
          const { goodsName, value } = data[item]

          return (
            <Item key={ index }>
              <WrapperChart>
                <ItemLeftChart width={ value } />
              </WrapperChart>
              <Label>{ value }%</Label>
              <LabelCategory>{ goodsName }</LabelCategory>
            </Item>
          )
        })
      }
    </ListItem>
  )
}
