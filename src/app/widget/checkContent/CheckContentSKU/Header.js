import React                from 'react'

import styled               from 'styled-components'


const Header = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  height: 50px;
  
  border-bottom: 1px solid #f9f9f9
`

const HeaderLeft = styled.div`
  display: table;
  height: 50px;
  width: 50%;
  background-color: #484848;
`

const HeaderRight = styled.div`
  display: table;
  height: 50px;
  width: calc(50% - 50px);
`

const Label = styled.label`
  display: table-cell;
  vertical-align: middle;
  font-family: HelveticaLight;
  font-size: 14px;
  color: #fff;
  padding-left: 50px;
`

const HeaderClose = styled.div`
  position: relative;
  display: block;
  box-sizing: border-box;
  width: 50px;
  height: 50px;
  border-left: 1px solid #7d7d7d;
  cursor: pointer;
  
  &:hover:before, :hover:after {
    opacity: 0.6;
  }
  &:before, &:after {
    content: "";
    position: absolute;
    top: 50%;
    left: 9px;
    width: 30px;
    height: 1px;
    opacity: 0.9;
    background-color: #fff;
    cursor: pointer;
  }
  &:before {
    transform: translateY(-50%) rotate(45deg);
  }
  &:after {
    transform: translateY(-50%) rotate(-45deg);
  }
`

export default (props) => {

  const onHandleClose = () => {
    const { onClose } = props

    onClose()
  }

  const { leftTitle, rightTitle } = props

  return (
    <Header>
      <HeaderLeft>
        <Label>{ leftTitle.toUpperCase() }</Label>
      </HeaderLeft>
      <HeaderRight>
        <Label>{ rightTitle }</Label>
      </HeaderRight>
      <HeaderClose onClick={ onHandleClose } />
    </Header>
  )
}
