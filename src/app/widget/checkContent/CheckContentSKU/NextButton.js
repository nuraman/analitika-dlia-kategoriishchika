import React                from 'react'

import styled               from 'styled-components'


const WrapperButton = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;
  
  height: 100%;
  width: 50px;
`

const NextIco = styled.div`
  display: ${ props=>props.display ? 'block' : 'none' };
  width: 11px;
  height: 21px;
  margin-top: 15px;
  margin-bottom: 15px;
  fill: #9e9e9e;
  cursor: pointer;

  background-image: url(assets/nextButton.svg);
  background-size: cover;
`


export default (props) => {
  const { display } = props

  const onHandleClickNext = () => {
    const { onShowNext } = props

    onShowNext()
  }

  return(
    <WrapperButton>
      <NextIco display={ display } onClick={ onHandleClickNext } />
    </WrapperButton>
  )
}
