import React, { Component } from 'react'

import ReacthighChart       from 'react-highcharts'

import styled               from 'styled-components'

import { compose }          from 'redux'

import { CircularProgress } from 'material-ui/Progress'


import withData             from '../hoc/withDataSKU'
import Header               from './Header'
import BlockNextButton      from './NextButton'
import BlockPrevButton      from './PreviousButton'
import BlockChart           from './BlockChart'
import Chart                from './Chart'

import dataIterator         from 'app/widget/checkContent/api/dataIterator'


const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 900px;
  height: 500px;
  
  background-color: #403e3e;
`

const Content = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  
  width: 100%;
  height: calc(100% - 50px);
`

const ContentRight = styled.div`
  display: block;
  width: 50%;
`

const ContentLeft = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 50%;
  height: 100%;
  background-color: #484848;
  
  position: relative;
`

const ContainerLoader = styled.div`
  display: flex;
  width: 900px;
  height: 500px;
  justify-content: space-around;
  align-items: center;
  
  background-color: #403e3e;
`

const Back  = styled.input`
  position: absolute;
  top: 10px;
  left: 0px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 150px;
  height: 40px;
  text-align: center;
  font-weight: 300;
  font-family: HelveticaRegular;
  font-size: 18px;
  border: none;
  outline: none;
  color: #fff;
  background-color: #48b9bf;
  cursor: pointer;
    
  &:hover {
    background-color: #59cad0;
  }
  
  &:focus, &:active {
    border: none;
    outline: none;
  }
`

const MAX_ITEMS = 7

class CheckContentSKU extends Component {
  constructor() {
    super()

    this.state = {
      lastIndex: 0,
      firstIndex: 0,
      newData: {},
      newOrder: [],

      chart: {},

      brandId: '',

      canPrev: false,
      canNext: true,

      height: 350,

      title: ''
    }
  }

  componentWillMount() {
    const { data, order = [], dataPie, orderPie = [], title } = this.props

    const chart = new Chart()

    const { height } = this.state

    if (order.length > 0) {
      const { newData, newOrder, config } = this.getChartProperty({ data, order, dataPie, orderPie, chart, height })

      this.setState({
        data,
        order,
        newData,
        newOrder,
        lastIndex: MAX_ITEMS,
        firstIndex: 0,
        config,
        canNext: order.length > MAX_ITEMS,
        canPrev: false,
        title
      })
    } else {
      this.setState({ chart })
    }
  }


  componentWillReceiveProps(nextProps) {
    const { data, order = [], dataPie, orderPie = [], title } = nextProps

    const { height } = this.state

    const chart = new Chart()

    if (order.length > 0) {
      const { newData, newOrder, config } = this.getChartProperty({ data, order, dataPie, orderPie, chart, height })

      this.setState({
        data,
        order,
        newData,
        newOrder,
        lastIndex: MAX_ITEMS,
        firstIndex: 0,
        config,
        canNext: order.length > MAX_ITEMS,
        canPrev: false,
        title
      })
    }
  }

  getChartProperty = ({ data, order, height, chart, orderPie, dataPie }) => {
    const newOrder = order.slice(0, MAX_ITEMS + 1)

    const newData = newOrder.reduce((newData, id) => {
      return { ...newData, [id]: { ...data[id] } }
    }, {})

    let config = {}
    if (orderPie.length > 0) {
      chart.options = { data: dataPie, order: orderPie }

      config = chart.getConfig({ height: 350, onSelect: this.onSelectBrand })
    }

    return {
      newData,
      newOrder,
      config
    }
  }

  onHandleClose = () => {
    const { onClose } = this.props

    onClose()
  }

  /*
    При выборе бренда на графике показываем только соответствующие SKU
    Если повторно кликнули по бренду на графике то показываем все
   */
  onSelectBrand = ({ id }) => {
    const { brandId } = this.state

    const { data, order } = this.props
    //если такой бренд уже выбран, то показываем все
    if (brandId == id) {
      const newOrder = order.slice(0, MAX_ITEMS + 1)

      const newData = newOrder.reduce((newData, id) => {
        return { ...newData, [id]: { ...data[id] } }
      }, {})

      return this.setState({
        data,
        order,
        newData,
        newOrder,
        lastIndex: MAX_ITEMS,
        firstIndex: 0,
        canNext: order.length > MAX_ITEMS,
        canPrev: false,
        brandId: '',
      })
    }
    //иначе ищем всех поставщиков
    let index = 0
    const newData = order.reduce((newData, item) => {
      const { providerId } = data[item]

      if (providerId === id) {
        return { ...newData, [index++]: { ...data[item] } }
      }

      return { ...newData }
    }, {})

    const newOrder = Object.keys(newData)

    if (newOrder.length > 0) {
      const dataItems = newOrder.slice(0, MAX_ITEMS + 1).reduce((dataItems, item, index) => {
        return { ...dataItems, [index]: { ...newData[item] } }
      }, {})

      const orderItems = Object.keys(dataItems)

      this.setState({
        data: newData,
        order: newOrder,
        canNext: newOrder.length > MAX_ITEMS,
        newOrder: orderItems,
        newData: dataItems,
        brandId: id,
        lastIndex: MAX_ITEMS,
        firstIndex: 0,
        canPrev: false,
      })
    }

    if (!newOrder.length) {
      this.setState({
        data: {},
        order: [],
        canNext: false,
        newOrder: [],
        newData: {},
        brandId: id
      })
    }
  }

  onHandleClickPrev = () => {
    const { data, order, firstIndex, lastIndex } = this.state

    const { newData, newOrder } = dataIterator.getPrevList({ data, order, firstIndex, max: MAX_ITEMS })

    if (newOrder.length > 0) {
      this.setState({
        firstIndex: newOrder[newOrder.length - 1],
        lastIndex: +newOrder[0],
        newOrder: newOrder.reverse(),
        newData,
        canNext: order.length - 1 >= lastIndex,
        canPrev: firstIndex > MAX_ITEMS + 1
      })
    }
  }

  onHandleClickNext = () => {
    const { data, order, lastIndex } = this.state

    const { newData, newOrder } = dataIterator.getNextList({ data, order, lastIndex, max: MAX_ITEMS })

    if (newOrder.length > 0) {
      this.setState({
        lastIndex: +newOrder[newOrder.length - 1],
        firstIndex: newOrder[0],
        newOrder,
        newData,
        canNext: order.length < lastIndex,
        canPrev: lastIndex > 0
      })
    }
  }


  onHandleBack = () => {
    const { onBack } = this.props

    onBack()
  }

  render() {
    const { open, loading, title, segmentTitle = '' } = this.props

    if (!open) return null

    const { newData, newOrder = [], canPrev, canNext, config } = this.state

    if (open && loading) {
      return (
        <ContainerLoader>
          <CircularProgress />
        </ContainerLoader>
      )
    }

    return (
      <Container>
        <Header onClose={ this.onHandleClose } leftTitle={ `${ title } ${ segmentTitle }` }  />
        <Content>
          <ContentLeft>
            <Back
              type='button'
              value='Назад'
              onClick={ this.onHandleBack }
            />
            <BlockPrevButton display={ canPrev } onShowPrev={ this.onHandleClickPrev } />
            <BlockChart data={ newData } order={ newOrder } />
            <BlockNextButton display={ canNext } onShowNext={ this.onHandleClickNext } />
          </ContentLeft>
          <ContentRight>
            { config.series.length > 0 ? <ReacthighChart config={ config } /> : null }
          </ContentRight>
        </Content>
      </Container>
    )
  }
}

export default compose(
  withData
)(CheckContentSKU)

