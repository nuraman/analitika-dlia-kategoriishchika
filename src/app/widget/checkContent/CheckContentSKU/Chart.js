function Chart() {
  this.series = []
  this.categories = []
  this.config = {}

  this.data = {}
  this.order = []
}

Chart.prototype = {
  set options (payload) {
    const { data, order } = payload

    this.data = data
    this.order = order
  }
}

const createConfig = ({ series, onSelect }) => {
  const config = {
    chart: {
      backgroundColor: 'transparent',
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie',
      style: {
        fontFamily: 'HelveticaRegular',
        fontSize: '14px',
        fontWeight: 'bold',
      }
    },
    title: {
      text: '',
    },
    tooltip: {
      headerFormat: '<span style="font-size: 10px">{series.name}</span><br/>',
      pointFormat: '<span style="color:{point.color}">{point.name}:</span><b>{point.y} %</b><br/>'
    },
    navigation: {
      buttonOptions: {
        enabled: false,
      }
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.percentage:.1f} %',
          style: {
            color: '#fff',
          }
        },
        events: {
          click: function (event) {
            onSelect && onSelect({ id: event.point.provider })
          }
        }
      }
    },

    series: series
  }

  return config
}

Chart.prototype.createSeries = function () {
  const dataChart = this.order.reduce((dataChart, item) => {
    const { value = 0, providerName, providerId } = this.data[item] || {}

    return [...dataChart, {
      name: providerName,
      y: value,
      provider: providerId,
  //    color: `#${ color }`,
    }]
  }, [])

  const series = [{
    name: 'Бренд',
    data: dataChart
  }]

  return series
}

Chart.prototype.getConfig = function ({ height, onSelect }) {
  const series = this.createSeries()

  return createConfig({ series, height, onSelect })
}

export default Chart


