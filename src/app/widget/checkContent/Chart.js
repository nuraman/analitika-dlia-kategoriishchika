import Highcharts           from 'highcharts'
import PageLogin from "../../page/web/PageLogin/PageLogin";


function Chart() {
  this.data = {}
  this.order = []

  this.series = []
  this.categories = []

  this.height = 0
}

Chart.prototype = {
  set options ({ data, order, height }) {
    this.data = data
    this.order = order
    this.height = height
    this.series = []
    this.categories = []
  }
}

Chart.prototype.createSeries = function () {
  const dataSeries = this.order.reduce((dataSeries, item) => {
    const { categoryId, value } = this.data[item]

    return [ ...dataSeries, { id: categoryId, y: value, color: '#dbcdcc' }]
  }, [])

  this.series = [{
    name: '',
    data: dataSeries
  }]
}

Chart.prototype.createCategories = function () {
  this.order.forEach((item) => {
    const { categoryName } = this.data[item]

    this.categories.push(categoryName)
  })
}

Chart.prototype.getCategories = function () {
  return this.categories
}

Chart.prototype.getConfig = function (onClick) {
  const heightChart = `${ this.height - 60 }px`

  this.createSeries()
  this.createCategories()

  const config = {
    chart: {
      type: 'bar',
      height: heightChart,
      backgroundColor: 'transparent'
    },
    title: {
      text: ''
    },
    navigation: {
      buttonOptions: {
        enabled: false,
      }
    },
    legend: {
      enabled: false,
    },
    xAxis: [{
      categories: this.categories,
      reversed: false,
      visible: false,
      labels: {
        step: 1
      },
    }, { // mirror axis on right side
      opposite: true,
      reversed: false,
      categories: this.categories,
      linkedTo: 0,
      labels: {
        step: 1
      },
      visible: false,
    }],
    yAxis: {
      title: {
        text: null
      },
      labels: {
        formatter: function () {
          return Math.abs(this.value) + '%';
        }
      },
      style: {
        fontFamily: 'HelveticaRegular',
        fontSize: '12px',
      },
      visible: false,
    },

    plotOptions: {
      series: {
        stacking: 'normal'
      },
      bar: {
        point: {
          events: {
            click: function() {
              onClick && onClick({ id: this.id })
            }
          }
        },
        style: {
          fontFamily: 'HelveticaRegular',
          fontSize: '12px',
          fontWeight: 'normal',
        }
      }
    },

    tooltip: {
      formatter: function () {
        return '<b>' + this.series.name + ', age ' + this.point.category + '</b><br/>' +
          'Population: ' + Highcharts.numberFormat(Math.abs(this.point.y), 0);
      }
    },

    series: this.series
  }

  return config
}

export default Chart
