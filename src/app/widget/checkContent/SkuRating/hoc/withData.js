import React                from 'react'
import { connect }          from 'react-redux'
import { compose }          from 'redux'


import withGrf16            from 'app/widget/graphql/withGrf16Sku'


function withData(ComposedComponent) {
  function decorator(props) {
    return (<ComposedComponent {...props} />)
  }

  function mapStateToProps(state) {
    const { discount, menu, checkContent } = state

    const { period: { dateBegin } } = menu

    const { param1 } = discount

    const { brandId } = checkContent

    return {
      options: {
        variables: {
          goodsCategory: brandId.toString(),
          dateBegin: dateBegin,
          color: param1,
        }
      }
    }
  }


  return compose(
    connect(mapStateToProps),
    withGrf16(),
  )(decorator)
}

export default withData
