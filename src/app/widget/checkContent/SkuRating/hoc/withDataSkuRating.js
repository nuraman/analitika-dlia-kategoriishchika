import React                from 'react'

import { connect }          from 'react-redux'
import { compose }          from 'redux'

import withGrf16            from 'app/widget/graphql/withGrf16Category'


function withData(ComposedComponent) {
  function decorator(props) {
      return (
        <ComposedComponent { ...props } />
      )
  }

  function mapStateToProps (state) {
    const { menu: { period }, discount: { param1 } } = state

    const { id, dateBegin, dateEnd } = period

    return {
      id,
      dateBegin,
      dateEnd,
      options: {
        variables: {
          dateBegin,
          color: param1,
        }
      }
    }
  }

  return compose(
    connect(mapStateToProps),
    withGrf16()
  )(decorator)
}

export default withData

