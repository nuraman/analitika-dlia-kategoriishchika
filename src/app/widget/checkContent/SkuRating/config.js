import types                from 'engine/types'


const inputNameMap = {
  GOODS_CATEGORY:                     'goodsCategory',
  DATE_BEGIN:                         'dateBegin',
  COLOR:                              'color'
}

const queryNameMap = {
  GRF16_SKU: 'grf16',
  GRF16_CATEGORY: 'grf16Category'
}

const resultNameMap = {
  GOODS_NAME:                   'goodsName',
  COUNT_CHECK:                  'countCheck',

  CATEGORY_ID:                  'categoryId',
  CATEGORY_NAME:                'categoryName',

  GOODS_ID:                     'goodsId',

  CAPTION:                       'caption',
  WIDGET:                        'vidget',
  DESCRIPTION:                   'description',
}

const queryMap = [
  {
    queryName: queryNameMap.GRF16_SKU,
    inputList: [
      {
        name: inputNameMap.GOODS_CATEGORY,
        type: types.String,
      },
      {
        name: inputNameMap.DATE_BEGIN,
        type: types.String,
      },
      {
        name: inputNameMap.COLOR,
        type: types.String,
      },

    ],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        }
      ],
      result: [
        {
          name: resultNameMap.GOODS_NAME,
          type: types.String,
        },
        {
          name: resultNameMap.COUNT_CHECK,
          type: types.Int,
        },
      ],
      title: [
        {
          name: resultNameMap.CAPTION,
          type: types.String,
        },
        {
          name: resultNameMap.WIDGET,
          type: types.String,
        },
        {
          name: resultNameMap.DESCRIPTION,
          type: types.String,
        }
      ]
    }
  },
  {
    queryName: queryNameMap.GRF16_CATEGORY,
    inputList: [
      {
        name: inputNameMap.DATE_BEGIN,
        type: types.String,
      },
      {
        name: inputNameMap.COLOR,
        type: types.String
      }
    ],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        }
      ],
      result: [
        {
          name: resultNameMap.CATEGORY_ID,
          type: types.Int,
        },
        {
          name: resultNameMap.CATEGORY_NAME,
          type: types.String,
        },
        {
          name: resultNameMap.GOODS_NAME,
          type: types.String,
        },
        {
          name: resultNameMap.GOODS_ID,
          type: types.Int
        }

      ],
      title: [
        {
          name: resultNameMap.CAPTION,
          type: types.String,
        },
        {
          name: resultNameMap.WIDGET,
          type: types.String,
        },
        {
          name: resultNameMap.DESCRIPTION,
          type: types.String,
        }
      ]

    }


  }
]

export default {
  queryMap,
  queryNameMap
}
