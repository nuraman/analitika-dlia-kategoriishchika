import React                from 'react'

import styled               from 'styled-components'


const Container = styled.div`
  display: block;
  width: 50%;
  height: 100%;
`

const List = styled.div`
  padding: 20px;
  overflow: auto;
  
  height: calc(100% - 40px);
  width: calc(100% - 40px);
  
   &::-webkit-scrollbar
  {
    width: 9px;
  }
  
  &::-webkit-scrollbar-track
  {
    background: #7d7d7d;
    border: 3px solid transparent;
    background-clip: content-box;
  }
  
  &::-webkit-scrollbar-thumb
  {
    background: #73c7c5;
    height: 35px;
    border-radius: 5px solid #73c7c5;
    border: 1px solid #73c7c5;
  }
`

const ListItem = styled.div`
  display: table;
  padding: 5px 0px 5px 0px;
  
  height: 20px;
  width: 100%;
  
  cursor: pointer;
`

const WrapperLabel = styled.div`
  display: table-cell;
  
  height: 100%;
  width: calc(90% - 10px);
  
  cursor: pointer;
`

const WrapperLabelIndex = styled.div`
  display: table-cell;
  height: 100%;
  width: 10%;
`

const LabelIndex = styled.label`
  font-family: HelveticaLight;
  font-size: 16px;
  color: #fff;
`

const Label = styled.label`
  font-family: HelveticaLight;
  font-size: 16px;
  color: #fff;
`

function BlockSkuList(props) {
  const { data, order = [] } = props

  if (!order.length) return <Container />

  return (
    <Container>
      <List>
        {
          order.map((item, index) => {
            const { goodsName } = data[item]

            return (
              <ListItem key={ index }>
                <WrapperLabelIndex>
                  <LabelIndex>{ index + 1 }</LabelIndex>
                </WrapperLabelIndex>
                <WrapperLabel>
                  <Label>{ goodsName }</Label>
                </WrapperLabel>
              </ListItem>
            )
          })
        }
      </List>
    </Container>
  )
}

export default BlockSkuList
