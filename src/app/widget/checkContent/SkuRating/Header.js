import React                from 'react'

import styled               from 'styled-components'


const Header = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  height: 50px;
  
  border-bottom: 1px solid #f9f9f9
`

const HeaderTitle = styled.div`
  display: table;
  height: 50px;
  width: calc(100% - 50px);
`

const LabelTitle = styled.label`
  display: table-cell;
  vertical-align: middle;
  font-family: HelveticaLight;
  font-size: 14px;
  color: #fff;
  padding-left: 50px;
`

const HeaderClose = styled.div`
  position: relative;
  display: block;
  box-sizing: border-box;
  width: 50px;
  height: 50px;
  border-left: 1px solid #7d7d7d;
  cursor: pointer;
  
  &:hover:before, :hover:after {
    opacity: 0.6;
  }
  &:before, &:after {
    content: "";
    position: absolute;
    top: 50%;
    left: 9px;
    width: 30px;
    height: 1px;
    opacity: 0.9;
    background-color: #fff;
    cursor: pointer;
  }
  &:before {
    transform: translateY(-50%) rotate(45deg);
  }
  &:after {
    transform: translateY(-50%) rotate(-45deg);
  }
`

export default (props) => {

  const onHandleClose = () => {
    const { onClose } = props

    onClose()
  }

  const { leftTitle } = props

  const title = leftTitle ? leftTitle.toUpperCase() : ''

  return (
    <Header>
      <HeaderTitle>
        <LabelTitle>{ title }</LabelTitle>
      </HeaderTitle>
      <HeaderClose onClick={ onHandleClose } />
    </Header>
  )
}
