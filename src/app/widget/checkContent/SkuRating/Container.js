import React, { Component } from 'react'

import { connect }          from 'react-redux'
import { compose }          from 'redux'

import styled               from 'styled-components'


import Header               from './Header'
import BlockCategoryList    from './CategoryList'
import SkuList              from './SkuList'

import withData             from './hoc/withDataSkuRating'


const WrapperContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 800px;
  height: 400px;
  
  background-color: #403e3e;
`

const Content = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  
  width: 100%;
  height: calc(100% - 50px);
`

const ContentLeft = styled.div`
  display: block;
  width: 50%;
  height: 100%;
  background-color: #484848;
  
  position: relative;
`

const Back  = styled.input`
  position: absolute;
  top: 10px;
  left: 0px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 150px;
  height: 35px;
  text-align: center;
  font-weight: 300;
  font-family: HelveticaRegular;
  font-size: 18px;
  border: none;
  outline: none;
  color: #fff;
  background-color: #48b9bf;
  cursor: pointer;
    
  &:hover {
    background-color: #59cad0;
  }
  
  &:focus, &:active {
    border: none;
    outline: none;
  }
`


class Container extends Component {
  constructor() {
    super()

    this.state = {
      data: {},
      order: [],
      categoryId: '',

      title: '',
      description: '',

      dataSku: {},
      orderSku: []
    }
  }

  componentWillMount() {
    const { data, order = [], title, description } = this.props

    if (order.length > 0) {
      this.setState({ data, order, title, description })
    }
  }

  componentWillReceiveProps(nextProps) {
    const { data, order = [], title, description } = nextProps

    if (order.length > 0) {
      this.setState({ data, order, title, description })
    }
  }

  onHandleClose = () => {
    const { onClose } = this.props

    onClose()
  }

  onSelectCategory = ({ id }) => {
    const { data, order } = this.state

    const dataSku = order.reduce((dataSku, item, index) => {
      const { categoryId, goodsId, goodsName } = data[item]

      if (categoryId === id) {
        return { ...dataSku, [index]: { goodsName, goodsId } }
      }

      return { ...dataSku }
    }, {})

    const orderSku = Object.keys(dataSku)

    this.setState({ dataSku, orderSku, categoryId: id })
  }

  onHandleBack = () => {
    const { onBack } = this.props

    onBack()
  }

  render() {
    const { open, loading } = this.props

    const { data, order, dataSku, orderSku, title } = this.state

    if (!open) return null

    return (
      <WrapperContainer>
        <Header leftTitle={ title } onClose={ this.onHandleClose } />
        <Content>
          <ContentLeft>
            <Back
              type='button'
              value='Назад'
              onClick={ this.onHandleBack }
            />
            <BlockCategoryList
              data={ data }
              order={ order }
              onSelect={ this.onSelectCategory }
              loading={ loading }
            />
          </ContentLeft>
          <SkuList data={ dataSku } order={ orderSku } />
        </Content>
      </WrapperContainer>
    )
  }
}

export default compose(
  connect(),
  withData,
)(Container)
