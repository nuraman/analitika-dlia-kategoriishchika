import React                from 'react'

import styled               from 'styled-components'

import { CircularProgress } from 'material-ui/Progress'


const List = styled.div`
  display: block;
  margin: 50px 20px;
  overflow: auto;
  
  border: 1px solid #7d7d7d;
  
  height: calc(100% - 102px);
  width: calc(100% - 40px);
  
   &::-webkit-scrollbar
  {
    width: 9px;
  }
  
  &::-webkit-scrollbar-track
  {
    background: #7d7d7d;
    border: 3px solid transparent;
    background-clip: content-box;
  }
  
  &::-webkit-scrollbar-thumb
  {
    background: #73c7c5;
    height: 35px;
    border-radius: 5px solid #73c7c5;
    border: 1px solid #73c7c5;
  }
`

const ListItem = styled.div`
  display: table;
  
  padding: 5px 0px 5px 0px;
  
  height: 20px;
  width: 100%;
 
  &: hover {
    color: #b3fffb;
    background-color: #314545;
  }
  
  cursor: pointer;
`

const Label = styled.label`
  font-family: HelveticaLight;
  font-size: 16px;
  color: #fff;
  
  height: 16px;
  
  padding: 0 10px;
  
  cursor: pointer;
 `

const EmptyContainer = styled.div`
  display: flex;
  align-items: center; 
  justify-content: space-around;
  
  margin: 50px 20px;
  
  border: 1px solid #7d7d7d;
  
  height: calc(100% - 102px);
  width: calc(100% - 40px);
`


export default (props) => {
  const { data, order = [], onSelect, loading } = props

  const onHandleClick = (id) => onSelect({ id })

  if (loading && !order.length) return (
    <EmptyContainer>
      <CircularProgress />
    </EmptyContainer>
  )

  if (!order.length) return null

  const categoryList = order.reduce((categoryList, id) => {
    const { categoryId, categoryName } = data[id]

    let index = categoryList.findIndex((item) => item.categoryId == categoryId)

    if (index === -1) {
      categoryList.push({ categoryId, categoryName })
    }

    return [...categoryList]
  }, [])

  return (
      <List>
        {
          categoryList.map((item, index) => {
            const { categoryId, categoryName } = item

            return (
              <ListItem key={ index } onClick={ () => onHandleClick(categoryId) }>
                <Label>{ categoryName }</Label>
              </ListItem>
            )
          })
        }
      </List>
  )
}
