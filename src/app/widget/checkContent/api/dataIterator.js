const getPrevList = ({ data, order, firstIndex, max }) => {
  let range = {
    from: +firstIndex - 1,
    to: +firstIndex - max - 1,
  }

  range[Symbol.iterator] = function () {
    let current = this.from
    let last = this.to

    return {
      next() {
        if (last <= current && order[current]) {
          return {
            done: false,
            value: order[current],
            current: current--
          }
        } else {
          return {
            done: true,
          }
        }
      }
    }
  }

  const newOrder = []
  for(let item of range) {
    newOrder.push(item)
  }

  const newData = newOrder.reduce((newData, id) => {
    return { ...newData, [id]: { ...data[id] } }
  }, {})

  return {
    newData,
    newOrder
  }
}

const getNextList = ({ data, order, lastIndex, max }) => {
  let range = {
    from: +lastIndex + 1,
    to: +lastIndex + 1 + max,
  }

  range[Symbol.iterator] = function () {
    let current = this.from
    let last = this.to

    return {
      next() {
        if (current <= last && order[current]) {
          return {
            done: false,
            value: order[current],
            current: current++
          }
        } else {
          return {
            done: true,
          }
        }
      }
    }
  }

  const newOrder = []
  for(let item of range) {
    newOrder.push(item)
  }

  const newData = newOrder.reduce((newData, id) => {
    return { ...newData, [id]: { ...data[id] } }
  }, {})

  return {
    newData,
    newOrder
  }
}

export default {
  getNextList,
  getPrevList
}
