import types                from 'engine/types'

const inputNameMap = {
  DATE_BEGIN:                         'dateBegin',
  }

const queryName = 'grf17'

const resultNameMap = {
  SEGMENTS:                           'segments',
  DATE:                               'date',
  VALUE:                              'value',
}

const titleNameMap = {
  CAPTION:                            'caption',
  WIDGET:                             'vidget',
  DESCRIPTION:                        'description',
}

const queryMap = [
  {
    queryName: queryName,
    inputList: [
      {
        name: inputNameMap.DATE_BEGIN,
        type: types.String,
      },
    ],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        }
      ],
      result: [
        {
          name: resultNameMap.SEGMENTS,
          type: types.String,
        },
        {
          name: resultNameMap.VALUE,
          type: types.Float,
        },
        {
          name: resultNameMap.DATE,
          type: types.String,
        }
      ],
      title: [
        {
          name: titleNameMap.CAPTION,
          type: types.String,
        },
        {
          name: titleNameMap.DESCRIPTION,
          type: types.String,
        },
        {
          name: titleNameMap.WIDGET,
          type: types.String,
        }
      ]
    }
  },
]

export default {
  queryMap,
  name: queryName,
}
