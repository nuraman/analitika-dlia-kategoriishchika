import React, { Component } from 'react'
import { connect }          from 'react-redux'
import { compose }          from 'redux'


import withGrf17            from 'app/widget/graphql/withGrf17'


function withData(ComposedComponent) {
  class Widget extends Component {
    render() {
      return (
        <ComposedComponent { ...this.props } />
      )
    }
  }

  function mapSateToProps(state) {
    const { menu } = state

    const { period: { dateBegin } } = menu

    return {
      dateBegin,
      options: {
        variables: {
          dateBegin: dateBegin,
        }
      }
    }
  }

  return compose(
    connect(mapSateToProps),
    withGrf17(),
  )(Widget)
}

export default withData
