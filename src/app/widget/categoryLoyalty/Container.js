import React, { Component } from 'react'

import { compose }          from 'redux'

import styled               from 'styled-components'


import ProgressBar          from 'app/analysis/web/WidgetProgressBar'

import EmptyWidget          from 'app/widget/WidgetEmpty'

import Widget               from 'app/widget/styledComponent/widget/DivContainer'
import WidgetHeader         from 'app/widget/header/Container'

import withData             from './hoc/withData'
import BlockColumn          from './Column'


const WidgetBody = styled.div`
  display: flex;
  flex-direction: row;
  flex-grow: 1;
  justify-content: space-around;
  
  width: 100%;
  height: calc(100% - 50px);
`


class Container extends Component {
  constructor() {
    super()

    this.state = {
      dateBegin: '',
      description: '',
      widgetId: '',


      resultList: [],
      title: '',
    }
  }

  componentWillReceiveProps(nextProps) {
    const { data, order = [], title, dateBegin } = nextProps

    const result = []

    const { caption, description, vidget } = title || {}

    if (order.length > 0) {
      const segmentList = order.reduce((segmentList, item) => {
        const { segments } = data[item]

        const index = segmentList.indexOf(segments)

        if (index === -1) {
          segmentList.push(segments)
        }

        return [...segmentList]
      }, [])

      if (segmentList.length > 0) {
        segmentList.forEach((s) => {
          const dataList = order.reduce((dataList, item) => {
            const { segments } = data[item]

            if (s === segments) dataList.push(data[item])

            return [...dataList]
          }, [])

          if (dataList.length > 0) result.push(dataList)
        })
      }
    }

    this.setState({
      data,
      description,
      dateBegin,

      order,
      result,
      title: caption,
      widgetId: vidget,
    })
  }

  //надо переделать, поскольку проблема при скачивании jpeg
  render() {
    const { result = [], title, description, dateBegin, widgetId } = this.state

    const { loading, order = [] } = this.props

    if (loading) return (
      <ProgressBar
        title='Динамика лояльности к категории'
      />
    )

    if (!loading && order.length === 0) return <EmptyWidget title='Динамика лояльности к категоии' />

    return (
      <Widget>
        <WidgetHeader
          title={ title }
          description={ description }
          dateBegin={ dateBegin }
          widgetId={ widgetId }
        />
        <WidgetBody>
          {
            result.map((item, index) => {
              return (<BlockColumn dataList={ item } key={ index } />)
            })
          }
        </WidgetBody>
      </Widget>
    )
  }
}

export default compose(
  withData
)(Container)

