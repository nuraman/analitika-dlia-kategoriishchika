import React, { Component } from 'react'

import ReactHighcharts      from 'react-highcharts'

import styled               from 'styled-components'


import Chart                from './Chart'


const Widget = styled.div`
  display: block;
  height: 100%;
`

const WidgetContent = styled.div`
  display: flex;
  height: 80%;
`

const WidgetFooter = styled.div`
  display: flex;
  height: 20%;
  align-items: center;
  justify-content: space-around;
`

const Label = styled.div`
  font-family: HelveticaRegular;
  font-size: 18px;
  color: #000;
`

class Column extends Component {
  constructor() {
    super()

    this.state = {
      chart: {},
      title: '',
      config: {},
      dataList: [],
    }
  }

  componentWillMount() {
    const { dataList = [] } = this.props

    const chart = new Chart()

    const { title, config } = this.getConfig({ dataList, chart })

    this.setState({ config, title, chart, dataList })
  }

  componentWillReceiveProps(nextProps) {
    const { dataList = [] } = nextProps

    const { chart } = this.state

    const { title, config } = this.getConfig({ dataList, chart })

    this.setState({ config, title, chart, dataList })
  }

  getConfig = ({ dataList, chart }) => {
    if (dataList.length > 0) {
      let title = ``

      const data = dataList.reduce((data, item, index) => {
        const { segments } = item

        title = segments

        return {...data, [index]: { ...item }}
      }, {})

      const order = Object.keys(data)

      chart.options = {data, order, height: 250, width: 200, name: 'dsaf'}

      return { title, config: chart.getConfig() }
    }

    return { title: '', config: {} }
  }

  render() {
    const { config, title } = this.state

    return(
      <Widget>
        <WidgetContent>
          <ReactHighcharts config={ config } />
        </WidgetContent>
        <WidgetFooter>
          <Label>
          { title }
          </Label>
        </WidgetFooter>
      </Widget>
    )
  }
}

export default Column
