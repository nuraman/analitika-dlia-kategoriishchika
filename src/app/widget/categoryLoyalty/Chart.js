function Chart() {
  this.data = {}
  this.order = []
  this.height = ''
  this.width = ''
  this.name = ''
}

Chart.prototype =  {
  set options(payload) {
    const { data, order, height, width, name } = payload

    this.data = data
    this.order = order
    this.height = height
    this.width = width
    this.name = name
  }
}

Chart.prototype.getSeries = function () {
  let name =''
  const dataSelf = this.order.reduce((dataSelf, item, index) => {
    const { value, segments, date } = this.data[item]
    name = segments

    if (index < this.order.length - 1) {
      dataSelf.push({ y: value, value: date, color: 'rgb(248, 128, 148)' })
    }

    if (index === this.order.length - 1) {
      dataSelf.push({ y: value, value: date, color: 'rgb(99, 218, 196)' })
    }
    return  [...dataSelf]
  }, [])

  const series = [{
    name,
    data: dataSelf,
    dataLabels: {
      enabled: true,
      align: 'center',
      x: 0,
      y: 0,
      color: '#666666',
      fontFamily: 'HelveticaRegular',
      formatter: function () {
        return this.y
      }
    }
  }]

  this.name = name

  return series
}

Chart.prototype.getCategories = function () {
  return [this.name]
}

Chart.prototype.getConfig = function () {
  const series = this.getSeries()
  const categories = this.getCategories()

  const config = {
    chart: {
      type: 'column',
      width: 150,
    },
    title: {
      text: ''
    },
    navigation: {
      buttonOptions: {
        enabled: false,
      }
    },
    xAxis: {
      categories: categories,
      title: {
        style: {
          fontFamily: 'HelveticaRegular',
        }
      },
      visible: false,
    },
    yAxis: {
      visible: false
    },
    plotOptions: {
      column: {
        borderWidth: 0,
        pointPadding: 0,
        pointWidth: 40
      },
    },
    legend: {
      enabled: false,
      itemStyle: {
        fontFamily: 'HelveticaBold',
        fontSize: '14px',
      }
    },
    tooltip: {
      useHTML: true,
      formatter: function() {
        return `<div stype="display: block;">Количество покупателей: ${this.y} за ${this.point.value}</div>`
      },
      style: {
        fontFamily: 'HelveticaRegular',
        fontSize: '14px'
      },
    },
    series: series
  }

  return config
}

export default Chart
