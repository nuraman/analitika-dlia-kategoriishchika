import styled               from 'styled-components'

const Menu = styled.div`
  display: table-cell;
  text-align: right;
  vertical-align: middle;
`

export default Menu
