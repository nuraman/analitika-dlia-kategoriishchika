import styled               from 'styled-components'

export default styled.label`
  font-family: HelveticaRegular;
  font-size: 22px;
`
