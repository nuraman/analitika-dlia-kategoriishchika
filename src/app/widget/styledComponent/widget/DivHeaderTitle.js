import styled               from 'styled-components'


const StyledHeaderTitle = styled.div`
  display: table-cell;
  vertical-align: middle;
  padding-left: 20px;
  width: ${ props=>props.width }px;
  
  font-size: 22px;
  font-family: HelveticaRegular;
  &:hover {
    > div: nth-child(2) {
      display:  flex;
   }
  }
}
`

export default StyledHeaderTitle
