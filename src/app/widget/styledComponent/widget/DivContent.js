import styled               from 'styled-components'

const StyledContent = styled.div`
  display: block;
  height: calc(100% - 60px);
  width: 100%;
`

export default StyledContent
