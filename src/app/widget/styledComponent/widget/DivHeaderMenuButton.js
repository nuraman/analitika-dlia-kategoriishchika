import styled               from 'styled-components'


const StyledHeaderMenuButton = styled.div`
  display: inline-block;
`

export default StyledHeaderMenuButton
