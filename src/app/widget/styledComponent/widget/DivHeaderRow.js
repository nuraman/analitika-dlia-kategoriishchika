import styled               from 'styled-components'


const StyledHeaderRow = styled.div`
  display: table;
  height: 100%;
  width: 100%;
`

export default StyledHeaderRow
