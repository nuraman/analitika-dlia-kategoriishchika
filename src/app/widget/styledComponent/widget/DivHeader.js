import styled               from 'styled-components'


const StyledHeader = styled.div`
  display: block;
  height: 60px;
  width: inherit;
  border-bottom: 1px solid rgba(0, 0, 0, 0.12);
`

export default StyledHeader
