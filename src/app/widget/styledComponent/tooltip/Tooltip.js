import styled               from 'styled-components'

const StyledTooltip = styled.div`
  position: absolute; 
  top: 60px;
  left: 0px;
  display: none;
  box-sizing: border-box;
  width: 300px;
  z-index: 1000;
  padding: 15px;
  border: 1px solid #dddddd;
  background-color: #fff;
  
  &:before, &:after {
    position: absolute;
    content: "";
    left: 20px;
    width: 0;
    height: 0;
  }
  
  &:before {
    bottom: 100%;
    border-right: 9px solid transparent;
    border-bottom: 14px solid #dddddd;
    border-left: 9px solid transparent;
  }
  &:after {
    bottom: calc(100% - 2px);
    border-right: 9px solid transparent;
    border-bottom: 14px solid #fff;
    border-left: 9px solid transparent;
  }
`

export default StyledTooltip
//border-color: transparent transparent rgb(223, 228, 222) transparent;
