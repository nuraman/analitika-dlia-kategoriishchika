import ReactHighchart       from 'react-highcharts'

import sizeMe               from 'react-sizeme'

import React, { Component } from 'react'

import { compose }          from 'redux'


import ProgressBar          from 'app/analysis/web/WidgetProgressBar'


import EmptyWidget          from 'app/widget/WidgetEmpty'

import withData             from 'app/widget/grf05/hoc/withData'

import WidgetHeader         from 'app/widget/header/Container'
import Widget               from 'app/widget/styledComponent/widget/DivContainer'

import Chart                from 'app/widget/grf05/hoc/Chart'


class Grf05Logic extends Component {
  constructor(props) {
    super(props)

    this.state = {
      dateBegin: '',
      config: {},
      chart: {},
      order: [],
      parameterId: '',
      title: '',
      description: '',
      widgetId: ''
    }
  }

  componentWillMount() {
    const chart = new Chart()

    this.setState({ chart })
  }

  componentWillReceiveProps(nextProps) {
    const {
      data = {},
      order = [],
      parameterId,
      size: { height },
      title,
      dateBegin
    } = nextProps

    const { chart } = this.state

    const { caption, description, vidget } = title || {}

    if (order.length > 0) {
      chart.options = { data, order, height }

      const config = chart.getConfig()

      this.setState({
        config,
        dateBegin,
        description,
        order,
        parameterId,
        title: caption,
        widgetId: vidget,
      })
    }

    if (order.length === 0) this.setState({ title: caption, description, widgetId: vidget, dateBegin })
  }

  exportImage = () => {
    const { title } = this.state

    this.chart.getChart().exportChart({ filename: title ? title : 'виджет' } , null)
  }

  render() {
    const { order, config, parameterId, title, description, widgetId, dateBegin } = this.state
    const { loading } = this.props

    if (loading === true) return (
      <ProgressBar
        title='Половозрастная структура'
      />
    )

    if (loading === false && order.length === 0) return <EmptyWidget title={ title } />

    return(
      <Widget>
        <WidgetHeader
          title={ title }
          description={ description }
          id = { parameterId }
          widgetId={ widgetId }
          dateBegin={ dateBegin }

          exportImage={ this.exportImage }
        />
        <ReactHighchart config={ config } ref={ (chart) => { this.chart = chart } } />
      </Widget>
    )
  }
}

export default compose (
  sizeMe({ monitorHeight: true, monitorWidth: true }),
  withData,
)(Grf05Logic)
