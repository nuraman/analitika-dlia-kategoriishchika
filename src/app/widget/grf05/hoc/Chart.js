import _                    from 'lodash'
import update               from 'immutability-helper'


const genderType = {
  MEN: 1,
  WOMAN: 2,
  NOT: 0
}

const genderColor = {
  NOT: '#9E9E9E',
  MEN: '#0D47A1',
  WOMEN: '#E91E63'
}

class Chart {
  constructor() {
    this.data = []
    this.order = {}
    this.height = ''
  }

  set options({ data, order, height }) {
    this.data = data
    this.order = order
    this.height = height
  }

  getConfig() {
    const series = this.getSeries()
    const categories = this.getCategories()

    const chartHeight = `${ this.height - 60 }px`

      const config = {
        chart: {
          type: 'bar',
          height: chartHeight
        },
        navigation: {
          buttonOptions: {
            enabled: false,
          }
        },
        plotOptions: {
          series: {
            stacking: 'normal'
          }
        },
        title: {
          text: ''
        },
        xAxis:{
          categories: categories,
          title: {
            text: 'Возраст',
            style: {
              fontFamily: 'HelveticaRegular',
              fontSize: '14px',
            }
          },
          labels: {
            style: {
              fontFamily: 'HelveticaRegular',
              fontSize: '12px',
            }
          }
        },
        yAxis: {
          min: 0,
          title: {
            text: 'Количество',
            style: {
              fontFamily: 'HelveticaRegular',
              fontSize: '14px',
            }
          },
          labels: {
            style: {
              fontFamily: 'HelveticaRegular',
              fontSize: '12px',
            }
          }
        },
        legend: {
          itemStyle: {
            fontFamily: 'HelveticaBold',
            fontSize: '14px',
          }
        },

        series: series
      }

      return config
  }

  getSeries = () => {
    const { dataGender, orderGender } = this.getDataForSeries()

    const series = orderGender.reduce((series, id) => {
      let color = ''
      const { valueList, name } = dataGender[id] || {}

      if (id == genderType.NOT) color = genderColor.NOT
      if (id == genderType.MEN) color = genderColor.MEN
      if (id == genderType.WOMAN) color = genderColor.WOMEN

      const dataList = valueList.reduce((dataList, item) => {
        const { value } = item

        return [...dataList, { color, y: value }]
      },[])

      return [...series, { name, data: dataList, color }]
    },[])

    return series
  }

  getAgeRangeList = () => {
    const ageRangeList = this.order.reduce((ageRangeList, item) => {
      const { ageBegin, ageEnd } = this.data[item] || {}
      const value = `${ ageBegin }-${ ageEnd }`

      const exist = _.find(ageRangeList, { ageBegin })

      if (exist) return [ ...ageRangeList ]

      return [ ...ageRangeList, { ageBegin, value } ]
    }, [])

    return _.sortBy(ageRangeList, 'value')
  }

  getCategories = (payload) => {
    const ageRangeList = this.getAgeRangeList()

    const categories = ageRangeList.reduce((categories, item) => {
      return [...categories, item.value]
    }, [])

    return categories
  }

  getDataForSeries = () => {
    const ageRangeList = this.getAgeRangeList()
    const { dataGender, orderGender } = this.getDataGender()
    this.checkDataGender({ dataGender, orderGender, ageRangeList })

    return {
      dataGender,
      orderGender,
    }
  }

  getDataGender = () => {
    const dataGender = this.order.reduce((dataGender, item) => {
      const doc = this.data[item] || {}
      const { gender, ageBegin, value } = doc

      const existDataGender = _.find(dataGender, { id: gender })

      if (!existDataGender) {
        let name = ''
        if (gender == genderType.NOT) name = 'Не указан'
        if (gender == genderType.MEN) name = 'Муж.'
        if (gender == genderType.WOMAN) name = 'Жен.'

        const valueList = [{ ageBegin, value }]

        return { ...dataGender, [gender]: { id: gender, valueList, name } }
      }

      const { valueList } = existDataGender

      if (!_.includes(valueList, { ageBegin })) {
        valueList.push({ ageBegin, value })
      }

      const newDataGender = update( dataGender, { [gender]: { valueList: { $set: valueList } } } )

      return { ...newDataGender }
    }, {})

    const orderGender = Object.keys(dataGender)

    return {
      dataGender,
      orderGender,
    }
  }

  checkDataGender = ({ dataGender, orderGender, ageRangeList }) => {
    orderGender.map((id) => {
      const { valueList } = dataGender[id] || {}

      const newValueList = ageRangeList.reduce((newValueList, item) => {
        const { ageBegin } = item

        const existValue = _.find(valueList, { ageBegin })

        if(existValue) {
          const { value } = existValue

          newValueList.push({ ageBegin, value })
        }

        if(!existValue) {
          newValueList.push({ ageBegin, value: 0 })
        }

        return newValueList
      }, [])

      return _.set(dataGender[id], 'valueList', newValueList)
    })
  }
}

export default Chart
