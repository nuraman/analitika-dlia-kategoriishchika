import React                from 'react'

import { connect }          from 'react-redux'
import { compose }          from 'redux'


import withGrf05            from 'app/widget/graphql/withGrf05'
import mapStateToProps      from 'app/widget/mapStateToProps'


function withData(ComposedComponent) {
  function decorator(props) {
    return (<ComposedComponent {...props} />)
  }

  return compose(
    connect(mapStateToProps),
    withGrf05(),
  )(decorator)
}

export default withData
