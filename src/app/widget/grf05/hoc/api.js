/*************************************
 * Формат входных данных
 * data = { 'id0': { id: id0, gender, ageBegin, ageEnd, value } }
 * где, gender = 0 (Он), 1 (Он), 2 (Онa), ageBegin и ageEnd - период возраста, value - количество
 * order = [ 'id0', ..., 'idN' ]
 *
 * Описание методов
 * onGetDataForSeries - формирует данные для объекта series
 * onGetDataGender    - формирует объект данных по ключевому полю gender
 * onGetAgeRangeList  - формирует список возрастных групп (0-17, 18-24, ...)
 * onChectDataGender  - проверяет объект данных на полноту, в случае чего дополняет нулями, необходимо для корректного отображения на графике
 * onGetSeries        - формирует объект series для графика
 * onGetCategories    - формирует категорию для графика
 *
 *Формат выходных данных
 * series = [{
 *  name: 'М',
 *  valueList: [1,2,3,...,N]
 * }]
 *
 * categories = [ '0-17', '18-24', '25-34', ... ]
 * ***********************************/
import _      from 'lodash'

import update from "immutability-helper"


const getSeries = (payload) => {
  const { data, order } = payload

  const { dataGender, orderGender } = onGetDataForSeries({ data, order })

  const series = orderGender.reduce((series, id) => {
    let color = ''
    const doc = dataGender[id] || {}
    const { valueList, name } = doc

    if (id === '0') color = '#9E9E9E' //не указан
    if (id === '1') color = '#0D47A1' //муж
    if (id === '2') color = '#E91E63' //жен

    const dataList = valueList.reduce((dataList, item) => {
      const { value } = item

      return [...dataList, {
        color: color,
        y: value
      }]
    },[])

    return [...series, {
      name,
      data: dataList,
      color: color
    }]
  },[])

  return series
}

const getAgeRangeList = (payload) => {
  const { data, order } = payload

  const ageRangeList = order.reduce((ageRangeList, item) => {
    const doc = data[item] || {}
    const { ageBegin, ageEnd } = doc
    const value = `${ ageBegin }-${ ageEnd }`

    const exist = _.find(ageRangeList, { ageBegin })

    if (exist) return [ ...ageRangeList ]

    return [ ...ageRangeList, { ageBegin, value } ]
  }, [])

  return _.sortBy(ageRangeList, 'value')
}

const getCategories = (payload) => {
  const { data, order } = payload

  const ageRangeList = getAgeRangeList({ data, order })

  const categories = ageRangeList.reduce((categories, item) => {
    return [...categories, item.value]
  }, [])

  return categories
}

const onGetDataForSeries = ({ data, order }) => {
  const ageRangeList = getAgeRangeList({ data, order })
  const { dataGender, orderGender } = onGetDataGender({ data, order })
  onCheckDataGender({ dataGender, orderGender, ageRangeList })

  return {
    dataGender,
    orderGender,
  }
}
//разделить данные на типы полов
const onGetDataGender = ({ data, order }) => {
  const dataGender = order.reduce((dataGender, item) => {
    const doc = data[item] || {}
    const { gender, ageBegin, value } = doc

    const existDataGender = _.find(dataGender, { id: gender })

    if (!existDataGender) {
      let name = ''
      if (gender === 0) name = 'Не указан'
      if (gender === 1) name = 'Муж.'
      if (gender === 2) name = 'Жен.'

      const valueList = [{ ageBegin, value }]

      return { ...dataGender, [gender]: { id: gender, valueList, name } }
    }

    const { valueList } = existDataGender

    if (!_.includes(valueList, { ageBegin })) {
      valueList.push({ ageBegin, value })
    }

    const newDataGender = update( dataGender, { [gender]: { valueList: { $set: valueList } } } )

    return { ...newDataGender }
  }, {})

  const orderGender = Object.keys(dataGender)

  return {
    dataGender,
    orderGender,
  }
}

const onCheckDataGender = ({ dataGender, orderGender, ageRangeList }) => {
  orderGender.map((id) => {
    const { valueList } = dataGender[id] || {}

    const newValueList = ageRangeList.reduce((newValueList, item) => {
      const { ageBegin } = item

      const existValue = _.find(valueList, { ageBegin })

      if(existValue) {
        const { value } = existValue

        newValueList.push({ ageBegin, value })
      }

      if(!existValue) {
        newValueList.push({ ageBegin, value: 0 })
      }

      return newValueList
    }, [])

    return _.set(dataGender[id], 'valueList', newValueList)
  })
}

const getInitConfig = (payload) => {
  const { series, categories, height } = payload

  const chartHeight = `${ height - 60 }px`

  const config = {
    chart: {
      type: 'bar',
      height: chartHeight
    },
    navigation: {
      buttonOptions: {
        enabled: false,
      }
    },
    plotOptions: {
      series: {
        stacking: 'normal'
      }
    },
    title: {
      text: ''
    },
    xAxis:{
      categories: categories,
      title: {
        text: 'Возраст',
        style: {
          fontFamily: 'HelveticaRegular',
          fontSize: '14px',
        }
      },
      labels: {
        style: {
          fontFamily: 'HelveticaRegular',
          fontSize: '12px',
        }
      }
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Количество',
        style: {
          fontFamily: 'HelveticaRegular',
          fontSize: '14px',
        }
      },
      labels: {
        style: {
          fontFamily: 'HelveticaRegular',
          fontSize: '12px',
        }
      }
    },

    series: series
  }

  return config
}


export default {
  getSeries,
  getCategories,
  getInitConfig,
}
