import types                from 'engine/types'


const inputNameMap = {
  DATE_BEGIN:                         'dateBegin',
}

const queryNameMap = {
  GRF05:                              'grf05',
}

const resultNameMap = {
  GENDER:                             'gender',
  AGE_BEGIN:                          'ageBegin',
  AGE_END:                            'ageEnd',
  VALUE:                              'value'
}

const titleNameMap = {
  CAPTION:                             'caption',
  WIDGET:                              'vidget',
  DESCRIPTION:                         'description',
}

const queryMap = [
  {
    queryName: queryNameMap.GRF05,
    inputList: [
      {
        name: inputNameMap.DATE_BEGIN,
        type: types.String,
      },
    ],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        }
      ],
      result: [
        {
          name: resultNameMap.GENDER,
          type: types.Int,
        },
        {
          name: resultNameMap.AGE_BEGIN,
          type: types.Int,
        },
        {
          name: resultNameMap.AGE_END,
          type: types.Int,
        },
        {
          name: resultNameMap.VALUE,
          type: types.Float,
        }
      ],
      title: [
        {
          name: titleNameMap.CAPTION,
          type: types.String,
        },
        {
          name: titleNameMap.DESCRIPTION,
          type: types.String,
        },
        {
          name: titleNameMap.WIDGET,
          type: types.String,
        }
      ]
    }
  },
]

export default {
  queryMap,
  name: queryNameMap.GRF05
}
