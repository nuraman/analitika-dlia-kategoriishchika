import moment               from 'moment'
import localization from "moment/locale/ru"

const MONTH = {
  1 : 'январь',
  2 : 'Февраль',
  3 : 'март',
  4 : 'апрель',
  5 : 'май',
  6 : 'июнь',
  7 : 'июль',
  8 : 'август',
  9 : 'сентябрь',
  10 : 'октбярь',
  11 : 'ноябрь',
  12 : 'декабрь'
}

const PERIOD = {
  WEEK: 1,
  MONTH: 2,
  QUARTER: 3,
  HALF_YEAR: 4,
  YEAR: 5,
}

const onGetDataSeries = ({ data, order }) => {
  const valueSelfList = [],
    valueOtherList = [],
    valueAnyList = [],
    categoryList = [],
    color={}

  order.forEach((id) => {
    const doc = data[id] || {}
    const { date, valueSelf, valueOther, valueAny, colorSelf, colorAny, colorOther } = doc

    color.self = colorSelf
    color.other = colorOther
    color.any = colorAny

    categoryList.push(date)
    valueSelfList.push(valueSelf)
    valueOtherList.push(valueOther)
    valueAnyList.push(valueAny)
  })

  const dataSelf = {
    color: `#${ color.self }`,
    data: valueSelfList,
    name: 'Мой бренд',
    id: 1,
  }

  const dataAny = {
    color: `#${ color.any }`,
    data: valueAnyList,
    name: 'Пересечения',
    id: 2,
  }

  const dataOther = {
    color: `#${ color.other }`,
    data: valueOtherList,
    name: 'Бренды конкурентов',
    id: 3,
  }

  return {
    categoryList,
    dataSelf,
    dataAny,
    dataOther,
  }
}

const getSeries = (payload) => {
  const { data, order } = payload

  const {  categoryList, dataSelf, dataAny, dataOther, } = onGetDataSeries({ data, order })

  const series = [
    dataSelf,
    dataAny,
    dataOther,
  ]

  return {
    series,
    categoryList,
  }
}

const getCategory = (payload) => {
  const { categoryList, periodName, periodType } = payload

  moment.locale('ru', localization)

  const newCategoryList = categoryList.reduce((newCategoryList, date, index) => {

    if (periodType === PERIOD.WEEK) {
      newCategoryList.push(`${ moment(date).format('DD MMMM') }`)
    } else {
      const dateInString = moment(date).format('DD MM YYYY').split(' ')

      try {
        let mNumber = +dateInString[1]
        let year = dateInString[2]

        const month = `${ MONTH[mNumber] } ${ year }`
        newCategoryList.push(month)
      }
      catch(e) {
        newCategoryList.push('Без даты')
      }
    }

    return [...newCategoryList]
  }, [])

  if (periodType === PERIOD.WEEK) {
    const weekNumber = moment().format('W')
    let j = 0

    for(let i = newCategoryList.length - 1; i >= 0; i--) {
      if (weekNumber - j > 0) {
        newCategoryList[i] = newCategoryList[i] + ` (${ weekNumber - j } ${ periodName })`
        j++
      } else {
        newCategoryList[i] = newCategoryList[i] + ` (${ periodName })`
      }
    }
  }

  return newCategoryList
}

export default {
  getSeries,
  getCategory,
}
