import React, { Component } from 'react'

import ReactHighchart       from 'react-highcharts'

import sizeMe               from 'react-sizeme'

import { compose }          from 'redux'


import reducerDiscount      from 'app/discount/reducer'

import WidgetProgressBar    from 'app/analysis/web/WidgetProgressBar'
import EmptyWidget          from 'app/widget/WidgetEmpty'

import withData             from 'app/widget/grf06/hoc/withData'

import Widget               from 'app/widget/styledComponent/widget/DivContainer'
import WidgetHeader         from 'app/widget/header/Container'

import Chart                from 'app/widget/grf06/Chart'


class Grf06Logic extends Component {
  constructor() {
    super()

    this.state = {
      chart: {},
      dateBegin: '',
      description: '',
      config: {},
      title: '',
      widgetId: ''
    }
  }

  onSelectDiscountCallback = (id) => {
    const { dispatch, onSelect } = this.props

    dispatch({
      type: reducerDiscount.types.SET_SUCCESS,
      payload: {
        widget: '6',
        param1: null,   // id выбранной категории
        param2: id
      }
    })

    onSelect()
  }

  componentWillMount() {
    const chart = new Chart()

    this.setState({ chart })
  }

  componentWillReceiveProps(nextProps) {
    const {
      data = {},
      dateBegin,
      order = [],
      periodId,
      title = {},
      size: { height },
      isProvider,
    } = nextProps

    const { chart } = this.state

    const { caption, description, vidget } = title

    chart.options = { data, order, height, periodId, provider: isProvider }

    const config = chart.getConfig(this.onSelectDiscountCallback)

    this.setState({
      title: caption,
      description,
      config,
      widgetId: vidget,
      dateBegin
    })
  }

  exportImage = () => {
    const { title } = this.state

    this.chart.getChart().exportChart({ filename: title ? title : 'виджет' }, null)
  }

  render() {
    const { title = '', description = '', config, widgetId, dateBegin } = this.state

    const { loading, order = [] } = this.props

    if (loading) return <WidgetProgressBar title='Потребление в категории по уникальным покупателям' />

    if (!loading && order.length === 0 ) return <EmptyWidget title='Потребление в категории по уникальным покупателям' />

    return(
      <Widget>
        <WidgetHeader
          title={ title }
          description={ description }
          widgetId={ widgetId }
          dateBegin={ dateBegin }

          exportImage={ this.exportImage }
        />
        <ReactHighchart config={ config } ref={ (chart) => { this.chart = chart } }/>
      </Widget>
    )
  }
}

export default compose(
  sizeMe({ monitorHeight: true, monitorWidth: true }),
  withData
)(Grf06Logic)
