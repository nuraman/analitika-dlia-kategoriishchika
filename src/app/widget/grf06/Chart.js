import moment               from 'moment/moment'
import localization         from 'moment/locale/ru'
import Highcharts           from 'highcharts'


const MONTH_LIST = {
  1: 'январь',
  2: 'Февраль',
  3: 'март',
  4: 'апрель',
  5: 'май',
  6: 'июнь',
  7: 'июль',
  8: 'август',
  9: 'сентябрь',
  10: 'октбярь',
  11: 'ноябрь',
  12: 'декабрь'
}

const WEEK = { id: 1, value: 'неделя' }

class Chart {
  constructor() {
    this.data = {}
    this.order = []
    this.height = ''
    this.period = ''
    this.provider = false
  }

  set options({ data, order, height, periodId, provider }) {
    this.data = data
    this.order = order
    this.height = height
    this.period = periodId
    this.provider = provider
  }

  getConfig(onSelect) {
    if (!this.provider) return this.getCategoryChartConfig()
    if (this.provider) return this.getProviderChartConfig(onSelect)
  }

  getCategoryChartConfig = () => {
    const { series } = this.getSeries()
    const categoryList = this.getCategory()

    const heightChart = `${ this.height - 60 }px`

    const config = {
      chart: {
        type: 'column',
        height: heightChart,
      },
      title: {
        text: '',
      },
      xAxis: {
        categories: categoryList,
        labels: {
          style: {
            fontFamily: 'HelveticaRegular',
            fontSize: '12px',
          }
        }
      },
      navigation: {
        buttonOptions: {
          enabled: false,
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Распределение покупателей',
        },
        stackLabels: {
          enabled: false,
          style: {
            fontWeight: 'bold',
            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
          },
        },
        labels: {
          style: {
            fontFamily: 'HelveticaRegular',
            fontSize: '12px',
          }
        }
      },
      legend: {
        enabled: true,
        style: {
          fontFamily: 'HelveticaBold',
          fontSize: '14px',
        },
        itemStyle: {
          fontFamily: 'HelveticaBold',
          fontSize: '14px',
        }
      },
      tooltip: {
        headerFormat: '<b>{point.x}</b><br/>',
        pointFormat: '{series.name}: {point.y}<br/>Общее количество: {point.stackTotal}'
      },
      plotOptions: {
        column: {
          stacking: 'normal',
          dataLabels: {
            enabled: false,
            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
            style: {
              fontFamily: 'HelveticaRegular',
              fontSize: '12px',
            }
          },
        }
      },
      series: series
    }

    return config
  }

  getProviderChartConfig = (onSelect) => {
    const { series } = this.getSeries()

    const categoryList = this.getCategory()

    const heightChart = `${ this.height - 60 }px`

    const config = {
      chart: {
        type: 'column',
        height: heightChart,
      },
      title: {
        text: '',
      },
      xAxis: {
        categories: categoryList,
        labels: {
          style: {
            fontFamily: 'HelveticaRegular',
            fontSize: '12px',
          }
        }
      },
      navigation: {
        buttonOptions: {
          enabled: false,
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Распределение покупателей',
        },
        stackLabels: {
          enabled: false,
          style: {
            fontWeight: 'bold',
            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
          },
        },
        labels: {
          style: {
            fontFamily: 'HelveticaRegular',
            fontSize: '12px',
          }
        }
      },
      legend: {
        itemStyle: {
          fontFamily: 'HelveticaBold',
          fontSize: '14px',
        }
      },
      tooltip: {
        headerFormat: '<b>{point.x}</b><br/>',
        pointFormat: '{series.name}: {point.y}<br/>Общее количество: {point.stackTotal}'
      },
      plotOptions: {
        column: {
          stacking: 'normal',
          dataLabels: {
            enabled: false,
            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
            style: {
              fontFamily: 'HelveticaRegular',
              fontSize: '12px',
            }
          },
          events: {
            click: function() {
              const { userOptions } = this
              const { id } = userOptions

              onSelect && onSelect(id)
            }
          },
          cursor: 'pointer',
        }
      },
      series: series
    }

    return config
  }

  getDataSeries = () => {
    const valueSelfList = [],
      valueOtherList = [],
      valueAnyList = [],
      categoryList = [],
      color = {}

    this.order.forEach((id) => {
      const { date, valueSelf, valueOther, valueAny, colorSelf, colorAny, colorOther } = this.data[id] || {}

      color.self = colorSelf
      color.other = colorOther
      color.any = colorAny

      categoryList.push(date)
      valueSelfList.push(valueSelf)
      valueOtherList.push(valueOther)
      valueAnyList.push(valueAny)
    })

    const dataSelf = {
      color: `#${ color.self }`,
      data: valueSelfList,
      name: 'Мой бренд',
      id: 1,
    }

    const dataAny = {
      color: `#${ color.any }`,
      data: valueAnyList,
      name: 'Пересечения',
      id: 2,
    }

    const dataOther = {
      color: `#${ color.other }`,
      data: valueOtherList,
      name: 'Бренды конкурентов',
      id: 3,
    }

    return {
      categoryList,
      dataSelf,
      dataAny,
      dataOther,
    }
  }

  getSeries = () => {
    const {  categoryList, dataSelf, dataAny, dataOther, } = this.getDataSeries()

    const series = [
      dataSelf,
      dataAny,
      dataOther,
    ]

    return {
      series,
      categoryList,
    }
  }

  getCategory = () => {
    moment.locale('ru', localization)

    const { categoryList } = this.getSeries()

    const newCategoryList = categoryList.reduce((newCategoryList, date, index) => {

      if (this.period === WEEK.id) {
        newCategoryList.push(`${ moment(date).format('DD MMMM') }`)
      } else {
        const dateInString = moment(date).format('DD MM YYYY').split(' ')

        try {
          let mNumber = +dateInString[1]
          let year = dateInString[2]

          const month = `${ MONTH_LIST[mNumber] } ${ year }`
          newCategoryList.push(month)
        }
        catch(e) {
          newCategoryList.push('Без даты')
        }
      }

      return [...newCategoryList]
    }, [])

    if (this.period === WEEK.id) {
      const weekNumber = moment().format('W')
      let j = 0

      for(let i = newCategoryList.length - 1; i >= 0; i--) {
        if (weekNumber - j > 0) {
          newCategoryList[i] = newCategoryList[i] + ` (${ weekNumber - j } ${ WEEK.value })`
          j++
        } else {
          newCategoryList[i] = newCategoryList[i] + ` (${ WEEK.value })`
        }
      }
    }

    return newCategoryList
  }
}

export default Chart
