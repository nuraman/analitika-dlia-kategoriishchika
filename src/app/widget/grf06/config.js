import types                from 'engine/types'


const inputNameMap = {
  DATE_BEGIN:                         'dateBegin',
}

const queryNameMap = {
  GRF06:                              'grf06',
}

const resultNameMap = {
  DATE:                               'date',
  VALUE_SELF:                         'valueSelf',
  VALUE_ANY:                          'valueAny',
  VALUE_OTHER:                        'valueOther',

  COLOR_SELF:                         'colorSelf',
  COLOR_OTHER:                        'colorOther',
  COLOR_ANY:                          'colorAny',

  CAPTION:                            'caption',
  WIDGET:                             'vidget',
  DESCRIPTION:                        'description',
}

const queryMap = [
  {
    queryName: 'grf06',
    inputList: [
      {
        name: inputNameMap.DATE_BEGIN,
        type: types.String,
      },
    ],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        }
      ],
      result: [
        {
          name: resultNameMap.DATE,
          type: types.String,
        },
        {
          name: resultNameMap.VALUE_SELF,
          type: types.Float,
        },
        {
          name: resultNameMap.VALUE_ANY,
          type: types.Float,
        },
        {
          name: resultNameMap.VALUE_OTHER,
          type: types.Float,
        },
        {
          name: resultNameMap.COLOR_SELF,
          type: types.String,
        },
        {
          name: resultNameMap.COLOR_OTHER,
          type: types.String,
        },
        {
          name: resultNameMap.COLOR_ANY,
          type: types.String,
        }
      ],
      title: [
        {
          name: resultNameMap.CAPTION,
          type: types.String,
        },
        {
          name: resultNameMap.WIDGET,
          type: types.String,
        },
        {
          name: resultNameMap.DESCRIPTION,
          type: types.String,
        }
      ]
    }
  },
]

export default {
  queryMap,
  name: queryNameMap.GRF06
}
