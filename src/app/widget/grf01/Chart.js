import Highcharts           from 'highcharts'


class Chart {
  constructor() {
    this.data = {}
    this.order = []
  }

  set options(payload) {
    const { data, order } = payload
    this.data = data
    this.order = order
  }

  getConfig({ onClick, height }) {
    const series =  this.createSeries()

    return this.createConfig({ onClick, height, series })
  }

  createSeries = () => {
    const dataChart = this.order.reduce((dataChart, item) => {
      const doc = this.data[item] || {}
      const {value = 0, providerName, provider, color} = doc

      return [...dataChart, {
        name: providerName,
        y: value,
        provider: provider,
        color: `#${ color }`,
      }]
    }, [])

    const series = [{
      name: 'Бренд',
      data: dataChart
    }]

    return series
  }

  createConfig = ({ onClick, height, series }) => {
    const heightChart = `${ height - 60 }px`

    const config = {
      chart: {
        height: heightChart,
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie',
        style: {
          fontFamily: 'HelveticaRegular',
          fontSize: '14px',
          fontWeight: 'bold',
        }
      },
      title: {
        text: '',
      },
      tooltip: {
        headerFormat: '<span style="font-size: 10px">{series.name}</span><br/>',
        pointFormat: '<span style="color:{point.color}">{point.name}:</span><b>{point.y} %</b><br/>'
      },
      navigation: {
        buttonOptions: {
          enabled: false,
        }
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
            style: {
              color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
            }
          },
          events: {
            click: function (event) {
              onClick && onClick({ providerId: event.point.provider })
            }
          }
        }
      },
      legend: {
        itemStyle: {
          fontFamily: 'HelveticaBold',
          fontSize: '14px',
        }
      },

      series: series
    }

    return config
  }
}

export default Chart


