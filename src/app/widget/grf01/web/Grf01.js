import React, { Component } from 'react'

import ReactHighchart       from 'react-highcharts'

import { compose }          from 'redux'

import sizeMe               from 'react-sizeme'


import ProgressBar          from 'app/analysis/web/WidgetProgressBar'

import go                   from 'app/story/go'

import EmptyWidget          from 'app/widget/WidgetEmpty'

import grf01Data            from 'app/widget/grf01/hoc/grf01Data'

import Widget               from 'app/widget/styledComponent/widget/DivContainer'
import WidgetHeader         from 'app/widget/header/Container'
import Chart                from 'app/widget/grf01/Chart'

class Grf01Logic extends Component {
  constructor() {
    super()


    this.state = {
      title: '',
      description: '',
      order: [],
      config: {},
      chart: {},
      widgetId: '',
      dateBegin: ''
     }
  }

  onClick = ({ providerId }) => {
    const { currentProvider } = this.props

    if (currentProvider === providerId)
      go(`/customer`)
  }

  onHandleDescription = () => {
    const { openWidgetDescription } = this.props

    const { title } = this.state

    openWidgetDescription({ title })
  }

  componentWillMount() {
    const chart = new Chart()

    this.setState({ chart })
  }

  componentWillReceiveProps(nextProps) {
    const { data, order = [], title, size: { height }, dateBegin } = nextProps

    const { chart } = this.state

    if (order.length > 0) {
      chart.options = { data, order }

      const config = chart.getConfig({ onClick: this.onClick, height })

      const { caption, description, vidget } = title

      this.setState({
        title: caption,
        description,
        config,
        order,
        widgetId: vidget,
        dateBegin
      })
    }
  }

  exportImage = () => {
    const { chart } = this.chart
    const { title } = this.state

    chart.exportChart({ filename: title ? title : 'виджет' } , null)
  }


  render() {
    const { config, title, description, widgetId, dateBegin } = this.state
    const { loading, parameterId, order = [] } = this.props

    if (loading === true) {
      return (<ProgressBar
        title='Доля в категории'
      />)
    }

    if (!loading && !order.length) return <EmptyWidget title='Доля в категории' />

    return (
      <Widget>
        <WidgetHeader
          description={ description }
          dateBegin={ dateBegin }

          id={ parameterId }
          title={ title }
          widgetId={ widgetId }

          exportImage={ this.exportImage }
          onClickDescription={ this.onHandleDescription }
        />
        <ReactHighchart config={ config } ref={ (chart) => { this.chart = chart } } />
      </Widget>
    )
  }
}

export default compose(
  sizeMe({ monitorWidth: true, monitorHeight: true }),
  grf01Data,
)(Grf01Logic)
