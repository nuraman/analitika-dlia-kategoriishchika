import React                from 'react'

import { connect }          from 'react-redux'
import { compose }          from 'redux'


import withGrf01            from 'app/widget/graphql/withGrf01'
import mapStateToProps      from 'app/widget/mapStateToProps'

function withData(ComposedComponent) {
  function decorator(props) {
    return (<ComposedComponent { ...props } />)
  }
  return compose(
    connect(mapStateToProps),
    withGrf01(),
  )(decorator)
}

export default withData
