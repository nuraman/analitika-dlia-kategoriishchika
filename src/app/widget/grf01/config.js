import types                from 'engine/types'


const inputNameMap = {
  DATE_BEGIN:                         'dateBegin',
  DATE_END:                           'dateEnd',
  GOODS_CATEGORY:                     'goodsCategory',
  PERIOD:                             'period',
  POINT_FORMAT:                       'pointFormat',
  TYPE_PARAMETER:                     'typeParameter',
  PROVIDER_LIST:                      'providerList',
}

const queryNameMap = {
  GRF01:                              'grf01',
 }

const resultNameMap = {
  COLOR:                              'color',

  PROVIDER:                           'provider',
  PROVIDER_NAME:                      'providerName',
  VALUE:                              'value',

  CAPTION:                            'caption',
  WIDGET:                             'vidget',
  DESCRIPTION:                        'description',
 }

const queryMap = [
  {
    queryName: 'grf01',
    inputList: [
      {
        name: inputNameMap.DATE_BEGIN,
        type: types.String,
      },
    ],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        }
      ],
      result: [
        {
          name: resultNameMap.PROVIDER,
          type: types.Int,
        },
        {
          name: resultNameMap.PROVIDER_NAME,
          type: types.String,
        },
        {
          name: resultNameMap.VALUE,
          type: types.Float,
        },
        {
          name: resultNameMap.COLOR,
          type: types.String
        }
      ],
      title: [
        {
          name: resultNameMap.CAPTION,
          type: types.String,
        },
        {
          name: resultNameMap.WIDGET,
          type: types.String,
        },
        {
          name: resultNameMap.DESCRIPTION,
          type: types.String,
        }
      ]
    }
  },
]

export default {
  queryMap,
  name: queryNameMap.GRF01,
  inputNameMap,
}
