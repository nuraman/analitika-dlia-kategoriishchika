import React, { Component } from 'react'


import withData             from 'app/menu/hoc/withData'

import config               from 'app/menu/config'


const PARAMETER = {
  1: 'сумме покупок',
  2: 'количеству покупок',
  3: 'количеству товара',
  4: 'количеству чеков'
}

class LabelParameter extends Component {
  constructor() {
    super()

    this.state = {
      value: ''
    }
  }

  componentWillReceiveProps(nextProps) {
    const { orderParameter = [], id } = nextProps

    if (orderParameter.length > 0 && id) {
      //const { value = '' } = dataParameter[id] || {}

      const value = PARAMETER[id]

      this.setState({ value: value.toLowerCase() })
    }
  }

  render() {
    const { value } = this.state

    if (!value) return null

    return (
      <label>по { value }</label>
    )
  }
}

export default withData(LabelParameter, { queryName: config.queryNameMap.PARAMETER_LIST })
