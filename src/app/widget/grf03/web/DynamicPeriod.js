import React                from 'react'

import styled               from 'styled-components'

const DivContainer = styled.div`
  display: table;
  width: 100%;
  height: 100%;
`

const DivContent = styled.div`
  display: table-cell;
  vertical-align: middle;
`

const DivTitleRow = styled.div`
  display: block;
  text-align: center;
`

const DivTitle = styled.div`
  font-family: HelveticaLight;
  font-size: 14px;
  color: #939393;
`

const DivValueRow = styled.div`
  margin-top: 10px;
  display: block;
  text-align: center; 
`

const DivValue = styled.div`
  font-family: HelveticaLight;
  font-size: 35px;
  color: #000000;
`

const DivImageRow = styled.div`
  display: block;
  text-align: center;
`

const DivImage = styled.div`
  display: inline-block;
  width: 118px;
  height: 82px;
  z-index: 131;
  margin-top: 22px;
`

function DynamicPeriod(props) {
  const { name, value = 0 } = props

  return (
    <DivContainer>
      <DivContent>
        <DivTitleRow>
          <DivTitle>{ name }</DivTitle>
        </DivTitleRow>
        <DivValueRow>
          <DivValue>{ `${ value }%` }</DivValue>
        </DivValueRow>
        <DivImageRow>
          <DivImage>
            { value >= 0 ? <img src='assets/dynamicTriangleUp.png' alt='' /> : <img src='assets/dynamicTriangleDown.png' alt='' /> }
          </DivImage>
        </DivImageRow>
      </DivContent>
    </DivContainer>
  )
}

export default DynamicPeriod
