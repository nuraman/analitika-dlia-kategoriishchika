import React, { Component } from 'react'

import PropTypes            from 'prop-types'

import styled               from 'styled-components'


import Widget               from 'app/widget/styledComponent/widget/DivContainer'
import WidgetHeader         from 'app/widget/header/Container'
import WidgetContent        from 'app/widget/styledComponent/widget/DivContent'

import ProgressBar          from 'app/analysis/web/WidgetProgressBar'

import DynamicPeriod        from './DynamicPeriod'

import EmptyWidget          from 'app/widget/WidgetEmpty'

import withData             from 'app/widget/grf03/hoc/withData'


const FirstTriangle = styled.div`
  display: inline-block;
  width: 50%;
  height: 100%;
`

const SecondTriangle = styled.div`
  display: inline-block;
  width: 50%;
  height: 100%;
`

const PeriodValue = {
  1:  'К прошлой неделе',
  2:  'К прошлому месяцу',
  3:  'К прошлому кварталу',
  4:  'К прошлому полугоду',
  5:  'К прошлому году',
}

class Grf03 extends Component {
  static propTypes = {
    resultData: PropTypes.array,
    description: PropTypes.string,
    caption: PropTypes.string,
  }

  constructor() {
    super()

    this.state = {
      error: '',
      periodId: '',
      resultData: [],
      title: '',
      description: '',
      dateBegin: '',
      widgetId: ''
    }
  }

  componentWillReceiveProps(nextProps) {
    const {
      dateBegin,
      description,
      periodId,
      resultData = [],
      title,
      widgetId,
    } = nextProps

    if (resultData.length > 0) {
      this.setState({
        error: '',
        periodId,
        resultData,
        title,
        description,
        dateBegin,
        widgetId
      })
    }
  }

  render() {
    const { resultData = [], periodId, title, description, widgetId, dateBegin } = this.state

    const { parameterId, loading } = this.props

    if (loading) return (
      <ProgressBar
        title='Показатели категории'
      />
    )

    if(!loading && resultData.length === 0) return <EmptyWidget title={ title } />

    return (
      <Widget>
        <WidgetHeader
          dateBegin={ dateBegin }
          description={ description }
          title={ title }
          id={ parameterId }
          widgetId={ widgetId }
        />
        <WidgetContent>
          <FirstTriangle>
            <DynamicPeriod
              name='К прошлому году'
              value={ resultData[1] }
            />
          </FirstTriangle>
          <SecondTriangle>
            <DynamicPeriod
              name={ PeriodValue[periodId] }
              value={ resultData[0] }
            />
          </SecondTriangle>
         </WidgetContent>
      </Widget>
    )
  }
}

export default withData(Grf03)
