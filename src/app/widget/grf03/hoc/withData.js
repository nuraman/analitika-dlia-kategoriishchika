import React, { Component } from 'react'
import { connect }          from 'react-redux'
import { compose }          from 'redux'


import withGrf03            from 'app/widget/graphql/withGrf03'
import mapStateToProps      from 'app/widget/mapStateToProps'


function withData(ComposedComponent) {
  class Widget extends Component {
    render() {
      return (
        <ComposedComponent {...this.props } />
      )
    }
  }

  return compose(
    connect(mapStateToProps),
    withGrf03(),
  )(Widget)
}

export default withData
