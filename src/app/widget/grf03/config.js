import types                from 'engine/types'

const inputNameMap = {
  DATE_BEGIN:               'dateBegin',
  DATE_END:                 'dateEnd',
  GOODS_CATEGORY:           'goodsCategory',
  PERIOD:                   'period',
  POINT_GROUP:              'pointGroup',
  POINT_FORMAT:             'pointFormat',
  POINT:                    'point',
  TYPE_PARAMETER:           'typeParameter',
  PROVIDER_LIST:            'providerList'
}

const queryNameMap = {
  GRF03:                     'grf03',
}

const resultNameMap = {
  VALUE_PREV_PERIOD:         'valuePrevPeriod',
  VALUE_PREV_YEAR:           'valuePrevYear',

  CAPTION:                   'caption',
  VIDGET:                    'vidget',
  DESCRIPTION:               'description',
}

const queryMap = [
  {
    queryName: 'grf03',
    inputList: [
      {
        name: inputNameMap.DATE_BEGIN,
        type: types.String,
      },
    ],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        }
      ],
      result: [
        {
          name: resultNameMap.VALUE_PREV_PERIOD,
          type: types.Float,
        },
        {
          name: resultNameMap.VALUE_PREV_YEAR,
          type: types.Float,
        },
      ],
      title: [
        {
          name: resultNameMap.CAPTION,
          type: types.String,
        },
        {
          name: resultNameMap.VIDGET,
          type: types.String,
        },
        {
          name: resultNameMap.DESCRIPTION,
          type: types.String,
        }
      ]
    }
  },
]

export default {
  queryMap,
  name: queryNameMap.GRF03,
  inputNameMap
}
