import Divider              from 'material-ui/Divider'

import styled               from 'styled-components'

import React                from 'react'


import HelveticaRegular     from 'app/theme/font/HelveticaRegular.ttf'


const DivContainer = styled.div`
  width: 100%;
  height: 100%;
  '@font-face': {
    font-family: HelveticaRegular;
    src: url(${ HelveticaRegular });
  }
`

const DivHeader = styled.div`
  height: 60px;
  width: 100%;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`

const DivTitle = styled.div`
   padding-left: 20px;
   font-family: HelveticaRegular;
   font-size: 22px;
`

const DivContent = styled.div`
    width: 100%;
    height: calc(100% - 60px);
    display: flex;
    align-items: center;
    justify-content: space-around;
    font-family: HelveticaRegular;
    font-size: 22px;
`


function EmptyWidget(props) {
  const { title } = props

  return (
    <DivContainer>
      <DivHeader>
        <DivTitle>
          { title }
        </DivTitle>
      </DivHeader>
      <Divider />
      <DivContent>
        Нет данных
      </DivContent>
    </DivContainer>
  )
}

export default EmptyWidget

