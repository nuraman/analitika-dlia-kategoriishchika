import _                    from 'lodash'

import React, { Component } from 'react'

import moment               from 'moment'


import DiscountChartView    from './DiscountChartView'

import withData             from 'app/widget/grf04/hoc/withData'


class DiscountChartLogic extends Component {
  constructor() {
    super()

    this.state = {
      dataDiscountOwn: {},
      orderDiscountOwn: {},
      currDataDiscountOwn: {},
      currOrderDiscountOwn: {},

      dataDiscountOther: {},
      orderDiscountOther: {},

      dateXAxis: [],

      selectedDiscount: {},
    }
  }
  //Обновление списка акций, попадающих в выбранный интервал времени
  onUpdateDiscountInSelectedRange = ({ selectedRange, data, order }) => {
    const selectedDateBegin = moment(selectedRange[0], 'YYYY-MM-DD').toDate().getTime()
    const selectedDateEnd = moment(selectedRange[1], 'YYYY-MM-DD').toDate().getTime()

    const newData = order.reduce((newData, item) => {
      const {id, dateBegin, dateEnd, name} = data[item] || {}

      if (dateBegin >= selectedDateBegin && dateEnd <= selectedDateEnd) {
        return {...newData, [id]: { id, dateBegin, dateEnd: dateEnd, name }}
      }

      if ((dateBegin >= selectedDateBegin && dateBegin <= selectedDateEnd) && (dateEnd > selectedDateEnd)) {
        return {...newData, [id]: { id, dateBegin, dateEnd: selectedDateEnd, name }}
      }

      if ((dateEnd >= selectedDateBegin && dateEnd <= selectedDateEnd) && (dateBegin < selectedDateBegin)) {
        return {...newData, [id]: { id, dateBegin: selectedDateBegin, dateEnd, name }}
      }

      return { ...newData }
    }, {})

    const newOrder = Object.keys(newData)

    return {
      newData,
      newOrder
    }
  }
  //Callback функция выбора акции
  onSelectDiscount = ({ id, x, y }) => {
    const { dataDiscountOwn } = this.state

    const selectedDiscountOwn = _.find(dataDiscountOwn, { id: parseInt(id, 10) })

    const { onOpenDiscountDescription } = this.props

    if (selectedDiscountOwn) {
      onOpenDiscountDescription && onOpenDiscountDescription({
        discount: selectedDiscountOwn,
        openDiscount: true,
        x,
        y,
      })

    }
  }

  componentWillReceiveProps(nextProps = {}) {
    const { resultDiscount, resultDiscountOther ,selectedRange = [], dateXAxis = [] }  = nextProps

    if (selectedRange.length === 0) {
      this.setState({
        dataDiscountOwn: resultDiscount.data,
        orderDiscountOwn: resultDiscount.order,
        currDataDiscountOwn: resultDiscount.data,
        currOrderDiscountOwn: resultDiscount.order,

        dataDiscountOther: resultDiscountOther.data,
        orderDiscountOther: resultDiscountOther.order,

        dateXAxis,
      })
    }

    if (selectedRange.length > 0) {
      const { newData = {}, newOrder = [] } = this.onUpdateDiscountInSelectedRange({ selectedRange, data: resultDiscount.data, order: resultDiscount.order })

      const newDateXAxis = []

      const currentDateBeginWithTime = moment(selectedRange[0], 'YYYY-MM-DD').toDate().getTime()
      const currentDateEndWithTime = moment(selectedRange[1], 'YYYY-MM-DD').toDate().getTime()

      newDateXAxis.push(currentDateBeginWithTime)
      newDateXAxis.push(currentDateEndWithTime)

      this.setState({
        dataDiscountOwn: resultDiscount.data,
        orderDiscountOwn: resultDiscount.order,
        currDataDiscountOwn: newData,
        currOrderDiscountOwn: newOrder,

        dataDiscountOther: resultDiscountOther.data,
        orderDiscountOther: resultDiscountOther.order,

        dateXAxis: newDateXAxis,
      })
    }
  }

  render() {
    const {
      currDataDiscountOwn = {},
      currOrderDiscountOwn = [],
      dateXAxis = [],
      dataDiscountOther = {},
      orderDiscountOther = [],
    } = this.state

    return(
      <DiscountChartView
        dataDiscountOwn={ currDataDiscountOwn }
        orderDiscountOwn={ currOrderDiscountOwn }

        dataDiscountOther={ dataDiscountOther }
        orderDiscountOther={ orderDiscountOther }

        dateXAxis={ dateXAxis }

        onSelectDiscount={ this.onSelectDiscount }
      />
    )
  }
}


export default withData(DiscountChartLogic)
