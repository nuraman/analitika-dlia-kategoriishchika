import _                    from 'lodash'

import React, { Component } from 'react'

import { connect }          from 'react-redux'
import { compose }          from 'redux'


import Widget               from 'app/widget/styledComponent/widget/DivContainerGrf04'
import WidgetBody           from 'app/widget/styledComponent/widget/DivContent'

import ProgressBar          from 'app/analysis/web/WidgetProgressBar'

import SaleResultChartLogic from './ResultSaleChartLogic'
import DiscountChartLogic   from './DiscountChartLogic'
import DiscountOwn          from './DiscountOwn'
import DiscountCompetition  from './DiscountCompetition/DiscountCompetition'

import WidgetHeader         from 'app/widget/header/Container'

import EmptyWidget          from 'app/widget/WidgetEmpty'

import withData             from 'app/widget/grf04/hoc/withData'

import reducer              from 'app/widget/grf04/reducer'


class Container extends Component {
  constructor() {
    super()

    this.state = {
      refSaleChart: {},

      description: '',
      discountDescription: {},
      discountGoodList: [],
      discountXCoordinate: '',
      discountYCoordinate: '',

      isUpdate: false,

      openDiscount: false,

      periodId: '',

      selectedRange: [],

      title: '',
      widgetId: '',
    }
  }

  componentWillMount() {
    const {
      dateXAxis,
      dateBegin,
      dateBeginRemember,
      dateEndRemember,
      isUpdate,
      title = {},
    }  = this.props

    const { caption, description, vidget } = title

    this.setState({
      description,
      dateXAxis,
      dateBegin,
      isUpdate,
      selectedRange: dateBeginRemember && dateEndRemember ? [dateBeginRemember, dateEndRemember] : [],
      title: caption,
      widgetId: vidget,
    })
  }

  componentWillReceiveProps(nextProps) {
    const {
      dateBegin,
      dateBeginOld,
      dateBeginRemember,
      dateEndRemember,
      periodIdRemember,
      isUpdate,
      title = {},
      periodId
    }  = nextProps

    const { caption, description, vidget } = title

    const isSetRange = this.isSetSelectedRange({
      dateBeginRemember,
      dateEndRemember,
      dateBeginOld,
      dateBegin,
      periodId,
      periodIdRemember
    })

    this.setState({
      description,
      dateBegin,
      isUpdate,
      selectedRange: isSetRange ? [dateBeginRemember, dateEndRemember] : [],
      title: caption,
      widgetId: vidget,
    })
  }
  /*
    проверяем надо ли установить выбранный период при открытии меню
    провермяем, чтобы текущая дата совпала с сохраненным в redux
   */
  isSetSelectedRange = ({ dateBeginRemember, dateEndRemember, dateBeginOld, dateBegin, periodId, periodIdRemember }) => {
    if (periodIdRemember === periodId && dateBeginOld === dateBegin && dateBeginRemember)
      return true

    return false
  }

  onHandleOpenDiscountDescription = ({ discount, openDiscount, x, y }) => {
    const { resultGood } = this.props

    const { data, order } = resultGood

    const discountGoodList = order.reduce((discountGoodList, item) => {
      const { discountId, value } = data[item]

      if (discountId === discount.id ) {
        discountGoodList.push(value)
      }

      return [ ...discountGoodList ]
    }, [])

    this.setState({
      discountDescription: discount,
      discountGoodList,
      openDiscount,
      discountXCoordinate: x,
      discountYCoordinate: y,
    })
  }

  onHandleCloseDiscountDescription = () => this.setState({ openDiscount: !this.state.openDiscount })
  /*
  При изменении периода выбора
  перерисовываем график акций в заданном периоде
   */
  onSelectRange = ({ currentDateBegin, currentDateEnd }) => {
    const selectedRange = [currentDateBegin, currentDateEnd]

    this.setState({
      selectedRange,
      isUpdate: false,
    })

    const { dispatch, periodId, dateBegin, } = this.props
    dispatch({
      type: reducer.types.SET_DATE_RANGE,
      payload: { dateBegin: currentDateBegin, dateEnd: currentDateEnd, periodId, dateBeginOld: dateBegin }
    })
  }

  //получаем ссылку на компоненту график
  getRefChart = (chart) => {
    const { refSaleChart } = this.state

    if (!Object.keys(refSaleChart).length) {
      this.setState({ refSaleChart: chart })
    }
  }

  exportImage = () => {
    const { refSaleChart, title } = this.state

    try {
      refSaleChart.exportChart({ filename: title ? title : 'виджет' } , null)
    } catch (e) {
      console.log(e.message)
    }
  }

  render() {
    const {
      discountDescription,
      discountGoodList,
      discountXCoordinate,
      discountYCoordinate,
      description,
      dateBegin,
      isUpdate,
      selectedRange,
      openDiscount,
      title,
      widgetId,
    } = this.state

    const { loading, resultSale } = this.props

    if (loading) return (
      <ProgressBar
        title='Влияние маркетинговых активностей на обьем потребления'
      />
    )

    if (_.isEmpty(resultSale) && !loading) return (<EmptyWidget title={ title } />)

    return (
      <Widget>
        <WidgetHeader
          description={ description }
          dateBegin={ dateBegin }
          title={ title }
          widgetId={ widgetId }
          exportImage={ this.exportImage }
        />
        <WidgetBody>
          <SaleResultChartLogic
            onSelectRange={ this.onSelectRange }
            selectedRange={ selectedRange }
            update={ isUpdate }

            getRefChart={ this.getRefChart }
          />
          <DiscountChartLogic
            selectedRange={ selectedRange }

            onOpenDiscountDescription={ this.onHandleOpenDiscountDescription }
          />
        </WidgetBody>
        <DiscountOwn
          discount={ discountDescription }
          discountGoodList={ discountGoodList }
          open={ openDiscount }
          x={ discountXCoordinate }
          y={ discountYCoordinate }

          onClose={ this.onHandleCloseDiscountDescription }
        />
        <DiscountCompetition />
      </Widget>
    )
  }
}

export default compose(
  connect(),
  withData
)(Container)
