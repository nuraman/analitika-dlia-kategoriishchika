import moment               from 'moment'

import PropTypes            from 'prop-types'

import React, { Component } from 'react'

import { compose }          from 'redux'
import { connect }          from 'react-redux'

import styled               from 'styled-components'


import
  withLoadDiscountComment   from 'app/widget/graphql/withLoadDiscountComment'

import config               from 'app/widget/grf04/config'


const DivContainer = styled.div`
  position: absolute;
  border: 1px solid #dddddd;
  background-color: #fff;
  visibility: ${props=>props.visibility};
  left: ${ props=>props.x }px;
  top: 20px;
  width: 361px;
 `

const DivHeaderTitle = styled.div`
  font-family: HelveticaRegular;
  font-size: 22px;
  margin-top: 29px;
  margin-left: 29px;
  text-transform: capitalize;
`

const DivInfoDesc = styled.div`
  color: #949494;
  font-family: HelveticaLight;
  font-size: 14px;
  margin-left: 29px;
  margin-top: 10px;  
  padding: 0;
`


const DivListItem = styled.div`
  display: flex;
  flex-direction: row;
  margin-left: 29px;
  align-items: center;
  height: 30px;
`

const DivListProductItem = styled.div`
  display: flex;
  flex-direction: row;
  margin-left: 29px;
  align-items: center;
`

const DivItemProductText = styled.div`
  font-family: HelveticaLight;
  width: calc(100% - 20px);
  font-size: 14px;
  margin-left: 10px;
  margin-right: 10px;
  overflow:hidden !important;
  text-overflow: ellipsis;
  padding-top: 1px;
`

const DivOtherProductWrap = styled.div`
  display: ${ props=>props.show ? 'flex' : 'none' };
  flex-direction: column;
  width: 100%;
  overflow: auto;
`

const DivItemOtherProductText = styled.div`
  font-family: HelveticaLight;
  width: calc(100% - 10px);
  font-size: 14px;
  margin-right: 10px;
  overflow:hidden !important;
  text-overflow: ellipsis;
  padding-top: 1px;
`

const DivItemText = styled.div`
  display: flex;
  align-items: center;
  font-family: HelveticaLight;
  width: calc(100% - 39px);
  font-size: 14px;
  margin-left: 10px;
  margin-right: 29px;
  white-space: nowrap;
  overflow:hidden !important;
  text-overflow: ellipsis;
`

const DivListItemFooter = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin-left: 29px;
  margin-right: 29px;
  padding-top: 5px;
`

const DivItemFooterText = styled.div`
  font-family: HelveticaLight;
  font-size: 14px;
`

const DivValue = styled.div`
  font-family: HelveticaLight;
  font-size: 14px;
  font-weight: bold;
`

const DivComment = styled.div`
  position: relative;
  margin-left: 29px;
  margin-right: 29px;
  margin-top: 0.625em;  
  border: 1px solid #dddddd;
  background-color: rgb(255, 242, 202);
  box-shadow: inset 0px 2px 2px 0px rgba(0, 0, 0, 0.14);
  height: 56px;
  z-index: 116;
  font-family: HelveticaItalic;
  font-size: 14px;

  &:before, &:after {
    position: absolute;
    content: "";
    width: 0;
    height: 0;
  }

  &:before {
    top: 5px;
    right: 100%;
    border-top: 0px solid transparent;
    border-right: 14px solid #dddddd;
    border-bottom: 16px solid transparent;
  }

  &:after {
    top: 6px;
    right: calc(100% - 2px);
    border-top: 0px solid transparent;
    border-right: 14px solid #fff2ca;
    border-bottom: 16px solid transparent;
  }
`

const DivFooter = styled.div`
  background-color: #f3fcfc;
  margin-top: 30px;
  padding-bottom: 15px;
`

const DivTextDescription = styled.div`
  color: #bdbdbd;
  font-family: HelveticaLight;
  font-size: 14px;
`

const PgaBlockInput = styled.textarea`
  box-sizing: border-box;
  width: 100%;
  height: 100%;
  background-color: transparent;
  outline: none;
  font-family: HelveticaItalic;
  resize: none;
  border: 0; 
  padding: 8px;
`

const DivPgaBlock = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`

const LinkClose = styled.a`
  cursor: pointer;
  height: 20px;
  width: 20px;
  margin-right: 19px;
`


const DivTable = styled.div`
  padding-top: 1px;
  display: table;
  padding-left: 10px;
`

const DivTableCell = styled.div`
  display: table-cell;
  vertical-align: middle;
  font-family: HelveticaLight;
  font-size: 14px;
`

const DivOtherProduct = styled.div`
  color: #48b9bf;
  border-color: #48b9bf;
  text-decoration: none;
  border-bottom: 1px dotted #fff;
  
  cursor: pointer;
`

const DivIco = styled.div`
  min-width: 20px;
  width: 20px;
  height: 17px;
  fill: #9e9e9e;
  
  background-image: url(${ props => props.path });
  background-size: cover;
`


class DiscountDescription extends Component {
  static propTypes = {
    discount: PropTypes.object,
    discountGoodList: PropTypes.array,
    open: PropTypes.bool,
  }

  constructor() {
    super()

    this.state = {
      id: '',
      commentValue: '',
      comment: '',
      dateBegin: '',
      dateEnd: '',
      description: '',
      name: '',
      countDiscount: '',
      countGoods: '',
      countExpectedCustomers: '',
      countRealCustomers: '',
      sumSoldGoods: '',
      countSoldGoods: '',

      discountGoodListStr: '',
      discountGoodList: [],

      x: '',
      y: '',

      open: false,
      openOtherProduct: false,
    }
  }

  onHandleChange = ({ target: { value } }) => this.setState({ commentValue: value })

  onHandleClick = () => {
    const { onClose } = this.props
    onClose()

    const { inputMutationNameMap } = config

    const { id, commentValue = '' } = this.state

    if (commentValue.length > 0) {
      this.props.actionUpdate({
        [inputMutationNameMap.DISCOUNT_ID]: id,
        [inputMutationNameMap.COMMENT]: commentValue,
      })
    }

    this.setState({ open: !this.state.open })
  }
  onHandleClickOtherProduct = () => this.setState({ openOtherProduct: !this.state.openOtherProduct })

  componentWillReceiveProps(nextProps) {
    const { discount = {}, discountGoodList = [], x, y, open } = nextProps

    if (discount) {
      const {
        id,
        comment,
        dateBegin,
        dateEnd,
        description,
        name,
        countDiscount,
        countGoods,
        countExpectedCustomers,
        countRealCustomers,
        sumSoldGoods,
        countSoldGoods,
      } = discount

      const start = moment(dateBegin, 'x').format('DD MMM YYYY')
      const end = moment(dateEnd, 'x').format('DD MMM YYYY')

      const discountGoodListStr = discountGoodList.reduce((discountGoodListStr, item, index) => {
        if (index === discountGoodList.length - 1) return discountGoodListStr = discountGoodListStr + `${ item }`

        return discountGoodListStr = discountGoodListStr + `${ item }, `
      },``)

      this.setState({
        id,
        commentValue: comment === null ? '' : comment,
        dateBegin: start,
        dateEnd: end,
        description,
        name,
        countDiscount,
        countGoods,
        countExpectedCustomers,
        countRealCustomers,
        sumSoldGoods,
        countSoldGoods,

        discountGoodListStr,
        discountGoodList,

        x,
        y,
        open,
      })
    }
  }

  render() {
    const {
      commentValue,
      dateBegin,
      dateEnd,
      description,
      discountGoodList,
      name,
      countExpectedCustomers,
      countRealCustomers,
      sumSoldGoods,
      countSoldGoods,

      x,
      y,

      open,
      openOtherProduct,
    } = this.state

    const periodStr = `${ dateBegin } - ${ dateEnd }`
    const expectedCustomersStr = `${ countExpectedCustomers } участников`

    return(
      <DivContainer
        x={ x ? x : 0 }
        y={ y }
        visibility={ open ? 'visible' : 'hidden' }
      >
        <DivHeaderTitle>
          <DivPgaBlock>
            { name }
            <LinkClose
              onClick={ this.onHandleClick }
            >
              <DivIco path='assets/discount/close.svg' />
            </LinkClose>
          </DivPgaBlock>
        </DivHeaderTitle>
        <DivInfoDesc>
          Наша собственная акция
        </DivInfoDesc>
        <DivListItem>
          <DivIco path='assets/discount/calendar.svg' />
          <DivTable>
            <DivTableCell>
            { periodStr }
            </DivTableCell>
          </DivTable>
        </DivListItem>
        <DivListProductItem>
          <DivIco path='assets/discount/product.svg' />
          <DivItemProductText>
            { discountGoodList[0] }
            <DivOtherProduct
              onClick={ this.onHandleClickOtherProduct }
            >
              еще { discountGoodList.length - 1 }  продуктов
            </DivOtherProduct>
            <DivOtherProductWrap show={ openOtherProduct }>
              {
                discountGoodList.map((item, index) => {
                  if (index !== 0)
                    return (<DivItemOtherProductText key={ index }>{ item }</DivItemOtherProductText>)

                  return null
                })
              }
            </DivOtherProductWrap>
          </DivItemProductText>
        </DivListProductItem>
        <DivListItem>
          <DivIco path='assets/discount/person.svg' />
          <DivItemText>
            { expectedCustomersStr }
          </DivItemText>
        </DivListItem>
        <DivListItem>
          <DivTextDescription>
          { description }
          </DivTextDescription>
        </DivListItem>
        <DivComment>
          <PgaBlockInput
            value={ commentValue }
            onChange={ this.onHandleChange }
          />
        </DivComment>
        <DivFooter>
          <DivListItemFooter>
            <DivItemFooterText>
              Количество уникальных покупателей
            </DivItemFooterText>
            <DivValue>
              { countRealCustomers === null ? 0 : countRealCustomers.toLocaleString() }
            </DivValue>
          </DivListItemFooter>
          <DivListItemFooter>
            <DivItemFooterText>
              Объем продаж акционного товара
            </DivItemFooterText>
            <DivValue>
              { sumSoldGoods === null ? 0 : sumSoldGoods.toLocaleString() }
            </DivValue>
          </DivListItemFooter>
          <DivListItemFooter>
            <DivItemFooterText>
              Прирост продаж на период акции
            </DivItemFooterText>
            <DivValue>
              { countSoldGoods === null ? 0 : countSoldGoods.toLocaleString() }
            </DivValue>
          </DivListItemFooter>
        </DivFooter>
      </DivContainer>
    )
  }
}

export default compose(
  connect(),
  withLoadDiscountComment(),
)(DiscountDescription)


