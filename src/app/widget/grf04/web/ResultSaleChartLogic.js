import moment               from 'moment/moment'


import PropTypes            from 'prop-types'

import React, { Component } from 'react'

import { connect }          from 'react-redux'
import { compose }          from 'redux'

import ReactHighstock       from 'react-highcharts/ReactHighstock'
import HighchartsExporting  from 'highcharts-exporting'

import styled               from 'styled-components'

import sizeMe               from 'react-sizeme'


import withData             from 'app/widget/grf04/hoc/withData'

import Chart                from 'app/widget/grf04/hoc/Chart'


HighchartsExporting(ReactHighstock.Highcharts)


const Div = styled.div`
  width: 100%;
  height: 60%;
  font-family: HelveticaRegular;
`

class ResultSaleChartLogic extends Component {
  static propTypes = {
    resultSale: PropTypes.object,
    update: PropTypes.bool,
  }

  constructor() {
    super()

    this.state = {
      chart: {},

      dateBeginRemember: '',
      dateEndRemember: '',

      series: [],
    }
  }

  componentWillMount() {
    const { resultSale, size: { height }, periodId } = this.props

    const chart = new Chart()
    const config = this.getConfig({ resultSale, height, periodId, chart })

    this.setState({
      config,
      chart,
    })
  }

  componentDidMount() {
    const { selectedRange = [], getRefChart } = this.props

    getRefChart(this.chart.getChart())

    if (selectedRange.length > 0) {
      setTimeout(() => {
        ReactHighstock.Highcharts.charts.forEach((chart, index) => {
          if (chart !== undefined) {
            chart.xAxis[0].setExtremes(moment(selectedRange[0], 'YYYY-MM-DD').toDate().getTime(), moment(selectedRange[1], 'YYYY-MM-DD').toDate().getTime())
          }
        })
      }, 1)
    }
  }

  componentWillReceiveProps(nextProps) {
    const { resultSale, size: { height }, periodId } = nextProps
    const { chart } = this.state

    const config = this.getConfig({ resultSale, height, periodId, chart })
    this.setState({ config })
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { update } = nextProps

    if (update) return true

    return false
  }
  //надо переделать, сейчас переделать, сейчас беруются при событии перерисовка
  onSelectRange = ({ currentDateBegin, currentDateEnd }) => {
    const { onSelectRange } = this.props

    onSelectRange && onSelectRange({ currentDateBegin, currentDateEnd })
  }

  getConfig = (payload) => {
    const { resultSale, height, periodId, chart } = payload

    const { data, order = [] } = resultSale
    chart.options = { data, order, height, periodId  }
    const config = chart.getConfig({ onSelectRange: this.onSelectRange })

    return config
  }

  render() {
    const { config } = this.state

    return (
      <Div>
        <ReactHighstock config={ config } ref={(chart) => { this.chart = chart } } />
      </Div>
    )
  }
}

export default compose(
  sizeMe({ monitorHeight: true, monitorWidth: true }),
  withData,
)(ResultSaleChartLogic)
