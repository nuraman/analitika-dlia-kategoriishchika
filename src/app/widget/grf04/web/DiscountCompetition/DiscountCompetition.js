import React, { Component } from 'react'


import styled               from 'styled-components'


const DivContainer = styled.div`
  position: absolute;
  display: ${ props=>props.open ? 'block' : 'none' };
  top: 0;
  left: 10px;
  width: 366px;
  height: 378px;
  background-color: #fff;
  border: 1px solid #dddddd;
 `

const DivHeader = styled.div`
  display: table;
  width: 100%;
  height: 40px;
  background-color: #484848;
`

const DivTitle = styled.div`
  display: table-cell;
  vertical-align: middle;
  padding-left: 29px;
  font-size: 14px;
  font-family: HelveticaLight;
  color: #fff;
`

const DivClose = styled.div`
  display: table-cell;
  text-align: right;
`

const DivPopupCastingClose = styled.div`
  display: inline-block;
  box-sizing: border-box;
  height: 100%;
  padding: 10px;
  cursor: pointer;
  
  &:hover:before, :hover:after {
    opacity: 0.6;
  }
  &:before, &:after {
    content: "";
    position: absolute;
    top: 5%;
    right: 1%;
    width: 20px;
    height: 1px;
    opacity: 0.9;
    background-color: #fff;
    cursor: pointer;
  }
  &:before {
    transform: translateY(-50%) rotate(45deg);
  }
  &:after {
    transform: translateY(-50%) rotate(-45deg);
  }
`

const DivBody = styled.div`
  display: block;
  height: 340px;
  overflow: auto;
  &::-webkit-scrollbar
  {
    width: 9px;
  }

  &::-webkit-scrollbar-track
  {
    background: #73c7c5;
    background-clip: content-box;
  }

  &::-webkit-scrollbar-thumb
  {
    background: #484848;
    height: 35px;
    border-radius: 5px solid #484848;
    border: 1px solid #484848;
  }
`

const DivBlock = styled.div`
  display: flex;
  flex-direction: column;
  width: calc(100% - 29px);
  border-bottom: 1px solid #bdbdbd;
  padding-left: 29px;
  padding-top: 10px;
  padding-bottom: 10px;
`

const DivDiscountBlockValue = styled.div`
  display: table;
  padding-top: 10px;
  padding-right: 29px;
`

const DivDiscountName = styled.div`
  display: table-cell;
  vertical-align: middle;
  font-family: HelveticaLight;
  font-size: 18px;
`

const DivDiscountBrand = styled.div`
  display: table-cell;
  vertical-align: middle;
  font-family: HelveticaLight;
  font-size: 14px;
  color: #bdbdbd;
`

const DivBlockPeriod = styled.div`
  display: flex;
  padding-top: 10px;
  flex-direction: row;
  align-items: center; 
`

const DivBlockPeriodText = styled.div`
  font-family: HelveticaLight;
  font-size: 14px;
  padding-left: 10px;
`

const DivIco = styled.div`
  min-width: 20px;
  width: 20px;
  height: 18px;
  fill: #9e9e9e;
  
  background-image: url(${ props => props.path });
  background-size: cover;
`

class DiscountCompetition extends Component {
  constructor() {
    super()

    this.state = {
      discountList: [],
      open: false,
    }
  }

  componentWillMount() {
    const discountList = []

    discountList.push({
      name: 'VENUS: Почувствуй себя богиней, длинное название',
      brand: 'Venus incorporated',
      dateBegin: '13 июля 2017г',
      dateEnd: '17 июля 2017г'
    })

    discountList.push({
      name: 'VENUS: Почувствуй себя богиней, длинное название',
      brand: 'Venus incorporated',
      dateBegin: '13 июля 2017г',
      dateEnd: '17 июля 2017г'
    })

    discountList.push({
      name: 'VENUS: Почувствуй себя богиней, длинное название',
      brand: 'Venus incorporated',
      dateBegin: '13 июля 2017г',
      dateEnd: '17 июля 2017г'
    })

    this.setState({ discountList })
  }

  render() {
    const { open, discountList } = this.state

    return (
      <DivContainer open={ open }>
        <DivHeader>
          <DivTitle>
            Акции конкурентов
          </DivTitle>
          <DivClose>
            <DivPopupCastingClose />
          </DivClose>
        </DivHeader>
        <DivBody>
          {

            discountList.map((item, index) => {
              const { name, brand, dateBegin, dateEnd } = item

              return (
                <DivBlock key={ index }>
                  <DivDiscountBlockValue>
                    <DivDiscountName>
                      { name }
                    </DivDiscountName>
                  </DivDiscountBlockValue>
                  <DivDiscountBlockValue>
                    <DivDiscountBrand>
                      Акцию запустил { brand }
                    </DivDiscountBrand>
                  </DivDiscountBlockValue>
                  <DivBlockPeriod>
                    <DivIco path='assets/calendar.svg' />
                    <DivBlockPeriodText>
                      { dateBegin } - { dateEnd }
                    </DivBlockPeriodText>
                  </DivBlockPeriod>
                </DivBlock>)
            })
          }
        </DivBody>
      </DivContainer>
    )
  }
}

export default DiscountCompetition
