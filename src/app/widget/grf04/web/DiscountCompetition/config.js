import types                from 'engine/types'

const queryName = 'action_other_list'


const inputNameMap = {
  DATE_BEGIN:                               'dateBegin',
  DATE_END:                                 'dateEnd',
}

const resultNameMap = {
  PROVIDER:                                 'provider',
  NAME:                                     'name',
  DATE_BEGIN:                               'dateBegin',
  DATE_END:                                 'dateEnd',

  CAPTION:                                   'caption',
  VIDGET:                                    'vidget',
  DESCRIPTION:                               'description'
}

const queryMap = [
  {
    queryName: queryName,
    inputList: [
      {
        name: inputNameMap.DATE_BEGIN,
        type: types.String,
      },
      {
        name: inputNameMap.DATE_END,
        type: types.String,
      },
    ],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        }
      ],
      result: [
        {
          name: resultNameMap.PROVIDER,
          type: types.String,
        },
        {
          name: resultNameMap.NAME,
          type: types.String,
        },
        {
          name: resultNameMap.DATE_BEGIN,
          type: types.String,
        },
        {
          name: resultNameMap.DATE_END,
          type: types.String,
        },
      ],
      title: [
        {
          name: resultNameMap.CAPTION,
          type: types.String,
        },
        {
          name: resultNameMap.WIDGET,
          type: types.String,
        },
        {
          name: resultNameMap.DESCRIPTION,
          type: types.String,
        }
      ]
    }
  },
]

export default {
  queryMap,
  name: queryName,
}
