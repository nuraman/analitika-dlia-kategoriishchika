import _                    from 'lodash'

import Highcharts           from 'highcharts'
import Highchartsmore       from 'highcharts-more'
import HighchartsExporting  from 'highcharts-exporting'

import moment               from 'moment'
import localization         from 'moment/locale/ru'

import React, { Component } from 'react'

import ReactHighchart       from 'react-highcharts'

import sizeMe               from 'react-sizeme'

import styled               from 'styled-components'


const StyledDiv = styled.div`
  width: 100%;
  height: 40%;
  font-family: HelveticaRegular;
  overflow: hidden;
`


Highcharts.seriesTypes.line.prototype.pointAttribs = function (point, state) {
  var pointOptions = point && point.options,
    pointMarkerOptions = (pointOptions && pointOptions.marker) || {},
    strokeWidth = 1,
    color = this.color,
    pointColorOption = pointOptions && pointOptions.color,
    pointColor = point && point.color,
    zoneColor,
    fill,
    stroke,
    zone;
  if (point && this.zones.length) {
    zone = point.getZone();
    if (zone && zone.color) {
      zoneColor = zone.color;
    }
  }

  color = pointColorOption || zoneColor || pointColor || color;
  fill = pointMarkerOptions.fillColor || point.color || color ;
  stroke = pointMarkerOptions.lineColor || point.color || color;

  return {
    'stroke': stroke,
    'stroke-width': strokeWidth,
    'fill': fill
  }
}

Highchartsmore(ReactHighchart.Highcharts)
HighchartsExporting(ReactHighchart.Highcharts)

const styleDiscount = {
  display: 'flex',
  height: '100%',
  alignItems: 'center',
  justifyContent: 'space-around',
  textAlign: 'center'
}

class DiscountChartView extends Component {
  constructor() {
    super()

    this.state = {
      config: {}
    }
  }

  onInitConfig = ({ data = [],categories, dateXAxis, onSelectDiscount = {}, height }) => {
    moment.locale('ru', localization)
    const heightChart = `${ height }px`
    const config = {
      chart: {
        type: 'columnrange',
        inverted: true,
        height: heightChart,
      },
      navigation: {
        buttonOptions: {
          enabled: false,
        }
      },
      yAxis: {
        title: {
          text: 'Дата'
        },
        type: 'datetime',
        min: dateXAxis[0],
        max: dateXAxis[1],
        visible: false,
      },
      xAxis: {
        categories: categories,
        height: '100px',
        visible: false,
      },
      tooltip: {
        enabled: false,
      },
      title: {
        text: '',
      },
      legend: {
        enabled: false,
      },

      plotOptions: {
        allowPointSelect: true,
        columnrange: {
          grouping: false,
          cursor: 'pointer',
          point: {
            events: {
              click: function(event) {
                const { clientX, clientY } = event
                onSelectDiscount({ id: this.id, x: clientX, y: clientY })
              }
            }
          },
          dataLabels: {
            align: 'center',
            verticalAlign: 'middle',
            inside: true,
            enabled: true,
            padding: 0,
            className: styleDiscount,
            style: {
              fontFamily: 'HelveticaRegular',
              fontSize: '14px',
              display: 'flex',
              height: '100%',
              alignItems: 'center',
              justifyContent: 'space-around',
              textAlign: 'center'
            },
            formatter: function() {
              const name = this.key

              return '<span style="font-weight: normal;">' + name + '</span>'
            }
          },
        }
      },
      series: [{
        data: data,
      }]
    }

    return config
  }

  onGetDiscountDataOwn = ({ data = {}, order = [], onSelectDiscount }) => {
    if (order.length === 0) return null

    const categoryOwnList = []
    const dataList = order.reduce((dataList, id) => {
      const doc = data[id] || {}
      const { dateBegin, dateEnd, name } = doc

      categoryOwnList.push(name)

      return [ ...dataList,  {
        id,
        low: dateBegin,
        high: dateEnd,
        name: name,
        color: 'rgb(99, 218, 196)',
      }]
    }, [])

    return dataList
  }

  onGetDiscountDataOther = ({ data = {}, order = [], onSelectDiscount }) => {
    if (order.length === 0) return null

    const categoryOtherList = []

    const dataOtherList = order.reduce((dataOtherList, id) => {
      const doc = data[id] || {}
      const { dateBegin, dateEnd, name } = doc

      categoryOtherList.push(name)

      return [ ...dataOtherList,  {
        id,
        low: dateBegin,
        high: dateEnd,
        name: name
      }]
    }, [])

    return dataOtherList
  }

  onGetCategories = (data) => {
    return data.reduce((categories, item) => {
      const { name } = item

      return [ ...categories, name ]
    }, [])
  }

  componentWillReceiveProps(nextProps = {}) {
    const {
      dataDiscountOwn = {},
      orderDiscountOwn = [],
      dateXAxis = [],
    //  dataDiscountOther = {},
    //  orderDiscountOther = [],
      onSelectDiscount,

      size: { height }
    } = nextProps

    if (dateXAxis.length === 0) return null

    const dataList = this.onGetDiscountDataOwn({ data: dataDiscountOwn, order: orderDiscountOwn, }) || {}
    //const dataOtherList = this.onGetDiscountDataOther({ data: dataDiscountOther, order: orderDiscountOther }) || {}

    //const data = _.union(dataList, dataOtherList)

    if (dataList.length > 0) {
      const data = dataList

      const categories = this.onGetCategories(data)


      const config = this.onInitConfig({ data, categories, dateXAxis, onSelectDiscount, height })

      this.setState({ config })
    } else this.setState({ config: {} })
  }

  render(){
    const { config = {} } = this.state

    if (_.isEmpty(config)) return null

    return (
      <StyledDiv>
        <ReactHighchart
          config={ config }
        />
      </StyledDiv>
    )
  }
}

const config = { monitorHeight: true, monitorWidth: true }

const withSizeMe = sizeMe(config)

export default withSizeMe(DiscountChartView)
