const name = 'grf04'

const types = {
  SET_DATE_RANGE: `${ name }/SetDateRange`
}

const STATE = {
  dateBeginRemember: '',
  dateEndRemember: '',
  periodIdRemember: '',
  dateBeginOld: '',
}

const reducer = (state = STATE, action) => {
  const { type, payload } = action

  switch (type) {
    case types.SET_DATE_RANGE: {
      const { dateBegin, dateEnd, periodId, dateBeginOld } = payload

      return {
        dateBeginRemember: dateBegin,
        dateEndRemember: dateEnd,
        periodIdRemember: periodId,
        dateBeginOld
      }
    }
    default:
      return state
  }
}

export default {
  reducer,
  types
}
