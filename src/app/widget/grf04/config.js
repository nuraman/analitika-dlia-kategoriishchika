import types                from 'engine/types'

const queryNameMap = {
  GRF04:                                     'grf04',
}

const inputNameMap = {
  DATE_BEGIN:                               'dateBegin',
  DATE_END:                                 'dateEnd',
  GOODS_CATEGORY:                           'goodsCategory',
  PERIOD:                                   'period',
  POINT_GROUP:                              'pointGroup',
  POINT_FORMAT:                             'pointFormat',
  POINT:                                    'point',
  TYPE_PARAMETER:                           'typeParameter',
  PROVIDER_LIST:                            'providerList',
}

const resultNameMap = {
  VALUE_AMOUNT:                             'valueAmount',
  VALUE_COUNT:                              'valueCount',
  VALUE_QUANTITY:                           'valueQuantity',
  VALUE_COUNT_CHECK:                        'valueCountCheck',
  VALUE_COUNT_CUSTOMER:                     'valueCountCustomers',
  DATE_BEGIN:                               'dateBegin',
  DATE_END:                                 'dateEnd',

  DISCOUNT_ID:                              'actionId',
  DISCOUNT_COMMENT:                         'comment',  //комменетарии к акции
  DISCOUNT_COUNT_GOODS:                     'countGoods',
  DISCOUNT_COUNT_PLAN_CUSTOMERS:            'countPlanCustomers',
  DISCOUNT_COUNT_FACT_CUSTOMERS:            'countFaktCustomers',
  DISCOUNT_COUNT_SOLD_GOODS:                'quantityFakt', //количество проданного акционного товара
  DISCOUNT_DATE_BEGIN:                      'dateBegin',
  DISCOUNT_DATE_END:                        'dateEnd',
  DISCOUNT_DESCRIPTION:                     'description',//описание акции
  DISCOUNT_NAME:                            'name',
  DISCOUNT_SUM_SOLD_GOODS:                  'amountFakt', //СУММА ПРОДАЖ АКЦИОННОГО ТОВАРА

  DISCOUNT_OTHER_NAME:                      'name',
  DISCOUNT_OTHER_COUNT:                     'actionCount',
  DISCOUNT_OTHER_DATE_BEGIN:                'dateBegin',
  DISCOUNT_OTHER_DATE_END:                  'dateEnd',
  DISCOUNT_OTHER_COUNT_GOODS:               'countGoods',
  DISCOUNT_OTHER_COUNT_EXPECTED_CUSTOMERS:  'countPlanCustomers',
  DISCOUNT_OTHER_COUNT_REAL_CUSTOMERS:      'countFaktCustomers',
  DISCOUNT_OTHER_SUM_SOLD_GOODS:            'amountFakt',
  DISCOUNT_OTHER_COUNT_SOLD_GOODS:          'quantityFakt',

  CAPTION:                                  'caption',
  WIDGET:                                   'vidget',
  DESCRIPTION:                              'description',
}

const resultGoodsMap = {
  ACTION_ID:                                'actionId',
  NAME:                                     'name',
}

const mutationName = 'actionUpdate'

const inputMutationNameMap = {
  DISCOUNT_ID:                               'actionId',
  COMMENT:                                   'comment'
}

const queryMap = [
  {
    queryName: 'grf04',
    inputList: [
      {
        name: inputNameMap.DATE_BEGIN,
        type: types.String,
      },
    ],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        }
      ],
      resultSale: [
        {
          name: resultNameMap.DATE_BEGIN,
          type: types.String,
        },
        {
          name: resultNameMap.DATE_END,
          type: types.String,
        },
        {
          name: resultNameMap.VALUE_AMOUNT,
          type: types.Float,
        },
        {
          name: resultNameMap.VALUE_COUNT,
          type: types.Float,
        },
        {
          name: resultNameMap.VALUE_QUANTITY,
          type: types.Float,
        },
        {
          name: resultNameMap.VALUE_COUNT_CHECK,
          type: types.float
        },
        {
          name: resultNameMap.VALUE_COUNT_CUSTOMER,
          type: types.float,
        },
      ],
      resultDiscount: [
        {
          name: resultNameMap.DISCOUNT_ID,
          type: types.Int,
        },
        {
          name: resultNameMap.DISCOUNT_COMMENT,
          type: types.String,
        },
        {
          name: resultNameMap.DISCOUNT_COUNT_GOODS,
          type: types.Int
        },
        {
          name: resultNameMap.DISCOUNT_COUNT_PLAN_CUSTOMERS,
          type: types.Int,
        },
        {
          name: resultNameMap.DISCOUNT_COUNT_FACT_CUSTOMERS,
          type: types.Int,
        },
        {
          name: resultNameMap.DISCOUNT_DATE_BEGIN,
          type: types.String,
        },
        {
          name: resultNameMap.DISCOUNT_DATE_END,
          type: types.String,
        },
        {
          name: resultNameMap.DISCOUNT_DESCRIPTION,
          type: types.String,
        },
        {
          name: resultNameMap.DISCOUNT_SUM_SOLD_GOODS,
          type: types.Int,
        },
        {
          name: resultNameMap.DISCOUNT_COUNT_SOLD_GOODS,
          type: types.Int,
        },
        {
          name: resultNameMap.DISCOUNT_NAME,
          type: types.String,
        },
      ],
      resultDiscountOther: [
        {
          name: resultNameMap.DISCOUNT_OTHER_NAME,
          type: types.String,
        },
        {
          name: resultNameMap.DISCOUNT_OTHER_COUNT,
          type: types.Int,
        },
        {
          name: resultNameMap.DISCOUNT_OTHER_DATE_BEGIN,
          type: types.String,
        },
        {
          name: resultNameMap.DISCOUNT_OTHER_DATE_END,
          type: types.String,
        },
        {
          name: resultNameMap.DISCOUNT_OTHER_COUNT_GOODS,
          type: types.Int,
        },
        {
          name: resultNameMap.DISCOUNT_OTHER_COUNT_EXPECTED_CUSTOMERS,
          type: types.Int,
        },
        {
          name: resultNameMap.DISCOUNT_OTHER_COUNT_REAL_CUSTOMERS,
          type: types.Int,
        },
        {
          name: resultNameMap.DISCOUNT_OTHER_SUM_SOLD_GOODS,
          type: types.Int,
        },
        {
          name: resultNameMap.DISCOUNT_OTHER_COUNT_SOLD_GOODS,
          type: types.Int
        }
      ],
      resultGoods: [
        {
          name: resultGoodsMap.ACTION_ID,
          type: types.Int,
        },
        {
          name: resultGoodsMap.NAME,
          type: types.String,
        }
      ],
      title: [
        {
          name: resultNameMap.CAPTION,
          type: types.String,
        },
        {
          name: resultNameMap.WIDGET,
          type: types.String,
        },
        {
          name: resultNameMap.DESCRIPTION,
          type: types.String,
        }
      ]
    }
  },
]

const mutationMap = [
  {
    mutationName: mutationName,
    inputList: [
      {
        name: [inputMutationNameMap.DISCOUNT_ID],
        type: types.Int,
      },
      {
        name: [inputMutationNameMap.COMMENT],
        type: types.String,
      },
    ],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        }
      ],
    }
  }
]

export default {
  queryMap,
  name: queryNameMap.GRF04,
  resultNameMap,
  mutationName,
  mutationMap,
  inputMutationNameMap,
  inputNameMap
}
