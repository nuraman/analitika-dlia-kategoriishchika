import _                    from 'lodash'

import React, { Component } from 'react'

import { connect }          from 'react-redux'
import { compose }          from 'redux'

import sizeMe               from 'react-sizeme'


import withGrf04            from 'app/widget/graphql/withGrf04'
import api                  from "./api";


function withData(ComposedComponent) {
  class Widget extends Component {
    constructor() {
      super()

      this.state = {
        resultSale: [],
        resultDiscount: [],
        resultDiscountOther: [],

        dateXAxis: [],

        isUpdate: false,
      }
    }

    componentWillReceiveProps(nextProps) {
      const { resultSale = {}, resultDiscount, resultDiscountOther } = nextProps
      if (!_.isEmpty(resultSale)) {
        const { data, order } = resultSale

        const dateXAxis = api.getXAxisData({ data, order })
        /*
        Сравниваем новые данные со старыми,
        если они не равны, то считаем, что поступили новые данные
        и заменяем старые на новые,
        если равны - то считаем, что ничего не изменился
        и оставляем все как есть.
        */
        const isEqual = api.compareData({
          newData: resultSale.data,
          newOrder: resultSale.order,

          oldData: this.state.resultSale.data,
          oldOrder: this.state.resultSale.order
        })

        if (!isEqual) {
          this.setState({
            dateXAxis,

            isUpdate: true,

            resultSale,
            resultDiscount,
            resultDiscountOther,
          })
        } else {
          this.setState({ isUpdate: false })
        }
      }
    }

    render() {
      return (
        <ComposedComponent { ...this.state } { ...this.props } />
      )
    }
  }

  function mapSateToProps(state) {
    const { menu, user, grf04 } = state

    const { userLogin: { data, order } } = user
    const { isProvider } = data[order[0]] || {}

    const { category, period, providerList } = menu

    const { dateBegin, dateEnd, id } = period

    const { dateBeginRemember, dateEndRemember, periodIdRemember, dateBeginOld } = grf04

    return {
      menu,
      dateBeginRemember,
      dateEndRemember,
      periodIdRemember,
      dateBeginOld,
      dateBegin,
      dateEnd,
      periodId: id,
      options: {
        variables: {
          dateBegin,
        }
      }
    }
  }

  return compose(
    connect(mapSateToProps),
    sizeMe({ monitorWidth: true, monitorHeight: true }),
    withGrf04(),
  )(Widget)
}

export default withData
