import moment               from 'moment'
import Highcharts           from "highcharts"
import ReactHighstock       from 'react-highcharts/ReactHighstock'
import localization         from "moment/locale/ru"
import _                    from 'lodash'


ReactHighstock.Highcharts.setOptions({
  global: {
    useUTC: false,
    timezone: 'Asia/Krasnoyarsk'
  },
  lang: {
    rangeSelectorTo: 'До',
    rangeSelectorFrom: 'От',
    rangeSelectorZoom: '',
    months: [
      'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Авг', 'Сент', 'Окт', 'Нояб', 'Дек'
    ],
    weekdays: [
      'Понедельник', 'Вторник', 'Cреда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'
    ],
    shortMonths: [
      'Янв', 'Февр', 'Март', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сент', 'Окт', 'Нояб', 'Дек'
    ]
  }
})

class Chart {
  constructor() {
    this.data = {}
    this.order = []
    this.height = ''
    this.periodId
  }

  set options ({ data, order, height, periodId }) {
    this.data = data
    this.order = order
    this.height = height
    this.periodId = periodId
  }

  getConfig({ onSelectRange }) {
    return this.createConfig(onSelectRange)
  }

  createConfig = (onSelectRange) => {
    moment.locale('ru', localization)

    const heightChart = `${ this.height }px`
    const series = this.createSeries()

    const config = {
      chart: {
        height: heightChart,
        events: {
          redraw: function(event) {
            const currentDateBegin = Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.rangeSelector.minInput.HCTime)
            const currentDateEnd = Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.rangeSelector.maxInput.HCTime)

            onSelectRange && onSelectRange({ currentDateBegin, currentDateEnd })
          }
        }
      },
      rangeSelector: {
        buttons: [{
          type: 'week',
          count: '0.8',
          text: 'н',
        },{
          type: 'month',
          count: 1,
          text: 'м',
        },{
          type: 'month',
          count: 3,
          text: 'кв'
        }, {
          type: 'month',
          count: '6',
          text: 'пг'
        },{
          type: 'year',
          count: 1,
          text: 'г'
        }],
        buttonSpacing: 2,
        selected: this.periodId - 1,
        inputDateFormat: '%d/%m/%Y',
        labelStyle: {
          fontFamily: 'HelveticaLight',
          fontSize: '14px',
        }
      },
      title: {
        align: 'left',
        text: '',
      },
      navigation: {
        buttonOptions: {
          enabled: false,
        }
      },
      navigator: {
        xAxis: {
          labels: {
            formatter: function () {
              return moment(this.value).format('DD MMMM')
            },
            style: {
              fontFamily: 'HelveticaLight',
              fontSize: '14px',
            }
          }
        },
      },
      xAxis: {
        minTickInterval:  6 * 24 * 3600 * 1000,
        labels: {
          formatter: function () {
            return moment(this.value).format('DD MMMM')
          },
          style: {
            fontFamily: 'HelveticaLight',
            fontSize: '14px',
          }
        },
      },
      plotOptions: {
        series: {
          compare: 'percent',
          showInNavigator: true
        }
      },
      tooltip: {
        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
        valueDecimals: 2,
        split: true
      },

      series: series
    }

    return config
  }

  createSeries = () => {
    const series              = []
    const dataCheckCount      = {}
    const dataPurchaseSum     = {}
    const dataGoodsCount      = {}
    const dataCountCheck      = {}
    const dataCountCustomers  = {}

    this.order.forEach((id) => {
      const { dateBegin, dateEnd, valueAmount, valueCount, valueQuantity, valueCountCheck, valueCountCustomers } = this.data[id] || {}

      _.merge( dataCheckCount, { [id]: { id, x: dateBegin, y: valueCount } })
      _.merge( dataCheckCount, { [id + 1]: { id: id + 1, x: dateEnd, y: valueCount } })

      _.merge( dataPurchaseSum, { [id]: { id, x: dateBegin, y: valueAmount } })
      _.merge( dataPurchaseSum, { [id + 1]: { id: id + 1, x: dateEnd, y: valueAmount } })

      _.merge( dataGoodsCount, { [id]: { id, x: dateBegin, y: valueQuantity } })
      _.merge( dataGoodsCount, { [id + 1]: { id: id + 1, x: dateEnd, y: valueQuantity } })

      _.merge( dataCountCheck, { [id]: { id, x: dateBegin, y: valueCountCheck } })
      _.merge( dataCountCheck, { [id + 1]: { id: id + 1, x: dateEnd, y: valueCountCheck } })

      _.merge( dataCountCustomers, { [id]: { id, x: dateBegin, y: valueCountCustomers } })
      _.merge( dataCountCustomers, { [id + 1]: { id: id + 1, x: dateEnd, y: valueCountCustomers } })
    })

    const orderCheckCount   = Object.keys(dataCheckCount)
    const orderPurchaseSum  = Object.keys(dataPurchaseSum)
    const orderGoodsCount   = Object.keys(dataGoodsCount)

    const seriesDataCheckCount   = this.getSeriesData({ data: dataCheckCount, order: orderCheckCount })
    const seriesDataPurchaseSum  = this.getSeriesData({ data: dataPurchaseSum, order: orderPurchaseSum })
    const seriesDataGoodsCount   = this.getSeriesData({ data: dataGoodsCount, order: orderGoodsCount })

    series.push({
      name: 'Количество чеков',
      data: seriesDataCheckCount,
    })

    series.push({
      name: 'Сумма продаж',
      data: seriesDataPurchaseSum,
    })

    series.push({
      name: 'Количество проданного товара',
      data: seriesDataGoodsCount,
    })

    return series
  }

  getSeriesData = ({ data, order }) => {
    const seriesData = order.reduce((seriesData, item) => {
      const doc = data[item] || {}
      const { x, y } = doc

      return [ ...seriesData, [ x, y ]]
    }, [])

    return seriesData
  }
}

export default Chart
