import _                    from 'lodash'

export default {
  getXAxisData: function(payload) {
    const {  data = {}, order = [] } = payload

    const dateXAxis = order.reduce((dateXAxis, id, index) => {
      if (index === 0) {
        const { dateBegin } = data[id]
        dateXAxis.push(dateBegin)
      }

      if (index === (order.length - 1)) {
        const { dateEnd } = data[id]
        dateXAxis.push(dateEnd)
      }

      return [ ...dateXAxis ]
    }, [])

    return dateXAxis
  },
  compareData: function(payload) {
    const { newData = {}, oldData = {}, newOrder = [], oldOrder = [] } = payload

    const newValueAmountList = newOrder.reduce((newValueAmountList, item) => {
      const { valueAmount } = newData[item]

      return [ ...newValueAmountList, valueAmount ]
    }, [])

    const oldValueAmountList = oldOrder.reduce((oldValueAmountList, item) => {
      const { valueAmount } = oldData[item]

      return [ ...oldValueAmountList, valueAmount ]
    }, [])

    return _.isEqual(newValueAmountList, oldValueAmountList)
  },
  createSeries: function(payload) {
    const series              = []
    const dataCheckCount      = {}
    const dataPurchaseSum     = {}
    const dataGoodsCount      = {}
    const dataCountCheck      = {}
    const dataCountCustomers  = {}


    const { data = {}, order = [] } = payload

    order.forEach((id) => {
      const { dateBegin, dateEnd, valueAmount, valueCount, valueQuantity, valueCountCheck, valueCountCustomers } = data[id] || {}

      _.merge( dataCheckCount, { [id]: { id, x: dateBegin, y: valueCount } })
      _.merge( dataCheckCount, { [id + 1]: { id: id + 1, x: dateEnd, y: valueCount } })

      _.merge( dataPurchaseSum, { [id]: { id, x: dateBegin, y: valueAmount } })
      _.merge( dataPurchaseSum, { [id + 1]: { id: id + 1, x: dateEnd, y: valueAmount } })

      _.merge( dataGoodsCount, { [id]: { id, x: dateBegin, y: valueQuantity } })
      _.merge( dataGoodsCount, { [id + 1]: { id: id + 1, x: dateEnd, y: valueQuantity } })

      _.merge( dataCountCheck, { [id]: { id, x: dateBegin, y: valueCountCheck } })
      _.merge( dataCountCheck, { [id + 1]: { id: id + 1, x: dateEnd, y: valueCountCheck } })

      _.merge( dataCountCustomers, { [id]: { id, x: dateBegin, y: valueCountCustomers } })
      _.merge( dataCountCustomers, { [id + 1]: { id: id + 1, x: dateEnd, y: valueCountCustomers } })
    })

    const orderCheckCount   = Object.keys(dataCheckCount)
    const orderPurchaseSum  = Object.keys(dataPurchaseSum)
    const orderGoodsCount   = Object.keys(dataGoodsCount)
//    const orderCountCheck         = Object.keys(dataCountCheck)
//    const orderCountCustomers     = Object.keys(dataCountCustomers)

    const seriesDataCheckCount   = getSeriesData({ data: dataCheckCount, order: orderCheckCount })
    const seriesDataPurchaseSum  = getSeriesData({ data: dataPurchaseSum, order: orderPurchaseSum })
    const seriesDataGoodsCount   = getSeriesData({ data: dataGoodsCount, order: orderGoodsCount })

    series.push({
      name: 'Количество чеков',
      data: seriesDataCheckCount,
    })

    series.push({
      name: 'Сумма продаж',
      data: seriesDataPurchaseSum,
    })

    series.push({
      name: 'Количество проданного товара',
      data: seriesDataGoodsCount,
    })

    return series
  }
}

const getSeriesData = ({ data, order }) => {
  const seriesData = order.reduce((seriesData, item) => {
    const doc = data[item] || {}
    const { x, y } = doc

    return [ ...seriesData, [ x, y ]]
  }, [])

  return seriesData
}
