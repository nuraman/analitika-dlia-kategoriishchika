import React, { Component } from 'react'

import styled               from 'styled-components'


const DivModal = styled.div`
  display: ${ props => props.display }; 
  position: fixed; 
  z-index: 1; 
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
  background-color: rgb(0,0,0);
  background-color: rgba(0,0,0,0.4);
`

const DivContainer = styled.div`
  align-items: center;
  display: flex;
  justify-content: space-around;
  width: 100%;
  height: 100%;
`

const DivContent = styled.div`
  background-color: #fefefe;
  height: 400px;
  position: relative;
  padding: 0;
  border: 1px solid #888;
  width: 500px;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
  -webkit-animation-name: animatetop;
  -webkit-animation-duration: 0.4s;
  animation-name: animatetop;
  animation-duration: 0.4s    
`

const DivHeader = styled.div`
  display: block;
  height: 50px;
`

const DivHeaderRow = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  height: 100%;
  border-bottom: 1px solid #7d7d7d;
`

const DivTitle = styled.div`
  font-size: 18px;
  font-family: HelveticaRegular;
  color: #000;
  padding-left: 10px;
`

const DivBold = styled.div`
  font-weight: bold;
`

const DivBody = styled.div`
  padding: 10px;
  font-family: HelveticaRegular;
`

const ButtonClose = styled.div`
  position: absolute;
  right: 0;
  display: block;
  box-sizing: border-box;
  width: 50px;
  height: 50px;
  border-left: 1px solid #7d7d7d;
  cursor: pointer;
  
  &:hover:before, :hover:after {
    opacity: 0.6;
  }
  &:before, &:after {
    content: "";
    position: absolute;
    top: 50%;
    left: 10px;
    width: 30px;
    height: 1px;
    opacity: 0.9;
    background-color: #000;
    cursor: pointer;
  }
  &:before {
    transform: translateY(-50%) rotate(45deg);
  }
  &:after {
    transform: translateY(-50%) rotate(-45deg);
  }
`


class WidgetDescription extends Component {
  constructor() {
    super()

    this.state = {
      open: false,
      title: '',
    }
  }

  onHandleClick = () => {
    this.setState({ open: !this.state.open })

    const { onClose } = this.props

    onClose()
  }

  onClickDocument = () => {
    const { open } = this.state

    if (open) {
      this.setState({open: !this.state.open})

      const {onClose} = this.props

      onClose()
    }
  }

  componentWillMount() {
    const { open, title } = this.props

    this.setState({ open, title })
  }

  componentWillReceiveProps(nextProps) {
    const { open, title } = nextProps

    this.setState({ open, title })
  }

  render() {
    const { open, title } = this.state

    return(
      <DivModal
        display={ open ? 'block' : 'none' }
        onClick={ this.onClickDocument }
      >
        <DivContainer>
          <DivContent>
            <DivHeader>
              <DivHeaderRow>
                <DivTitle>
                  { title }
                </DivTitle>
                <ButtonClose
                  onClick={ this.onHandleClick }
                />
              </DivHeaderRow>
            </DivHeader>
            <DivBody>
              <div>
                <DivBold>
                  Назначение:
                </DivBold>
                <div>
                  Виджет показывает долю каждого поставщика в реализованном через торговую сеть товаре выбранной категории.
                  Зеленым цветом закрашена своя доля. Остальными цветами - доли конкурентов.
                  На виджете приводится процентное соотношение долей.
                </div>
              </div>
              <div>
                <DivBold>
                  Параметры:
                </DivBold>
                <div>
                  <div>
                    Через настройку контекста отображения (левая боковая панель) можно выбрать:
                  </div>
                  <div>
                    - тип отображаемых данных (сумма, кол-во товара, кол-во покупок, кол-во чеков);
                  </div>
                  <div>
                    - тип периода (неделя, месяц, квартал, полгода, год);
                  </div>
                  <div>
                    - магазины (один выбранный, формат магазинов, группа магазинов);
                  </div>
                  <div>
                    В соответствии с выбранными параметрами виджет будет немедленно перестроен.
                  </div>
                </div>
                  <DivBold>
                    Ограничения:
                  </DivBold>
                  <div>
                    Данные для построения виджета хранятся не более 3-ти лет назад от текущей даты.
                  </div>
              </div>
            </DivBody>
          </DivContent>
        </DivContainer>
      </DivModal>
    )
  }
}

export default WidgetDescription
