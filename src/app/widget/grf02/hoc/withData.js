import React                from 'react'
import { connect }          from 'react-redux'
import { compose }          from 'redux'


import withGoodsCategory    from 'app/menu/graphql/withMenuList'
import withParameterList    from 'app/menu/graphql/withMenuList'
import withGrf02            from 'app/widget/graphql/withGrf02'

import config               from 'app/menu/config'


function withData(ComposedComponent) {
  function decorator(props) {
     return (<ComposedComponent  { ...props } />)
  }

  function mapSateToProps(state) {
    const { user, menu } = state

    const { category, parameter, period: { dateBegin } } = menu

    const { userLogin: { data, order } } = user

    const { provider, isProvider } = data[order[0]] || {}

    return {
      categoryId: category.id,
      parameterId: parameter.id,
      dateBegin,
      provider,
      isProvider,
      options: {
        isAuth: order.length > 0,
      }
    }
  }

  return compose(
    connect(mapSateToProps),
    withGrf02(),
    /*
    нужно запросить список категорий, поскольку у нас есть только id выбранной категории,
    а названия категории нету, тоже самое и для параметра
     */
    withGoodsCategory({ queryName: config.queryNameMap.GOODS_CATEGORY_LIST }),
    withParameterList({ queryName: config.queryNameMap.PARAMETER_LIST }),
  )(decorator)
}

export default withData
