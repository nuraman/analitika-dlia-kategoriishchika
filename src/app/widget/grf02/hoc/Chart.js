import Highcharts           from 'highcharts'

import _                    from 'lodash'


function Chart() {
  this.data = []
  this.order = []
  this.dataGoodsCategory = {}
  this.dataParameter = {}
  this.provider = ''
  this.categoryId = ''
  this.parameterId = ''

  this.getValue = function({ id, data }) {
    if (!id) return null

    const match = _.find(data, { 'id': +id })

    return match ? match.value : null
  }

  this.getSeries = function () {
    let colorSelfData, colorOtherData

    const dataSelf = this.order.reduce((dataSelf, item) => {
      const doc = this.data[item] || {}
      const { valueSelf, colorSelf } = doc

      colorSelfData = colorSelf

      return  [ ...dataSelf, valueSelf ]
    }, [])

    const dataOther = this.order.reduce((dataOther, item) => {
      const doc = this.data[item] || {}
      const { valueOther, colorOther } = doc

      colorOtherData = colorOther

      return  [ ...dataOther, valueOther ]
    }, [])

    const series = [{
      name: 'Мой бренд',
      data: dataSelf,
      id: this.provider,
      color: `#${ colorSelfData }`
    }, {
      id: null,
      name: 'Конкуренты',
      data: dataOther,
      color: `#${ colorOtherData }`
    }]

    return series
  }

  this.getCategories = function() {
    const categories = this.order.reduce((xCategories, item) => {
      const doc = this.data[item] || {}
      const { month } = doc
      return [ ...xCategories, month]
    }, [])

    return categories
  }
}

Chart.prototype = {
  set options ({ data, order, categoryId, parameterId, dataGoodsCategory, dataParameter, provider }) {
    this.data = data
    this.order = order
    this.dataGoodsCategory = dataGoodsCategory
    this.dataParameter = dataParameter
    this.provider = provider
    this.categoryId = categoryId
    this.parameterId = parameterId
  }
}

Chart.prototype.getConfig = function ({ onSelect, height }) {
  const series = this.getSeries()
  const categories = this.getCategories()
  const categoryValue = this.getValue({ id: this.categoryId, data: this.dataGoodsCategory })
  const parameterValue = this.getValue({ id: this.parameterId, data: this.dataParameter })

  const heightChart = `${ height - 60 }px`
  const config = {
    chart: {
      height: heightChart,
      type: 'column',
    },
    title: {
      align: 'center',
      text: categoryValue,
      style: {
        fontFamily: 'HelveticaLight',
        fontSize: '14px',
        color: '#939393'
      }
    },
    navigation: {
      buttonOptions: {
        enabled: false,
      }
    },
    xAxis: {
      categories: categories,
      title: {
        style: {
          fontFamily: 'HelveticaRegular',
        }
      },
    },
    yAxis: {
      title: {
        text: 'Количество',
        style: {
          fontFamily: 'HelveticaRegular',
        }
      },
    },
    tooltip: {
      headerFormat: `<span style="font-size: 14px">${ parameterValue }</span><br/>`,
    },
    plotOptions: {
      column: {
        stacking: 'normal',
        dataLabels: {
          enabled: false,
          color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
        },
        events: {
          click: function() {
            const { userOptions } = this
            const { id } = userOptions
            onSelect && onSelect({ id })
          }
        },
        cursor: 'pointer',
      }
    },
    legend: {
      itemStyle: {
        fontFamily: 'HelveticaBold',
        fontSize: '14px',
      }
    },
    series: series
  }

  return config
}

export default Chart



