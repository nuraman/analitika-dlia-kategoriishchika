import ReactHighchart       from 'react-highcharts'

import PropTypes            from 'prop-types'

import React, { Component } from 'react'

import sizeMe               from 'react-sizeme'
import { compose }          from 'redux'


import ProgressBar          from 'app/analysis/web/WidgetProgressBar'
import EmptyWidget          from 'app/widget/WidgetEmpty'

import go                   from 'app/story/go'

import withData             from 'app/widget/grf02/hoc/withData'

import Widget               from 'app/widget/styledComponent/widget/DivContainer'
import WidgetContent        from 'app/widget/styledComponent/widget/DivContent'
import WidgetHeader         from 'app/widget/header/Container'
import Chart                from 'app/widget/grf02/hoc/Chart'


class Grf02Logic extends Component {
  static propTypes = {
   dataGoodsCategory: PropTypes.object,
   dataParameter: PropTypes.object,
   data: PropTypes.object,
   order: PropTypes.array,
   title: PropTypes.object
  }

  constructor() {
   super()

   this.state = {
     config: {},
     chart: {},
     description: '',
     dateBegin: '',
     order: [],
     title: '',
     widgetId: ''
   }
  }

  onHandleSelect = ({ id }) => {
   const { provider } = this.props

   if (provider !== id) go('/competition')
  }

  componentWillMount() {
    const chart = new Chart()

    this.setState({ chart })
  }

  componentWillReceiveProps(nextProps) {
   const {
     categoryId,
     dataGoodsCategory,
     dataParameter,
     data = {},
     dateBegin,
     order = [],
     parameterId,
     provider,
     title,
     size: { height }
   } = nextProps

   const { chart } = this.state

   const { caption = '', description = '', vidget = '' } = title || {}

   if (order.length > 0) {
    chart.options = {  data, order, categoryId, parameterId, dataGoodsCategory, dataParameter, provider }

    const config = chart.getConfig({ onSelect: this.onHandleSelect, height })

    return this.setState({
      config,
      description,
      dateBegin,
      order,
      title: caption,
      widgetId: vidget,
    })
   }

   this.setState({ title: caption, description, widgetId: vidget, dateBegin })
 }

  exportImage = () => {
    const { chart } = this.chart
    const { title } = this.state

    chart.exportChart({ filename: title ? title : 'виджет' } , null)
  }

 render() {
   const { title, description, config, widgetId, dateBegin }  = this.state

   const { loadingData, parameterId, order = [] } = this.props

   if (loadingData === true) return (
     <ProgressBar
      title='Динамика категории'
     />
   )

   if (loadingData === false && order.length === 0) return <EmptyWidget title='Динамика категории' />

   return (
     <Widget>
       <WidgetHeader
         description={ description }
         dateBegin={ dateBegin }
         id={ parameterId }
         title={ title }
         widgetId={ widgetId }

         exportImage={ this.exportImage }
       />
       <WidgetContent>
         <ReactHighchart config = { config } ref={(chart) => { this.chart = chart }} />
       </WidgetContent>
     </Widget>
    )
  }
}

export default compose(
  sizeMe({ monitorHeight: true, monitorWidth: true }),
  withData,
)(Grf02Logic)
