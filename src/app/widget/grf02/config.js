import types                from 'engine/types'

const inputNameMap = {
  DATE_BEGIN:               'dateBegin',
  DATE_END:                 'dateEnd',
  GOODS_CATEGORY:           'goodsCategory',
  PERIOD:                   'period',
  POINT_FORMAT:             'pointFormat',
  TYPE_PARAMETER:           'typeParameter',
  PROVIDER_LIST:            'providerList'
}

const queryNameMap = {
  GRF02:                    'grf02',
 }

const resultNameMap = {
  COLOR_SELF:               'colorSelf',
  COLOR_OTHER:              'colorOther',
  VALUE_SELF:               'value_self',
  VALUE_OTHER:              'value_other',
  MONTH:                    'month',

  CAPTION:                   'caption',
  WIDGET:                    'vidget',
  DESCRIPTION:               'description',
}

const queryMap = [
  {
    queryName: queryNameMap.GRF02,
    inputList: [],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        }
      ],
      result: [
        {
          name: resultNameMap.COLOR_SELF,
          type: types.String,
        },
        {
          name: resultNameMap.COLOR_OTHER,
          type: types.String,
        },
        {
          name: resultNameMap.MONTH,
          type: types.String,
        },
        {
          name: resultNameMap.VALUE_SELF,
          type: types.Float,
        },
        {
          name: resultNameMap.VALUE_OTHER,
          type: types.Float,
        },
      ],
      title: [
        {
          name: resultNameMap.CAPTION,
          type: types.String,
        },
        {
          name: resultNameMap.WIDGET,
          type: types.String,
        },
        {
          name: resultNameMap.DESCRIPTION,
          type: types.String,
        }
      ]
    }
  },
]

export default {
  queryMap,
  name: queryNameMap.GRF02,
  inputNameMap
}
