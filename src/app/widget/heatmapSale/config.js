import types                from 'engine/types'

const inputNameMap = {
  DATE_BEGIN:               'dateBegin',
}

const queryNameMap = {
  GRF09:                    'grf09',
}

const resultNameMap = {
  DAY:                      'day',
  VALUE01:                  'value01',
  VALUE02:                  'value02',
  VALUE03:                  'value03',
  VALUE04:                  'value04',
  VALUE05:                  'value05',
  VALUE06:                  'value06',
  VALUE07:                  'value07',
  VALUE08:                  'value08',
  VALUE09:                  'value09',
  VALUE10:                  'value10',
  VALUE11:                  'value11',
  VALUE12:                  'value12',
  VALUE13:                  'value13',
  VALUE14:                  'value14',
  VALUE15:                  'value15',
  VALUE16:                  'value16',
  VALUE17:                  'value17',
  VALUE18:                  'value18',
  VALUE19:                  'value19',
  VALUE20:                  'value20',
  VALUE21:                  'value21',
  VALUE22:                  'value22',
  VALUE23:                  'value23',
}

const titleNameMap = {
  CAPTION:                  'caption',
  WIDGET:                   'vidget',
  DESCRIPTION:              'description',
}

const queryMap = [
  {
    queryName: 'grf09',
    inputList: [
      {
        name: inputNameMap.DATE_BEGIN,
        type: types.String,
      },
    ],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        }
      ],
      result: [
        {
          name: resultNameMap.DAY,
          type: types.Int,
        },
        {
          name: resultNameMap.VALUE01,
          type: types.Float,
        },
        {
          name: resultNameMap.VALUE02,
          type: types.Float,
        },
        {
          name: resultNameMap.VALUE03,
          type: types.Float,
        },
        {
          name: resultNameMap.VALUE04,
          type: types.Float,
        },
        {
          name: resultNameMap.VALUE05,
          type: types.Float,
        },
        {
          name: resultNameMap.VALUE06,
          type: types.Float,
        },
        {
          name: resultNameMap.VALUE07,
          type: types.Float,
        },
        {
          name: resultNameMap.VALUE08,
          type: types.Float,
        },
        {
          name: resultNameMap.VALUE09,
          type: types.Float,
        },
        {
          name: resultNameMap.VALUE10,
          type: types.Float,
        },
        {
          name: resultNameMap.VALUE11,
          type: types.Float,
        },
        {
          name: resultNameMap.VALUE12,
          type: types.Float,
        },
        {
          name: resultNameMap.VALUE13,
          type: types.Float,
        },
        {
          name: resultNameMap.VALUE14,
          type: types.Float,
        },
        {
          name: resultNameMap.VALUE15,
          type: types.Float,
        },
        {
          name: resultNameMap.VALUE16,
          type: types.Float,
        },
        {
          name: resultNameMap.VALUE17,
          type: types.Float,
        },
        {
          name: resultNameMap.VALUE18,
          type: types.Float,
        },
        {
          name: resultNameMap.VALUE19,
          type: types.Float,
        },
        {
          name: resultNameMap.VALUE20,
          type: types.Float,
        },
        {
          name: resultNameMap.VALUE21,
          type: types.Float,
        },
        {
          name: resultNameMap.VALUE22,
          type: types.Float,
        },
        {
          name: resultNameMap.VALUE23,
          type: types.Float,
        },
      ],
      title: [
        {
          name: titleNameMap.CAPTION,
          type: types.String,
        },
        {
          name: titleNameMap.WIDGET,
          type: types.String,
        },
        {
          name: titleNameMap.DESCRIPTION,
          type: types.String,
        }
      ]
    }
  },
]

export default {
  queryMap,
  name: queryNameMap.GRF09
}
