import React, { Component } from 'react'

import ReactHighchart       from 'react-highcharts'

import sizeMe               from 'react-sizeme'

import { compose }          from 'redux'


import ProgressBar          from 'app/analysis/web/WidgetProgressBar'

import EmptyWidget          from 'app/widget/WidgetEmpty'
import withData             from 'app/widget/heatmapSale/hoc/withData'

import api                  from 'app/widget/heatmapSale/hoc/api'

import Widget               from 'app/widget/styledComponent/widget/DivContainer'
import WidgetHeader         from 'app/widget/header/Container'


class HeatmapSaleLogic extends Component {
  constructor() {
    super()

    this.state = {
      config: {},
      description: '',
      dateBegin: '',
      parameter: {},
      title: '',
      widgetId: '',
    }
  }

  componentWillReceiveProps(nextProps) {
    const {
      description,
      dateBegin,
      series = [],
      order = [],
      parameterValue,
      parameter,
      size: { height },
      title,
      xAxisLabel,
      yAxisLabel,
      widgetId
    } = nextProps

    if (order.length > 0) {
      const config = api.getInitConfig({ series, xAxisLabel, yAxisLabel, height, parameterValue })

      return this.setState({ config, description, parameter, title, dateBegin, widgetId })
    }


    return this.setState({ description, title, widgetId, dateBegin })
  }

  exportImage = () => {
    const { title } = this.state

    this.chart.getChart().exportChart({ filename: title ? title : 'виджет' }, null)
  }

  render() {
    const { title, description, config, parameter, widgetId, dateBegin } = this.state

    const { loadingData, order = [] } = this.props

    if (loadingData) return (
      <ProgressBar
        title='Хитмап продаж'
      />
    )

    if (!loadingData && order.length === 0) return <EmptyWidget title={ title } />

    return(
      <Widget>
        <WidgetHeader
          description={ description }
          dateBegin={ dateBegin }
          id={ parameter.id }
          title={ title }
          widgetId={ widgetId }

          exportImage={ this.exportImage }
        />
        <ReactHighchart config={ config }  ref={ (chart) => { this.chart = chart } } />
      </Widget>
    )
  }
}

export default compose(
  sizeMe({ monitorHeight: true, monitorWidth: true }),
  withData
)(HeatmapSaleLogic)
