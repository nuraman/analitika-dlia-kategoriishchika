import React, { Component } from 'react'
import { connect }          from 'react-redux'
import { compose }          from 'redux'


import withGrf09            from 'app/widget/graphql/withGrf09'

import withParameterList    from 'app/menu/graphql/withMenuList'
import config               from 'app/menu/config'

import api                  from './api'


function withData(ComposedComponent) {
  class Widget extends Component {
    constructor() {
      super()

      this.state = {
        description: '',
        series: [],
        title: '',
        xAxisLabel: [],
        yAxisLabel: [],
        parameterValue: '',
        widgetId: '',
      }
    }

    componentWillReceiveProps(nextProps) {
      const { data = {}, dataParameter = {}, order = [], parameter, title = {} } = nextProps

      const { caption, description, vidget } = title

      if (order.length > 0) {
        const series = api.getSeries({ data, order })
        const { xAxisLabel, yAxisLabel } = api.getAxisLabel({ data, order })

        const { id, value } = parameter

        const parameterValue = api.getValue({ id, value, data: dataParameter })

        this.setState({
          description: description,
          series,
          title: caption,
          xAxisLabel,
          yAxisLabel,
          parameterValue,
          widgetId: vidget
        })
      } else {
        this.setState({ title: caption, description, widgetId: vidget })
      }
    }

    render() {
      return (
        <ComposedComponent { ...this.props } { ...this.state } />
      )
    }
  }

  function mapSateToProps(state) {
    const { menu, user } = state

    const { period, category, parameter, pointGroup, pointFormat, point, providerList } = menu
    const { id, dateBegin, dateEnd } = period

    const { userLogin: { data, order } } = user
    const { isProvider } = data[order[0]] || {}

    const isAuth = order.length !== 0

    return {
      parameter,
      isProvider,
      dateBegin,
      options: {
        isAuth,
        variables: {
          dateBegin: dateBegin,
        }
      }
    }
  }

  return compose(
    connect(mapSateToProps),
    withGrf09(),
    withParameterList({ queryName: config.queryNameMap.PARAMETER_LIST }),
  )(Widget)
}

export default withData
