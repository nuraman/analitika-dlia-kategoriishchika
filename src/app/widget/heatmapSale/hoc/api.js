import _                    from 'lodash'

const getDataSeries = ({ data, order }) => {
  const seriesData = order.reduce((seriesData, item, i) => {
    const doc = data[item] || {}

    const { timeList } = doc

    timeList.forEach((time, j) => {
      const { value } = time

      seriesData.push([ j, i, value ])
    })

    return [ ...seriesData ]
  }, [])

  return seriesData
}

const getSeries = (payload) => {
  const { data, order } = payload

  const dataSeries = getDataSeries({ data, order })

  return [{
    name: 'Что-то',
    borderWidth: 1,
    data: dataSeries,
    color: '#E0F2F1',
    dataLabels: {
      enabled: false,
      color: '#c42525'
    }
  }]
}

const getAxisLabel = (payload) => {
  const { data, order } = payload

  const xAxisLabel = []

  const yAxisLabel = order.reduce((yAxisLabel, item) => {
    const doc = data[item] || {}

    const { day, timeList } = doc

    if (xAxisLabel.length === 0) {
      timeList.map((time) => {
        const { hour } = time

        xAxisLabel.push(hour)

        return [ ...xAxisLabel ]
      })
    }

    return [ ...yAxisLabel, day ]
  }, [])

  return {
    xAxisLabel,
    yAxisLabel,
  }
}

const getInitConfig = (payload) => {
  const { series, xAxisLabel, yAxisLabel, height, parameterValue } = payload

  const chartHeight = `${ height - 60 }px`

  const config = {
    chart: {
      height: chartHeight,
      type: 'heatmap',
      plotBorderWidth: 1,
    },
    title: {
      text: '',
    },
    xAxis: {
      categories: xAxisLabel,
      labels: {
        enabled: true,
        style: {
          fontFamily: 'HelveticaRegular',
          fontSize: '12px',
        }
      },
      opposite: true,
    },
    yAxis: {
      categories: yAxisLabel,
      labels: {
        enabled: true,
        style: {
          fontFamily: 'HelveticaRegular',
          fontSize: '12px',
        }
      },
      title: {
        text: '',
      },
      reversed: true,
    },
    plotOptions: {
      heatmap: {
        color: '#E0F2F1'
      },
    },
    colorAxis: {
      min: 0,
      minColor: '#80CBC4',
      maxColor: '#004D40'
    },
    navigation: {
      buttonOptions: {
        enabled: false,
      }
    },
    legend: {
      align: 'right',
      layout: 'vertical',
      margin: 0,
      verticalAlign: 'top',
      y: 25,
      symbolHeight: 280
    },

    tooltip: {
      formatter: function () {
        return '<b>' + 'Время: ' + this.series.xAxis.categories[this.point.x] + ':00' + '</b><br>' +
          '<b>' + 'День недели: ' + this.series.yAxis.categories[this.point.y] + '</b><br>' +
          '<b>' + parameterValue +': ' + this.point.value.toLocaleString() + '</b>'
      }
    },

    series: series
  }

  return config
}

const getValue = (payload) => {
  const { id, value, data } = payload

  if (id && value) return value
  if (!id && !value) return null
  if (id && !value) {
    const match = _.find(data, { 'id': parseInt(id, 10) })

    return match ? match.value : null
  }
}

export default {
  getSeries,
  getAxisLabel,
  getInitConfig,
  getValue,
}
