const name = 'consumptionPeriod'

const types = {
  SET_GOOD: `${ name }/SET_GOOD`
}

const STATE = {
  id: '',
  value: ''
}

const reducer = (state = STATE, action) => {
  const { type, payload } = action

  switch (type) {
    case types.SET_GOOD: {
      const { id, value } = payload

      return {
        ...state,
        id,
        value
      }
    }
    default: return state
  }
}

export default {
  types,
  reducer
}
