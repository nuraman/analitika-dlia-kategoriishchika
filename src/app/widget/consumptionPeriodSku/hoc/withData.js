import React                from 'react'

import { connect }          from 'react-redux'
import { compose }          from 'redux'


import withGoodsList        from 'app/widget/graphql/withGoodList'


function withData(ComposedComponent) {
  function decorator(props) {
    return (
      <ComposedComponent { ...props } />
    )
  }

  function mapStatToProps(state) {
    const { menu: { period } } = state

    const { dateBegin } = period

    return {
      dateBegin,
      options: {
        variables: {
          dateBegin: dateBegin,
        }
      }
    }
  }

  return compose(
    connect(mapStatToProps),
    withGoodsList()
  )(decorator)
}

export default withData
