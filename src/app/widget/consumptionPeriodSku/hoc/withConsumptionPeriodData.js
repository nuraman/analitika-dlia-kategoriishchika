import React                from 'react'

import { connect }          from 'react-redux'
import { compose }          from 'redux'


import withGrf18            from 'app/widget/graphql/withGrf18'


function withData(ComposedComponent) {
  function decorator(props) {
    return (
      <ComposedComponent { ...props } />
    )
  }

  function mapStatToProps(state) {
    const { menu: { period }, consumptionPeriod } = state

    const { dateBegin } = period
    const { id, value } = consumptionPeriod

    return {
      value,
      options: {
        variables: {
          dateBegin,
          goodsId: id
        }
      }
    }
  }

  return compose(
    connect(mapStatToProps),
    withGrf18()
  )(decorator)
}

export default withData
