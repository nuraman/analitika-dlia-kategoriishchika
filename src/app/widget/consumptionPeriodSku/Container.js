import { CircularProgress } from 'material-ui/Progress'

import React, { Component } from 'react'

import { compose }          from 'redux'

import styled               from 'styled-components'


import Widget               from 'app/widget/styledComponent/widget/DivContainer'
import WidgetHeader         from 'app/widget/header/Container'
import BlockSkuList         from './SkuList'
import ConsumptionPeriod    from './ConsumptionPeriod'

import withData             from './hoc/withData'


const WigetContent = styled.div`
  display: flex;
  flex-direction: row;
  height: 100%;
  width: 100%;
`

const WidgetContentLeft = styled.div`
  display: block;
  width: 30%;
  height: 100%;
`

const WidgetContentRight = styled.div`
  display: block;
  width: 70%;
  height: 100%;
`


class Container extends Component {
  constructor() {
    super()

    this.state = {
      chart: {},
      config: {},
      data: {},
      dateBegin: '',
      description: '',
      order: [],
      openChart: false,
      refChart: {},
      title: '',
      widgetId: '',
      exportImage: false,
    }
  }


  componentWillMount() {
    const { data, order = [], dateBegin, title } = this.props

    const { caption, description, vidget } = title || {}

    if (order.length > 0) {
      return this.setState({ data, order, dateBegin, title: caption, description, widgetId: vidget })
    }
   }

  componentWillReceiveProps(nextProps) {
    const { data, order = [], title, dateBegin } = nextProps

    const { caption, description, vidget } = title || {}

    if (order.length > 0) {
      return this.setState({ data, order, title: caption, description, widgetId: vidget, dateBegin })
    }
  }

  openDiscount = ({ id }) => {
    const { onSelect } = this.props

    onSelect()
  }

  getRefChart = (refChart) => {
    this.setState({ refChart })
  }

  exportImage = () => {
    this.setState({ exportImage: true })

    setInterval(() => {
      this.setState({ exportImage: false })
    }, 3000)
  }
  //открытие описания виджета на будущее
  onOpenDescription = () => {

  }

  render() {
    const { data, order = [], widgetId, dateBegin, title, description, exportImage } = this.state

    const { loading } = this.props

    if (loading) return (
      <CircularProgress />
    )

    if (!loading && !order.length) return null

    return (
      <Widget>
        <WidgetHeader
          title={ title }
          description={ description }
          dateBegin={ dateBegin }
          widgetId={ widgetId }

          exportImage={ this.exportImage }
          onClickDescription={ this.onOpenDescription }
        />
        <WigetContent>
          <WidgetContentLeft>
            <BlockSkuList data={ data } order={ order } />
          </WidgetContentLeft>
          <WidgetContentRight>
            <ConsumptionPeriod
              getRefChart={ this.getRefChart }
              onSelect={ this.openDiscount }
              exportImage={ exportImage }
            />
          </WidgetContentRight>
        </WigetContent>
      </Widget>
    )
  }
}


export default compose(
  withData
)(Container)
