import React                from 'react'

import { compose }          from 'redux'
import { connect }          from 'react-redux'

import styled               from 'styled-components'


import reducer              from './reducer'


const Container = styled.div`
  display: block;
  width: 100%;
  height: 100%;
  
  background-color: #484848;
`

const List = styled.ul`
  padding: 20px 0px;
  margin: 0px;
  overflow: auto;
  height: calc(100% - 100px);
  
  &::-webkit-scrollbar
  {
    width: 9px;
  }
  
  &::-webkit-scrollbar-track
  {
    background: #7d7d7d;
    border: 3px solid transparent;
    background-clip: content-box;
  }
  
  &::-webkit-scrollbar-thumb
  {
    background: #73c7c5;
    height: 35px;
    border-radius: 5px solid #73c7c5;
    border: 1px solid #73c7c5;
  }
`

const ListItem = styled.li`
  cursor: pointer;
  
  margin-bottom: 5px;
  
  &:hover {
    background-color: grey;
  }
`

const Div = styled.div`
  margin: 0px 20px;
`

const Label = styled.label`
  font-family: HelveticaLight;
  font-size: 16px;
  color: #fff;
  
  cursor: pointer;
`

function BlockSkuList(props) {
  const { data, order = [] } = props

  if (!order.length) {
    return <Container />
  }

  const onHandleClick = ({ id, value }) => {
    const { dispatch } = props

    dispatch({
      type: reducer.types.SET_GOOD,
      payload: { id, value }
    })
  }

  return (
    <Container>
      <List>
        {
          order.map((item, index) => {
            const { goodsId, goodsName } = data[item]

            return (
              <ListItem key={ index } onClick={ () => onHandleClick({ id: goodsId, value: goodsName }) }>
                <Div>
                  <Label>{ goodsName }</Label>
                </Div>
              </ListItem>
            )
          })
        }
      </List>
    </Container>
  )
}

export default compose(
  connect()
)(BlockSkuList)
