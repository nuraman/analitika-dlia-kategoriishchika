import moment               from 'moment'


function Chart() {
  this.data = {}
  this.order = []
  this.height = ''
  this.name = ''
}

Chart.prototype =  {
  set options(payload) {
    const { data, order, height, name } = payload

    this.data = data
    this.order = order
    this.height = height
    this.name = name
  }
}

Chart.prototype.getCategories = function () {
  const categoryList = this.order.reduce((categoryList, id) => {
    const { date } = this.data[id]

    return [ ...categoryList, moment(date, 'DD.MM.YYYY').toDate().getTime() ]
  }, [])

  return categoryList
}

Chart.prototype.getSeries = function () {
  const goodEndList = []
  const goodWillEndList = []
  const goodExistList = []

  this.order.forEach((id) => {
    const { count1, count2, count3 } = this.data[id]

    goodEndList.push(count1)
    goodWillEndList.push(count2)
    goodExistList.push(count3)
  })


  const series = [{
    name: 'Товар закончился',
    data: goodEndList,
    id: 1
  },{
    name: 'Товар скоро закончится',
    data: goodWillEndList,
    id: 2,
  },{
    name: 'Товар есть',
    data: goodExistList,
    id: 3
  }]


  return series
}

Chart.prototype.getConfig = function ({ onSelect }) {
  const series = this.getSeries()
  const categories = this.getCategories()


  const config = {
    chart: {
      type: 'column',
    },
    title: {
      text: this.name
    },
    navigation: {
      buttonOptions: {
        enabled: false,
      }
    },
    xAxis: {
      categories: categories,
      title: {
        style: {
          fontFamily: 'HelveticaRegular',
        },
     },
      labels: {
        formatter: function() {
          return moment(this.value).format('DD MMMM YYYY')
        }
      }
    },
    yAxis: {
      title: {
        text: 'Количество'
      }
    },
    legend: {
      itemStyle: {
        fontFamily: 'HelveticaBold',
        fontSize: '14px',
      }
    },
    tooltip: {
      useHTML: true,
      formatter: function() {
        return `<div>Количество покупателей: ${this.y}</div>`
      },
      style: {
        fontFamily: 'HelveticaRegular',
        fontSize: '14px'
      },
    },
    plotOptions: {
      column: {
        events: {
          click: function() {
            const { userOptions } = this
            const { id } = userOptions
            onSelect && onSelect({ id })
          }
        },
        cursor: 'pointer',
      }
    },
    series: series
  }

  return config
}

export default Chart
