import types                from 'engine/types'

const queryNameMap = {
  GOOD_LIST: 'grf18Goods',
  GRF18: 'grf18'
}

const inputNameMap = {
  GOODS_CATEGORY:                     'goodsCategory',
  DATE_BEGIN:                         'dateBegin',
  GOODS_ID:                           'goodsId'
}

const resultNameMap = {
  GOODS_ID:                           'goodsId',
  GOODS_NAME:                         'goodsName',

  DATE:                               'date',
  COUNT1:                             'count1',
  COUNT2:                             'count2',
  COUNT3:                             'count3'
}

const titleNameMap = {
  CAPTION:                            'caption',
  WIDGET:                             'vidget',
  DESCRIPTION:                        'description',
}

const queryMap = [
  {
    queryName: queryNameMap.GOOD_LIST,
    inputList: [
      {
        name: inputNameMap.DATE_BEGIN,
        type: types.String,
      },
    ],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        }
      ],
      result: [
        {
          name: resultNameMap.GOODS_ID,
          type: types.Int,
        },
        {
          name: resultNameMap.GOODS_NAME,
          type: types.String,
        },
      ],
      title: [
        {
          name: titleNameMap.CAPTION,
          type: types.String,
        },
        {
          name: titleNameMap.DESCRIPTION,
          type: types.String
        },
        {
          name: titleNameMap.WIDGET,
          type: types.String
        }
      ]
    }
  },
  {
    queryName: queryNameMap.GRF18,

    inputList: [
      {
        name: inputNameMap.DATE_BEGIN,
        type: types.String,
      },
      {
        name: inputNameMap.GOODS_ID,
        type: types.Int,
      }
    ],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        }
      ],
      result: [
        {
          name: resultNameMap.DATE,
          type: types.String,
        },
        {
          name: resultNameMap.COUNT1,
          type: types.Int,
        },
        {
          name: resultNameMap.COUNT2,
          type: types.Int,
        },
        {
          name: resultNameMap.COUNT3,
          type: types.Int,
        },
      ],
      title: [
        {
          name: titleNameMap.CAPTION,
          type: types.String,
        },
        {
          name: titleNameMap.DESCRIPTION,
          type: types.String
        },
        {
          name: titleNameMap.WIDGET,
          type: types.String
        }
      ]
    }
  }
]

export default {
  queryMap,
  name: queryNameMap.GOOD_LIST,
  queryNameMap,
  inputNameMap
}
