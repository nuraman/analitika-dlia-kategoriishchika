import React, { Component } from 'react'

import ReactHighcharts      from 'react-highcharts'

import { connect }          from 'react-redux'
import { compose }          from 'redux'


import styled               from 'styled-components'


import Chart                from './Chart'
import withData             from './hoc/withConsumptionPeriodData'


const EmptyContainer = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  
  align-items: center;
  justify-content: space-around;
`

const Label = styled.div`
  font-family: HelveticaRegular;
  font-size: 18px;
  color: #434343;
`

class Container extends Component {
  constructor() {
    super()

    this.state = {
      config: {},
      data: {},
      order: [],
    }
  }

  componentWillReceiveProps(nextProps) {
    const { value, data, order = [], exportImage } = nextProps

    if (exportImage) {
      this.chart1.getChart().exportChart({ filename: 'виджет' }, null)
    }

    if (order.length > 0) {
      const config = this.getConfig({ value, data, order })

      return this.setState({
        config,
        data,
        order,
      })
    }

    this.setState({
      config: {},
      data,
      order,
    })
  }

  getConfig = ({ value, data, order }) => {
    const chart = new Chart()

    chart.options = { data, order, height: 300, name: value }

    return chart.getConfig({ onSelect: this.openDiscount })
  }

  openDiscount = ({ id }) => {
    const { onSelect } = this.props

    onSelect({ id })
  }

  render() {
    const { config, order = [] } = this.state

    const { loading } = this.props

    if (!order.length && !loading) return (<EmptyContainer><Label>Выберите товар</Label></EmptyContainer>)

    if (config.series && config.series.length > 0)
      return (<ReactHighcharts config={ config } ref={(chart) => { this.chart1 = chart } } />)

    return null
  }
}

const mapStateToProps = (state) => {
  const { menu: { period } } = state

  const { dateBegin } = period

  return {
    dateBegin
  }
}


export default compose(
  connect(mapStateToProps),
  withData
)(Container)
