import IconButton           from 'material-ui/IconButton'
import NavIcon              from 'material-ui-icons/Menu'

import React                from 'react'

import styled               from 'styled-components'


const Container = styled.div`
  display: table-cell;
  text-align: right;
  vertical-align: middle;
`

const DropdownContent = styled.div`
  position: absolute;
  display: none;
  width: 200px;

  left: 0px;
  margin-left: -150px;
    
  z-index: 500;
  
  background-color: #fff;
  box-shadow: 0px 2px 20px 0px rgba(19, 92, 99, 0.16);
`

const Ul = styled.ul`
  list-style-type: none;
  margin: 15px 0px;
`

const Li = styled.li`
  margin-right: 30px;
  
  text-align: right;
  
  font-family: HelveticaRegular;
  font-size: 15px;
  color: #000;
  
  white-space: nowrap;
  
  &:hover {
    color: #958033;
  }
`

const Link = styled.a`
  text-decoration: none;
  color: #000;
  &:hover {
    color: #958033;
  }
`

const Dropdown = styled.div`
  position: relative;
  
  &:hover ${ DropdownContent } {
    display: block;
  }
`

export default (props) => {
  const { openWidgetDescription, exportImage, url } = props

  const onHandleClickWidgetDescription = () => openWidgetDescription()

  const onHandleClickExportImage = () => {
    if (exportImage) exportImage()
  }

  return (
    <Container>
      <IconButton>
        <Dropdown>
          <NavIcon />
          <DropdownContent>
            <Ul>
              <Li onClick={ onHandleClickWidgetDescription }>
                Описание виджета
              </Li>
              <Li>
                <Link href={ url }>
                  Скачать в excell
                </Link>
              </Li>
              <Li onClick={ onHandleClickExportImage }>
                Скачать в jpeg
              </Li>
            </Ul>
          </DropdownContent>
        </Dropdown>
      </IconButton>
    </Container>
  )
}
