import React                from 'react'


import styled               from 'styled-components'


import LabelParameter       from 'app/widget/LabelParameter'


const Container = styled.div`
  display: table-cell;
  vertical-align: middle;
  padding-left: 20px;
  width: ${ props=>props.width }px;
  
  font-size: 22px;
  font-family: HelveticaRegular;
  &:hover {
    > div: nth-child(2) {
      display:  flex;
   }
  }
}
`

const Tooltip = styled.div`
  position: absolute; 
  top: 60px;
  left: 0px;
  display: none;
  box-sizing: border-box;
  width: 300px;
  z-index: 1000;
  padding: 15px;
  border: 1px solid #dddddd;
  background-color: #fff;
  
  &:before, &:after {
    position: absolute;
    content: "";
    left: 20px;
    width: 0;
    height: 0;
  }
  
  &:before {
    bottom: 100%;
    border-right: 9px solid transparent;
    border-bottom: 14px solid #dddddd;
    border-left: 9px solid transparent;
  }
  &:after {
    bottom: calc(100% - 2px);
    border-right: 9px solid transparent;
    border-bottom: 14px solid #fff;
    border-left: 9px solid transparent;
  }
`

const TooltipText = styled.div`
 display: flex;
 flex-direction: row; 
 font-size: 14px;
 font-family: HelveticaRegular;
`

const IcoHelper = styled.div`
  min-width: 20px;
  width: 20px;
  height: 18px;
  fill: #9e9e9e;
  
  background-image: url(assets/helper.svg);
  background-size: cover;
`


export default (props) => {
  const { title, id, description } = props

  return (
    <Container>
      { title } <LabelParameter id={ id } />
      <Tooltip>
        <TooltipText>
          <IcoHelper /> { description }
        </TooltipText>
      </Tooltip>
    </Container>
  )
}
