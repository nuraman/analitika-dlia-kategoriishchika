import React                from 'react'

import styled               from 'styled-components'


import BlockTitle           from './Title'
import BlockHelper          from './Helper'


const Container = styled.div`
  display: block;
  height: 60px;
  width: inherit;
  border-bottom: 1px solid rgba(0, 0, 0, 0.12);
`

const Content = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  
  height: 100%;
  width: 100%;
`

const Flex = styled.div`
  display: flex;
  flex: 1;
`

const url = 'http://mindcloud.pro/kpl/exceldata.php'

export default (props) => {
  const { id, description, title, exportImage, widgetId, dateBegin } = props

  const onHandleDescription = () => {
    const { onClickDescription } = props || {}

    onClickDescription && onClickDescription()
  }

  const getUrlWithParameter = () => {
    return `${ url }?id=${ widgetId }&dateBegin=${ dateBegin }`
  }

  const onHandleExportImage = () => {
    if (exportImage) exportImage()
  }

  return (
    <Container>
      <Content>
        <BlockTitle title={ title } id={ id } description={ description } />
        <Flex />
        <BlockHelper
          openWidgetDescription={ onHandleDescription }
          exportImage={ onHandleExportImage }
          url={ getUrlWithParameter() }
        />
      </Content>
    </Container>
  )
}


