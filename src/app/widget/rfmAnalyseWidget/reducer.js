const name = 'rfmAnalyse'

const types = {
  SET_PARAMETER: `${ name }/SET_PARAMETER`,
}

const STATE = {
  brandOut: 0,
  categoryOut: 0,
  networkOut: 0,
}

const reducer = (state = STATE, action) => {
  const { type, payload } = action

  switch(type) {
    case types.SET_PARAMETER: {
      const { brandOut, categoryOut, networkOut } = payload

      return {
        ...state,
        brandOut,
        categoryOut,
        networkOut
      }
    }
    default: {
      return state
    }
  }
}

export default {
  types,
  reducer,
}

