import React                from 'react'

import styled               from 'styled-components'


import BlockTitle           from 'app/widget/header/Title'
import BlockFilter          from './Filter'
import BlockHelper          from 'app/widget/header/Helper'


const Container = styled.div`
  display: block;
  height: 60px;
  width: inherit;
  border-bottom: 1px solid rgba(0, 0, 0, 0.12);
`

const Content = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  
  height: 100%;
  width: 100%;
`

const FILTER_MAP = {
  CHECKED_BRAND_OUT: 1,
  CHECKED_CATEGORY_OUT: 2,
  CHECKED_NETWORK_OUT: 3,
}

const url = 'http://mindcloud.pro/kpl/exceldata.php'

export default (props) => {
  const {
    checkedBrandOut,
    checkedCategoryOut,
    checkedNetworkOut,
    description,
    title,
    widgetId,
    dateBegin,
    onOpenDescription,
    exportImage
  } = props

  const onSelectFilter = (id) => {
    const { onSelect } = props

    onSelect(id)
  }

  const getUrlWithParameter = () => {
    return `${ url }?id=${ widgetId }&dateBegin=${ dateBegin }`
  }

  const onHandleDescription = () => {
    onOpenDescription && onOpenDescription()
  }

  const onHandleExportImage = () => {
    if (exportImage) exportImage()
  }

  return (
    <Container>
      <Content>
        <BlockTitle
          title={ title }
          description={ description }
        />
        <BlockFilter
          checked={ checkedBrandOut }
          onSelectFilter={ onSelectFilter }
          id={ FILTER_MAP.CHECKED_BRAND_OUT }
          name='Отобразить ушедших из бренда'
        />
        <BlockFilter
          checked={ checkedCategoryOut }
          onSelectFilter={ onSelectFilter }
          id={ FILTER_MAP.CHECKED_CATEGORY_OUT }
          name='Отобразить ушедших из категории'
        />
        <BlockFilter
          checked={ checkedNetworkOut }
          onSelectFilter={ onSelectFilter }
          id={ FILTER_MAP.CHECKED_NETWORK_OUT }
          name='Отобразить ушелших из сети'
        />
        <BlockHelper
          openWidgetDescription={ onHandleDescription }
          exportImage={ onHandleExportImage }
          url={ getUrlWithParameter() }
        />
      </Content>
    </Container>
  )
}
