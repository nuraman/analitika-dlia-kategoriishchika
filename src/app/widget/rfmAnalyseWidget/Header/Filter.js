import React                from 'react'

import styled               from 'styled-components'


const Container = styled.div`
  display: table-cell;
  vertical-align: middle;
`


const Content = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  height: 100%;
`

const Input = styled.input`
  display: none;
  
  &:checked {
    display: block; 
  }
`

const LabelNewCheck = styled.label`
  position: relative;
  width: 18px;
  min-width: 18px;
  height: 18px;
  margin-right: 10px;
  border: 1px solid #48b9bf;
  cursor: pointer;
  
  &:before {
    display: ${ props=>props.checked ? 'block' : 'none' };
    content: "";
    position: absolute;
    width: 12px;
    height: 12px;
    top: 3px;
    left: 3px;
    background-color: #48b9bf;
  }
`

const Label = styled.label`
  margin-bottom: -2px;
  font-weight: 300;
  font-size: 14px;
  font-family: HelveticaRegular;
  color: #000000;
  cursor: pointer;
`

export default (props) => {
  const { checked, onSelectFilter, id, name } = props

  const onHandleClick = () => {
    onSelectFilter(id)
  }

  return (
    <Container>
      <Content>
        <Input type='checkbox' />
        <LabelNewCheck
          checked={ checked }
          onClick={ onHandleClick }
        />
        <Label
          onClick={ onHandleClick }
        >
          { name }
        </Label>
      </Content>
    </Container>
  )
}
