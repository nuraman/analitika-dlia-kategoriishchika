import types                from 'engine/types'

const inputNameMap = {
  DEL_PROVIDERS:            'delProviders',
  DEL_CATEGORIES:           'delCategories',
  DEL_SELLERS:              'delSellers',
}

const queryNameMap = {
  GRF11:                    'grf11',
}

const resultNameMap = {
  COLOR:                     'color',
  X:                         'X',
  Y:                         'Y',
  VALUE:                     'value',

  NAME:                      'name',
  TOTAL:                     'total',
}

const titleNameMap = {
  CAPTION:                   'caption',
  WIDGET:                    'vidget',
  DESCRIPTION:               'description'
}

const queryMap = [
  {
    queryName: 'grf11',
    inputList: [
      {
        name: inputNameMap.DEL_PROVIDERS,
        type: types.Int,
      },
      {
        name: inputNameMap.DEL_CATEGORIES,
        type: types.Int,
      },
      {
        name: inputNameMap.DEL_SELLERS,
        type: types.Int,
      },
    ],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        }
      ],
      result: [
        {
          name: resultNameMap.X,
          type: types.String,
        },
        {
          name: resultNameMap.Y,
          type: types.String,
        },
        {
          name: resultNameMap.VALUE,
          type: types.Int,
        },
        {
          name: resultNameMap.COLOR,
          type: types.String
        },
        {
          name: resultNameMap.NAME,
          type: types.String
        },
        {
          name: resultNameMap.TOTAL,
          type: types.String,
        }
      ],
      title: [
        {
          name: titleNameMap.CAPTION,
          type: types.String,
        },
        {
          name: titleNameMap.WIDGET,
          type: types.String,
        },
        {
          name: titleNameMap.DESCRIPTION,
          type: types.String
        }
      ]
    }
  },
]

const idMap = {
  OPEN_DISCOUNT: 0,
  OPEN_CHECK_CONTENT: 1,
  OPEN_SKU: 2,
  OPEN_SKU_RATING: 3,
}

export default {
  queryMap,
  inputNameMap,
  name: queryNameMap.GRF11,
  idMap
}
