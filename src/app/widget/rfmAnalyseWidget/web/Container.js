import Heatmap              from 'highcharts/modules/heatmap'

import React, { Component } from 'react'

import ReactHighchart       from 'react-highcharts'

import sizeMe               from 'react-sizeme'

import { compose }          from 'redux'


import ProgressBar          from 'app/analysis/web/WidgetProgressBar'

import EmptyWidget          from 'app/widget/WidgetEmpty'

import  reducer             from 'app/widget/rfmAnalyseWidget/reducer'
import  reducerDiscount     from 'app/discount/reducer'

import withData             from 'app/widget/rfmAnalyseWidget/hoc/withData'

import providerChartConfig  from './providerChartConfig'
import WidgetHeader         from '../Header/WidgetHeader'
import WidgetContainer      from 'app/widget/styledComponent/widget/DivContainer'


Heatmap(ReactHighchart.Highcharts)


class RfmLogic extends Component {
  constructor() {
    super()

    this.state = {
      config: {},
      description: '',
      dateBegin: '',
      seriesData: [],
      title: '',

      checkedBrandOut: false,
      checkedCategoryOut: false,
      checkedNetworkOut: false,

      widgetId: ''
    }
  }

  onInitConfig = ({ seriesData, xAxisLabel, yAxisLabel, height, onSelectDiscount }) => {
    const config = providerChartConfig({ seriesData, xAxisLabel, yAxisLabel, height, onSelectDiscount })

    return config
  }

  onRunFilter = () => {
    const { checkedBrandOut, checkedCategoryOut, checkedNetworkOut } = this.state

    const { dispatch } = this.props

    dispatch({
      type: reducer.types.SET_PARAMETER,
      payload: {
        brandOut: checkedBrandOut ? 1 : 0,
        categoryOut: checkedCategoryOut ? 1 : 0,
        networkOut: checkedNetworkOut ? 1 : 0,
      }
    })
  }

  onSelectFilter = (id) => {
    if (id === 1) this.setState({ checkedBrandOut: !this.state.checkedBrandOut })
    if (id === 2) this.setState({ checkedCategoryOut: !this.state.checkedCategoryOut })
    if (id === 3) this.setState({ checkedNetworkOut: !this.state.checkedNetworkOut })

    setTimeout(() => {
      this.onRunFilter()
    }, 4000)
  }

  onSelectDiscountCallback = ({ color }) => {
    const { dispatch, brandBuyer, categoryOut, networkOut, onOpenModal } = this.props

    const param2 = `${ brandBuyer }, ${ categoryOut }, ${ networkOut }`

    dispatch({
      type: reducerDiscount.types.SET_SUCCESS,
      payload: {
        widget: '11',
        param1: color,
        param2: param2,
      }
    })

    onOpenModal()
  }

  componentWillReceiveProps(nextProps) {
    const {
      brandOut,
      categoryOut,
      description,
      isProvider,
      networkOut,
      order = [],
      title = {},
      seriesData,
      size: { height },
      xAxisLabel,
      yAxisLabel,
      widgetId,
      dateBegin
    } = nextProps

    if (order.length > 0) {
      const config = this.onInitConfig({
        isProvider,
        height,
        onSelectDiscount: this.onSelectDiscountCallback,
        seriesData,
        xAxisLabel,
        yAxisLabel,
      })

      this.setState({
        checkedBrandOut: brandOut,
        checkedCategoryOut: categoryOut,
        checkedNetworkOut: networkOut,
        config,
        description,
        dateBegin,
        seriesData,
        title,
        widgetId
      })
    }

    if (!order.length) this.setState({ title, description, widgetId, dateBegin})
  }
  /*
  Метод открытия описания виджета
   */
  onClickDescription = () => {

  }

  exportImage = () => {
    const { title } = this.state

    this.chart.getChart().exportChart({ filename: title ? title : 'виджет' }, null)
  }

  render() {
    const {
      config,

      checkedBrandOut,
      checkedNetworkOut,
      checkedCategoryOut,
      dateBegin,
      description,
      seriesData,
      title,
      widgetId
    } = this.state

    const { loading } = this.props

    if (loading) return (
      <ProgressBar title='Сегментация покупателей' />
    )

    if (!loading && seriesData.length === 0) return <EmptyWidget title={ title } />

    return(
      <WidgetContainer>
        <WidgetHeader
          checkedBrandOut={ checkedBrandOut }
          checkedCategoryOut={ checkedCategoryOut }
          checkedNetworkOut={ checkedNetworkOut }
          description={ description }
          dateBegin={ dateBegin }
          title={ title }
          widgetId={ widgetId }

          onOpenDescription={ this.onClickDescription }
          exportImage={ this.exportImage }
          onSelect={ this.onSelectFilter }
        />
        <ReactHighchart config={ config } ref={ (chart) => { this.chart = chart } }/>
      </WidgetContainer>
    )
  }
}

export default compose(
  sizeMe({ monitorHeight: true, monitorWidth: true }),
  withData,
)(RfmLogic)
