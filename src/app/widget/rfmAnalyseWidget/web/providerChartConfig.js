export default ({ seriesData, xAxisLabel, yAxisLabel, height, onSelectDiscount }) => {
  const heightChart = `${ height - 60 }px`

  const config = {
    chart: {
      type: 'heatmap',
      height: heightChart,
    },
    title: {
      text: '',
    },
    tooltip: {
      style: {
        display: "none",
      }
    },
    xAxis: {
      categories: xAxisLabel,
      labels: {
        enabled: true,
        style: {
          fontFamily: 'HelveticaRegular',
          fontSize: '12px',
        }
      },
      title: {
        align: 'low',
        text: 'Дней с последней покупки'
      },
      opposite: true,
      reversed: true,
      lineWidth: 0,
    },
    yAxis: {
      categories: yAxisLabel,
      labels: {
        enabled: true,
        style: {
          fontFamily: 'HelveticaRegular',
          fontSize: '12px',
        }
      },
      gridLineWidth: 0,
      title: {
        align: 'low',
        text: 'Число покупок за всю историю'
      },
      reversed: false,
    },
    navigation: {
      buttonOptions: {
        enabled: false,
      }
    },
    legend: {
      enabled: false,
    },
    series: [{
      name: '',
      data: seriesData,
      dataLabels: {
        enabled: true,
        formatter: function() {
          if (this.point.name !== null) {
            const str = `<b>${ this.point.name }</b><br/>${ this.point.total }`
            return str
          }
        },
        style: {
          fontFamily: 'HelveticaRegular',
          fontSize: '10px',
          textTransform: 'capitalize',
        }
      }
    }],
    plotOptions: {
      heatmap:{
        dataLabels: {
          style: {
            fontFamily: 'HelveticaRegular',
            fontSize: '10px',
            textTransform: 'capitalize',
          }
        },
        point: {
          events: {
            click: function() {
              onSelectDiscount({ color: this.color })
            }
          }
        },
        cursor: 'pointer',
      }
    }
  }

  return config
}
