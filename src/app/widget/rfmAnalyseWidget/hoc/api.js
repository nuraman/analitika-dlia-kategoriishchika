const getSeriesDataForChart = (payload) => {
  const { data, order } = payload

  let i = 0, j = 0

  const seriesData = order.reduce((seriesData, id, index) => {
    const { value, color, name, total } = data[id] || {}

    if (index >= 0 && index < 6) {
      j = 0

      seriesData.push({
        x: index,
        y: j,
        value,
        color: `#${ color }`,
        name,
        total
      })
    }

    if (index >= 6 && index < 12) {
      j = 1
      i = index - 6

      seriesData.push({ x: i, y: j, value, color: `#${ color }`, name, total })
    }

    if (index >= 12 && index < 18) {
      j = 2
      i = index - 12

      seriesData.push({ x: i, y: j, value, color: `#${ color }`, name, total })
    }

    if (index >= 18 && index < 24) {
      j = 3
      i = index - 18

      seriesData.push({ x: i, y: j, value, color: `#${ color }`, name, total })
    }

    if (index >= 24 && index < 30) {
      j = 4
      i = index - 24
      seriesData.push({ x: i, y: j, value, color: `#${ color }`, name, total })
    }

    if (index >= 30 && index < 36) {
      j = 5
      i = index - 30
      seriesData.push({ x: i, y: j, value, color: `#${ color }`, name, total })
    }

    return [ ...seriesData ]
  }, [])

  return seriesData
}

const getAxisLabel = (payload) => {
  const { data, order } = payload

  const xAxisLabel = []
  const yAxisLabel = []

  order.forEach((id, index) => {
    const { X, Y } = data[id]

    if (index <= 5) {
      xAxisLabel.push(X)
    }

    if (index === 0 || index === 6 || index === 12 || index === 18 || index === 24 || index === 30) {
      yAxisLabel.push(Y)
    }
  })

  return { xAxisLabel, yAxisLabel }
}

export default {
  getAxisLabel,
  getSeriesDataForChart
}
