import React, { Component } from 'react'
import { connect }          from 'react-redux'
import { compose }          from 'redux'

import config               from 'app/widget/rfmAnalyseWidget/config'

import withGrf11            from 'app/widget/graphql/withGrf11'
import api                  from 'app/widget/rfmAnalyseWidget/hoc/api'


function withData(ComposedComponent) {
  class Widget extends Component {
    constructor() {
      super()

      this.state = {
        seriesData: [],
        xAxisLabel: [],
        yAxisLabel: [],
        title: '',
        description: '',
        widgetId: ''
      }
    }

    componentWillReceiveProps(nextProps) {
      const { data = {}, order = [], title = {} } = nextProps

      const { caption, description, vidget } = title

      if (order.length > 0) {
        const seriesData = api.getSeriesDataForChart({ data, order })
        const { xAxisLabel, yAxisLabel } = api.getAxisLabel({ data, order })

        this.setState({
          description,
          seriesData,
          title: caption,
          xAxisLabel,
          yAxisLabel,
          widgetId: vidget
        })
      }
    }

    render() {
      return (
        <ComposedComponent { ...this.props } { ...this.state } />
      )
    }
  }

  function mapSateToProps(state) {
    const { menu, rfmAnalyse, user } = state

    const { period: { dateBegin } } = menu

    const { brandOut, categoryOut, networkOut } = rfmAnalyse

    const { inputNameMap } = config

    const { userLogin: { data, order } } = user
    const { isProvider } = data[order[0]] || {}

    return {
      brandOut,
      categoryOut,
      networkOut,
      isProvider,
      dateBegin,
      options: {
        variables: {
          [inputNameMap.DEL_PROVIDERS]: brandOut,
          [inputNameMap.DEL_CATEGORIES]: categoryOut,
          [inputNameMap.DEL_SELLERS]: networkOut,
        }
      }
    }
  }

  return compose(
    connect(mapSateToProps),
    withGrf11(),
  )(Widget)
}

export default withData
