import React                from 'react'

import styled               from 'styled-components'


const DivDeltaIcon = styled.div`
  display: inline-block;
  width: 50px;
  padding-left: ${ props => props.paddingLeft }px;
`

const DivCategoryIcon = styled.div`
  display: inline-block;
  width: 50px;
  padding-left: ${ props => props.paddingLeft }px;
`

const DivBlockRow = styled.div`
  display: flex;
  padding: 0 40px;
  border-bottom: 1px solid #f0f0f0;
`

const DivBlockColumn = styled.div`
  width: 100%;
  max-width: 280px;
  margin: 0 20px;
  padding: 15px 0;
`

const DivBlockName = styled.div`
  font-family: HelveticaLight;
  font-size: 14px;
  color: #949494;
`

const DivBlockValue = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  font-family: HelveticaLight;
  margin: 5px 0;
  font-size: 18px;
`

const DivBlockIco = styled.div`
  width: 20px;
  min-width: 20px;
  height: 20px;
  margin-right: 20px;
  padding: 15px 0;
`

const DivIco = styled.div`
  min-width: 20px;
  width: 20px;
  height: 18px;
  fill: #9e9e9e;
  
  background-image: url(${ props => props.path });
  background-size: cover;
`

const DivIcoDynamic = styled.div`
  min-width: 20px;
  width: 20px;
  height: 13px;
  fill: #9e9e9e;
  
  background-image: url(${ props => props.path });
  background-size: cover;
`

function BlockMiddlePeriod(props) {

  const { key, value, delta = 0, category } = props

  const renderDeltaMark = ({ delta }) => {
    if (delta > 0) return <DivIcoDynamic path='assets/customerParameter/triangleUp.svg' />
    if (delta < 0) return <DivIcoDynamic path='assets/customerParameter/triangleDown.svg' />
    if (delta === 0) return null
  }

  const renderCategoryMark = ({ category }) => {
    switch (category) {
      case 'Низкая': return <DivIcoDynamic path='assets/customerParameter/circleLow.svg' />
      case 'Средняя': return <DivIcoDynamic path='assets/customerParameter/circleMiddle.svg' />
      case 'Высокая': return <DivIcoDynamic path='assets/customerParameter/circleHigh.svg' />
      default: return null
    }
  }

  const deltaPercent = delta === null ? 0 : `${ delta }%`

  return (
    <DivBlockRow key={ key }>
      <DivBlockIco>
        <DivIco path='assets/customerParameter/time.svg' />
      </DivBlockIco>
      <DivBlockColumn>
        <DivBlockName>
          Средний интервал между покупками в днях
        </DivBlockName>
        <DivBlockValue>
          { value ? value : 'Нет данных' }
        </DivBlockValue>
      </DivBlockColumn>
      <DivBlockColumn>
        <DivBlockName>
          Динамика среднего интервала
        </DivBlockName>
        <DivBlockValue>
          { deltaPercent ? deltaPercent : 'Нет данных' }
          <DivDeltaIcon
            paddingLeft={ 5 }
          >
            { renderDeltaMark({ delta }) }
          </DivDeltaIcon>
        </DivBlockValue>
      </DivBlockColumn>
      <DivBlockColumn>
        <DivBlockName>
          Категория по интервалу между покупками
        </DivBlockName>
        <DivBlockValue>
          { category }
          <DivCategoryIcon paddingLeft={ 5 }>
            { renderCategoryMark({ category }) }
          </DivCategoryIcon>
        </DivBlockValue>
      </DivBlockColumn>
    </DivBlockRow>
  )
}

export default BlockMiddlePeriod
