import React                from 'react'

import styled               from 'styled-components'


const DivBlockRow = styled.div`
  display: flex;
  padding: 0 40px;
  border-bottom: 1px solid #f0f0f0;
`

const DivBlockColumn = styled.div`
  width: 100%;
  max-width: 280px;
  margin: 0 20px;
  padding: 15px 0;
`

const DivBlockName = styled.div`
  font-family: HelveticaLight;
  font-size: 14px;
  color: #949494;
`

const DivBlockValue = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  font-family: HelveticaLight;
  margin: 5px 0;
  font-size: 18px;
`

const DivBlockNameRed = DivBlockName.extend`
  color: #bf1334;
`

const DivBlockNameGreen = DivBlockName.extend`
  color: #00977c;
`

const DivBlockValueRed = DivBlockValue.extend`
  color: #bf1334;
`

const DivBlockValueGreen = DivBlockValue.extend`
  color: #00977c;
`

const DivBlockIco = styled.div`
  width: 20px;
  min-width: 20px;
  height: 20px;
  margin-right: 20px;
  padding: 15px 0;
`

const DivIco = styled.div`
  min-width: 20px;
  width: 20px;
  height: 18px;
  fill: #9e9e9e;
  
  background-image: url(${ props => props.path });
  background-size: cover;
`


function BlockMinSumCount(props) {
  const { key, minValue, maxValue } = props

  return (
    <DivBlockRow key={ key }>
      <DivBlockIco>
        <DivIco path='assets/customerParameter/product.svg' />
      </DivBlockIco>
      <DivBlockColumn>
        <DivBlockNameRed>
          Минимальная сумма покупок
        </DivBlockNameRed>
        <DivBlockValueRed>
          { minValue ? minValue.toLocaleString() : 'Нет данных' }
        </DivBlockValueRed>
      </DivBlockColumn>
      <DivBlockColumn>
        <DivBlockNameGreen>
          Максимальная сумма покупок
        </DivBlockNameGreen>
        <DivBlockValueGreen>
          { maxValue ? maxValue.toLocaleString() : 'Нет данных' }
        </DivBlockValueGreen>
      </DivBlockColumn>
    </DivBlockRow>
  )
}

export default BlockMinSumCount
