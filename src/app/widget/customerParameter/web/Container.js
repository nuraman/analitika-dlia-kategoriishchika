import React, { Component } from 'react'

import styled               from 'styled-components'


import Widget               from 'app/widget/styledComponent/widget/DivContainer'

import ProgressBar          from 'app/analysis/web/WidgetProgressBar'
import EmptyWidget          from 'app/widget/WidgetEmpty'

import withData             from 'app/widget/customerParameter/hoc/withData'
import WidgetHeader         from 'app/widget/header/Container'

import BlockCheckAverage    from 'app/widget/customerParameter/web/CheckAverage'
import BlockMiddlePeriod    from 'app/widget/customerParameter/web/MiddlePeriod'
import BlockCheckCount      from 'app/widget/customerParameter/web/CheckCount'
import BlockSumCount        from 'app/widget/customerParameter/web/SumCount'
import BlockMinSumCount     from 'app/widget/customerParameter/web/MinSumCount'
import BlockMinPeriod       from 'app/widget/customerParameter/web/MinPeriod'


const WidgetContent = styled.div`
  padding: 20px 0;
`

const rowType = {
  CHECK_AVERAGE: 1,
  MIDDLE_PERIOD: 2,
  CHECK_COUNT: 3,
  SUM_COUNT: 4,
  MIN_SUM_COUNT: 6,
  MIN_PERIOD: 8,
}


class CustomerParameter extends Component {
  constructor() {
    super()

    this.state = {
      data: {},
      order: [],

      title: '',
      description: '',
    }
  }

  componentWillReceiveProps(nextProps) {
    const { data = {}, order = [], title = {}, dateBegin } = nextProps

    const { caption, description, vidget } = title

    if (order.length > 0) {
      return this.setState({
        data,
        order,
        title: caption,
        description,
        widgetId: vidget,
        dateBegin
      })
    }

    this.setState({
      title: caption,
      description,
      dateBegin,
      widgetId: vidget
    })
  }

  componentWillMount() {
    const { data = {}, order = [], title = {}, dateBegin } = this.props

    const { caption, description, vidget } = title

    if (order.length > 0) {
      return this.setState({
        data,
        dateBegin,
        order,
        title: caption,
        description,
        widgetId: vidget,
      })
    }

    this.setState({
      title: caption,
      description,
      widgetId: vidget,
      dateBegin
    })
  }

  render() {
    const { data = {}, order = [], title, description, widgetId, dateBegin } = this.state

    const { loading } = this.props

    if (loading) return <ProgressBar title='Параметры потребления' />

    if (!loading && order.length === 0) return <EmptyWidget title={ title } />

    return (
      <Widget>
        <WidgetHeader
          description={ description }
          dateBegin={ dateBegin }
          title={ title }
          widgetId={ widgetId }
        />
        <WidgetContent>
          {
            order.map((item) => {
              const { id, value, delta, category } = data[item] || {}

              if (id == rowType.CHECK_AVERAGE) return <BlockCheckAverage key={ id } value={ value } delta={ delta } category={ category } />
              if (id == rowType.MIDDLE_PERIOD) return <BlockMiddlePeriod key={ id } value={ value } delta={ delta } category={ category } />
              if (id == rowType.CHECK_COUNT) return <BlockCheckCount key={ id } value={ value } delta={ delta } category={ category } />
              if (id == rowType.SUM_COUNT) return <BlockSumCount key={ id } value={ value } delta={ delta } category={ category } />
              if (id == rowType.MIN_SUM_COUNT) return <BlockMinSumCount key={ id } maxValue={ value } minValue={ data[5].value } />
              if (id == rowType.MIN_PERIOD) return <BlockMinPeriod key={ id } minPeriodValue={ data[7].value } maxPeriodValue={ value } />

              return null
            })
          }
        </WidgetContent>
      </Widget>
    )
  }
}

export default withData(CustomerParameter)
