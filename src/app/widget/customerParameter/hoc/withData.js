import React                from 'react'
import { connect }          from 'react-redux'
import { compose }          from 'redux'


import withGrf13            from 'app/widget/graphql/withGrf13'
import mapStateToProps      from 'app/widget/mapStateToProps'


function withData(ComposedComponent) {
  function decorator(props) {
    return (<ComposedComponent { ...props } />)
  }

  return compose(
    connect(mapStateToProps),
    withGrf13(),
  )(decorator)
}

export default withData
