import types                from 'engine/types'


const inputNameMap = {
  DATE_BEGIN:                         'dateBegin',
 }

const queryName = 'grf13'

const resultNameMap = {
  PARAMETER_ID:                       'parameterId',
  PARAMETER_NAME:                     'parameterName',
  VALUE:                              'value',

  DELTA:                              'delta',
  CATEGORY:                           'category',

  CAPTION:                            'caption',
  WIDGET:                             'vidget',
  DESCRIPTION:                        'description',
}

const queryMap = [
  {
    queryName: queryName,
    inputList: [
      {
        name: inputNameMap.DATE_BEGIN,
        type: types.String,
      },
    ],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        }
      ],
      result: [
        {
          name: resultNameMap.CATEGORY,
          type: types.String,
        },
        {
          name: resultNameMap.DELTA,
          type: types.Float,
        },
        {
          name: resultNameMap.PARAMETER_NAME,
          type: types.String,
        },
        {
          name: resultNameMap.PARAMETER_ID,
          type: types.Int,
        },
        {
          name: resultNameMap.VALUE,
          type: types.Float,
        },
      ],
      title: [
        {
          name: resultNameMap.CAPTION,
          type: types.String,
        },
        {
          name: resultNameMap.WIDGET,
          type: types.String,
        },
        {
          name: resultNameMap.DESCRIPTION,
          type: types.String,
        }
      ]

    }
  },
]

export default {
  queryMap,
  name: queryName
}
