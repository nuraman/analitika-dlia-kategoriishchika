import { graphql }          from 'react-apollo'

import uuid                 from 'uuid-v4'


import config               from 'app/widget/grf04/web/DiscountCompetition/config'

import queryGenerate        from 'engine/graphql/generate/create/queryGrf11'


export default (payload) => {
  const { options } = payload || {}

  const { queryMap, name } = config

  const query = queryGenerate({ queryMap, name })

  return graphql(query, {
    options: (ownProps) => ({ ...options, ...(ownProps.options || {}) }),
    skip: (ownProps) => {
      const { options } = ownProps || {}
      const { variables } = options
      const { dateBegin, dateEnd, } = variables

      if(!dateBegin && !dateEnd) return true

      return false
    },
    props: ({ data }) => {
      const { action_other_list = {}, loading } = data || {}

      if (action_other_list && action_other_list.errors && !action_other_list.errors.length) {
        const { errors, result = [], title } = action_other_list

        const { code, message } = errors

        if (code === 0 && result !== null) {
            return { data: result }
        }
      }

      return { loading }
    }
  })
}
