import { graphql }          from 'react-apollo'


import config               from 'app/widget/grf03/config'

import generateQuery        from 'engine/graphql/generate/create/queryGrf11'


export default (payload) => {
  const { options } = payload || {}
  const { queryMap, name } = config

  const query = generateQuery({ queryMap, name })

  return graphql(query, {
    options: (ownProps) => ({
      ...options, ...(ownProps.options || {}),
      fetchPolicy: 'cache-and-network'
    }),
    skip: (ownProps) => {
      const { options } = ownProps || {}
      const { variables: { dateBegin } } = options || {}

      return !dateBegin
    },
    props: ({ data }) => {
      const { grf03 = {}, loading } = data || {}

      if (grf03 && grf03.errors && !grf03.errors.length) {
        const { errors, result, title } = grf03
        const { code } = errors

        const { caption, description, vidget } = title

        if (!code && result) {
         const { valuePrevPeriod, valuePrevYear } = result

         const resultData = [ valuePrevPeriod, valuePrevYear ]

         return { resultData, loading, title: caption, description, widgetId: vidget }
        }

        return { resultData: [], loading, title: caption, description, widgetId: vidget }
      }

      return { loading }
    }
  })
}
