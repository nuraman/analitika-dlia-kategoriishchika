import { graphql }          from 'react-apollo'

import config               from 'app/widget/checkContent/config'

import queryGenerate        from 'engine/graphql/generate/create/queryGrf15'


export default (payload) => {
  const { options } = payload || {}

  const { queryMap, queryNameMap } = config

  const query = queryGenerate({ queryMap, name: queryNameMap.GRF15 })

  return graphql(query, {
    options: (ownProps) => ({
      ...options, ...(ownProps.options || {}),
      fetchPolicy: 'cache-and-network'
    }),
    skip: (ownProps) => {
      const { options } = ownProps || {}
      const { variables } = options || {}

      const { otherCategory  } = variables
      if ( !otherCategory ) return true

      return false
    },
    props: ({ data }) => {
      const { grf15 = {}, loading } = data || {}

      if (grf15 && grf15.errors && !grf15.errors.length) {
        const { errors, result = [], pie = [],title } = grf15

        const { caption, description } = title

        const { code } = errors
        if (code === 0 && result !== null) {
          const data = result.reduce((data, item, index) => {
            const { goodsName, value, providerId } = item

            return { ...data, [index]: { goodsName, value, providerId } }
          },{})

          const order = Object.keys(data)

          const dataPie = pie.reduce((dataPie, item, index) => {
            const { providerId, providerName, value } = item

            return { ...dataPie, [index]: { providerId, providerName, value } }
          }, {})

          const orderPie = Object.keys(dataPie)

          return {
            order,
            data,
            orderPie,
            dataPie,
            loading,
            title: caption,
            description
          }
        }

        if (code === 0 && result === null) {

          return { order: [], data: {}, loading, title: caption, description }
        }
      }

      return { loading }
    }
  })
}
