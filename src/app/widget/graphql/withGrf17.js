import { graphql }          from 'react-apollo'

import config               from 'app/widget/categoryLoyalty/config'

import queryGenerate        from 'engine/graphql/generate/create/queryGrf11'


export default (payload) => {
  const { options } = payload || {}

  const { queryMap, name } = config

  const query = queryGenerate({ queryMap, name })

  return graphql(query, {
    options: (ownProps) => ({ ...options, ...(ownProps.options || {}) }),
    skip: (ownProps) => {
      const { options } = ownProps || {}
      const { variables: { dateBegin } } = options || {}

      if ( !dateBegin ) return true

      return false
    },
    props: ({ data }) => {
      const { grf17 = {}, loading } = data || {}

      if (grf17 && grf17.errors && !grf17.errors.length) {
        const { errors, result = [], title } = grf17

        const { code } = errors
        if (code === 0 && result !== null) {

          const data = result.reduce((data, item, index) => {
            return { ...data, [index]: { ...item } }
          },{})

          const order = Object.keys(data)

          return {
            order,
            data,
            loading,
            title
          }
        }

        if (code === 0 && result === null) {
          return { order: [], data: {}, loading, title }
        }
      }

      return { loading }
    }
  })
}
