import { graphql }          from 'react-apollo'

import queryGenerate        from 'engine/graphql/generate/create/queryGrf11'

import config               from 'app/widget/checkContent/SkuRating/config'


export default (payload) => {
  const { options } = payload || {}
  const { queryMap, queryNameMap } = config

  const query = queryGenerate({ queryMap, name: queryNameMap.GRF16_SKU })

  return graphql(query, {
    options: (ownProps) => ({ ...options, ...(ownProps.options || {}) }),
    skip: (ownProps) => {
      const { options } = ownProps || {}
      const { variables: { goodsCategory } } = options || {}

      if ( !goodsCategory ) return true

      return false
    },
    props: ({ data }) => {
      const { grf16 = {}, loading } = data || {}

      if (grf16 && grf16.errors && !grf16.errors.length) {
        const { errors, result = {} } = grf16
        const { code } = errors

        if (code === 0 && result !== null) {
          const data = result.reduce((data, item, index) => {
            return { ...data, [index]: { ...item } }
          }, {})

          const order = Object.keys(data)

          return { order, data, loading }
        }
      }

      return { loading }
    }
  })
}
