import { graphql }          from 'react-apollo'

import uuid                 from 'uuid-v4'


import config               from 'app/widget/grf02/config'

import generateQuery        from 'engine/graphql/generate/create/queryGrf11'


export default (payload) => {
  const { options } = payload || {}

  const { queryMap, name } = config

  const query = generateQuery({ queryMap, name })

  return graphql(query, {
    options: {
      fetchPolicy: 'cache-and-network'
    },
    props: ({ data }) => {
      const { grf02 = {}, loading } = data || {}

      if (grf02 && grf02.errors && !grf02.errors.length) {
        const { errors, result = [], title } = grf02
        const { code } = errors

        if (code === 0 && result !== null) {
          const data = result.reduce((data, item) => {
            const { month, value_self, value_other, colorSelf, colorOther } = item
            const id = uuid()

            return { ...data, [id]: { month, valueSelf: value_self, valueOther: value_other, colorSelf, colorOther } }
          }, {})

          const order = Object.keys(data)

          return { order, data, loadingData: loading, title }
        } else if (code === 0 && result === null) {
          return { order: [], data: {}, loadingData: loading, title }
        }
      }

      return { loadingData: loading }
    }
  })
}
