import { graphql }          from 'react-apollo'

import config               from 'app/widget/rfmAnalyseWidget/config'

import generateQuery        from 'engine/graphql/generate/create/queryGrf11'


export default (payload) => {
  const { options } = payload || {}
  const { queryMap, name } = config

  const query = generateQuery({ queryMap, name })

  return graphql(query, {
    options: (ownProps) => ({
      ...options, ...(ownProps.options || {}),
      fetchPolicy: 'cache-and-network'
    }),
    props: ({ data }) => {
      const { grf11 = {}, loading } = data || {}

      if (grf11 && grf11.errors && !grf11.errors.length) {
        const { errors, result, title } = grf11
        const { code } = errors

        if (code === 0 && result !== null) {
          const dataGrf11 = result.map((dataGrf11, item, index) => {
            const { X, Y, value, color, name, total } = item || {}

            return { ...dataGrf11 ,[index]: { id: index, x: X, y: Y, value, color, name, total }, }
          })
          const order = Object.keys(dataGrf11)

          return {
            data: dataGrf11,
            order,
            title,
            loading,
          }
        }

        if (code === 0 && result === null) {
          return {
            data: {},
            order: [],
            title,
            loading,
          }
        }
      }

      return { loading }
    }
  })
}
