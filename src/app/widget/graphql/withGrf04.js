import moment               from 'moment'
import localization         from 'moment/locale/ru'

import { graphql }          from 'react-apollo'

import uuid                 from 'uuid-v4'


import config               from 'app/widget/grf04/config'

import generateQuery        from 'engine/graphql/generate/create/queryGrf04'


export default (payload) => {
  const { options } = payload || {}
  const { queryMap, name } = config

  const query = generateQuery({ queryMap, name })

  return graphql(query, {
    options: (ownProps) => ({
      ...options, ...(ownProps.options || {}),
      fetchPolicy: 'cache-and-network'
    }),
    skip: (ownProps) => {
      const { options } = ownProps || {}
      const { variables } = options || {}

      const { dateBegin  } = variables
      if (!dateBegin) return true

      return false
    },
    props: ({ data }) => {
      const { grf04 = {}, loading } = data || {}
      moment.locale('ru', localization)
      if (grf04 && grf04.errors && !grf04.errors.length) {
        const {
          errors,
          resultSale = [],
          resultDiscount = [],
          resultOther = [],
          resultGoods = [],
          title
        } = grf04
        const { code } = errors

        let dataSale={}, orderSale=[],
          dataDiscount={}, orderDiscount=[],
          dataDiscountOther={}, orderDiscountOther=[],
          dataGood={}, orderGood=[],
          dateBeginChart = 0, dateEndChart = 0

        if (code === 0 && resultSale !== null) {
          dataSale = resultSale.reduce((dataSale, item, index) => {
            const { dateBegin, dateEnd, valueAmount, valueCount, valueQuantity, valueCountCheck, valueCountCustomers } = item || {}
            const id = uuid()

            if (index === 0) {
              dateBeginChart = moment(dateBegin, 'DD.MM.YYYY').toDate().getTime()
            }

            if (index === resultSale.length - 1) {
              dateEndChart = moment(dateEnd, 'DD.MM.YYYY').toDate().getTime()
            }

            return {
              ...dataSale, [id]: {
                id,
                dateBegin: moment(dateBegin, 'DD.MM.YYYY').toDate().getTime(),
                dateEnd: moment(dateEnd, 'DD.MM.YYYY').toDate().getTime(),
                valueAmount,
                valueCount,
                valueQuantity,
                valueCountCheck,
                valueCountCustomers,
              }
            }
          }, {})
          orderSale = Object.keys(dataSale)

          if (resultDiscount !== null) {
            dataDiscount = resultDiscount === null ? {} : resultDiscount.reduce((dataDiscount, item) => {
              const {
                amountFakt,
                actionId,
                comment,
                countGoods,
                countPlanCustomers,
                countFaktCustomers,
                dateBegin,
                dateEnd,
                description,
                name,
                quantityFakt
              } = item || {}
              const id = uuid()

              return {
                ...dataDiscount, [id]: {
                  id: actionId,
                  dateBegin: moment(dateBegin, 'DD.MM.YYYY').toDate().getTime(),
                  dateEnd: moment(dateEnd, 'DD.MM.YYYY').toDate().getTime(),
                  name,
                  description,
                  comment,
                  countGoods,
                  countExpectedCustomers: countPlanCustomers,
                  countRealCustomers: countFaktCustomers,
                  sumSoldGoods: amountFakt,
                  countSoldGoods: quantityFakt,
                }
              }
            }, {})
            orderDiscount = Object.keys(dataDiscount)
          }

          if (resultOther !== null) {
            dataDiscountOther = resultOther.reduce((dataDiscountOther, item) => {
              const { name, actionCount, dateBegin, dateEnd, countGoods, countPlanCustomers, countFaktCustomers, amountFakt, quantityFakt } = item || {}
              const id = uuid()

              return {
                ...dataDiscountOther, [id]: {
                  id,
                  dateBegin: moment(dateBegin, 'DD.MM.YYYY').toDate().getTime(),
                  dateEnd: moment(dateEnd, 'DD.MM.YYYY').toDate().getTime(),
                  name,
                  countDiscount: actionCount,
                  countGoods,
                  countExpectedCustomers: countPlanCustomers,
                  countRealCustomers: countFaktCustomers,
                  sumSoldGoods: amountFakt,
                  countSoldGoods: quantityFakt,
                }
              }
            }, {})
            orderDiscountOther = Object.keys(dataDiscountOther)
          }

          if (resultGoods !== null) {
            dataGood = resultGoods.reduce((dataGood, item) => {
              const id = uuid()

              const {actionId, name} = item

              return {...dataGood, [id]: {id, discountId: actionId, value: name}}
            }, {})
            orderGood = Object.keys(dataGood)
          }

          return {
            resultSale: {
              data: dataSale,
              order: orderSale,
              dateBeginChart,
              dateEndChart,
            },
            resultDiscount: {
              data: dataDiscount,
              order: orderDiscount,
            },
            resultDiscountOther: {
              data: dataDiscountOther,
              order: orderDiscountOther,
            },
            resultGood: {
              data: dataGood,
              order: orderGood,
            },
            title,
            loading
          }
        }
        if (code === 0 && resultSale === null) {
          return {
            resultSale: {},
            title,
            loading
          }
        }
      }

      return { loading }
    }
  })
}
