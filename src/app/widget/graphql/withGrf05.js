import { graphql }          from 'react-apollo'

import uuid                 from 'uuid-v4'


import config               from 'app/widget/grf05/config'

import queryGenerate        from 'engine/graphql/generate/create/queryGrf11'


export default (payload) => {
  const { options } = payload || {}

  const { queryMap, name } = config

  const query = queryGenerate({ queryMap, name })

  return graphql(query, {
    options: (ownProps) => ({
      ...options, ...(ownProps.options || {}),
      fetchPolicy: 'cache-and-network'
    }),
    skip: (ownProps) => {
      const { options } = ownProps || {}
      const { variables: { dateBegin } } = options || {}

      return !dateBegin
    },
    props: ({ data }) => {
      const { grf05, loading } = data || {}

      if (grf05 && grf05.errors && !grf05.errors.length) {
        const { errors, result, title } = grf05
        const { code } = errors

        if (code === 0 && result !== null) {
          const data = result.reduce((data, item) => {
            const { gender, ageBegin, ageEnd, value } = item
            const id = uuid()

            return { ...data, [id]: { id, gender, ageBegin, ageEnd, value } }
          }, {})

          const order = Object.keys(data)

          return { order, data, loading, title }
        } else if (!code && !result) {
          return { order: [], data: {}, loading, title }
        }
      }

      return { loading }
    }
  })
}
