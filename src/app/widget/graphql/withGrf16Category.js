import { graphql }          from 'react-apollo'

import queryGenerate        from 'engine/graphql/generate/create/queryGrf11'

import config               from 'app/widget/checkContent/SkuRating/config'


export default (payload) => {
  const { options } = payload || {}
  const { queryMap, queryNameMap } = config

  const query = queryGenerate({ queryMap, name: queryNameMap.GRF16_CATEGORY })

  return graphql(query, {
    options: (ownProps) => ({ ...options, ...(ownProps.options || {}) }),
    skip: (ownProps) => {
      const { options } = ownProps || {}
      const { variables: { color } } = options || {}

      if ( !color ) return true

      return false
    },
    props: ({ data }) => {
      const { grf16Category = {}, loading } = data || {}

      if (grf16Category && grf16Category.errors && !grf16Category.errors.length) {
        const { errors, result = {}, title } = grf16Category
        const { code } = errors

        if (code === 0 && result !== null) {
          const data = result.reduce((data, item, index) => {
            return { ...data, [index]: { ...item } }
          }, {})

          const order = Object.keys(data)

          const { caption, description } = title

          return { order, data, loading, title: caption, description }
        }
      }

      return { loading }
    }
  })
}
