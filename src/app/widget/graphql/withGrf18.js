import { graphql }          from 'react-apollo'

import config               from 'app/widget/consumptionPeriodSku/config'

import queryGenerate        from 'engine/graphql/generate/create/queryGrf11'


export default (payload) => {
  const { options } = payload || {}

  const { queryMap, queryNameMap } = config

  const query = queryGenerate({ queryMap, name: queryNameMap.GRF18 })

  return graphql(query, {
    options: (ownProps) => ({ ...options, ...(ownProps.options || {}) }),
    skip: (ownProps) => {
      const { options } = ownProps || {}
      const { variables: { goodsId } } = options || {}

      if ( !goodsId ) return true

      return false
    },
    props: ({ data }) => {
      const { grf18 = {}, loading } = data || {}

      if (grf18 && grf18.errors && !grf18.errors.length) {
        const { errors, result = [], title } = grf18

        const { code } = errors
        if (code === 0 && result !== null) {
          const data = result.reduce((data, item, index) => {
            return { ...data, [index]: { ...item } }
          },{})

          const order = Object.keys(data)

          return {
            order,
            data,
            loading,
            title,
          }
        }

        if (code === 0 && result === null) {
          return { order: [], data: {}, loading, title }
        }
      }

      return { loading }
    }
  })
}
