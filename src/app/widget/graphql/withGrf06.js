import { graphql }          from 'react-apollo'

import moment               from 'moment'

import uuid                 from 'uuid-v4'


import config               from 'app/widget/grf06/config'

import generateQuery        from 'engine/graphql/generate/create/queryGrf11'


export default (payload) => {
  const { options } = payload || {}
  const { queryMap, name } = config

  const query = generateQuery({ queryMap, name })

  return graphql(query, {
    options: (ownProps) => ({
      ...options, ...(ownProps.options || {}),
      fetchPolicy: 'cache-and-network'
    }),
    skip: (ownProps) => {
      const { options } = ownProps || {}
      const { variables: { dateBegin } } = options || {}

      return !dateBegin
    },
    props: ({ data }) => {
      const { grf06 = {}, loading } = data || {}

      if (grf06 && grf06.errors && !grf06.errors.length) {
        const { errors, result, title } = grf06
        const { code } = errors

        if (code === 0 && result !== null) {
          const resultData = result.reduce((resultData, item) => {
            const { date, valueSelf, valueAny, valueOther, colorSelf, colorAny, colorOther } = item || {}
            const id = uuid()

            return {
              ...resultData, [id]: {
                id,
                date: moment(date, 'DD.MM.YYYY').toDate(),
                valueSelf,
                valueAny,
                valueOther,

                colorSelf,
                colorAny,
                colorOther
              }
            }
          }, {})

          const order = Object.keys(resultData)

          return {
            data: resultData,
            order,
            title,
            loading,
          }
        }

        if (code === 0 && result === null) {
          return {
            data: {},
            order: [],
            title,
            loading,
          }
        }
      }

      return { loading }
    }
  })
}
