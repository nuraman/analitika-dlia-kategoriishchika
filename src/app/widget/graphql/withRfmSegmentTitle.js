import { graphql }          from 'react-apollo'

import config               from 'app/widget/checkContent/config'

import queryGenerate        from 'engine/graphql/generate/create/queryRfmSegmentTitle'


export default (payload) => {
  const { options } = payload || {}

  const { queryMap, queryNameMap } = config

  const query = queryGenerate({ queryMap, name: queryNameMap.SEGMENT_TITLE })

  return graphql(query, {
    options: (ownProps) => ({ ...options, ...(ownProps.options || {}) }),
    skip: (ownProps) => {
      const { options } = ownProps || {}
      const { variables } = options || {}

      const { color  } = variables
      if ( !color ) return true

      return false
    },
    props: ({ data }) => {
      const { segmentsTitle = {}, loading } = data || {}

      if (segmentsTitle && segmentsTitle.errors && !segmentsTitle.errors.length) {
        const { title } = segmentsTitle

        const { caption, description } = title

        return { title: caption, description }
      }

      return { loading }
    }
  })
}
