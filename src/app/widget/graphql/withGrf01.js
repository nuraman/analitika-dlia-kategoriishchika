import { graphql }          from 'react-apollo'

import uuid                 from 'uuid-v4'


import config               from 'app/widget/grf01/config'

import queryGenerate        from 'engine/graphql/generate/create/queryGrf11'


export default (payload) => {
  const { options } = payload || {}

  const { queryMap, name } = config

  const query = queryGenerate({ queryMap, name })

  return graphql(query, {
    options: (ownProps) => ({
      ...options, ...(ownProps.options || {}),
      fetchPolicy: 'cache-and-network'
    }),
    skip: (ownProps) => {
      const { options } = ownProps || {}
      const { variables } = options
      const { dateBegin } = variables
      if(!dateBegin) return true

      return false
    },
    props: ({ data }) => {
      const { grf01 = {}, loading } = data || {}

      if (grf01 && grf01.errors && !grf01.errors.length) {
        const { errors, result = [], title } = grf01

        const { code } = errors

        if (code == 0 && result) {

          const data = result.reduce((data, item) => {
            const { color, provider, providerName, value } = item
            const id = uuid()

            return { ...data, [id]: { provider, providerName, value, color } }
          }, {})

          const order = Object.keys(data)

          return {
            order,
            data,
            loading,
            title
          }
        } else if(code === 0 && !result) {
          return {
            order: [],
            data: {},
            loading,
            title
          }
        }
      }

      return { loading }
    }
  })
}
