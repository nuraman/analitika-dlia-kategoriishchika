import { graphql }          from 'react-apollo'

import uuid                 from 'uuid-v4'


import config               from 'app/widget/heatmapSale/config'

import generateQuery        from 'engine/graphql/generate/create/queryGrf11'

const Day = {
  1: 'Пн',
  2: 'Вт',
  3: 'Ср',
  4: 'Чт',
  5: 'Пт',
  6: 'Сб',
  7: 'Вс'
}

const getDay = (number) => {
  return Day[number]
}

export default (payload) => {
  const { options } = payload || {}
  const { queryMap, name } = config

  const query = generateQuery({ queryMap, name })

  return graphql(query, {
    options: (ownProps) => ({
      ...options, ...(ownProps.options || {}),
      fetchPolicy: 'cache-and-network'
    }),
    skip: (ownProps) => {
      const { options } = ownProps || {}

      const { variables: { dateBegin } } = options

      return !dateBegin
    },
    props: ({ data }) => {
      const { grf09 = {}, loading } = data || {}

      if (grf09 && grf09.errors && !grf09.errors.length) {
        const { errors, result, title } = grf09
        const { code } = errors

        if (code === 0 && result !== null) {
          const dataHeatmap = result.reduce((dataHeatmap, item) => {
            const { day,
              value01, value02, value03, value04, value05, value06, value07,
              value08, value09, value10, value11, value12, value13, value14,
              value15, value16, value17, value18, value19, value20, value21,
              value22, value23} = item || {}

            const id = uuid()

            const timeList = [
              { hour: '01', value: value01, },
              { hour: '02', value: value02, },
              { hour: '03', value: value03, },
              { hour: '04', value: value04, },
              { hour: '05', value: value05, },
              { hour: '06', value: value06, },
              { hour: '07', value: value07, },
              { hour: '08', value: value08, },
              { hour: '09', value: value09, },
              { hour: '10', value: value10, },
              { hour: '11', value: value11, },
              { hour: '12', value: value12, },
              { hour: '13', value: value13, },
              { hour: '14', value: value14, },
              { hour: '15', value: value15, },
              { hour: '16', value: value16, },
              { hour: '17', value: value17, },
              { hour: '18', value: value18, },
              { hour: '19', value: value19, },
              { hour: '20', value: value20, },
              { hour: '21', value: value21, },
              { hour: '22', value: value22, },
              { hour: '23', value: value23, },
            ]

            return { ...dataHeatmap, [id]: { id, day: getDay(day), timeList } }
          }, {})

          const order = Object.keys(dataHeatmap)

          return {
            data: dataHeatmap,
            order,
            title,
            loadingData: loading,
          }
        }

        if (code === 0 && result === null) {
          return {
            data: {},
            order: [],
            title,
            loadingData: loading,
          }
        }
      }

      return { loadingData: loading }
    }
  })
}
