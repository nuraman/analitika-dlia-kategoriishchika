import { graphql }          from 'react-apollo'

import config               from 'app/widget/checkContent/config'

import queryGenerate        from 'engine/graphql/generate/create/queryGrf11'


export default (payload) => {
  const { options } = payload || {}

  const { queryMap, queryNameMap } = config

  const query = queryGenerate({ queryMap, name: queryNameMap.GRF14 })

  return graphql(query, {
    options: (ownProps) => ({
      ...options, ...(ownProps.options || {}),
      fetchPolicy: 'cache-and-network'
    }),
    skip: (ownProps) => {
      const { options } = ownProps || {}
      const { variables } = options || {}

      const { color  } = variables
      if ( !color ) return true

      return false
    },
    props: ({ data }) => {
      const { grf14 = {}, loading } = data || {}

      if (grf14 && grf14.errors && !grf14.errors.length) {
        const { errors, result = [], title } = grf14

        const { caption, description } = title

        const { code } = errors
        if (code === 0 && result !== null) {
          const data = result.reduce((data, item, index) => {
            const { categoryId, categoryName, value } = item

            return { ...data, [index]: { categoryId, categoryName, value } }
          },{})

          const order = Object.keys(data)

          return { order, data, loading, title: caption, description }
        }

        if (code === 0 && result === null) {

          return { order: [], data: {}, loading, title: caption, description }
        }
      }

      return { loading }
    }
  })
}
