import { graphql }          from 'react-apollo'

import config               from 'app/widget/customerParameter/config'

import queryGenerate        from 'engine/graphql/generate/create/queryGrf11'


export default (payload) => {
  const { options } = payload || {}

  const { queryMap, name } = config

  const query = queryGenerate({ queryMap, name })

  return graphql(query, {
    options: (ownProps) => ({
      ...options, ...(ownProps.options || {}),
      fetchPolicy: 'cache-and-network'
    }),
    skip: (ownProps) => {
      const { options } = ownProps || {}
      const { variables: { dateBegin } } = options

      if(!dateBegin) return true

      return false
    },
    props: ({ data }) => {
      const { grf13 = {}, loading } = data || {}

      if (grf13 && grf13.errors && !grf13.errors.length) {
        const { errors, result = [], title } = grf13

        const { code } = errors
        if (code === 0 && result !== null) {
          const data = result.reduce((data, item) => {
            const { parameterId, parameterName, value, delta, category } = item

            return { ...data, [parameterId]: {
                id: parameterId,
                parameterName,
                value: value === null ? '' : value,
                delta: delta === null ? '' : delta,
                category
              }
            }
          }, {})

          const order = Object.keys(data)

          return { order, data, loading, title }
        }

        if (code === 0 && result === null) {
          return { order: [], data: {}, loading, title }
        }
      }

      return { loading }
    }
  })
}
