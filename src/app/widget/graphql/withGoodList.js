import { graphql }          from 'react-apollo'

import queryGenerate        from 'engine/graphql/generate/create/queryGrf11'

import config               from 'app/widget/consumptionPeriodSku/config'


export default (payload) => {
  const { options } = payload || {}
  const { queryMap, name } = config

  const query = queryGenerate({ queryMap, name })

  return graphql(query, {
    options: (ownProps) => ({ ...options, ...(ownProps.options || {}) }),
    skip: (ownProps) => {
      const { options } = ownProps || {}
      const { variables } = options || {}

      const { dateBegin  } = variables

      if ( !dateBegin ) return true

      return false
    },
    props: ({ data }) => {
      const { grf18Goods = {}, loading } = data || {}

      if (grf18Goods && grf18Goods.errors && !grf18Goods.errors.length) {
        const { errors, result = {}, title } = grf18Goods
        const { code } = errors

        if (code === 0 && result !== null) {
          const data = result.reduce((data, item, index) => {
            return { ...data, [index]: { ...item } }
          }, {})

          const order = Object.keys(data)

          return { order, data, loading, title }
        }
      }

      return { loading }
    }
  })
}
