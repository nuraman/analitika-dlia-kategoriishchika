import _                    from 'lodash'

import { graphql }          from 'react-apollo'


import config               from 'app/widget/checkComparison/config'
import generateQuery        from 'engine/graphql/generate/create/queryGrf11'


export default (payload) => {
  const { options } = payload || {}
  const { queryMap, name } = config

  const query = generateQuery({ queryMap, name })

  return graphql(query, {
    options: (ownProps) => ({
      ...options, ...(ownProps.options || {}),
      fetchPolicy: 'cache-and-network'
    }),
    skip: (ownProps) => {
      const { options } = ownProps || {}
      const { variables : { dateBegin } } = options || {}

      return !dateBegin
    },
    props: ({ data }) => {
      const { grf12 = {}, loading } = data || {}

      if (grf12 && grf12.errors && !grf12.errors.length) {
        const { errors, result, title } = grf12
        const { code } = errors
        let resultData = {}

        if (code === 0 && result !== null) {
          for( let i = 0; i < result.length; i++ ) {
            const { categoryId, categoryName, valueSelf, valueOther, colorSelf, colorOther } = result[i]

            _.merge( resultData, { [i]: { id: i, categoryId, categoryName, valueSelf, valueOther, colorSelf, colorOther } })
          }

          const order = Object.keys(resultData)

          return {
            data: resultData,
            order,
            title,
            loading,
          }
        }

        if (code === 0 && result === null) {
          return {
            data: {},
            order: [],
            title,
            loading,
          }
        }
      }

      return { loading }
    }
  })
}
