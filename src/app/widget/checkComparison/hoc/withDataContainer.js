import React, { Component } from 'react'
import { connect }          from 'react-redux'
import { compose }          from 'redux'


import withGrf12            from 'app/widget/graphql/withGrf12'


function withData(ComposedComponent) {
  class Widget extends Component {
    render() {
      return (
        <ComposedComponent { ...this.props } />
      )
    }
  }

  function mapSateToProps(state) {
    const { menu, checkComparison, user } = state

    const { period: { dateBegin } } = menu
    const { categorySelectedList = [], valueBigger, valueDifference } = checkComparison

    const { userLogin: { data, order } } = user
    const { isProvider } = data[order[0]] || {}

    return {
      categorySelectedList,
      isProvider,
      valueBigger,
      valueDifference,
      dateBegin,
      options: {
        variables: {
          dateBegin: dateBegin,
        }
      }
    }
  }

  return compose(
    connect(mapSateToProps),
    withGrf12(),
  )(Widget)
}

export default withData
