import React                from 'react'

import { connect }          from 'react-redux'
import { compose }          from 'redux'


function WithDataCategorySelect(ComposedComponent) {
  function WrapperFunction(props) {
    return (
      <ComposedComponent { ...props } />
    )
  }

  function mapStateToProps(state) {
      const { checkComparison } = state

      const { categorySelectedList, valueBigger, valueDifference } = checkComparison

      return {
        categorySelectedList,
        valueBigger,
        valueDifference
      }
  }

  return compose(
    connect(mapStateToProps)
  )(WrapperFunction)
}

export default WithDataCategorySelect
