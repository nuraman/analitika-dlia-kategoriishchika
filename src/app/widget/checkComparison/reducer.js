const name = 'checkComparison'

const types = {
  SET_CATEGORY_LIST: `${ name }/SET_CATEGORY_LIST`
}

const STATE = {
  categorySelectedList: [],
  valueBigger: '3',
  valueDifference: ''
}

const reducer = (state = STATE, action) => {
  const { type, payload  } = action

  switch (type) {
    case types.SET_CATEGORY_LIST: {
      const { categorySelectedList = [], valueBigger, valueDifference } = payload

      return {
        categorySelectedList,
        valueBigger,
        valueDifference
      }
    }
    default: {
      return state
    }
  }
}

export default {
  types,
  reducer,
}
