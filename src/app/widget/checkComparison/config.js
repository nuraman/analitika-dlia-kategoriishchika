import types                from 'engine/types'

const inputNameMap = {
  DATE_BEGIN:                         'dateBegin',
}

const queryNameMap = {
  GRF12:                              'grf12',
}

const resultNameMap = {
  CATEGORY_ID:                        'categoryId',
  CATEGORY_NAME:                      'categoryName',
  VALUE_SELF:                         'valueSelf',
  VALUE_OTHER:                        'valueOther',

  COLOR_SELF:                         'colorSelf',
  COLOR_OTHER:                        'colorOther',
}

const titleNameMap = {
  CAPTION:                            'caption',
  WIDGET:                             'vidget',
  DESCRIPTION:                        'description',
}

const queryMap = [
  {
    queryName: queryNameMap.GRF12,
    inputList: [
      {
        name: inputNameMap.DATE_BEGIN,
        type: types.String,
      },
    ],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        }
      ],
      result: [
        {
          name: resultNameMap.CATEGORY_ID,
          type: types.Int,
        },
        {
          name: resultNameMap.CATEGORY_NAME,
          type: types.String,
        },
        {
          name: resultNameMap.VALUE_SELF,
          type: types.Float,
        },
        {
          name: resultNameMap.VALUE_OTHER,
          type: types.Float,
        },
        {
          name: resultNameMap.COLOR_SELF,
          type: types.String,
        },
        {
          name: resultNameMap.COLOR_OTHER,
          type: types.String,
        }
      ],
      title: [
        {
          name: titleNameMap.CAPTION,
          type: types.String,
        },
        {
          name: titleNameMap.DESCRIPTION,
          type: types.String,
        },
        {
          name: titleNameMap.WIDGET,
          type: types.String,
        }
      ]
    }
  },
]

export default {
  queryMap,
  name: queryNameMap.GRF12,
}
