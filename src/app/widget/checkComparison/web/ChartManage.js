import PropTypes            from 'prop-types'

import _                    from 'lodash'

import React, { Component } from 'react'

import ReactHighcharts      from 'react-highcharts'

import styled               from 'styled-components'

import Chart                from './api/Chart'


const WidgetContent = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  height: calc(100% - 60px);
`

const DivCategoryShift = styled.div`
  display: flex;
  align-items: center;
  width: 30px;
  height: calc(100% - 60px);
`

const DivChart = styled.div`
  display: inline-block;
  width: calc(100% - 60px);
`

const PrevIco = styled.div`
  display: ${ props=>props.display ? 'block' : 'none' };
  width: 11px;
  height: 21px;
  margin-left: auto;
  fill: black;
  cursor: pointer;
  
  background-image: url(assets/prevButtonBlack.svg);
  background-size: cover;
`

const NextIco = styled.div`
  display: ${ props=>props.display ? 'block' : 'none' };
  width: 11px;
  height: 21px;
  margin-top: 15px;
  margin-bottom: 15px;
  fill: #9e9e9e;
  cursor: pointer;

  background-image: url(assets/nextButtonBlack.svg);
  background-size: cover;
`


const MAX_COUNT = 10

class ChartManage extends Component {
  static propTypes = {
    categorySelectedList: PropTypes.array,
    data: PropTypes.object,
    order: PropTypes.array,
    isProvider: PropTypes.bool,
    height: PropTypes.number,
  }

  static defaultProps = {
    height: 363,
  }

  constructor() {
    super()

    this.state = {
      config: {},
      categorySelectedList: [],
      data: {},
      firstIndexItem: 0,
      height: 0,
      isProvider: false,
      isNext: false,
      isPrev: false,
      lastIndexItem: 0,
      order: [],
    }
  }

  componentWillMount() {
    const { categorySelectedList, data, height, isProvider, order } = this.props

    if (categorySelectedList.length > 0) {
      this.setCategory({ categorySelectedList, data, height, isProvider, order })
    }
  }

  componentWillReceiveProps(nextProps) {
    const { categorySelectedList, data, height, isProvider, order } = nextProps

    if (categorySelectedList.length > 0) {
      this.setCategory({ categorySelectedList, data, height, isProvider, order })
    }
  }

  onClickPrev = () => {
    const { categorySelectedList, data, height, isProvider, order } = this.state

    const { onSelectDiscountCallback } = this.props

    let { firstIndexItem } = this.state

    let newCategorySelectedList = []

    const range = this.getPrevRange({ firstIndexItem, categorySelectedList })

    let firstIndex = 0
    let lastIndex = 0
    let i = 0


    for (let index of range) {
      newCategorySelectedList.push(categorySelectedList[index])

      if (i === 0) lastIndex = index
      i++
      firstIndex = index
    }

    if (newCategorySelectedList.length > 0) {
      const { newData, newOrder } = this.getNewData({ data, order, categoryList: newCategorySelectedList })
      const config = this.getConfig({ data: newData, order: newOrder, onSelect: onSelectDiscountCallback, height, isProvider })

      this.setState({
        config,
        isFullList: false,
        firstIndexItem: firstIndex === 0 ? firstIndex - 1 : firstIndex,
        lastIndexItem: lastIndex + 1,
        isPrev: firstIndex !== 0,
        isNext: true,
      })
    }
  }

  onClickNext = () => {
    const { categorySelectedList, data, height, isProvider, order } = this.state

    const { onSelectDiscountCallback } = this.props

    let { lastIndexItem } = this.state

    let newCategorySelectedList = []

    var range = this.getNextRange({ lastIndexItem, categorySelectedList })

    let lastIndex = 0
    let firstIndex = 0
    let i = 0

    for (let index of range) {
      newCategorySelectedList.push(categorySelectedList[index])
      if (i === 0) firstIndex = index

      i++

      lastIndex = index
    }

    if (newCategorySelectedList.length > 0) {
      const { newData, newOrder } = this.getNewData({ data, order, categoryList: newCategorySelectedList })
      const config = this.getConfig({ data: newData, order: newOrder, onSelect: onSelectDiscountCallback, height, isProvider })

      this.setState({
        config,
        firstIndexItem: firstIndex,
        isFullList: false,
        lastIndexItem: lastIndex + 1,
        isNext: categorySelectedList.length - 1 !== lastIndex,
        isPrev: true,
      })
    }
  }

  getPrevRange = ({ firstIndexItem, categorySelectedList }) => {
    const range = {
      from: firstIndexItem - 1,
      max: 10,
      categoryList: categorySelectedList
    }


    range[Symbol.iterator] = function () {
      let current = this.from
      let categoryList = this.categoryList
      let max = this.max

      return {
        next() {
          const correctValue = categoryList[current]
          if (correctValue && max > 0) {
            max--

            return {
              done: false,
              value: current--
            }
          } else {
            return {
              done: true
            }
          }
        }
      }
    }

    return range
  }

  getNextRange = ({ lastIndexItem, categorySelectedList }) => {
    var range = {
      from: lastIndexItem,
      to: lastIndexItem + 10,
      categorySelectedList,
    }

    range[Symbol.iterator] = function() {
      let current = this.from
      let categoryList = this.categorySelectedList
      let to = this.to

      return {
        next() {
          const correctValue = categoryList[current]
          if (correctValue && current < to) {

            return {
              done: false,
              value: current++,
            }
          } else {
            return {
              done: true
            }
          }
        }
      }
    }

    return range
  }

  setCategory  = ({  categorySelectedList, data, height, isProvider, order }) => {
    let config = {}
    let firstIndexItem = 0, lastIndexItem = 0
    let isNext = false, isPrev = false

    const { onSelectDiscountCallback } = this.props

    if (categorySelectedList.length <= MAX_COUNT) {
      const { newData, newOrder } = this.getNewData({ data, order, categoryList: categorySelectedList })

      config = this.getConfig({ data: newData, order: newOrder, onSelect: onSelectDiscountCallback, isProvider, height })

      firstIndexItem = 0
      lastIndexItem = categorySelectedList.length
    } else {

      let categoryList = categorySelectedList.slice(0, MAX_COUNT)

      const { newData, newOrder } = this.getNewData({ data, order, categoryList })

      config = this.getConfig({ data: newData, order: newOrder, onSelect: onSelectDiscountCallback, isProvider, height })

      firstIndexItem = 0
      lastIndexItem = 10
      isNext = true
    }

    this.setState({
      config,
      categorySelectedList,
      height,
      isProvider,
      data,
      order,
      firstIndexItem: firstIndexItem,
      lastIndexItem: lastIndexItem,
      isNext,
      isPrev
    })
  }

  getNewData = ({ data, order, categoryList }) => {
    if (categoryList.length <= MAX_COUNT) {
      const newData = order.reduce((newData, item) => {
        const { id, categoryId } = data[item]

        categoryList.forEach((category) => {
          if (category.categoryId === categoryId)
            _.merge(newData, { [id]: { ...data[item] } })
        })

        return { ...newData }
      }, {})

      const newOrder = Object.keys(newData)

      return {
        newData,
        newOrder
      }
    }
  }

  getConfig = ({ data, order, onSelect, height, isProvider }) => {
    const chart = new Chart()

    chart.setSeriesChart({ data, order })

    const config = isProvider ?
      chart.getConfigForProvider({ onSelect, height }) :
      chart.getConfigForCategoryManager({ height })

    return config
  }

  render() {
    const { config, isNext, isPrev } = this.state

    return (
      <WidgetContent>
        <DivCategoryShift>
          <PrevIco
            display={ isPrev }
            onClick={ this.onClickPrev }
          />
        </DivCategoryShift>
        <DivChart>
          <ReactHighcharts config={ config } />
        </DivChart>
        <DivCategoryShift>
          <NextIco
            display={ isNext }
            onClick={ this.onClickNext }
          />
        </DivCategoryShift>
      </WidgetContent>
    )
  }
}

export default ChartManage
