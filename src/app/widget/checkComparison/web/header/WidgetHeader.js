import React                from 'react'

import styled               from 'styled-components'


import BlockTitle           from 'app/widget/header/Title'
import BlockHelper          from 'app/widget/header/Helper'
import BlockFilterButton    from './FilterButton'


const Wrapper = styled.div`
  display: block;
  height: 60px;
  width: inherit;
  border-bottom: 1px solid rgba(0, 0, 0, 0.12);
`

const Content = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  
  height: 100%;
  width: 100%;
`

const Flex = styled.div`
  display: flex;
  flex: 1;
`

const url = 'http://mindcloud.pro/kpl/exceldata.php'

export default function Container(props) {
  const {
    description,
    title,
    onOpenCategorySelect,
    exportImage,
    widgetId,
    dateBegin
  } = props

  const onHandleClick = () => onOpenCategorySelect()

  //в будушем открытий описания виджета
  const onHandleDescription = () => {

  }

  const onHandleExportImage = () => exportImage()

  const getUrlWithParameter = () => `${ url }?id=${ widgetId }&dateBegin=${ dateBegin }`

  return (
      <Wrapper>
        <Content>
          <BlockTitle
            title={ title }
            description={ description }
          />
         <BlockFilterButton onClick={ onHandleClick } />
         <Flex />
         <BlockHelper
           openWidgetDescription={ onHandleDescription }
           exportImage={ onHandleExportImage }
           url={ getUrlWithParameter() }
         />
        </Content>
      </Wrapper>
    )

}


