import PropTypes            from 'prop-types'

import React                from 'react'
import styled               from 'styled-components'


const Wrapper = styled.div`
  display: table-cell;
  text-align: center;
  padding-left: 50px;
  
  vertical-align: middle;
  
  
  font-family: HelveticaRegular;
  font-size: 18px;
`

const Link = styled.a`
  cursor: pointer;
  height: calc(100% - 1px);
  width: 100%;
  border-bottom: 1px dotted black;
`

function FilterButton(props) {
  const { onClick } = props

  const onHandleClick = () => onClick()

  return (
    <Wrapper>
      <Link
        onClick={ onHandleClick }
      >
        Выберите категорию
      </Link>
    </Wrapper>
  )
}

FilterButton.propTypes = {
  onClick: PropTypes.func
}

export default FilterButton
