import _                    from 'lodash'

import PropTypes            from 'prop-types'

import React, { Component } from 'react'

import ReactHighcharts      from 'react-highcharts'

import sizeMe               from 'react-sizeme'

import { compose }          from 'redux'


import reducerDiscount      from 'app/discount/reducer'

import WidgetProgressBar    from 'app/analysis/web/WidgetProgressBar'
import EmptyWidget          from 'app/widget/WidgetEmpty'

import withData             from 'app/widget/checkComparison/hoc/withDataContainer'

import Chart                from 'app/widget/checkComparison/web/api/Chart'

import Widget               from 'app/widget/styledComponent/widget/DivContainer'
import WidgetHeader         from './header/WidgetHeader'

import ChartManage          from './ChartManage'
import categorySelect from "./api/categorySelect";


const MAX_VALUE_PERCENT = 3
const MAX_COUNT = 10

class Container extends Component {
  static propTypes = {
    categorySelectedList: PropTypes.array,
    data: PropTypes.object,
    isProvider: PropTypes.bool,
    order: PropTypes.array,
    size: PropTypes.object,
    title: PropTypes.object,
  }

  static defaultProps = {
    categorySelectedList: [],
  }

  constructor() {
    super()

    this.state = {
      categoryList: [],
      categorySelectedList: [],

      description: '',
      data: {},
      dateBegin: '',

      isProvider: false,

      order: [],

      openCategorySelect: false,

      title: '',
      widgetId: ''
    }
  }

  componentWillMount() {
    const {
      categorySelectedList = [],
      data = {},
      isProvider,
      order = [],
      size: { height },
      title = {},
      dateBegin
    } = this.props

    const { caption, description, vidget } = title

    if (!order.length) {
      return this.setState({ description, title: caption })
    }

    const categoryList = this.getCategoryList({ data, order })

    this.setState({
      data,
      dateBegin,
      description,
      categorySelectedList: categorySelectedList.length ? categorySelectedList : categoryList,
      categoryList,
      height,
      isProvider,
      order,
      title: caption,
      widgetId: vidget
    })
  }

  componentWillReceiveProps(nextProps) {
    const {
      categorySelectedList = [],
      data = {},
      dateBegin,
      isProvider,
      order = [],
      size: { height },
      title = {}
    }  = nextProps

    const { caption, description, vidget } = title

    if (order.length === 0) {
      return this.setState({ title: caption, description, data, order, })
    }

    const categoryList = this.getCategoryList({ data, order })
    console.log(categoryList)
    this.setState({
      data,
      dateBegin,
      description,
      categorySelectedList: categorySelectedList.length ? categorySelectedList : categoryList,
      categoryList,
      height,
      isProvider,
      order,
      title: caption,
      widgetId: vidget
    })
  }

  onSelectDiscountCallback = ({ id, name }) => {
    const { dispatch, onSelect } = this.props

    dispatch({
      type: reducerDiscount.types.SET_SUCCESS,
      payload: {
        widget: '12',
        param1: id,   // id выбранной категории
        param2: name  //свой - чужой
      }
    })

    onSelect && onSelect({ id, name })
  }

  onHandleCategorySelect = () => {
    const { onGetCategoryList } = this.props

    const { categoryList } = this.state

    onGetCategoryList(categoryList)
  }

  getCategoryList = ({ data, order }) => {
    return order.reduce((categoryList, item) => {
      const { valueSelf, valueOther } = data[item]

      if (valueSelf >= MAX_VALUE_PERCENT && valueOther >= MAX_VALUE_PERCENT) {
        categoryList.push({ ...data[item] })
      }

      return [...categoryList]
    }, [])
  }

  //формируем объект config и список категорий
 /* getChartData = (options) => {
    const { data, height, isProvider, order } = options

    const chart = new Chart()

    chart.setSeriesChart({ data, order, maxValue: MAX_VALUE_PERCENT })

    let config = {}
    if (isProvider) config = chart.getConfigForProvider({ onSelect: this.onSelectDiscountCallback, height })
    if (!isProvider) config = chart.getConfigForCategoryManager({ height })

    return {
      config,
      categoryList: chart.getCategoryList()
    }
  }*/

  onExportImage = () => {
    const { title } = this.state
    //if (this.chart) {
    //  this.chart.getChart().exportChart({filename: title ? title : 'виджет'}, null)
   // }
  }

  render() {
    const {
      categorySelectedList,
      data,
      description,
      dateBegin,
      height,
      isProvider,
      order = [],
      title,
      widgetId,
    } = this.state

    const { loading } = this.props

    if (loading) return (
      <WidgetProgressBar title='Сравнение состава чеков' />
    )

    if (!loading && order.length === 0) return <EmptyWidget title='Сравнение состава чеков' />

    return (
      <Widget>
        <WidgetHeader
          title={ title }
          description={ description }
          dateBegin={ dateBegin }
          widgetId={ widgetId }

          exportImage={ this.onExportImage }
          onOpenCategorySelect={ this.onHandleCategorySelect }
        />
        <ChartManage
          categorySelectedList={ categorySelectedList }
          data={ data }
          height={ height }
          isProvider={ isProvider }
          order={ order }

          onSelectDiscountCallback={ this.onSelectDiscountCallback }
        />
      </Widget>
    )
  }
}

export default compose(
  sizeMe({ monitorHeight: true, monitorWidth: true }),
  withData
)(Container)
