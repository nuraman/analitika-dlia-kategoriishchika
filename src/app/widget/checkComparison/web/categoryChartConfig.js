import Highcharts           from 'highcharts'


export default ({ series, categoryChart, height }) => {
  const heightChart = `${ height - 60 }px`

  const config = {
    chart: {
      type: 'bar',
      height: heightChart,
    },
    legend: {
      title: {
        style: {
          fontFamily: 'HelveticaRegular',
          fontSize: '12px',
        }
      }
    },
    navigation: {
      buttonOptions: {
        enabled: false,
      }
    },
    plotOptions: {
      series: {
        stacking: 'normal'
      },
      bar: {
        style: {
          fontFamily: 'HelveticaRegular',
          fontSize: '12px',
          fontWeight: 'normal',
        }
      }
    },
    title: {
      text: ''
    },
    tooltip: {
      headerFormat: '<span style="font-size: 12px; font-weight: bold;">{series.name}</span><br/>',
      pointFormatter: function () {
        return 'Категория: ' + this.category + '</b><br/>' +
          'Проценты: ' + Highcharts.numberFormat(Math.abs(this.y), 1) +'%';
      },
      style: {
        fontFamily: 'HelveticaRegular'
      }
    },
    xAxis: [{
      categories: categoryChart,
      reversed: false,
      labels: {
        step: 1
      }
    }, { // mirror axis on right side
      opposite: true,
      reversed: false,
      categories: categoryChart,
      linkedTo: 0,
      labels: {
        step: 1
      }
    }],
    yAxis: {
      title: {
        text: null
      },
      labels: {
        formatter: function () {
          return Math.abs(this.value) + '%';
        },
        style: {
          fontFamily: 'HelveticaRegular',
          fontSize: '12px',
        }
      }
    },

    series: series
  }

  return config
}
