import React, { Component } from 'react'

import PropTypes            from 'prop-types'

import styled               from 'styled-components'

import config               from 'app/discount/theme'

import BlockHeader          from './Header'
import BlockFilter          from './Filter'
import BlockSearch          from './Search'
import BlockFooter          from './Footer'
import BlockCategoryList    from './CategoryList'

import withData             from 'app/widget/checkComparison/hoc/withDataCategorySelect'


const Container = styled.div`
  position: absolute;
  top: 10px; 
  left: 25%;
  width: 500px;
  height: 350px;
  background-color: ${ config.colorMap.BACKGROUND_COLOR_CONTAINER_RIGHT_SIDE };
  font-family: HelveticaRegular;
  
  input::-webkit-input-placeholder {
    opacity: 1;
    font-weight: 300;
    font-size: 18px;
    color: #fff;
    transition: opacity 0.2s ease;
  }

  input::-moz-placeholder {
    opacity: 1;
    font-weight: 300;
    font-size: 18px;
    color: #fff;
    transition: opacity 0.2s ease;
  }
  
  input:-moz-placeholder {
    opacity: 1;
    font-weight: 300;
    font-size: 18px;
    color: #fff;
    transition: opacity 0.2s ease;
  }
  
  input:-ms-input-placeholder {
    opacity: 1;
    font-weight: 300;
    font-size: 18px;
    color: #fff;
    transition: opacity 0.2s ease;
  }
  
  input:focus::-webkit-input-placeholder {
    opacity: 0;
    transition: opacity 0.2s ease;
  }
  
  input:focus::-moz-placeholder {
    opacity: 0;
    transition: opacity 0.2s ease;
  }
  
  input:focus:-moz-placeholder {
    opacity: 0;
    transition: opacity 0.2s ease;
  }
  
  input:focus:-ms-input-placeholder {
    opacity: 0;
    transition: opacity 0.2s ease;
  }
`

const Content = styled.div`
  display: block;
  height: 65%;
  padding-left: 10px;
  padding-right: 10px;
  padding-top: ${ props => props.paddingTop }px;
  padding-bottom: ${ props => props.paddingBottom }px;
`


class CategorySelect extends Component {
  static propTypes = {
    categoryList: PropTypes.array,
    categorySelectedList: PropTypes.array,
    open: PropTypes.bool,
    valueBigger: PropTypes.string,
    valueDifference: PropTypes.string
  }

  constructor(props) {
    super(props)

    const { open = false, categoryList = [], valueBigger } = props || {}

    this.state = {
      checkedList: [],
      checkedAll: false,

      categoryList, //весь список категорий
      categoryListBuffer: categoryList, // рабочий список категорий, изначально устанавливаем все
      categorySelectedList: [], //список только выбранных категорий

      valueBigger: valueBigger,
      valueDifference: '',

      open,
    }
  }


  componentWillReceiveProps(nextProps) {
    const {
      categoryList = [],
      categorySelectedList = [],
      valueBigger,
      valueDifference,

      open,
    } = nextProps

    if (categorySelectedList.length > 0) {
      const newCheckedList = categorySelectedList.reduce((newCheckedList, item) => {

        const { categoryId } = item

        return [ ...newCheckedList, categoryId ]
      }, [])

      return this.setState({
        categoryList,
        categoryListBuffer: categorySelectedList,
        categorySelectedList,
        checkedList: newCheckedList,
        valueBigger,
        valueDifference,
        open
      })
    }

    this.setState({
      categoryList,
      categoryListBuffer: categoryList,
      categorySelectedList,
      open
    })
  }

  onChange = (checkedList) => this.setState({ checkedList })

  onChangeAll = () => {
    const { checkedAll, categoryList } = this.state

    let newCheckedList = []
    if (checkedAll === false) {
      newCheckedList = categoryList.map((item) => {
        const { categoryId } = item

        return categoryId
      })
    }

    this.setState({
      checkedAll: !checkedAll,
      checkedList: newCheckedList,
      categorySelectedList: categoryList,
      categoryListBuffer: categoryList
    })
  }

  onHandleClick = () => {
    const { categoryList, checkedAll, open, checkedList, valueBigger, valueDifference } = this.state

    const { onSelect } = this.props

    if (categoryList.length === checkedList.length && checkedAll) {
      onSelect && onSelect({ categorySelectedList: [], valueBigger: '', valueDifference: '' })
    } else {
      const categorySelectedList = categoryList.reduce((categorySelectedList, item) => {
        const { categoryId } = item

        if (checkedList.indexOf(categoryId) !== -1)
          categorySelectedList.push(item)

        return [...categorySelectedList]
      }, [])

      onSelect && onSelect({ categorySelectedList, valueBigger, valueDifference })
    }


    this.setState({
      checkedAll: false,
      open: !open,
      valueBigger: '',
      valueDifference: ''
    })
  }

  onHandleClose = () => {
    const { onClose } = this.props

    onClose()

    this.setState({ open: false })
  }

  onSelectCategoryList = ({ categoryList, valueBigger, valueDifference }) => {
    const checkedList = categoryList.reduce((checkedList, item) => {
      const { categoryId } = item

      checkedList.push(categoryId)

      return [...checkedList]
    }, [])

    this.setState({
      checkedList,
      categorySelectedList: categoryList,
      categoryListBuffer: categoryList,
      valueBigger,
      valueDifference,
    })
  }

  onSelectSuggestionList = (categorySuggestionList) => this.setState({ categoryListBuffer: categorySuggestionList })

  render() {
    const {
      categoryListBuffer,
      categoryList,
      checkedAll,
      checkedList,
      open,

      valueBigger,
      valueDifference,
    } = this.state

    if (open === false) return null

    return(
      <Container>
       <BlockHeader
         onClick={ this.onHandleClose }
       />
        <Content>
          <BlockFilter
            categoryList={ categoryList }
            valueBigger={ valueBigger }
            valueDifference={ valueDifference }

            checked={ checkedAll }

            onSelect={ this.onSelectCategoryList }
            onClick={ this.onChangeAll }
          />
          <BlockSearch
            categoryList={ categoryList }

            onSelect={ this.onSelectSuggestionList }
          />
          <BlockCategoryList
            categoryList={ categoryListBuffer }
            checkedList={ checkedList }

            onGetSelectedItem={ this.onChange }
          />
        </Content>
       <BlockFooter
         onClick={ this.onHandleClick }
       />
      </Container>
    )
  }
}

export default withData(CategorySelect)
