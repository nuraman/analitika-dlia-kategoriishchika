import React, { Component } from 'react'

import styled               from 'styled-components'
import config               from "../../../../discount/theme"

import BlockCompareBetween  from './CompareBetween'


const Container = styled.div`
  display: flex;
  height: 40px;
  width: calc(100% - 2px);
  flex-direction: row;
  align-items: center;
  border: 1px solid ${ config.colorMap.COLOR_INPUT_BORDER };
`

const SelectAll = styled.div`
  display: flex;
  height: 30px;
  width: 140px;
  flex-direction: row;
  align-items: center;
  padding: 5px;
`

const Input = styled.input`
  display: none;
  
  &:checked {
    display: block; 
  }
`

const LabelNewCheck = styled.label`
  position: relative;
  width: 18px;
  min-width: 18px;
  height: 18px;
  margin-right: 10px;
  border: 1px solid #48b9bf;
  cursor: pointer;
  
  &:before {
    display: ${ props=>props.checked ? 'block' : 'none' };
    content: "";
    position: absolute;
    width: 12px;
    height: 12px;
    top: 3px;
    left: 3px;
    background-color: #48b9bf;
  }
`

const Label = styled.label`
  margin-bottom: -2px;
  font-weight: 300;
  font-size: 16px;
  font-family: HelveticaRegular;
  color: #b3fffb;
  cursor: pointer;
`

class CategoryFilter extends Component {
  constructor() {
    super()

    this.state = {
      categoryFullList: [],
      valueBigger: '',
      valueDifference: '',

      clearCompareBetween: false,

      checked: false,
    }
  }

  componentWillMount() {
    const { categoryList = [], valueBigger, valueDifference } = this.props

    this.setState({ categoryFullList: categoryList, valueBigger, valueDifference, })
  }

  onChangeAll = () => {
    const { onClick } = this.props

    this.setState({ checked: !this.state.checked, clearCompareBetween: true })

    setTimeout(() => {
      this.setState({ clearCompareBetween: false })
    }, 0)

    onClick()
  }

  onSelectCategory = ({ categoryList, valueBigger, valueDifference }) => {
    const { onSelect } = this.props

    this.setState({ valueDifference: '' })

    onSelect({ categoryList, valueBigger, valueDifference })
  }

  render() {
    const {
      checked,
      categoryFullList,
      valueBigger,
      valueDifference,
      clearCompareBetween
    } = this.state

    return (
      <Container>
        <SelectAll>
          <Input type='checkbox' name='' value='' />
          <LabelNewCheck
            checked={ checked }
            onClick={ this.onChangeAll }
          />
          <Label
            onClick={ this.onChangeAll }
          >
            Выбрать все
          </Label>
        </SelectAll>
        <BlockCompareBetween
          categoryList={ categoryFullList }
          valueBigger={ valueBigger }
          valueDifference={ valueDifference }

          clearValue={ clearCompareBetween }

          onSelect={ this.onSelectCategory }
        />
      </Container>
    )
  }
}

export default CategoryFilter
