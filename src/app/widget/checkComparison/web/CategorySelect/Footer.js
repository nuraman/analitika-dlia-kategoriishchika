import React                from 'react'

import styled               from 'styled-components'


const DivBlockFooter = styled.div`
  display: flex;
  height: 20%;
  align-items: center;
  padding-left: 10px;
  padding-right: 10px;
  width: calc(100% - 20px);
`

const LinkCastingBtn = styled.a`
  text-decoration: none;
  border: none;
  outline: none;
  cursor: pointer;
  width: 100%;
`

const DivLinkText = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  box-sizing: border-box;
  width: 100%;
  height: 30px;
  text-align: center;
  font-weight: 300;
  font-size: 14px;
  font-family: HelveticaRegular;
  color: #fff;
  background-color: #48b9bf;
`

export default (props) => {
  const { onClick } = props

  const onHandleClick = () => onClick()

  return (
    <DivBlockFooter>
      <LinkCastingBtn
        onClick={ onHandleClick }
      >
        <DivLinkText>
          Выбрать
        </DivLinkText>
      </LinkCastingBtn>
    </DivBlockFooter>
  )
}
