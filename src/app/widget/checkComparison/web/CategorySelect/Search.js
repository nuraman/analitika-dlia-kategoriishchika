import _                    from 'lodash'

import React, { Component } from 'react'

import styled               from 'styled-components'


import config               from "../../../../discount/theme";


const Container = styled.div`
  display: flex;
  height: 39px;
  width: calc(100% - 2px);
  flex-direction: row;
  align-items: center;
  border-left: 1px solid ${ config.colorMap.COLOR_INPUT_BORDER };
  border-right: 1px solid ${ config.colorMap.COLOR_INPUT_BORDER };
  border-bottom: 1px solid ${ config.colorMap.COLOR_INPUT_BORDER };
`

const Input = styled.input`
  box-sizing: border-box;
  width: 100%;
  padding: 10px;
  height: 40px;
  background-color: transparent;
  border: none;
  outline: none;
  color: #fff;
  font-family: HelveticaRegular;
  font-size: 14px;
`

class CategorySearch extends Component {
  constructor() {
    super()

    this.state = {
      value: '',
      categoryList: [],
      categorySuggestionList: []
    }
  }

  componentWillMount() {
    const { categoryList } = this.props

    this.setState({ categoryList })
  }

  onIsContain = (name, inputValue) => {
    const inputList = _.words(inputValue)

    const matchList = inputList.reduce((matchList, item) => {
      if (name.toLowerCase().includes(item)) matchList.push(item)

      return matchList
    }, [])

    if ( matchList.length >= inputList.length ) return name

    return null
  }

  onGetSuggestions = (value, categoryList) => {
    const inputValue = value.trim().toLowerCase()
    const inputLength = value.length

    return inputLength === 0 ? categoryList : categoryList.filter(x => this.onIsContain(x.categoryName, inputValue))
  }

  onChange = ({ currentTarget: { value } }) => {
    const { categoryList } = this.state

    const { onSelect } = this.props

    const categorySuggestionList = this.onGetSuggestions(value, categoryList)

    onSelect(categorySuggestionList)

    this.setState({ value })
  }

  render() {
    const { value } = this.state

    return (
      <Container>
        <Input
          placeholder='Введите название категории'
          type='text'
          value={ value }

          onChange={ this.onChange }
        />
      </Container>
    )
  }
}

export default CategorySearch
