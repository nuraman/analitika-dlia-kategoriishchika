import React, { Component } from 'react'

import styled               from 'styled-components'

import api                  from 'app/widget/checkComparison/web/api/categorySelect'

const Container = styled.div`
  display: flex;
`

const Bigger = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  color: #fff;
`

const Link = styled.a`
  text-decoration: none;
  border: none;
  cursor: pointer;
  width: 100%;
  padding-left: 5px;
`

const Label = styled.label`
  display: flex;
  align-items: center;
  justify-content: center;
  box-sizing: border-box;
  width: 80px;
  height: 25px;
  text-align: center;
  font-weight: 300;
  font-size: 14px;
  font-family: HelveticaRegular;
  color: #fff;
  background-color: #404040;
`

const LinkText = Label.extend`
  background-color: #48b9bf;
  margin-right: 5px;
`

const Input = styled.input`
  box-sizing: border-box;
  width: 30px;
  height: 25px;
  background-color: transparent;
  border-bottom: 1px solid #fff;
  border-top: none;
  border-left: none;
  border-right: none;
  outline: none;
  color: #fff;
  font-family: HelveticaRegular;
  font-size: 14px;
`

const Difference = Bigger.extend`
  padding: 0;
`


class CompareBetween extends Component {
  constructor() {
    super()

    this.state = {
      categoryList: [],
      valueBigger: '',
      valueDifference: ''
    }
  }

  componentWillMount() {
    const { categoryList = [], valueBigger, valueDifference } = this.props

    this.setState({ categoryList, valueBigger, valueDifference })
  }

  componentWillReceiveProps(nextProps) {
    const { clearValue } = nextProps

    if (clearValue) {
      this.setState({ valueBigger: '', valueDifference: '' })
    }
  }

  onChangeBigger = ({ currentTarget: { value } }) => this.setState({ valueBigger: value, valueDifference: '' })
  onChangeDifference = ({ currentTarget: { value } }) => this.setState({ valueDifference: value, valueBigger: '' })

  onHandleClick = () => {
    const { valueBigger, valueDifference, categoryList } = this.state

    const { onSelect } = this.props

    let newCategoryList = []

    if (valueBigger) newCategoryList = api.selectCategoryListByBigger({ valueBigger, categoryList })
    if (valueDifference) newCategoryList = api.selectCategoryListByDifference({ valueDifference, categoryList })

    onSelect && onSelect({ categoryList: newCategoryList, valueBigger, valueDifference })
  }

  render() {
    const { valueBigger, valueDifference } = this.state

    return (
      <Container>
        <Bigger>
          <Label>
            Показать >
          </Label>
          <Input
            type='text'
            value={ valueBigger }

            onChange={ this.onChangeBigger }
          />
          <div>
            %
          </div>
        </Bigger>
        <Difference>
          <Label>
            Разница >
          </Label>
          <Input
            value={ valueDifference }
            type='text'

            onChange={ this.onChangeDifference }
          />
          <div>
            %
          </div>
        </Difference>
        <Link onClick={ this.onHandleClick }>
          <LinkText>
            Применить
          </LinkText>
        </Link>
      </Container>
    )
  }
}

export default CompareBetween
