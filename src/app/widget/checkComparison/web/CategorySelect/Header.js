import React                from 'react'

import styled               from 'styled-components'
import config               from 'app/discount/theme'

const Container = styled.div`
  display: block;
  height: 15%;
  padding-left: 10px;
  padding-right: 10px;
`

const Table = styled.div`
  display: table;
  height: 100%;
  width: 100%;
`

const Right = styled.div`
  display: table-cell;
  text-align: right;
  vertical-align: middle;
`

const Flex = styled.div`
  display: flex;
  height: 100%;
  flex-direction: row;
  align-items: center;
`

const Icon = styled.div`
  min-width: 20px;
  width: 20px;
  height: 17px;
  fill: #9e9e9e;
  
  background-image: url(${ props => props.path });
  background-size: cover;
`

const Title = styled.div`
  font-family; HelveticaRegular;
  font-size: 14px;
  color: ${ config.colorMap.COLOR_FONT };
  height: 14px;
  padding-left: ${ props => props.paddingLeft }px;
`

const WrapperIcon = styled.div`
  display: table-cell;
  text-align: center;
  vertical-align: middle;
  background-color: ${ config.colorMap.BACKGROUND_COLOR_CONTAINER_RIGHT_SIDE };
`

const Link = styled.a`
  display: inline-table;
  height: 100%;
  cursor: pointer;
  background-color: ${ config.colorMap.BACKGROUND_COLOR_CONTAINER_RIGHT_SIDE }
`


export default (props) => {
  const { onClick } = props

  const onHandleClose = () => {
    onClick()
  }

  return (
    <Container>
      <Table>
        <Flex>
          <Icon path='assets/product.svg' />
          <Title paddingLeft={ 10 }>
            Выберите категории товаров
          </Title>
        </Flex>
        <Right>
          <Link onClick={ onHandleClose }>
            <WrapperIcon>
              <Icon path='assets/close.svg' />
            </WrapperIcon>
          </Link>
        </Right>
      </Table>
    </Container>
  )
}
