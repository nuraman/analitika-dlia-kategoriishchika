import React, { Component } from 'react'

import styled               from 'styled-components'


import config               from 'app/discount/theme'


const Content = styled.div`
  display: block;
  height: calc(100% - 82px);
  padding-top: ${ props => props.paddingTop }px;
  padding-bottom: ${ props => props.paddingBottom }px;
`

const List = styled.div`
  height: 100%;
  border-left: 1px solid ${ config.colorMap.COLOR_INPUT_BORDER };
  border-right: 1px solid ${ config.colorMap.COLOR_INPUT_BORDER };
  border-bottom: 1px solid ${ config.colorMap.COLOR_INPUT_BORDER };
  overflow: hidden;
`

const ListScroll = styled.div`
  height: 100%;
  width: calc(100% - 10px);
  margin-right: 10px;
  overflow: auto;
  
  &::-webkit-scrollbar
  {
    width: 9px;
  }
  
  &::-webkit-scrollbar-track
  {
    background: ${ config.colorMap.COLOR_INPUT_BORDER };
    border: 3px solid transparent;
    background-clip: content-box;
  }
  
  &::-webkit-scrollbar-thumb
  {
    background: #73c7c5;
    height: 35px;
    border-radius: 5px solid #73c7c5;
    border: 1px solid #73c7c5;
  }
`


const Input = styled.input`
  display: none;
  
  &:checked {
    display: block; 
  }
`

const LabelCheck = styled.label`
  position: relative;
  width: 18px;
  min-width: 18px;
  height: 18px;
  margin-right: 10px;
  border: 1px solid #48b9bf;
  cursor: pointer;
  
  &:before {
    display: ${ props=>props.checked ? 'block' : 'none' };
    content: "";
    position: absolute;
    width: 12px;
    height: 12px;
    top: 3px;
    left: 3px;
    background-color: #48b9bf;
  }
`

const Label = styled.label`
  margin-bottom: -2px;
  font-weight: 300;
  font-size: 16px;
  font-family: HelveticaRegular;
  color: #b3fffb;
  cursor: pointer;
`

const ListItem = styled.div`
  display: flex;
  align-items: center;
  padding: 5px;
`


class CategoryList extends Component {
  constructor() {
    super()

    this.state = {
      categoryList: [],
      checkedList: []
    }
  }

  componentWillMount() {
    const { categoryList = [], checkedList = [] } = this.props

    this.setState({ categoryList, checkedList })
  }

  componentWillReceiveProps(nextProps) {
    const { categoryList = [], checkedList = [] } = nextProps

    this.setState({ categoryList, checkedList })
  }

  onChange = (id) => {
      const { checkedList } = this.state
      const { onGetSelectedItem } = this.props


      const currentIndex = checkedList.indexOf(id)

      if (currentIndex === -1) {
        checkedList.push(id)
      } else {
        checkedList.splice(currentIndex, 1)
      }

      onGetSelectedItem(checkedList)
  }

  render() {
    const { categoryList, checkedList } = this.state

    return (
      <Content>
        <List>
          <ListScroll>
            {
              categoryList.map((item) => {
                const { id, categoryId, categoryName } = item

                const isChecked = checkedList.indexOf(categoryId) !== -1

                return (
                  <ListItem key={ id }>
                    <Input type='checkbox' name='' value='' />
                    <LabelCheck
                      checked={ isChecked }
                      onClick={ () => this.onChange(categoryId) }
                    />
                    <Label
                      onClick={ () => this.onChange(categoryId) }
                    >
                      { categoryName }
                    </Label>
                  </ListItem>
                )
              })
            }
          </ListScroll>
        </List>
      </Content>
    )
  }
}

export default CategoryList
