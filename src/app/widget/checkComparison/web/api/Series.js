function Series() {
  this.dataSelf = []
  this.dataOther = []
  this.categoryList = []

  this.colorSeriesOwn = ''
  this.colorSeriesOther = ''
}

Series.prototype.getSeries = function(payload) {
  const { data, order } = payload

  order.forEach((item) => {
    const { id, categoryId, categoryName, valueSelf, valueOther, colorSelf, colorOther } = data[item] || {}

    this.colorSeriesOwn = colorSelf
    this.colorSeriesOther = colorOther

    //1 - cвой
    //2 - чужой

    this.dataSelf.push({
      name: '1',
      id: categoryId,
      y: valueSelf,
      color: `#${ colorSelf }`
    })
    this.dataOther.push({
      name: '2',
      id: categoryId,
      y: -valueOther,
      color: `#${ colorOther }`
    })

    this.categoryList.push({
      categoryName,
      id,
      categoryId,
      valueSelf,
      valueOther
    })
  })

  const series = [
    {
      name: 'Конкуренты',
      data: this.dataOther,
      color: `#${ this.colorSeriesOther }`
    },
    {
      name: 'Мой бренд',
      data: this.dataSelf,
      color: `#${ this.colorSeriesOwn }`
    }
  ]

  return series
}

Series.prototype.getCategoryList = function () {
  return this.categoryList
}

Series.prototype.getSeriesWithCondition = function (payload) {
  const { data, order, maxValue } = payload

  order.forEach((item) => {
    const { id, categoryId, categoryName, valueSelf, valueOther, colorSelf, colorOther } = data[item] || {}

    this.colorSeriesOwn = colorSelf
    this.colorSeriesOther = colorOther

    if (valueSelf > maxValue || valueOther > maxValue) {
      this.dataSelf.push({
        name: '1',
        id: categoryId,
        y: valueSelf,
        color: `#${ colorSelf }`
      })
      this.dataOther.push({
        name: '2',
        id: categoryId,
        y: -valueOther,
        color: `#${ colorOther }`
      })

      this.categoryList.push({
        categoryName,
        id,
        categoryId,
        valueSelf,
        valueOther
      })
    }
  })


  const series = [
    {
      name: 'Конкуренты',
      data: this.dataOther,
      color: `#${ this.colorSeriesOther }`
    },
    {
      name: 'Мой бренд',
      data: this.dataSelf,
      color: `#${ this.colorSeriesOwn }`
    }
  ]

  return series
}

export default Series
