const selectCategoryListByBigger = ({ valueBigger, categoryList }) => {

    let newCategoryList = []

    if (+valueBigger) {
      try {
        newCategoryList = categoryList.reduce((newCategoryList, item) => {
          const { id, categoryName, categoryId, valueSelf, valueOther } = item

          if (valueSelf > valueBigger || valueOther > valueBigger) {
            newCategoryList.push({ id, categoryName, categoryId, valueSelf, valueOther })
          }

          return [...newCategoryList]
        }, [])
      }
      catch(e) {
        newCategoryList = []
      }
    }

    return newCategoryList
}

const selectCategoryListByDifference = ({ valueDifference, categoryList }) => {
  let newCategoryList = []

  if (+valueDifference) {
    try {
      newCategoryList = categoryList.reduce((newCategoryList, item) => {
        const {id, categoryName, categoryId, valueSelf, valueOther} = item

        let difference = Math.abs(valueSelf - valueOther)

        if (difference > valueDifference) {
          newCategoryList.push({id, categoryName, categoryId, valueSelf, valueOther})
        }

        return [...newCategoryList]
      }, [])
    }
    catch (e) {
      newCategoryList = []
    }
  }

  return newCategoryList
}

export default {
  selectCategoryListByDifference,
  selectCategoryListByBigger
}
