import Highcharts           from 'highcharts'

import Series               from './Series'


function Chart() {
  this.categoryChart = []
  this.seriesList = []

  this.series = new Series()

  this.setCategoryChart = function({ categoryList }) {
    this.categoryChart = categoryList.reduce((newCategoryChart, item) => {
      const { categoryName } = item

      newCategoryChart.push(categoryName)

      return newCategoryChart
    }, [])
  }
}

Chart.prototype.setSeriesChart = function(payload) {
  const { data, order, maxValue } = payload

  if (maxValue) {
    this.seriesList = this.series.getSeriesWithCondition({data, order, maxValue})
  } else {
    this.seriesList = this.series.getSeries({ data, order })
  }

  const categoryList = this.series.getCategoryList()

  this.setCategoryChart({ categoryList })
}

Chart.prototype.getCategoryList = function() {
  return this.series.getCategoryList()
}

Chart.prototype.getConfigForProvider = function ({ onSelect, height }) {
  const heightChart = `${ height - 60 }px`

  const config = {
    chart: {
      type: 'bar',
      height: heightChart,
    },
    legend: {
      itemStyle: {
        fontFamily: 'HelveticaBold',
        fontSize: '14px',
      }
    },
    navigation: {
      buttonOptions: {
        enabled: false,
      }
    },
    plotOptions: {
      series: {
        stacking: 'normal'
      },
      bar: {
        point: {
          events: {
            click: function() {
              onSelect && onSelect({ id: this.id, name: this.name })
            }
          }
        },
        style: {
          fontFamily: 'HelveticaRegular',
          fontSize: '12px',
          fontWeight: 'normal',
        }
      }
    },
    title: {
      text: ''
    },
    tooltip: {
      headerFormat: '<span style="font-size: 12px; font-weight: bold;">{series.name}</span><br/>',
      pointFormatter: function () {
        return 'Категория: ' + this.category + '</b><br/>' +
          'Проценты: ' + Highcharts.numberFormat(Math.abs(this.y), 1) +'%';
      },
      style: {
        fontFamily: 'HelveticaRegular'
      }
    },
    xAxis: [{
      categories: this.categoryChart,
      reversed: false,
      labels: {
        step: 1
      }
    }, { // mirror axis on right side
      opposite: true,
      reversed: false,
      categories: this.categoryChart,
      linkedTo: 0,
      labels: {
        step: 1
      }
    }],
    yAxis: {
      title: {
        text: null
      },
      labels: {
        formatter: function () {
          return Math.abs(this.value) + '%';
        },
        style: {
          fontFamily: 'HelveticaRegular',
          fontSize: '12px',
        }
      }
    },

    series: this.seriesList
  }

  return config
}

Chart.prototype.getConfigForCategoryManager = function ({ height }) {
  const heightChart = `${ height - 60 }px`

  const config = {
    chart: {
      type: 'bar',
      height: heightChart,
    },
    legend: {
      itemStyle: {
        fontFamily: 'HelveticaBold',
        fontSize: '14px',
      }
    },
    navigation: {
      buttonOptions: {
        enabled: false,
      }
    },
    plotOptions: {
      series: {
        stacking: 'normal'
      },
      bar: {
        style: {
          fontFamily: 'HelveticaRegular',
          fontSize: '12px',
          fontWeight: 'normal',
        }
      }
    },
    title: {
      text: ''
    },
    tooltip: {
      headerFormat: '<span style="font-size: 12px; font-weight: bold;">{series.name}</span><br/>',
      pointFormatter: function () {
        return 'Категория: ' + this.category + '</b><br/>' +
          'Проценты: ' + Highcharts.numberFormat(Math.abs(this.y), 1) +'%';
      },
      style: {
        fontFamily: 'HelveticaRegular'
      }
    },
    xAxis: [{
      categories: this.categoryChart,
      reversed: false,
      labels: {
        step: 1
      }
    }, { // mirror axis on right side
      opposite: true,
      reversed: false,
      categories: this.categoryChart,
      linkedTo: 0,
      labels: {
        step: 1
      }
    }],
    yAxis: {
      title: {
        text: null
      },
      labels: {
        formatter: function () {
          return Math.abs(this.value) + '%';
        },
        style: {
          fontFamily: 'HelveticaRegular',
          fontSize: '12px',
        }
      }
    },

    series: this.seriesList
  }

  return config
}

export default Chart
