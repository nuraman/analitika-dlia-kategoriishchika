import config               from 'app/widget/grf01/config'


export default function mapStateToProps(state) {
  const { user, menu } = state

  const { userLogin: { data, order } } = user
  const { provider, isProvider } = data[order[0]] || {}

  const { period, parameter } = menu

  const { dateBegin } = period

  return {
    menu,
    periodId: period.id,
    parameterId: parameter.id,
    currentProvider: provider,
    dateBegin,
    isProvider,
    options: {
      isProvider,
      variables: {
        [config.inputNameMap.DATE_BEGIN]: dateBegin,
      }
    }
  }
}
