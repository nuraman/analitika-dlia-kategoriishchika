import _update              from 'immutability-helper'


const name = 'user'

const types = {
  USER_SUCCESS:               `${ name }/USER_SUCCESS`,           //Успешно авторизован
  USER_REMOVE:                `${ name }/USER_REMOVE`,            //Пользователь вышел из сайта
}

const STATE = {
  data: {},
  queries: { userCurrent: {
      data: {}
    }
  },
  userLogin: {
    data: {},
    order: [],
    errors: [],
  },
  userLogout: {
    data: {},
    order: [],
    errors: [],
  }
}

const reducer = (state = STATE, action) => {
  const { type, payload } = action
  const { data, queries, userLogin } = state
  switch(type) {
    case types.USER_SUCCESS: {
      const { userId, session, provider, providerName, userName, isProvider } = payload

      const newData = _update(data, { $merge: { [userId]: { id: userId, session, provider, providerName, userName, isProvider } } })

      const newQueries = _update(queries, {
        userCurrent: { data: { $merge: { [userId]: { id: userId, session, provider, providerName, userName, isProvider } } } }
      })

      const newUserLogin = _update(userLogin, {
        data: { $merge: { [userId]: { id: userId, session, provider, providerName, userName, isProvider } } },
        order: { $push: [userId] }
      })

      return {
        ...state,
        data: newData,
        queries: newQueries,
        userLogin: newUserLogin,
      }
    }
    case types.USER_REMOVE: {
      return {
        data: {},
        queries: {
          userCurrent: {
            data: {}
          }
        },
        userLogin: {
          data: {},
          order: [],
          errors: [],
        },
      }
    }
    default:
      return state
  }
}

export default {
  types,
  reducer
}
