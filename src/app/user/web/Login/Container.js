import md5                  from 'js-md5'

import React, { Component } from 'react'

import { connect }          from 'react-redux'
import { compose }          from 'react-apollo'

import { withApollo }       from 'react-apollo'

import go                   from 'app/story/go'

import config               from 'app/user/config'
import withUserLogin        from 'app/user/graphQl/userLogin'
import withChangePassword   from 'app/user/graphQl/userChangePassword'

import reducer              from 'app/user/reducer'

import LoginForm            from 'app/user/web/Login/LoginForm'

import UserLoadSetting      from 'app/page/web/UserLoadSetting'
import UserFormReset        from 'app/user/web/UserFormReset'


const codeMap = {
  ERROR: 1,
  SUCCESS: 0,
}

class Container extends Component {
  constructor(){
    super()

    this.state = {
      isChangePassword: false,
      message: '',
      password: '',
      username: '',

      reset: false,

      userLoadSetting: false,
    }
  }

  onSubmit = ({ username, password, rememberMe }) => {
    const { isChangePassword } = this.state

    if (!isChangePassword) this.onAuthUser({ username, password })

    if (isChangePassword) this.onFirstChangePassword({ password })

    this.onRemeberMe({ username, password, rememberMe })
  }

  onAuthUser = ({ username, password }) => {
    const { dispatch } = this.props

    const encryptedPassword = this.onEncryptPasswordWithMD5({ password })

    localStorage.removeItem('sessionID')
    //seller - пока не нужен и скорее всего вообще не пригодится, нужно поговорить с бэкендщиком
    this.props.userLogin({ seller: 1, username, password: encryptedPassword }).then(({ data }) => {
      const { userLogin } = data
      const { result, errors } = userLogin

      const { code, message } = errors

      if (code === codeMap.ERROR) {
        return this.setState({ message })
      }

      const { changePassword, provider, user, session, providerName, userName } = result

      if (changePassword === true && code === 0) {
        this.setState({ isChangePassword: true, message })

        dispatch({
          type: reducer.types.USER_SUCCESS,
          payload: {
            userId: user,
            session,
            provider,
            providerName,
            userName,
            isProvider: providerName !== '' //определяется категорист или поставщик
          },
        })

        localStorage.setItem('sessionID', session)
      }

      if (changePassword === false && code === 0) {
        dispatch({
          type: reducer.types.USER_SUCCESS,
          payload: {
            userId: user,
            session,
            provider,
            providerName,
            userName,
            isProvider: providerName !== ''  //определяется категорист или поставщик
          },
        })
        localStorage.setItem('sessionID', session)
        this.setState({ userLoadSetting: !this.state.userLoadSetting })
      }
    }).catch((error) => {
      console.log('there some errors', error)
    })
  }

  onFirstChangePassword = ({ password }) => {
    const encryptedPassword = this.onEncryptPasswordWithMD5({ password })

    this.props.userChangePassword({ password: encryptedPassword }).then(({ data }) => {
      const { userChangePassword = {} } = data
      const { errors: { code, message } } = userChangePassword

      if (code === codeMap.ERROR) {
        return this.setState({ message })
      }

      if (code === codeMap.SUCCESS) {
        this.setState({ isChangePassword: false })

        go('/')
      }
    })
  }

  onEncryptPasswordWithMD5 = ({ password }) => {
    const { key } = config

    md5(key)

    const passwordHash = md5.update(password)

    return passwordHash.hex()
  }

  onForgetPassword = () => this.setState({ reset: !this.state.reset })

  onResetSuccessPassword = ({ resetSuccessMessage }) => {
    this.setState({ reset: !this.state.reset, message: resetSuccessMessage })
  }


  onRemeberMe = ({ username, password, rememberMe }) => {
    if (rememberMe) {
      localStorage.setItem('username', username)
      localStorage.setItem('password', password)
    }

    if (!rememberMe) {
      localStorage.removeItem('username')
      localStorage.removeItem('password')
    }
  }

  componentWillMount() {
    const username = localStorage.getItem('username')
    const password = localStorage.getItem('password')

    this.setState({ username, password })
  }

  render() {
    const {
      message,
      username,
      password,
      userLoadSetting,
      reset
    } = this.state

    if (userLoadSetting) return <UserLoadSetting />

    if (reset) {
      return <UserFormReset
        onReset={ this.onResetSuccessPassword }
      />
    }

    return(
      <LoginForm
        message={ message }
        password={ password }
        username={ username }

        onSubmit={ this.onSubmit }
        onForgetPassword={ this.onForgetPassword }
      />
    )
  }
}

const withApolloContainer = withApollo(Container)

export default compose(
  connect(),
  withUserLogin(),
  withChangePassword(),
)(withApolloContainer)

