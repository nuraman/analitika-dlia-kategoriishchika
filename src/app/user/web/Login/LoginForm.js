import React, { Component } from 'react'

import styled               from 'styled-components'


const DivWrapAuth = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 100vh;
  background: #484848 url(assets/login/big.png) center/cover no-repeat;
  
  input::-webkit-input-placeholder {
    opacity: 1;
    font-weight: 300;
    font-size: 18px;
    color: #fff;
    transition: opacity 0.2s ease;
  }

  input::-moz-placeholder {
    opacity: 1;
    font-weight: 300;
    font-size: 18px;
    color: #fff;
    transition: opacity 0.2s ease;
  }
  
  input:-moz-placeholder {
    opacity: 1;
    font-weight: 300;
    font-size: 18px;
    color: #fff;
    transition: opacity 0.2s ease;
  }
  
  input:-ms-input-placeholder {
    opacity: 1;
    font-weight: 300;
    font-size: 18px;
    color: #fff;
    transition: opacity 0.2s ease;
  }
  
  input:focus::-webkit-input-placeholder {
    opacity: 0;
    transition: opacity 0.2s ease;
  }
  
  input:focus::-moz-placeholder {
    opacity: 0;
    transition: opacity 0.2s ease;
  }
  
  input:focus:-moz-placeholder {
    opacity: 0;
    transition: opacity 0.2s ease;
  }
  
  input:focus:-ms-input-placeholder {
    opacity: 0;
    transition: opacity 0.2s ease;
  }
`

const DivAuthBlock = styled.div`
  width: 680px;
  margin-top: -10%;
`

const DivAuthBlockWrapTitle = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-between;
  margin: -5px;
  padding: 20px 65px;
`

const DivAuthBlockTitle = styled.div`
  margin: 5px;
  font-size: 28px;
  font-family: HelveticaRegular;
  color: #fff;
`

const DivAuthBlockCaption = styled.div`
  margin: 5px;
`

const DivAuthBlockWrapForm = styled.div`
  margin: -5px;
  background-color: #383838;
`

const DivFormAb = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: flex-start;
  padding: 40px 65px;
  
  > div: nth-child(2) {
    display: flex;
    align-items: center;
    width: calc(50% - 12px);
    height: 50px;
    margin: 5px;
  }
  
  > div: nth-child(3) {
    display: flex;
    align-items: center;
    width: calc(50% - 12px);
    height: 50px;
    margin: 5px;
  }
  
  > div: nth-child(4) {
    display: flex;
    align-items: center;
    width: calc(50% - 12px);
    height: 50px;
    margin: 5px;
  }
  
  > div: nth-child(5) {
    display: flex;
    align-items: center;
    width: calc(50% - 12px);
    height: 50px;
    margin: 5px;
  }
`

const DivAuthBlockRowForget = styled.div`
  width: 100%;
  text-align: right;
`

const LinkAuthBlockForget = styled.a`
  margin: 5px;
  text-decoration: none;
  font-size: 14px;
  font-family: HelveticaRegular;
  color: #48b9bf;
  cursor: pointer;
  
  &:hover {
    text-decoration: underline;
    color: #59cad0;
  }
`

const DivInputWrap = styled.div`
  display: flex;
  align-items: center;
  border: 1px solid #9e9e9e;
`

const DivIcoPersonForm = styled.div`
  width: 40px;
  height: 100%;
  background: center no-repeat;
  
  &.person {
    background-image: url(assets/login/ico-auth-user.png);
    background-size: 14px 14px;
  }
`

const DivIcoPasswordForm = styled.div`
  width: 40px;
  height: 100%;
  background: center no-repeat;
  
  &.password {
    background-image: url(assets/login/ico-auth-pass.png);
    background-size: 18px 11px;
  }
`

const Input = styled.input`
  display: block;
  box-sizing: border-box;
  width: 100%;
  padding: 7px 0;
  border: none;
  outline: none;
  font-weight: 300;
  font-size: 18px;
  color: #fff;
  background-color: transparent;
  &:focus { 
    outline:none; 
  }
`

const DivCheckWrap = styled.div`
  border: 1px solid transparent;
`

const InputCheck = styled.input`
  display: none;
  
  &:checked {
    display: block; 
  }
`

const LabelNewCheck = styled.label`
  position: relative;
  width: 22px;
  height: 22px;
  margin-right: 10px;
  border: 1px solid #9e9e9e;
  cursor: pointer;
  
  &:before {
    display: ${ props=>props.checked ? 'block' : 'none' };
    content: "";
    position: absolute;
    width: 14px;
    height: 14px;
    top: 4px;
    left: 4px;
    background-color: #48b9bf;
  }
`

const Label = styled.label`
  font-weight: 300;
  font-size: 18px;
  font-family: HelveticaRegular;
  color: #fff;
  cursor: pointer;
`

const DivBtnWrap = styled.div`
  border: 1px solid transparent;
`

const ButtonForm = styled.input`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
  text-align: center;
  font-weight: 300;
  font-family: HelveticaRegular;
  font-size: 18px;
  border: none;
  outline: none;
  color: #fff;
  background-color: #48b9bf;
  cursor: pointer;
    
  &:hover {
    background-color: #59cad0;
  }
  
  &:focus, &:active {
    border: none;
    outline: none;
  }
`

const DivAuthLogo = styled.div`
  position: absolute;
  left: 50%;
  bottom: 5%;
  transform: translateX(-50%);
`

const DivIncorrectLogin = styled.div`
  display: ${ props=>props.error ? 'flex' : 'none' };
  align-items: center;
  height: 30px;
  width: 100%;
  font-family: HelveticaRegular;
  font-size: 18px;
  color: red; 
`

class LoginForm extends Component {
  constructor(){
    super()

    this.state = {
      username: '',
      password: '',
      rememberMe: false,
      message: '',

      error: false,
    }
  }

  onChangeLogin = ({ target: { value } }) => this.setState({ username: value })
  onChangePassword = ({ target: { value } }) => this.setState({ password: value })
  onChangeRememberMe = () => this.setState({ rememberMe: !this.state.rememberMe })

  onHandleKeyPress = (e) => {
    if (e.key === 'Enter') {
      this.onSubmit()
    }
  }

  onSubmit = () => {
    const { username, password, rememberMe } = this.state
    const { onSubmit } = this.props

    onSubmit && onSubmit({ username, password, rememberMe })
  }

  onForgetPassword = () => {
    const { onForgetPassword } = this.props

    onForgetPassword()
  }

  componentWillReceiveProps(nextProps) {
    const { message, username, password } = nextProps

    if (message) this.setState({ message })

    if (username && password) this.setState({ username, password, rememberMe: true  })
  }

  componentWillMount() {
    const { message, username, password } = this.props

    if (message) this.setState({ message })

    if (username && password) this.setState({ username, password, rememberMe: true  })
  }

  render() {
    const { username, password, rememberMe, message } = this.state

    return (
      <DivWrapAuth>
        <DivAuthBlock>
          <DivAuthBlockWrapTitle>
            <DivAuthBlockTitle>
              Авторизация
            </DivAuthBlockTitle>
            <DivAuthBlockCaption>
              <img src='assets/img-com.png' alt='' />
            </DivAuthBlockCaption>
          </DivAuthBlockWrapTitle>
          <DivAuthBlockWrapForm>
            <DivFormAb>
              <DivAuthBlockRowForget>
                <LinkAuthBlockForget
                  onClick={ this.onForgetPassword }
                >
                  Забыли пароль?
                </LinkAuthBlockForget>
              </DivAuthBlockRowForget>
              <DivInputWrap>
                <DivIcoPersonForm className='person' />
                <Input
                  type='text'
                  placeholder='Имя пользователя'
                  autocomplete='off'
                  value={ username }
                  onChange={ this.onChangeLogin }
                />
              </DivInputWrap>
              <DivInputWrap>
                <DivIcoPasswordForm className='password' />
                <Input
                  type='password'
                  placeholder='Пароль'
                  autocomplete='off'
                  value={ password }
                  onChange={ this.onChangePassword }

                  onKeyPress={ this.onHandleKeyPress }
                />
              </DivInputWrap>
              <DivCheckWrap>
                <InputCheck type='checkbox' name='' value='' />
                <LabelNewCheck
                  checked={ rememberMe }
                  onClick={ this.onChangeRememberMe }
                />
                <Label>Запомнить меня</Label>
              </DivCheckWrap>
              <DivBtnWrap>
                <ButtonForm
                  type='submit'

                  value='Вход'

                  onClick={ this.onSubmit }
                />
              </DivBtnWrap>
              <DivIncorrectLogin error={ message.length > 0 }>
                { message }
              </DivIncorrectLogin>
            </DivFormAb>
          </DivAuthBlockWrapForm>
        </DivAuthBlock>
        <DivAuthLogo>
          <img src='assets/logo.png' alt='' />
        </DivAuthLogo>
      </DivWrapAuth>
    )
  }
}

export default LoginForm
