import React, { Component } from 'react'

import { compose }          from 'redux'
import { connect }          from 'react-redux'

import styled               from 'styled-components'


import withResetPassword    from 'app/user/graphQl/userResetPassword'


const DivWrapAuth = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 100vh;
  background: #484848 url(assets/login/big.png) center/cover no-repeat;
`

const DivAuthBlock = styled.div`
  width: 680px;
  margin-top: -10%;
`

const DivAuthBlockWrapTitle = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-between;
  margin: -5px;
  padding: 20px 65px;
`

const DivAuthBlockTitle = styled.div`
  margin: 5px;
  font-size: 28px;
  font-family: HelveticaRegular;
  color: #fff;
`


const DivAuthBlockCaption = styled.div`
  margin: 5px;
`

const DivAuthBlockWrapForm = styled.div`
  margin: -5px;
  background-color: #383838;
`

const DivFormAb = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
  align-items: center;
  padding: 40px 65px;
  
   > div: nth-child(1) {
    display: flex;
    align-items: center;
    width: calc(50% - 12px);
    height: 50px;
    margin: 5px;
  }
  
  > div: nth-child(2) {
    display: flex;
    align-items: center;
    width: calc(50% - 12px);
    height: 50px;
    margin: 5px;
  }
`

const DivInputWrap = styled.div`
  display: flex;
  align-items: center;
  border: 1px solid #9e9e9e;
  width: calc(50% - 12px);
`

const DivIcoPersonForm = styled.div`
  width: 40px;
  height: 100%;
  background: center no-repeat;

  &.person {
    background-image: url(assets/login/ico-auth-user.png);
    background-size: 14px 14px;
  }
`

const Input = styled.input`
  display: block;
  box-sizing: border-box;
  width: 100%;
  padding: 7px 0;
  border: none;
  outline: none;
  font-weight: 300;
  font-size: 18px;
  color: #fff;
  background-color: transparent;
  &:focus { 
    outline:none; 
  }
`

const DivBtnWrap = styled.div`
  border: 1px solid transparent;
`

const ButtonForm = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
  text-align: center;
  font-weight: 300;
  font-family: HelveticaRegular;
  font-size: 18px;
  border: none;
  outline: none;
  color: #fff;
  background-color: #48b9bf;
  cursor: pointer;
    
  &:hover {
    background-color: #59cad0;
  }
  
  &:focus, &:active {
    border: none;
    outline: none;
  }
`

const DivAuthLogo = styled.div`
  position: absolute;
  left: 50%;
  bottom: 5%;
  transform: translateX(-50%);
`

const DivIncorrectLogin = styled.div`
  display: ${ props=>props.error ? 'flex' : 'none' };
  align-items: center;
  height: 30px;
  width: 100%;
  font-family: HelveticaRegular;
  font-size: 18px;
  color: red; 
`
const codeMap = {
  SUCCESS: 0,
  ERROR: 1,
}

class UserFormReset extends Component {
  constructor() {
    super()

    this.state = {
      username: '',
      message: '',
    }
  }

  onChange = ({ target: { value } }) => {
    this.setState({ username: value })
  }

  onClick = () => {
    const { username } = this.state
    const { onReset } = this.props

    this.props.userResetPassword({ seller: 1, username }).then(({ data }) => {
      const { userResetPassword = {} } = data

      const { errors: { code, message } } = userResetPassword

      if (code === codeMap.ERROR) {
        this.setState({ message })
      }

      if (code === codeMap.SUCCESS) {
        const resetSuccessMessage = 'Пароль изменен на первоначальный'

        onReset({ resetSuccessMessage })

        this.setState({ message })
      }
    })
  }

  render() {
    const { username, message } = this.state

    return(
      <DivWrapAuth>
        <DivAuthBlock>
          <DivAuthBlockWrapTitle>
            <DivAuthBlockTitle>
              Сброс пароля
            </DivAuthBlockTitle>
            <DivAuthBlockCaption>
              <img src='assets/img-com.png' alt='' />
            </DivAuthBlockCaption>
          </DivAuthBlockWrapTitle>
          <DivAuthBlockWrapForm>
            <DivFormAb>
              <DivInputWrap>
                <DivIcoPersonForm className='person' />
                <Input
                  type='text'
                  placeholder='Введите логин'
                  autocomplete='off'
                  value={ username }
                  onChange={ this.onChange }
                />
              </DivInputWrap>
              <DivBtnWrap>
                <ButtonForm
                  onClick={ this.onClick }
                >
                  Сбросить пароль
                </ButtonForm>
              </DivBtnWrap>
              <DivIncorrectLogin error={ message.length > 0 }>
                { message }
              </DivIncorrectLogin>
            </DivFormAb>
          </DivAuthBlockWrapForm>
        </DivAuthBlock>
        <DivAuthLogo>
          <img src='assets/logo.png' alt='' />
        </DivAuthLogo>
      </DivWrapAuth>
    )
  }
}

export default compose(
  connect(),
  withResetPassword()
)( UserFormReset)

