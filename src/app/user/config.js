const queryNameMap = {
  USER_LOGIN: 'userLogin',
  USER_LOGOUT: 'userLogout',
  USER_CHANGE_PASSWORD: 'userChangePassword',
  USER_RESET_PASSWORD: 'userResetPassword',
  USER_LOAD_SETTING: 'userLoadSettings'
}

const objectMap = {
  SELLER:   'seller',
  LOGIN: 'login',
  PASSWORD: 'password',
  USER: 'user',
  SESSION: 'session',
}

const key = '946D20C91F154795805CEBDEFE919EF7'

export default { queryNameMap, objectMap, key }
