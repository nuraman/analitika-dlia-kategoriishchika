import { graphql }          from 'react-apollo'


import query                from 'app/user/graphQl/query/userLogout'


const userLogout = () => {
  return graphql(query(), {
    props: ({ mutate }) => ({
      userLogout: (variables) => mutate({ variables })
    })
  })
}

export default userLogout
