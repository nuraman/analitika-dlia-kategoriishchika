import gql                  from 'graphql-tag'


import config               from 'app/user/config'


export default function userLogout() {
  const { queryNameMap } = config

  const query = `mutation ${ queryNameMap.USER_LOGOUT } {
    ${ queryNameMap.USER_LOGOUT }
    {
      errors { code message }
    }
  }`

  return gql`${ query }`
}
