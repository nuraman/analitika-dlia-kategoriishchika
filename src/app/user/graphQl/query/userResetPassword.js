import gql                  from 'graphql-tag'


import config               from 'app/user/config'


export default function userResetPassword() {
  const { queryNameMap, objectMap } = config

  const query = `mutation ${ queryNameMap.USER_RESET_PASSWORD } ($seller: Int, $username: String) {
    ${ queryNameMap.USER_RESET_PASSWORD } (
      ${ objectMap.SELLER }: $seller,
      ${ objectMap.LOGIN }: $username,
    )
    {
      errors { code message }
    }
  }`

  return gql`${ query }`
}
