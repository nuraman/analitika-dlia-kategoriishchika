import gql                  from 'graphql-tag'


import config               from 'app/user/config'


export default function userChangePassword() {
  const { queryNameMap, objectMap } = config

  const query = `mutation ${ queryNameMap.USER_CHANGE_PASSWORD } ($password: String) {
    ${ queryNameMap.USER_CHANGE_PASSWORD } (
      ${ objectMap.PASSWORD }: $password,
    )
    {
      errors { code message }
    }
  }`

  return gql`${ query }`
}


