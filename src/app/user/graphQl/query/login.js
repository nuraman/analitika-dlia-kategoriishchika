import gql                  from 'graphql-tag'


import config               from 'app/user/config'


export default function generateLoginQuery () {
  const { queryNameMap, objectMap } = config
  const query = `mutation ${ queryNameMap.USER_LOGIN } ($seller: Int, $username: String, $password: String) {
    ${ queryNameMap.USER_LOGIN } (
      ${ objectMap.SELLER }: $seller,
      ${ objectMap.LOGIN }: $username,
      ${ objectMap.PASSWORD }: $password
    )
    {
      errors { code message }
      result { changePassword provider user session providerName userName }
    }
  }`

  return gql`${ query }`
}
