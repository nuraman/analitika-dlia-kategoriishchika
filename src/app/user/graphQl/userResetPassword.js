import { graphql }          from 'react-apollo'


import query                from 'app/user/graphQl/query/userResetPassword'


const userResetPassword = () => {
  return graphql( query(), {
    props: ({ mutate }) => ({
      userResetPassword: (variables) =>  mutate({ variables, })
    })
  })
}

export default userResetPassword
