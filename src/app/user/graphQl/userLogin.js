import { graphql }          from 'react-apollo'


import loginQuery           from 'app/user/graphQl/query/login'


const userLogin = () => {
  return graphql(loginQuery(), {
    props: ({ mutate }) => ({
      userLogin: (variables) => mutate({variables})
    })
  })
}

export default userLogin

