import { graphql }          from 'react-apollo'


import query                from 'app/user/graphQl/query/userChangePassword'


const userChangePassword = () => {
  return graphql( query(), {
    props: ({ mutate }) => ({
      userChangePassword: (variables) =>  mutate({ variables, })
    })
  })
}

export default userChangePassword

