import url                  from 'url'


import go                   from 'engine/history/go'

import subConfig            from './config'

export default (location) => go(url.resolve(subConfig.root, location))
