export default {
  Competition: {
    Title: 'Карта конкуренции',
    Url: '/competition'
  },
  Customer: {
    Title: 'Карта клиента',
    Url: '/customer'
  },
  CustomerBahavior: {
    Title: 'Поведение клиента',
    Url: '/customerbehavior'
  },
  CustomerProfile: {
    Title: 'Профиль клиента',
    Url: '/customerprofile'
  },
  Discount: {
    Title: `Оформление акции`,
    Url: `/discount`
  },
  Home: {
    Title: 'Главная',
    Url: '/home'
  },
  UserLogin: {
    Title: 'Логин',
    Url: '/Login'
  },
  UserLogout: {
    Title: 'Выйти',
    Url: '/logout'
  }
}
