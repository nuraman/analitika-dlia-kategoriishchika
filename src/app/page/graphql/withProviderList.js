import { graphql }          from 'react-apollo'


import config               from 'app/page/web/Aside/Provider/config'

import queryGenerate        from 'engine/graphql/generate/create/query'


export default (payload) => {
  const { options } = payload || {}

  const { queryMap, name } = config

  const query = queryGenerate({ queryMap, name })
  return graphql(query, {
    options: (ownProps) => ({ ...options, ...(ownProps.options || {}) }),
    skip: (ownProps = {}) => {
      const { options: {  variables } } = ownProps

      const { goodsCategory } = variables

      if (!goodsCategory) return true

      return false
    },
    props: ({ data }) => {
      const { providerListItems = {}, loading } = data || {}
      let dataNewProvider = {}, orderNewProvider = []

      if (providerListItems && providerListItems.errors && !providerListItems.errors.length) {
          const { errors, result = [] } = providerListItems
          const { code } = errors //{ code, message } = errors

        if (code === 0 && result !== null) {
          dataNewProvider = result.reduce((dataProvider, item, index) => {
            const { id, value, selected } = item

            return { ...dataProvider, [index]: { index, id, value, selected } }
          }, {})

          orderNewProvider = Object.keys(dataNewProvider)

          return  {
            dataNewProvider,
            orderNewProvider,

            loading
          }
        }

        return { errors, userLoadSetting: loading }
      }

      return { userLoadSetting: loading }
    }
  })
}
