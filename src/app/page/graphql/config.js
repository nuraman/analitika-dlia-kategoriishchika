import types                from 'engine/types'


const resultNameMap = {
  DATE_BEGIN:                         'dateBegin',
  DATE_END:                           'dateEnd',
  GOODS_CATEGORY:                     'goodsCategory',
  PERIOD:                             'period',
  //POINT_GROUP:                        'pointGroup',
  POINT_FORMAT:                       'pointFormat',
  //POINT:                              'point',
  TYPE_PARAMETER:                     'typeParameter',

  USER:                                'user',
  PROVIDER:                            'provider',
  PROVIDER_NAME:                       'providerName',
  USERNAME:                            'userName',
}

const providerNameMap = {
  ID:                                  'id',
  VALUE:                               'value',
  SELECTED:                            'selected'
}

const queryNameMap = {
  USER_LOAD_SETTING:                    'userLoadSettings',
}

const queryMap = [
  {
    queryName: queryNameMap.USER_LOAD_SETTING,
    inputList: [],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        }
      ],
      result: [
        {
          name: resultNameMap.GOODS_CATEGORY,
          type: types.Int,
        },
    /*    {
          name: resultNameMap.POINT_GROUP,
          type: types.Int,
        },*/
        {
          name: resultNameMap.POINT_FORMAT,
          type: types.Int,
        },
        /*{
          name: resultNameMap.POINT,
          type: types.String,
        },*/
        {
          name: resultNameMap.PERIOD,
          type: types.Int,
        },
        {
          name: resultNameMap.DATE_BEGIN,
          type: types.String,
        },
        {
          name: resultNameMap.DATE_END,
          type: types.String,
        },
        {
          name: resultNameMap.TYPE_PARAMETER,
          type: types.Int,
        },
        {
          name: resultNameMap.USER,
          type: types.Int,
        },
        {
          name: resultNameMap.PROVIDER,
          type: types.Int,
        },
        {
          name: resultNameMap.PROVIDER_NAME,
          type: types.String,
        },
        {
          name: resultNameMap.USERNAME,
          type: types.String,
        }
      ],
      providerList: [
        {
          name: providerNameMap.ID,
          type: types.Int,
        },
        {
          name: providerNameMap.VALUE,
          type: types.String,
        },
        {
          name: providerNameMap.SELECTED,
          type: types.Boolean,
        }
      ]
    }
  },
]

export default {
  queryMap,
  name: queryNameMap.USER_LOAD_SETTING,
}
