import { graphql }          from 'react-apollo'


import config               from 'app/page/graphql/config'

import queryGenerate        from 'engine/graphql/generate/create/queryProvider'


export default (payload) => {
  const { queryMap, name } = config

  const query = queryGenerate({ queryMap, name })

  return graphql(query, {
    options: {
      fetchPolicy: 'network-only'
    },
    props: ({ data }) => {
      const { userLoadSettings = {}, loading } = data || {}
      let dataProvider = {}, orderProvider = []

      if (userLoadSettings && userLoadSettings.errors && !userLoadSettings.errors.length) {
        const { errors, result, providerList } = userLoadSettings
        const { code } = errors

        if (code === 0 && result !== null) {
          const { goodsCategory, pointFormat, period, dateBegin, dateEnd, typeParameter, user, provider, userName, providerName } =  result

          const userSetting = { goodsCategory, pointFormat, period, dateBegin, dateEnd, typeParameter, }

          const userInfo = { user, provider, userName, providerName }
          if (providerList !== null) {
            dataProvider = providerList.reduce((dataProvider, item, index) => {
              const { id, value, selected } = item

              return { ...dataProvider, [index]: { index, id, value, selected } }
            }, {})

            orderProvider = Object.keys(dataProvider)
          }

          return  {
            dataProvider,
            orderProvider,

            userSetting,
            userInfo,
            userLoadSetting: loading,

            errors
          }
        }

        return { errors, userLoadSetting: loading }
      }

      return { userLoadSetting: loading }
    }
  })
}
