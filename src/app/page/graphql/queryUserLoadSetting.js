import config               from './config'

import queryUserLoadSetting from 'engine/graphql/generate/create/queryProvider'

export default () => {
  const { queryMap, name } = config

  const query = queryUserLoadSetting({ queryMap, name })

  return query
}
