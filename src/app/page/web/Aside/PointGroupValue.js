import React                from 'react'

import styled               from 'styled-components'


import PointGroup           from 'app/menu/web/PointGroupList'


const DivPgaBlockValue = styled.div`
  font-size: 18px;
  color: ${ props=>props.color };
  font-family: HelveticaLight;
`


export default (props) => {
  const { value, openPoint, openPointGroup, onSelect } = props

  const onHandleClick = ({ openPoint, openPointGroup }) => {
    const { onClickPoint } = props

    onClickPoint({ openPoint, openPointGroup })
  }

  //если выбрана группа торговой точки, изменяем цвет
  if (!openPointGroup && openPoint) {
    return (
      <DivPgaBlockValue
        onClick={ () => onHandleClick({ openPoint, openPointGroup }) }
        color='#9e9e9e'
      >
        { value ? value : 'Не выбрано' }
      </DivPgaBlockValue>
    )
  }
  //если выбрана торговая точка, то показываем список
  if (openPointGroup) {
    return <PointGroup onSelect={ onSelect } selectedValue={ value }/>
  }

  return (
    <DivPgaBlockValue
      onClick={ () => onHandleClick({ openPoint, openPointGroup }) }
      color='#b3fffb'
    >
      { value ? value : 'Не выбрано' }
    </DivPgaBlockValue>
  )
}
