import React, { Component } from 'react'

import { connect }          from 'react-redux'
import { compose }          from 'redux'

import styled               from 'styled-components'


import reducer              from 'app/user/reducer'
import reducerMenu          from 'app/menu/reducer'

import withUserLogout       from 'app/user/graphQl/userLogout'

import go                   from 'app/story/go'


const PgaBtnLogout = styled.a`
  display: flex;
  align-items: center;
  margin: 10px;
  padding: 7px 15px;
  text-decoration: none;
  cursor: pointer;
  &:hover {
    opacity: 0.7;
  }
`

const DivIcoLogout = styled.div`
  min-width: 20px;
  width: 20px;
  height: 20px;
  margin: 0 15px;
  fill: #fff;
  
  background-image: url(assets/logout.svg);
  background-size: cover;
`

const SpanLogoutText = styled.span`
  margin-top: 1px;
  font-size: 14px;
  color: #fff;
  font-family: HelveticaRegular;
`

class Logout extends Component {
  onHandleClick = () => {
    const { dispatch, id, session, provider } = this.props

    this.props.userLogout().then(({ data }) => {
      const { userLogout } = data
      const { errors: { code } } = userLogout

      if (code === 0) {
        localStorage.removeItem('sessionID')

        go('/Login')

        dispatch({
          type: reducer.types.USER_REMOVE,
          payload: { userId: id, session, provider }
        })

        dispatch({
          type: reducerMenu.types.RESET_MENU,
          payload: {},
        })
      }
    })
  }

  render() {
    return (
      <PgaBtnLogout
        onClick={ this.onHandleClick }
      >
        <DivIcoLogout />
        <SpanLogoutText>
          Выйти из системы
        </SpanLogoutText>
      </PgaBtnLogout>
    )
  }
}

const mapStateToProps = (state) => {
  const { user } = state

  const { userLogin: { data, order } } = user

  const { id, session, provider, isProvider } = data[order[0]] || {}

  return { id, session, provider, isProvider }
}

export default compose(
  connect(mapStateToProps),
  withUserLogout(),
)(Logout)
