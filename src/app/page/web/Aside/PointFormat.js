import React, { Component } from 'react'

import styled               from 'styled-components'


import PointFormatList      from 'app/menu/web/PointFormatList'


const DivBlock = styled.div`
  display: flex;
  align-items: flex-start;
  margin: 10px 0 18px 0;
`

const DivBlockIco = styled.div`
  min-width: 20px;
  width: 20px;
  height: 17px;
  margin: 2px 15px 0 15px;
  fill: #9e9e9e;
  
  background-image: url(${ props=>props.path });
  background-size: cover;
`

const DivBlockContent = styled.div`
  width: 100%;
  cursor: pointer;
`

const DivBlockName = styled.div`
  margin: 3px 0;
  font-size: 14px;
  color: #fff;
  font-family: HelveticaLight;
`

const DivBlockValue = styled.div`
  font-size: 18px;
  color: #b3fffb;
  font-family: HelveticaLight;
`


class BlockPointFormat extends Component {
  constructor() {
    super()

    this.state = {
      pointSelectedList: [],
      value: ``
    }
  }

  onHandleClick = () => {
    const { onChangeState, pointType } = this.props

    onChangeState({ pointType })
  }

  onSelect = ({ id, value }) => {
    const { onSelect, pointType } = this.props

    onSelect({ id, value, pointType })
  }

  componentWillReceiveProps(nextProps) {
    const { pointSelectedList = [] } = nextProps
    let newValue = ``

    if (pointSelectedList.length > 0) {
      pointSelectedList.forEach((item, index) => {
        const { value } = item

        index === pointSelectedList.length - 1 ? newValue += value : newValue += value + `, `
      })
    }

    this.setState({ pointSelectedList, value: newValue })
  }

  render() {
    const { pointSelectedList, value } = this.state
    const { open } = this.props

    return (
      <DivBlock>
        <DivBlockIco path='assets/pointFormat.svg' />
        <DivBlockContent>
          <DivBlockName onClick={ this.onHandleClick }>
            Формат торговой точки
          </DivBlockName>
          { open ? <PointFormatList onSelect={ this.onSelect } pointSelectedList={ pointSelectedList } /> : <DivBlockValue>{ value }</DivBlockValue> }
        </DivBlockContent>
      </DivBlock>
    )
  }
}

export default BlockPointFormat
