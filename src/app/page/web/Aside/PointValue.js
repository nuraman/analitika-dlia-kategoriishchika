import React                from 'react'

import styled               from 'styled-components'


import PointList            from 'app/menu/web/PointList'


const DivPgaBlockValue = styled.div`
  font-size: 18px;
  color: ${ props=>props.color };
  font-family: HelveticaLight;
`


export default (props) => {
  const { pointList, openPoint, openPointGroup, onSelect } = props

  const onHandleClick = ({ openPoint, openPointGroup }) => {
    const { onClick } = props

    onClick({ openPoint, openPointGroup })
  }

  let pointListInStr = pointList.reduce((pointListInStr, item) => {
    let { value } = item

    pointListInStr += value

    return pointListInStr
  }, ``)
  //если выбран группа торговой точки, изменяем цвет
  if (!openPoint && openPointGroup) {
    return (
      <DivPgaBlockValue
        color='#9e9e9e'

        onClick={ () => onHandleClick({ openPoint, openPointGroup }) }
      >
        { pointListInStr ? pointListInStr : 'Не выбрано' }
      </DivPgaBlockValue>
    )
  }
  //если выбран торговые точки, то показываем список магазинов
  if (openPoint) {
    return <PointList onSelect={ onSelect } pointList={ pointList } />
  }

  return (
    <DivPgaBlockValue
      color='#b3fffb'

      onClick={ () => onHandleClick({ openPoint, openPointGroup }) }
    >
      { pointListInStr ? pointListInStr : 'Не выбрано' }
    </DivPgaBlockValue>
  )
}

