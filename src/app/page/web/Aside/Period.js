import React                from 'react'

import styled               from 'styled-components'


import PeriodList           from 'app/menu/web/PeriodList'


const Container = styled.div`
  display: flex;
  align-items: flex-start;
  margin: 10px 0 18px 0;
`

const Icon = styled.div`
  min-width: 20px;
  width: 20px;
  height: 17px;
  margin: 2px 15px 0 15px;
  fill: #9e9e9e;
  
  background-image: url(${ props=>props.path });
  background-size: cover;
`

const Content = styled.div`
  width: 100%;
  cursor: pointer;
`

const Title = styled.div`
  margin: 3px 0;
  font-size: 14px;
  color: #fff;
  font-family: HelveticaLight;
`

const PeriodName = styled.div`
  font-size: 18px;
  color: #b3fffb;
  font-family: HelveticaLight;
`


export default function Period(props){
  const { value, open, onChangeState, onSelect } = props

  const onHandleClick = () => onChangeState()

  const onSelectPeriod = ({ id, value }) => onSelect({ id, value })

  return (
    <Container>
      <Icon path='assets/calendar.svg'/>
      <Content
        onClick={ onHandleClick }
      >
        <Title>
          Период анализа
        </Title>
        {open ? <PeriodList onSelect={ onSelectPeriod } /> : <PeriodName>{ value }</PeriodName>}
      </Content>
    </Container>
  )
}
