import types                from 'engine/types'


const resultNameMap = {
  ID:                                  'id',
  VALUE:                               'value',
  SELECTED:                            'selected'
}

const inputNameMap = {
  GOODS_CATEGORY:                      'goodsCategory'
}

const queryNameMap = {
  PROVIDER_LIST_ITEMS:                 'providerListItems',
}

const queryMap = [
  {
    queryName: queryNameMap.PROVIDER_LIST_ITEMS,
    inputList: [
      {
        name: inputNameMap.GOODS_CATEGORY,
        type: types.String,
      }
    ],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        }
      ],
      result: [
        {
          name: resultNameMap.ID,
          type: types.Int,
        },
        {
          name: resultNameMap.VALUE,
          type: types.String,
        },
        {
          name: resultNameMap.SELECTED,
          type: types.Boolean,
        }
      ],
    }
  },
]

export default {
  queryMap,
  name: queryNameMap.PROVIDER_LIST_ITEMS,
}
