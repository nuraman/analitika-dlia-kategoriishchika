import _                    from 'lodash'

import React, { Component } from 'react'

import { compose }          from 'react-apollo'

import styled               from 'styled-components'


import EntitySelect         from '../EntitySelect'

import withData             from 'app/page/web/Aside/Provider/hoc/withData'


const DivBlock = styled.div`
  display: flex;
  align-items: flex-start;
  margin: 10px 0 18px 0;
`

const DivBlockIco = styled.div`
  min-width: 20px;
  width: 20px;
  height: 17px;
  margin: 1px 15px 0 15px;
  fill: #9e9e9e;
  
  background-image: url(${ props=>props.path });
  background-size: cover;
`

const DivBlockContent = styled.div`
  width: 100%;
  cursor: pointer;
`

const DivBlockName = styled.div`
  margin: 3px 0;
  font-size: 14px;
  color: #fff;
  font-family: HelveticaLight;
`

const DivBlockValue = styled.div`
  font-size: 18px;
  color: #b3fffb;
  font-family: HelveticaLight;
`


class BlockProvider extends Component {
  constructor() {
    super()

    this.state = {
      open: false,
      checkedProviderId: '',
      value: '',
      isProvider: false,
      dataProvider: {},
      orderProvider: []
    }
  }

  onHandleClick = () => {
    const { onChangeState } = this.props

    onChangeState()
  }

  onChange = ({ id, value }) => {
    const { onSelect } = this.props

    this.setState({ checkedProviderId: id, value })

    onSelect({ id })
  }

  onChangeProvider = ({ dataNewProvider, orderNewProvider, open }) => {
    let newCheckedProviderId = ''
    let providerValue = ''
    //проверяем выбран ли постащик уже
    orderNewProvider.forEach((item, index) => {
      const { id, selected, value } = dataNewProvider[item]

      if (selected) {
        newCheckedProviderId = id
        providerValue = value
      }
    })
    //если не выбран, то делаем первого поставщика по умолчанию
    if (newCheckedProviderId === '') {
      const item = Object.keys(orderNewProvider)[0]

      const { id, value } = dataNewProvider[item]

      newCheckedProviderId = id
      providerValue = value
    }

    this.setState({
      dataProvider: dataNewProvider,
      orderProvider: orderNewProvider,
      checkedProviderId: newCheckedProviderId,
      value: providerValue,
      open
    })
  }

  componentWillReceiveProps(nextProps) {
    const {
      open,
      dataNewProvider = {},
      loading,
      orderNewProvider = [],

      onSelect
    } = nextProps
    //если изменились данные о поставщиках
    if (orderNewProvider.length > 0 && !_.isEqual(dataNewProvider, this.props.dataNewProvider))
      return this.onChangeProvider({ dataNewProvider, orderNewProvider, open })
    //если данные о поставщиках те же
    if (orderNewProvider.length > 0 && _.isEqual(dataNewProvider, this.props.dataNewProvider)) {
      const { checkedProviderId } = this.state

      onSelect({ id: checkedProviderId })

      return this.setState({
        dataProvider: dataNewProvider,
        orderProvider: orderNewProvider,
        checkedProviderId,
        open
      })
    }
  }

  renderEntitySelect = ({ dataProvider, orderProvider, checkedProviderId }) => {
    return (
      <EntitySelect
        data={ dataProvider }
        order={ orderProvider }
        checkedId={ checkedProviderId }

        onSelect={ this.onChange }
      />
    )
  }

  renderValue = ({ value }) => {
    return (
      <DivBlockValue
        onClick={ this.onHandleClick }
      >
        { value }
      </DivBlockValue>
    )
  }

  render() {
    const { dataProvider, orderProvider, open, checkedProviderId, value } = this.state

    const { isProvider } = this.props

    if (isProvider) return null

    return (
      <DivBlock>
        <DivBlockIco path='assets/setting.svg' />
        <DivBlockContent>
          <DivBlockName
            onClick={ this.onHandleClick }
          >
            Поставщики
          </DivBlockName>
          {
            open ? this.renderEntitySelect({ dataProvider, orderProvider, checkedProviderId }) : this.renderValue({ value })
          }
        </DivBlockContent>
      </DivBlock>
    )
  }
}

export default compose(
 withData,
)(BlockProvider)
