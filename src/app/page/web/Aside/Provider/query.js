import config               from 'app/page/web/Aside/Provider/config'

import queryGenerate        from 'engine/graphql/generate/create/query'


export default () => {
  const { queryMap, name } = config

  const query = queryGenerate({ queryMap, name })

  return query;
}
