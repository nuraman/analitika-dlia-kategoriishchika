const name = `provider`

const types = {
  SET_CATEGORY: `${ name }/SET_CATEGORY `
}

const STATE = {
  goodCategory: null,
}

const reducer = (state=STATE, action) => {
  const { type, payload } = action

  switch (type) {
    case types.SET_CATEGORY: {
      const { id } = payload

      return {
        ...state,
        goodCategory: id,
      }
    }
    default:
      return state
  }
}

export default {
  reducer,
  types
}
