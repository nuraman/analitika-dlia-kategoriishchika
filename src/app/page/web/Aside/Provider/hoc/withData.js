import React, { Component } from 'react'

import { connect }          from 'react-redux'
import { compose }          from 'redux'


import withProviderList     from 'app/page/graphql/withProviderList'


export default (WrappedComponent) => {
  class Provider extends Component {
    render() {
      return (<WrappedComponent { ...this.props } />)
    }
  }

  function mapStateToProps(state) {
    const { menu: { category } } = state
    const { provider: { goodCategory } } = state

    return {
      goodsCategoryId: goodCategory ? goodCategory : category.id,

      options: {
        variables: {
          goodsCategory: goodCategory ? goodCategory : category.id,
        }
      }
    }
  }

  return compose(
    connect(mapStateToProps),
    withProviderList(),
  )(Provider)
}
