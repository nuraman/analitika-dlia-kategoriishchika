import React, { Component } from 'react'

import styled               from 'styled-components'


import ParameterList        from 'app/menu/web/ParameterList'


const Container = styled.div`
  display: flex;
  align-items: flex-start;
  margin: 10px 0 18px 0;
`

const Icon = styled.div`
  min-width: 20px;
  width: 20px;
  height: 17px;
  margin: 2px 15px 0 15px;
  fill: #9e9e9e;
  
  background-image: url(${ props=>props.path });
  background-size: cover;
`

const Content = styled.div`
  width: 100%;
  cursor: pointer;
`

const Name = styled.div`
  margin: 3px 0;
  font-size: 14px;
  color: #fff;
  font-family: HelveticaLight;
`

const Value = styled.div`
  font-size: 18px;
  color: #b3fffb;
  font-family: HelveticaLight;
`


class Parameter extends Component {

  onHandleClick = () => {
    const { onChangeState } = this.props

    onChangeState()
  }

  onSelect = ({ id, value }) => {
    const { onSelect } = this.props

    onSelect({ id, value })
  }

  render() {

    const { open, value } = this.props

    return (
      <Container>
        <Icon path='assets/setting.svg' />
        <Content
          onClick={ this.onHandleClick }
        >
          <Name> Параметр агрегации  </Name>
          { open ? <ParameterList onSelect={ this.onSelect } /> : <Value>{ value }</Value> }
        </Content>
      </Container>
    )
  }
}

export default Parameter
