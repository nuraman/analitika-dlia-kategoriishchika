import React, { Component } from 'react'

import styled, { css }      from 'styled-components'


import reducer              from 'app/menu/reducer'

import Logout               from './Logout'

import withData             from 'app/page/web/Aside/hoc/withData'

import BlockGoodsCategory   from './GoodsCategory'
import BlockPeriod          from './Period'
import BlockParameter       from './Parameter'
import BlockPointEntity     from './PointEntity'
import BlockProvider        from './Provider/Container'
import BlockFeedback        from './Feedback'


const Aside = styled.aside`
  width: 0px;
  height: 100%;
  overflow-x: hidden;
  overflow-y: auto;
  background-color: #484848;
  transition: width 0.4s;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  
   &::-webkit-scrollbar
  {
    width: 9px;
  }

  &::-webkit-scrollbar-track
  {
    background: #73c7c5;
    background-clip: content-box;
  }

  &::-webkit-scrollbar-thumb
  {
    background: #484848;
    height: 35px;
    border-radius: 5px solid #484848;
    border: 1px solid #484848;
  }
`

const Container = Aside.extend`
  ${ props => props.active && css`
     width: 320px;
     transition: width 0.4s;
  `}
`

const Content = styled.div`
  box-sizing: border-box;
  width: 320px;
  padding: 20px;
  display: table;
`

const Footer = styled.div`
  box-sizing: border-box;
  width: 320px;
  padding: 20px;
  display: table;
`

const DivBlock = styled.div`
  display: block;
`


const Link = styled.a`
  display: flex;
  align-items: center;
  justify-content: center;
  box-sizing: border-box;
  width: 100%;
  height: 50px;
  text-align: center;
  font-weight: 300;
  font-size: 18px;
  text-decoration: none;
  border: none;
  outline: none;
  color: #fff;
  font-family: HelveticaRegular;
  background-color: #48b9bf;
  cursor: pointer;
  
  &:hover {
    background-color: #59cad0;
  }
  &:focus, &:active {
    border: none;
    outline: none;
  }
`

const AsideLogo = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;
  border-top: 1px solid #5c5c5c;
  padding-top: 20px;
  padding-bottom: 20px;
`

const Logo = styled.div`
  min-width: 169px;
  width: 169px;
  height: 41px;
`

const POINT_FORMAT = 'Формат магазина'

class OwnAside extends Component {
  constructor() {
    super()

    this.state = {
      dataPointFormat: {},
      goodsCategory:  { id: '', value: '' },
      isProvider: false,
      open: false,
      orderPointFormat: [],
      openGoodsCategory: false,
      openPeriod: false,
      openParameter: false,
      openPointFormat: false,
      openProvider: false,
      period:         { id: '', value: '' },
      parameter:      { id: '', value: '' },
      pointFormatList:    [],
      providerSelected: ''
    }
  }

  componentWillMount() {
    const {
      category,
      dataPointFormat,
      parameter,
      period,
      pointFormatList,
      isProvider,
      orderPointFormat,
      open
    } = this.props

    this.setState({
      dataPointFormat,
      goodsCategory: { id: category.id, value: category.value },
      isProvider,
      period: { id: period.id, value: period.value },
      parameter: { id: parameter.id, value: parameter.value },
      pointFormatList,
      orderPointFormat,
      open
    })
  }

  onSelectGoodsCategory = ({ id, value }) => this.setState({ goodsCategory: { id, value } })
  onSelectParameter     = ({ id, value }) => this.setState({ parameter: { id, value } })
  onSelectPeriod        = ({ id, value }) => this.setState({ period: { id, value } })
  onSelectPoint         = ({ pointCheckedList, pointType }) => this.setState({ pointFormatList: pointCheckedList })
  onSelectProvider = ({ id }) => this.setState({ providerSelected: id })

  onChangeStateGoodsCategory  = () => this.setState({
    openGoodsCategory: !this.state.openGoodsCategory,
    openParameter: false,
    openPeriod: false,
    openPointFormat: false,
    openProvider: false,
  })
  onChangeStateParameter      = () => this.setState({
    openGoodsCategory: false,
    openParameter: !this.state.openParameter,
    openPeriod: false,
    openPointFormat: false,
    openProvider: false,
  })
  onChangeStatePeriod         = () => this.setState({
    openGoodsCategory: false,
    openParameter: false,
    openPeriod: !this.state.openPeriod,
    openPointFormat: false,
    openProvider: false,
  })
  onChangeStateProvider       = () => this.setState({
    openGoodsCategory: false,
    openParameter: false,
    openPeriod: false,
    openPointFormat: false,
    openProvider: !this.state.openProvider,
  })
  onChangeStatePoint = ({ pointType }) => {
    this.setState({
      openGoodsCategory: false,
      openPeriod: false,
      openParameter: false,
      openPointFormat: !this.state.openPointFormat,
      openProvider: false,
    })
  }

  onHandleClick = () => {
    const { open, goodsCategory, parameter, period, pointFormatList, providerSelected } = this.state
    const { onClose, dispatch } = this.props
    const pointFormatListInString = this.getPointListInString({ pointList: pointFormatList })

    this.setState({
      open: !open,
      openPointFormat: false,
      openProvider: false,
    })

    dispatch({
      type: reducer.types.SET_SETTING,
      payload: {
        newCategory:      goodsCategory,
        newParameter:     parameter,
        newPeriod:        period,
        newPointFormat:   { id: pointFormatListInString, value: '' },
        newProviderList:  providerSelected,
        selected:         true,
      }
    })

    this.props.userSaveSetting({
      dateBegin: null,
      dateEnd: null,
      goodsCategory: goodsCategory.id,
      period: period.id,
      pointFormat: pointFormatListInString ? pointFormatListInString : null,
      providerList: providerSelected ? providerSelected : null,
      typeParameter: parameter.id
    })

    onClose({ shift: !open })
  }

  getPointListInString = ({ pointList }) => {
    if (pointList.length > 0) {
      const pointListId = pointList.reduce((pointListId, item, index) => {
        const { id } = item

        if (id) {
          if (index === pointList.length - 1) {
            return pointListId += id
          }

          pointListId += id + ','
        }
        return pointListId
      }, ``)

      return pointListId
    }

    return ''
  }

  onClickFeedback = () => {
    const { onClickFeedback } = this.props

    onClickFeedback()
  }

  render() {
    const {
      dataPointFormat,

      goodsCategory,

      isProvider,

      orderPointFormat,
      open,
      openGoodsCategory,
      openPeriod,
      openParameter,
      openPointFormat,
      openProvider,

      period,
      parameter,
      pointFormatList,
    } = this.state

    if (!period.id) return null

    return (
      <Container
        active={ open }
      >
        <Content>
          <BlockGoodsCategory
            open={ openGoodsCategory }
            value={ goodsCategory.value }

            onChangeState={ this.onChangeStateGoodsCategory }
            onSelect={ this.onSelectGoodsCategory }
          />
          <BlockPeriod
            open={ openPeriod }
            value={ period.value }

            onChangeState={ this.onChangeStatePeriod }
            onSelect={ this.onSelectPeriod }
          />
          <BlockParameter
            open={ openParameter }
            value={ parameter.value }

            onChangeState={ this.onChangeStateParameter }
            onSelect={ this.onSelectParameter }
          />
          <BlockPointEntity
            data={ dataPointFormat }
            order={ orderPointFormat }
            open={ openPointFormat }
            pointList={ pointFormatList }
            pointType={ POINT_FORMAT }

            onChangeState={ this.onChangeStatePoint }
            onSelect={ this.onSelectPoint }
          />
          <BlockProvider
            goodsCategoryId={ goodsCategory.id }
            open={ openProvider }
            isProvider={ isProvider }

            onChangeState={ this.onChangeStateProvider }
            onSelect={ this.onSelectProvider }
          />
          <Link
            onClick={ this.onHandleClick }
          >
            Сформировать
          </Link>
        </Content>
        <Footer>
          <DivBlock>
            <BlockFeedback
              onClickFeedback={ this.onClickFeedback }
            />
            <Logout />
            <AsideLogo>
              <Logo>
                <img src='assets/logo.png' alt='' />
              </Logo>
            </AsideLogo>
          </DivBlock>
        </Footer>
      </Container>
    )
  }
}

export default withData(OwnAside)
