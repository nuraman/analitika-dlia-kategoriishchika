import React, { Component } from 'react'

import styled               from 'styled-components'


import PointList            from 'app/menu/web/PointList'


const DivBlock = styled.div`
  display: flex;
  align-items: flex-start;
  margin: 10px 0 18px 0;
`

const DivBlockIcoPoint = styled.div`
  min-width: 20px;
  width: 20px;
  height: 17px;
  margin: 5px 15px 0 15px;
  fill: #9e9e9e;
  
  background-image: url(assets/point.svg);
  background-size: cover;
`

const DivBlockContent = styled.div`
  width: 100%;
  cursor: pointer;
`

const DivBlockNameColor = styled.div`
  margin: 3px 0;
  font-size: 14px;
  color: ${ props=>props.color };
  font-family: HelveticaRegular;
`

const DivPgaBlockValue = styled.div`
  font-size: 18px;
  color: ${ props=>props.color };
  font-family: HelveticaLight;
`

const DivValueOther = styled.div`
  display: inline-block;
  color: #48b9bf;
`

const POINT_TYPE = {
  POINT: 'Адрес магазина',
  POINT_GROUP: 'Группа магазинов',
  POINT_FORMAT: 'Формат магазина'
}


const MAX_POINT_VALUE = 2

class BlockPoint extends Component {
  constructor() {
    super()

    this.state = {
      data: {},
      pointType: '',
      order: [],
      pointList: [],
      pointListValue: '',

      showListValueOther: false,
      pointListValueOther: ''
    }
  }

  onHandleClick = () => {
    const { onChangeState } = this.props

    const { pointType } = this.state

    onChangeState({ pointType })
  }

  onHandleClickAllPointListValue = (event) => {
    event.stopPropagation()

    const { pointList, showListValueOther } = this.state

    let newPointListValueOther = ``

    if (!showListValueOther) {
      newPointListValueOther = pointList.reduce((newPointListValueOther, item, index) => {
        const { value } = item

        if (index > MAX_POINT_VALUE) {
          index === pointList.length - 1 ? newPointListValueOther += `${ value }` : newPointListValueOther += `${ value }, `
        }

        return newPointListValueOther
      }, ``)
    } else {
      const count = pointList.length - 1 - MAX_POINT_VALUE
      if (count > 0) newPointListValueOther = `, ещё ${ count }`
    }

    this.setState({ pointListValueOther: newPointListValueOther, showListValueOther: !showListValueOther })
  }

  onSelect = (pointCheckedList) => {
    const { onSelect } = this.props

    const { pointType } = this.state

    onSelect({ pointCheckedList, pointType })
  }

  getPointListValue = ({ data, order, pointList, pointType }) => {
    let pointListValue = ``
    let pointListValueOther = ``

    if (pointList.length > 0) {
      if (order.length === pointList.length) {
        if (pointType === POINT_TYPE.POINT) pointListValue = 'Все магазины'
        if (pointType === POINT_TYPE.POINT_FORMAT) pointListValue = 'Все форматы'
        if (pointType === POINT_TYPE.POINT_GROUP) pointListValue = 'Все группы магазинов'
      } else {
        if (pointList.length <= MAX_POINT_VALUE) {
          pointListValue = pointList.reduce((pointListValue, item, index) => {
            const { value } = item

            index === pointList.length - 1 ? pointListValue += value : pointListValue += value + `, `

            return pointListValue
          }, ``)
        } else {
          const count = pointList.length - 1 - MAX_POINT_VALUE
          let i = 0
          while (i <= MAX_POINT_VALUE) {
            const { value } = pointList[i]

            i === MAX_POINT_VALUE ? pointListValue += value : pointListValue += value + `, `

            i++;
          }

          if (count > 0) pointListValueOther = `, ещё ${ count }`
        }
      }
    }

    return {
      pointListValue,
      pointListValueOther,
    }
  }

  componentWillReceiveProps(nextProps) {
    const { data, order = [], pointList = [], pointType } = nextProps

    let { pointListValue, pointListValueOther } = this.getPointListValue({ data, order, pointList, pointType })

    this.setState({ data, order, pointType, pointList, pointListValue, pointListValueOther })
  }

  render() {
    const { open, openPointGroup } = this.props
    const { data, order, pointList, pointListValue, pointType, pointListValueOther } = this.state

    return (
      <DivBlock>
        <DivBlockIcoPoint />
        <DivBlockContent>
          <DivBlockNameColor
            onClick={ this.onHandleClick }
            color={ openPointGroup && !open ? '#9e9e9e' : '#fff' }
          >
            { pointType }
          </DivBlockNameColor>
          {
            open ?  <PointList onSelect={ this.onSelect } pointList={ pointList } data={ data } order={ order } /> :
              <DivPgaBlockValue
                color='#b3fffb'

                onClick={ this.onHandleClick }
              >
                { pointListValue ?
                  <div>
                    { pointListValue }
                    <DivValueOther
                      onClick={ this.onHandleClickAllPointListValue }
                    >
                      { pointListValueOther }
                    </DivValueOther>
                  </div>
                  :
                  'Не выбрано'
                }
              </DivPgaBlockValue>
          }
        </DivBlockContent>
      </DivBlock>
    )
  }
}

export default BlockPoint
