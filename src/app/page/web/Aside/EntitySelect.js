import React, { Component } from 'react'

import styled               from 'styled-components'


const DivContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  border: 1px solid rgb(158, 158, 158);
`

const DivContent = styled.div`
  overflow-x: hidden;
  height: 160px;
  width: 100%;
  &::-webkit-scrollbar
  {
    width: 9px;
  }

  &::-webkit-scrollbar-track
  {
    background: #73c7c5;
    background-clip: content-box;
  }

  &::-webkit-scrollbar-thumb
  {
    background: #484848;
    height: 35px;
    border-radius: 5px solid #484848;
    border: 1px solid #484848;
  }
`

const DivSelectOption = styled.div`
  display: flex;
  align-items: center;
  padding: 5px;
`

const Input = styled.input`
  display: none;
  
  &:checked {
    display: block; 
  }
`

const LabelNewCheck = styled.label`
  position: relative;
  width: 18px;
  min-width: 18px;
  height: 18px;
  margin-right: 10px;
  border: 1px solid #48b9bf;
  cursor: pointer;
  
  &:before {
    display: ${ props=>props.checked ? 'block' : 'none' };
    content: "";
    position: absolute;
    width: 12px;
    height: 12px;
    top: 3px;
    left: 3px;
    background-color: #48b9bf;
  }
`

const Label = styled.label`
  margin-bottom: -2px;
  font-weight: 300;
  font-size: 16px;
  font-family: HelveticaLight;
  color: #b3fffb;
  cursor: pointer;
`

class EntitySelectCheckBox extends Component {
  constructor(props) {
    super(props)

    this.state = {
      checkedId: '',
      data: {},
      order: []
    }
  }

  onChange = ({ id, value }) => {
    this.setState({ checkedId: id })

    const { onSelect } = this.props

    onSelect && onSelect({ id, value })
  }

  componentWillMount() {
    const { checkedId, data, order } = this.props

    this.setState({
      checkedId,
      data,
      order
    })
  }

  render() {
    const { checkedId, data, order } = this.state

    return(
      <DivContainer>
        <DivContent>
        {
          order.map((index) => {
            const { value = 0, id } = data[index]

            return (
              <DivSelectOption key={ index }>
                <Input type='checkbox' id={ `item${ index }` } name='' value='' />
                <LabelNewCheck
                  checked={ id === checkedId  }
                  onClick = { () => this.onChange({ id, value }) }
                />
                <Label
                  onClick = { () => this.onChange({ id, value }) }
                >
                  { value }
                </Label>
              </DivSelectOption>
            )
          })
        }
        </DivContent>
      </DivContainer>
    )
  }
}

export default EntitySelectCheckBox
