import React, { Component } from 'react'

import styled               from 'styled-components'


import GoodsCategoryList    from 'app/menu/web/GoodsCategoryList'


const DivBlock = styled.div`
  display: flex;
  align-items: flex-start;
  margin: 10px 0 18px 0;
`

const DivBlockContent = styled.div`
  width: 100%;
  cursor: pointer;
`

const DivBlockName = styled.div`
  margin: 3px 0;
  font-size: 14px;
  color: #fff;
  font-family: HelveticaLight;
`

const DivBlockValue = styled.div`
  font-size: 18px;
  color: #b3fffb;
  font-family: HelveticaLight;
`

const DivBlockIco = styled.div`
  min-width: 20px;
  width: 20px;
  height: 17px;
  margin: 2px 15px 0 15px;
  fill: #9e9e9e;
  
  background-image: url(${ props=>props.path });
  background-size: cover;
`


class BlockGoodsCategory extends Component {
  onHandleClick = () => {
    const { onChangeState } = this.props

    onChangeState()
  }

  onSelect = ({ id, value }) => {
    const { onSelect } = this.props

    onSelect({ id, value })
  }

  render() {
    const { open, value } = this.props

    return (
      <DivBlock>
        <DivBlockIco path='assets/product.svg' />
        <DivBlockContent
          onClick={ this.onHandleClick }
        >
          <DivBlockName> Категория товаров </DivBlockName>
          { open ? <GoodsCategoryList onSelect={ this.onSelect } /> : <DivBlockValue>{ value }</DivBlockValue> }
        </DivBlockContent>
      </DivBlock>
    )
  }
}

export default BlockGoodsCategory
