import React, { Component } from 'react'

import { connect }          from 'react-redux'
import { compose }          from 'redux'

import styled               from 'styled-components'


const DivFeedback = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-left: 10px;
  padding-left: 15px;
  cursor: pointer;
  &:hover {
    opacity: 0.7;
  }
`

const DivFeedbackIco = styled.div`
  min-width: 20px;
  width: 20px;
  height: 20px;
  margin: 0 15px;
  fill: #fff;
  
  background-image: url(assets/mail.svg);
  background-size: cover;
`

const DivFeedbackText = styled.div`
  font-family: HelveticaLight;
  font-size: 14px;
  color: #fff;
  height: 20px;
`


class BlockFeedback extends Component {

  onClick = () => {
    const { onClickFeedback } = this.props

    onClickFeedback()
  }

  render() {
    return (
      <DivFeedback
        onClick={ this.onClick }
      >
        <DivFeedbackIco />
        <DivFeedbackText>
          Обратная связь
        </DivFeedbackText>
      </DivFeedback>
    )
  }
}

export default compose(
  connect()
)(BlockFeedback)
