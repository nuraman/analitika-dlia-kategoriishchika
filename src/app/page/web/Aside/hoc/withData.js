import _                    from 'lodash'

import React, { Component } from 'react'

import { connect }          from 'react-redux'

import { compose }          from 'redux'


import withUserSaveSetting  from 'app/menu/graphql/withUserSaveSetting'
import withPeriodList       from 'app/menu/graphql/withMenuList'
import withParameterList    from 'app/menu/graphql/withMenuList'
import
  withGoodsCategoryList     from 'app/menu/graphql/withMenuList'
import withPointFormatList  from 'app/menu/graphql/withMenuList'

import config               from 'app/menu/config'


function withData(ComposedComponent) {
  class Aside extends Component {
    constructor() {
      super()

      this.state = {
        category: {},
        parameter: {},
        period: {},
        pointFormatList: {},
      }
    }

    componentWillReceiveProps(nextProps) {
      const {
        category,

        dataPeriod          = {},
        dataGoodsCategory   = {},
        dataParameter       = {},
        dataPointFormat     = {},

        goodCategory,

        parameter,
        period,
        pointFormat,
      } = nextProps


      const categoryValue = this.onGetValue({ item: goodCategory ? { id: goodCategory, value: '' } : category, data: dataGoodsCategory })
      const parameterValue = this.onGetValue({ item: parameter, data: dataParameter })
      const periodValue = this.onGetValue({ item: period, data: dataPeriod })

      this.setState({
        category: { id: goodCategory ? goodCategory : category.id, value: categoryValue },
        period: { id: period.id, value: periodValue },
        parameter: { id: parameter.id, value: parameterValue },
        pointFormatList: this.getPointList(pointFormat, dataPointFormat),
      })
    }

    onGetValue = ({ item, data }) => {
      const { id, value } = item

      if (id && value) return value

      if (id === null) return null

      if (id && !value) {
        const match = _.find(data, { 'id': +id })

        return match ? match.value : ''
      }
    }

    getPointList = (point, dataPoint) => {
      const { id } = point
        if (id && Object.keys(dataPoint).length > 0) {
          const pointIdList = id.toString().split(',')

          const pointList = pointIdList.reduce((pointList, item) => {
            let value = this.onGetValue({ item: { id: item, value: '' }, data: dataPoint })

            pointList.push({id: item, value})

            return pointList
          }, [])

          return pointList
        }

        return []
    }

    render() {
      const { open } = this.props

      if (!open) return null

      return (
        <ComposedComponent { ...this.props } { ...this.state } />
      )
    }
  }

  function mapSateToProps(state) {
    const { menu, user, provider } = state

    const { category, parameter, period, pointFormat, providerList } = menu

    const { userLogin: { data, order } } = user

    const { goodCategory } = provider

    const { isProvider } = data[order[0]] || {}

    return {
      category,
      goodCategory,

      isProvider,

      parameter,
      period,
      pointFormat,
      providerList,

      options: {
        isAuth: order.length !== 0
      }
    }
  }

  return compose(
    connect(mapSateToProps),
    withPeriodList({ queryName: config.queryNameMap.PERIOD_LIST }),
    withParameterList({ queryName: config.queryNameMap.PARAMETER_LIST }),
    withGoodsCategoryList({ queryName: config.queryNameMap.GOODS_CATEGORY_LIST }),
    withPointFormatList({ queryName: config.queryNameMap.POINT_FORMAT_LIST }),

    withUserSaveSetting(),
  )(Aside)
}

export default withData
