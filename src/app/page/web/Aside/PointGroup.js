import React, { Component } from 'react'

import styled               from 'styled-components'


import GroupValue           from './PointGroupValue'


const DivBlock = styled.div`
  display: flex;
  align-items: flex-start;
  margin: 10px 0 18px 0;
`

const DivBlockIco = styled.div`
  min-width: 20px;
  width: 20px;
  height: 17px;
  margin: 2px 15px 0 15px;
  fill: #9e9e9e;
  
  background-image: url(${ props=>props.path });
  background-size: cover;
`

const DivBlockContent = styled.div`
  width: 100%;
  cursor: pointer;
`

const DivBlockNameColor = styled.div`
  margin: 3px 0;
  font-size: 14px;
  color: ${ props=>props.color };
  font-family: HelveticaLight;
`

class BlockPointGroup extends Component {
  constructor() {
    super()

    this.state = {
      pointList: [],
      pointListStr: ''
    }
  }

  componentWillReceiveProps(nextProps) {
    const { pointList } = nextProps

    const pointListStr = pointList.reduce((pointListStr, item, index) => {
      const { value } = item

      index === pointList.length - 1 ? pointListStr += value : pointListStr += value + `, `

      return pointListStr
    }, ``)

    this.setState({ pointList, pointListStr })
  }

  onHandleClick = () => {
    const { onChangeState } = this.props

    const { id, value } = this.state

    onChangeState({ id, value })
  }

  onSelect = ({ id, value }) => {
    const { onSelect } = this.props

    this.setState({ id, value })

    if (id) onSelect({ id, value })
  }

  render() {

    const { open, openPoint } = this.props
    const { value } = this.state

    return (
      <DivBlock>
        <DivBlockIco path='assets/pointGroup.svg' />
        <DivBlockContent>
          <DivBlockNameColor
            color={ openPoint && !open ? '#9e9e9e' : '#fff' }

            onClick={ this.onHandleClick }
          >
            Группа магазинов
          </DivBlockNameColor>
          <GroupValue
            openPoint={ openPoint }
            openPointGroup={ open }
            value={ value }

            onSelect={ this.onSelect }
            onClickPoint={ this.onHandleClick }
          />
        </DivBlockContent>
      </DivBlock>
    )
  }
}

export default BlockPointGroup
