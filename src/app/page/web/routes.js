import routeCompetition     from 'app/page/web/PageCompetition/route'
import routeCustomer        from 'app/page/web/PageCustomer/route'
import routeHome            from 'app/page/web/PageHome/route'
import routeLogin           from 'app/page/web/PageLogin/route'


export default {
  path: '/',

  children: [
    routeCompetition,
    routeCustomer,
    routeHome,
    routeLogin,
  ],

  async action({ next }) {
    const route = await next()

    route.title = `${ route.title }`
    route.description = ``

    return route
  }
}
