export default (height) => {
  const firstRowHeight = (height) * 0.4

  const layout1 = [
    { i: 'a', x: 0, y: 0, h: (firstRowHeight)/20, w: 6 },
    { i: 'b', x: 6, y: 0, h: (firstRowHeight)/20, w: 6 },
    { i: 'c', x: 0, y: 1, h: 28, w: 12 },
    { i: 'd', x: 0, y: 1, h: 25, w: 12 },
  ]

  const layout2 = [
    { i: 'a', x: 0, y: 0, h: (firstRowHeight)/20, w: 5, },
    { i: 'b', x: 5, y: 0, h: (firstRowHeight)/20, w: 5, },
    { i: 'c', x: 0, y: 1, h: 28, w: 10, },
    { i: 'd', x: 0, y: 1, h: 25, w: 12 },
  ]

  const layout3 = [
    { i: 'a', x: 0, y: 0, h: (firstRowHeight)/20, w: 3, },
    { i: 'b', x: 3, y: 0, h: (firstRowHeight)/20, w: 3, },
    { i: 'c', x: 0, y: 1, h: 28, w: 6 },
    { i: 'd', x: 0, y: 1, h: 25, w: 12 },
  ]

  const layouts = {
    lg: layout1,
    md: layout2,
    sm: layout3
  }

  return layouts
}
