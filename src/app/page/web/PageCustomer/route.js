import React                from 'react'


import routeName            from 'app/story/routeNames'

import Customer             from './Customer'


const title = routeName.Customer.Title

export default {
  path: '/customer',

  action() {
    return {
      title,
      component: (
        <Customer title={ title } />
      ),
    }
  },
}
