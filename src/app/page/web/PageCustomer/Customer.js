import Paper                from 'material-ui/Paper'

import React, { Component } from 'react'

import { Responsive,
    WidthProvider }         from 'react-grid-layout'

import styled               from 'styled-components'


import go                   from 'app/story/go'

import AgeSexGroupChart     from 'app/widget/grf05/web/Grf05Logic'
import HeatMapLogic         from 'app/widget/heatmapSale/web/HeatmapSaleLogic'
import CustomerParameter    from 'app/widget/customerParameter/web/Container'
import ConsumptionPeriod    from 'app/widget/consumptionPeriodSku/Container'
import Discount             from 'app/discount/web/Discount'

import gridLyaout           from './gridLayout'

import withData             from 'app/page/web/PageCustomer/hoc/withData'


const ResponsiveReactGridLayout = WidthProvider(Responsive)

const DivContainer = styled.div`
  width: 100%;
  height: inherit;
`

class Customer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      width: '1300px',
      height: 900,
      layouts: {},

      openDiscount: false
    }
  }

  onSetLayout = ({ height, width }) => {
    const layouts = gridLyaout(height)

    this.setState({ width, layouts })
  }

  componentWillMount() {
    const { isAuth, width } = this.props

    const { height } = this.state

    if (!isAuth) go('/')

    if (width !== 0 && height !== 0) this.onSetLayout({ height, width })
  }

  componentWillReceiveProps(nextProps) {
    const { width } = nextProps

    const { height } = this.state

    if (width !== 0 && height !== 0) this.onSetLayout({ height, width })
  }

  onHandleDiscountOpen = () => this.setState({ openDiscount: !this.state.openDiscount })

  render() {
    const { width, layouts, openDiscount } = this.state

    const { classes } = this.props

    return (
      <DivContainer>
        <ResponsiveReactGridLayout
          width={ width }
          margin={ [0, 0] }
          rowHeight={ 20 }
          measureBeforeMount={ true }
          className='layout'
          isDraggable={ false }
          layouts={ layouts }
          breakpoints={{ lg: 1300, md: 960, sm: 768, xs: 568 }}
          cols={{ lg: 12, md: 10, sm: 6, xs: 4 }}
        >
          <div key='a'>
            <Paper className={ classes.paperA }>
              <AgeSexGroupChart />
            </Paper>
          </div>
          <div key='b'>
            <Paper className={ classes.paperB }>
              <HeatMapLogic />
            </Paper>
          </div>
          <div key='c'>
            <Paper className={ classes.paperC }>
              <CustomerParameter />
            </Paper>
          </div>
          <div key='d'>
            <Paper className={ classes.paperD }>
              <ConsumptionPeriod onSelect={ this.onHandleDiscountOpen }/>
            </Paper>
          </div>
        </ResponsiveReactGridLayout>
        <Discount
          open={ openDiscount }
          onClose={ this.onHandleDiscountOpen }
        />
      </DivContainer>
    )
  }
}


export default withData(Customer)
