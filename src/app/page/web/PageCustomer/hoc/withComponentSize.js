import React                from 'react'

import sizeMe               from 'react-sizeme'


function withComponentSize(WrappedComponent) {
  class ComponentSizeProvider extends React.Component {
    constructor(props) {
      super(props)

      this.state = {
        width: 1200,
        height: 908,
      }
    }

    componentWillMount() {
      this.setState({
        width: window.innerWidth,
        height: window.innerHeight - 80,
      })
    }

    componentWillReceiveProps(nextProps) {
      const { size } = nextProps

      const { width, height } = size

      if (width !== 0 && height !== 0) this.setState({ width, height })
    }

    render() {
      return (
        <WrappedComponent
          { ...this.props }
          { ...this.state }
        />
      )
    }
  }

  const config = { monitorHeight: true, monitorWidth: true }

  const withSize = sizeMe(config)

  return withSize(ComponentSizeProvider)
}

export default withComponentSize
