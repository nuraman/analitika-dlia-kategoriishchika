import React, { Component } from 'react'

import { connect }          from 'react-redux'
import { compose }          from 'redux'

import { withStyles }       from 'material-ui/styles'

import withComponentSize    from 'app/page/web/PageCustomer/hoc/withComponentSize'


const style = {
  paperA: {
    marginLeft: 10,
    marginRight: 5,
    marginTop: 10,
    height: 'calc(100% - 10px)',
  },
  paperB: {
    marginLeft: 5,
    marginRight: 10,
    marginTop: 10,
    height: 'calc(100% - 10px)',
  },
  paperC: {
    height: 'calc(100% - 20px)',
    marginLeft: 10,
    marginRight: 5,
    marginTop: 10,
    marginBottom: 10,
  },
  paperD: {
    height: 'calc(100% - 20px)',
    marginLeft: 5,
    marginRight: 10,
    marginTop: 10,
    marginBottom: 10,
  },
  paperE: {
    height: 'calc(100% - 20px)',
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
  }
}

function withData(WrappedComponent) {
  class Container extends Component {
    render() {
      return (<WrappedComponent { ...this.props } />)
    }
  }

  function mapStateToProps(state) {
    const { user, checkComparison } = state

    const { userLogin: { order } } = user

    const isAuth = order.length !== 0

    const { categorySelectedList, valueBigger, valueDifference } = checkComparison

    return {
      isAuth,
      categorySelectedList,
      valueBigger,
      valueDifference
    }
  }

  const WithSizeCustomer = withComponentSize(Container)

  return compose(
    connect(mapStateToProps),
    withStyles(style)
  )(WithSizeCustomer)
}

export default withData
