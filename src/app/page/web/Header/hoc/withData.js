import React, { Component } from 'react'
import { connect }          from 'react-redux'


function withData(ComposedComponent) {
  class Header extends Component {
    render() {
      return (
        <ComposedComponent {...this.props } />
      )
    }
  }

  function mapSateToProps(state) {
    const { user: { userLogin } } = state

    const { data, order } = userLogin

    const id = order[0]
    const { providerName, userName } = data[id] || {}

    return {
      isAuth: order.length === 0 ? false : true,
      currentUserName: providerName ? providerName : userName
    }
  }

  return connect(mapSateToProps)(Header)
}

export default withData
