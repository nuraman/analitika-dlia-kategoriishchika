import React, { Component } from 'react'

import styled, { css }      from 'styled-components'


import go                   from 'app/story/go'
import routeName            from 'app/story/routeNames/link'


const DivMenuTabs = styled.div`
  align-self: flex-end;
  display: flex;
`

const DivMenuTab = styled.a`
  position: relative;
  display: flex;
  width: 130px;
  flex-shrink: 1;
  margin-right: 5px;
  padding: 7px 10px;
  text-decoration: none;
  font-size: 14px;
  font-family: HelveticaRegular;
  color: #fff;
  cursor: pointer;
  background-color: #5ea5a5;
  text-align: center;
  align-items: center;
  justify-content: space-around;
`

const DivMenuTabActive = DivMenuTab.extend`
  ${ props => props.active && css`
    color: #111111;
    background-color: #f5f5f5;
  `}
`

const PAGE_ID = {
  HOME:         1,
  CUSTOMER:     2,
  COMPETITION:  3,
}

class Tabs extends Component {
  constructor() {
    super()

    this.state = {
      pageHomeActive: true,
      pageCompetitionActive: false,
      pageCustomerActive: false,
    }
  }

  onChangeState = (home, customer, competition) => {
    this.setState({
      pageHomeActive: home,
      pageCustomerActive: customer,
      pageCompetitionActive: competition,
    })
  }

  onHandleClick = (id) => {
    switch (id) {
      case PAGE_ID.HOME: {
        this.onChangeState(true, false, false)

        return go('/')
      }
      case PAGE_ID.CUSTOMER: {
        this.onChangeState(false, true, false)

        return go('/customer')
      }
      case PAGE_ID.COMPETITION: {
        this.onChangeState(false, false, true)

        return go('/competition')
      }
      default: {
        this.onChangeState(true, false, false)

        return go('/')
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    const { title } = nextProps

    if (title === routeName.Customer.Title) this.onChangeState(false, true, false)

    if (title === routeName.Competition.Title) this.onChangeState(false, false, true)
  }

  render() {
    const {
      pageCustomerActive,
      pageCompetitionActive,
      pageHomeActive,
    } = this.state

    return(
      <DivMenuTabs>
        <DivMenuTabActive
          active={ pageHomeActive }
          onClick = { () => this.onHandleClick(PAGE_ID.HOME) }
        >
          Главная
        </DivMenuTabActive>
        <DivMenuTabActive
          active={ pageCustomerActive }
          onClick = { () => this.onHandleClick(PAGE_ID.CUSTOMER) }
        >
          Карта потребителя
        </DivMenuTabActive>
        <DivMenuTabActive
          active={ pageCompetitionActive }
          onClick = { () => this.onHandleClick(PAGE_ID.COMPETITION) }
        >
          Карта конкуренции
        </DivMenuTabActive>
      </DivMenuTabs>
    )
  }
}

export default Tabs
