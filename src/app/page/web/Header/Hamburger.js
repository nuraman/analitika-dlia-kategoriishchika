import React, { Component } from 'react'

import styled, { css }      from 'styled-components'


const Div = styled.div`
  position: relative;
  box-sizing: border-box;
  width: 60px;
  min-width: 60px;
  height: 50px;
  padding: 10px 20px;
  cursor: pointer;
  background-color: #7bd9d4;  
  ${ props => !props.active && css`
    &:hover > div: first-child {
      opacity: 0.5;
    }
  
    &:hover > div: last-child {
      opacity: 0.5;
    }`
  }
`

const DivLine = styled.div`
  position: absolute;
  width: 20px;
  height: 2px;
  background-color: #FFF;
`

const DivLineTop = DivLine.extend`
  ${ props => !props.active && css`
    top: 17px;
    transition: opacity 0.3s, top 0.3s;
  `}
  
  ${ props => props.active && css`
    top: -2px;
    opacity: 0;
    transition: opacity 0.2s, top 0.3s;
  `}
`

const DivLineCenter = DivLine.extend`
  ${ props => props.active && css`
    transition: transform 0.3s;
    top: 50%;
  `}
  ${ props => !props.active && css`
    top: 50%;
    transform: translateY(-50%);
    transition: transform 0.3s;
  `}
`

const DivLineCenter1 = DivLineCenter.extend`
  ${ props => props.active && css`
    transform: translateY(-50%) rotate(45deg);
  `}
`

const DivLineCenter2 = DivLineCenter.extend`
  ${ props => props.active && css`
    transform: translateY(-50%) rotate(-45deg);
  `}
`

const DivLineBottom = DivLine.extend`
  ${ props => props.active && css`
    bottom: -2px;
    opacity: 0;
    transition: opacity 0.3s, bottom 0.3s;
  `}
  
  ${ props => !props.active && css`
    bottom: 17px;
    transition: opacity 0.2s, bottom 0.3s;
  `}
`


class Hamburger extends Component {
  constructor() {
    super()

    this.state = {
      active: false,
    }
  }

  onHandleClick = () => {
    const { active } = this.state

    const { onClickHamburger } = this.props

    this.setState({ active: !active })

    onClickHamburger && onClickHamburger()
  }

  componentWillReceiveProps(nextProps) {
    const { active } = nextProps

    this.setState({ active })
  }

  render() {
    const { active } = this.state

    return (
      <Div
        active={ active }
        onClick = { this.onHandleClick }
      >
        <DivLineTop active={ active } />
        <DivLineCenter1 active={ active } />
        <DivLineCenter2 active={ active } />
        <DivLineBottom active={ active } />
      </Div>
    )
  }
}

export default Hamburger
