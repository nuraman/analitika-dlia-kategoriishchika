import React, { Component } from 'react'

import styled               from 'styled-components'


import Tabs                 from './Tabs'
import PeriodControl        from 'app/menu/web/PeriodControl'
import PerionNextButton     from 'app/menu/web/PeriodNext'
import PeriodPrevButton     from 'app/menu/web/PeriodPrev'


import Hamburger            from 'app/page/web/Header/Hamburger'

import withData             from 'app/page/web/Header/hoc/withData'


const DivHeader = styled.div`
  display: flex;
  align-items: center;
  height: 50px;
`

const DivHeaderLogo = styled.div`
  width: 250px;
  padding: 7px 15px;
  font-family: HelveticaRegular;
  font-size: 18px;
  color: #fff;
  @media(max-width: 1200px) {
    font-size: 16px;
  }
  @media(max-width: 900px) {
    font-size: 14px;
  }
`

const DivPeriod = styled.div`
  padding: 7px 15px;
  display: flex;
  align-items: center;
  justify-content: space-between; 
  margin: 0 0px 0 auto;
`
//  margin: 0 10px 0 auto;

class Header extends Component {
  constructor() {
    super()

    this.state = {
      open: false
    }
  }

 onClickHamburder = () => {
   const { onContentShift } = this.props
   this.setState({ open: !this.state.open, close: true })

   onContentShift()
 }

 render() {
   const { isAuth, title, currentUserName, active } = this.props

   if (!isAuth) return null

   return(
     <DivHeader>
       <Hamburger
        onClickHamburger = { this.onClickHamburder }
        active={ active }
       />
       <DivHeaderLogo>
         { currentUserName }
       </DivHeaderLogo>
       <Tabs className='primary' title={ title } />
       <DivPeriod>
        <PeriodPrevButton />
        <PeriodControl />
        <PerionNextButton />
       </DivPeriod>
       <DivHeaderLogo>
         <img src='assets/img-com.png' alt='' />
       </DivHeaderLogo>
     </DivHeader>
   )
 }
}

export default withData(Header)
