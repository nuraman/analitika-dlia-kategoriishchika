import React                from 'react'


import Login                from './PageLogin'


const title = `Login`

export default {
  path: '/Login',

  action() {
    return {
      title,
      component: ( <Login /> )
    }
  },
}
