import Paper                from 'material-ui/Paper'

import React, { Component } from 'react'

import { Responsive
  , WidthProvider }         from 'react-grid-layout'

import styled               from 'styled-components'


import reducerMenu          from 'app/menu/reducer'

import UserLoadSetting      from 'app/page/web/UserLoadSetting'

import go                   from 'app/story/go'

import withData             from 'app/page/web/PageHome/hoc/withData'

import CategoryGoodsChart   from 'app/widget/grf01/web/Grf01'
import BrandSaleResultChart from 'app/widget/grf02/web/Grf02Logic'
import DynamicTurnoverChart from 'app/widget/grf03/web/Grf03'
import SaleResultChart      from 'app/widget/grf04/web/Container'

import WidgetDescription    from 'app/widget/WidgetDescription'

import layout               from './layout'


const Div = styled.div`
  width: 100%;
  height: 100%;
`

const ResponsiveReactGridLayout = WidthProvider(Responsive)

class Home extends Component {
  constructor() {
    super()

    this.state = {
      height: 900,
      isAuth: false,
      layouts: {},
      openWidgetDescription: false,
      resize: false,
      userParameterReady: false,
      width: 1300,
      widgetTitle: '',
    }
  }

  //Проверка параметров пользователя
  onCheckUserParameter = () => {

    const {
      categoryId,
      parameterId,

      userSetting,
      dataProvider = {},
      orderProvider = [],

      dispatch
    } = this.props
    //если еще параметры не выбраны, то устанавливаем
    if (!categoryId && !parameterId) {
      const { goodsCategory, pointGroup, pointFormat = 0, point, period, typeParameter } = userSetting || {}

      if (orderProvider.length > 0) {
        let provider = {}

        orderProvider.forEach((item) => {
          const { id, value, selected } = dataProvider[item] || {}

          if (selected) provider = { id, value }
        })

        dispatch({
          type: reducerMenu.types.SET_SETTING,
          payload: {
            newCategory: { id: goodsCategory, value: '' },
            newParameter: { id: typeParameter, value: '' },
            newPeriod: { id: period, value: '' },
            newPoint: { id: point ? point.toString() : '', value: '' },
            newPointFormat: { id: pointFormat === null ? '' : pointFormat, value: '' },
            newPointGroup: { id: pointGroup ? pointGroup : '', value: '' },
            newProviderList: provider.id,

            selected: true
          }
        })

        this.setState({ userParameterReady: true })
      }
    }
  }

  onHandleDescription = (payload) => {
    const { title } = payload || {}

    this.setState({ openWidgetDescription: !this.state.openWidgetDescription, widgetTitle: title })
  }

  componentWillReceiveProps(nextProps) {
    const { isAuth, width } = nextProps

    const { height } = this.state

    const { userParameterReady } = this.state

    if (isAuth && !userParameterReady) this.onCheckUserParameter() // проверяем параметры пользователя

    if (width !== 0 && height !== 0) {
      const layouts = layout(height)

      this.setState({ width, layouts, isAuth })
    }

    this.setState({ isAuth })
  }

  componentWillMount() {
    const { width, isAuth } = this.props

    const { height } = this.state

    const token = localStorage.getItem('sessionID')
    if (!token) go('/login')

    const { userParameterReady } = this.state

    if (isAuth && !userParameterReady) this.onCheckUserParameter() //проверяем параметры пользователя

    if (width !== 0 && height !== 0) {
      const layouts = layout(height)

      this.setState({ width, layouts, isAuth })
    }
  }

  render() {
    const { isAuth, width, layouts, openWidgetDescription, widgetTitle } = this.state

    const { classes } = this.props

    if (!isAuth) return (
      <UserLoadSetting />
    )

    return (
      <Div>
        <ResponsiveReactGridLayout
          width={ width }
          className='layout'
          margin={ [0, 0] }
          rowHeight={ 20 }
          isDraggable={ false }
          isResizable={ true }
          layouts={ layouts }
          breakpoints={{ lg: 1300, md: 960, sm: 768, xs: 568 }}
          cols={{ lg: 12, md: 10, sm: 6, xs: 4 }}
        >
          <div key='a'>
            <Paper className={ classes.paperA }>
              <CategoryGoodsChart openWidgetDescription={ this.onHandleDescription } />
            </Paper>
          </div>
          <div key='b'>
            <Paper className={ classes.paperB }>
              <BrandSaleResultChart openWidgetDescription={ this.onHandleDescription } />
            </Paper>
          </div>
          <div key='c'>
            <Paper className={ classes.paperC }>
              <DynamicTurnoverChart openWidgetDescription={ this.onHandleDescription } />
            </Paper>
          </div>
          <div key='d'>
            <Paper className={ classes.paperD }>
              <SaleResultChart openWidgetDescription={ this.onHandleDescription } />
            </Paper>
          </div>
        </ResponsiveReactGridLayout>
        <WidgetDescription
          open={ openWidgetDescription }

          title={ widgetTitle }

          onClose={ this.onHandleDescription }
        />
      </Div>
    )
  }
}

export default withData(Home)
