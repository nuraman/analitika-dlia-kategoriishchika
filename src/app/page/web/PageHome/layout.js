export default (height) => {
  const firstRowHeight = height * 0.4
  const secondRowHeight = height * 0.6

  const layout1 = [
    { i: 'a', x: 0, y: 0, h: firstRowHeight/20, w: 4 },
    { i: 'b', x: 4, y: 0, h: firstRowHeight/20, w: 4 },
    { i: 'c', x: 8, y: 0, h: firstRowHeight/20, w: 4 },
    { i: 'd', x: 0, y: 1, h: secondRowHeight/20, w: 12 },
  ]

  const layout2 = [
    { i: 'a', x: 0, y: 0, h: firstRowHeight/20, w: 3.4 },
    { i: 'b', x: 3.4, y: 0, h: firstRowHeight/20, w: 3.4 },
    { i: 'c', x: 6.8, y: 0, h: firstRowHeight/20, w: 3.2 },
    { i: 'd', x: 0, y: 1, h: secondRowHeight/20, w: 10 },
  ]

  const layout3 = [
    { i: 'a', x: 0, y: 0, h: firstRowHeight/20, w: 2, },
    { i: 'b', x: 2, y: 0, h: firstRowHeight/20, w: 2, },
    { i: 'c', x: 4, y: 0, h: firstRowHeight/20, w: 2, },
    { i: 'd', x: 0, y: 1, h: secondRowHeight/20, w: 6 },
  ]

  const layouts = {
    lg: layout1,
    md: layout2,
    sm: layout3
  }

  return layouts
}
