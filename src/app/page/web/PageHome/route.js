import React                from 'react'


import routeName            from 'app/story/routeNames'

import Home                 from './PageHome'


const title = routeName.Home.Title

export default {
  path: '/',

  action() {
    return {
      title,
      component: ( <Home /> )
    }
  },
}
