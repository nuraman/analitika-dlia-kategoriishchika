import React, { Component } from 'react'
import { connect }          from 'react-redux'
import { compose }          from 'redux'


import withPeriodList       from 'app/menu/graphql/withMenuList'
import withParameterList    from 'app/menu/graphql/withMenuList'
import
  withGoodsCategoryList     from 'app/menu/graphql/withMenuList'
import withPointFormatList  from 'app/menu/graphql/withMenuList'
import withUserLoadSetting  from 'app/page/graphql/userLoadSetting'

import withSize             from 'app/page/web/PageHome/hoc/withWindowSize'
import { withStyles }       from 'material-ui/styles'

import config               from 'app/menu/config'


const style = {
  paperA: {
    marginLeft: 10,
    marginTop: 10,
    height: 'calc(100% - 10px)',
  },
  paperB: {
    marginLeft: 10,
    marginTop: 10,
    height: 'calc(100% - 10px)',
  },
  paperC: {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    height: 'calc(100% - 10px)',
  },
  paperD: {
    height: 'calc(100% - 20px)',
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    marginBottom: 10,
  }
}

function withData(ComposedComponent) {
  class Container extends Component {
    render() {
      return (
        <ComposedComponent { ...this.props } />
      )
    }
  }

  function mapSateToProps(state) {
    const { user, menu } = state

    const { userLogin: { order } } = user

    const { category, parameter, selected, period, pointFormat, providerList } = menu

    const isAuth = order.length !== 0

    return {
      isAuth,
      selected,
      parameter,
      categoryId: category.id ? category.id : null,
      parameterId: parameter.id ? parameter.id : null,
      periodId: period.id ? period.id : null,
      pointFormatId: pointFormat.id ? pointFormat.id : null,
      providerId: providerList ? providerList : null,
      options: {
        isAuth,
      }
    }
  }


  const WithSizeContainer = withSize(Container)

  return compose(
    connect(mapSateToProps),
    withGoodsCategoryList({ queryName: config.queryNameMap.GOODS_CATEGORY_LIST }),
    withParameterList({ queryName: config.queryNameMap.PARAMETER_LIST }),
    withPeriodList({ queryName: config.queryNameMap.PERIOD_LIST }),
    withPointFormatList({ queryName: config.queryNameMap.POINT_FORMAT_LIST }),
    withUserLoadSetting(),
    withStyles(style),
  )(WithSizeContainer)
}


export default withData
