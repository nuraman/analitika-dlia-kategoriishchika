import Dialog               from 'material-ui/Dialog'
import Slide                from 'material-ui/transitions/Slide'

import { MuiThemeProvider } from 'material-ui/styles'

import { createMuiTheme }   from 'material-ui/styles'

import React, { Component } from 'react'

import { connect }          from 'react-redux'
import { compose }          from 'redux'

import ProgrssBar           from 'app/analysis/web/CircularProgressBar'


const theme = createMuiTheme({
  overrides: {
    MuiDialog: {
      root: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      },
      paper: {
        height: 0,
        width: 0,
      }
    },
  }
})

class LoadingHomePage extends Component {
  constructor(props){
    super(props)

    this.state = {
      open: true
    }
  }

  componentWillReceiveProps(nextProps) {
    const { userLoadSetting } = nextProps

    this.setState({ open: userLoadSetting })
  }

  componentWillMount() {
    const { userLoadSetting } = this.props

    this.setState({ open: userLoadSetting })
  }

  render() {
    const { open } = this.state

    return (
      <MuiThemeProvider theme={ theme } >
        <Dialog open={ open } transition={ Slide } >
          <ProgrssBar />
        </Dialog>
      </MuiThemeProvider>
    )
  }
}

export default compose(
  connect(),
)(LoadingHomePage)

