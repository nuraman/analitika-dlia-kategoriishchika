import _                    from 'lodash'

import React, { Component } from 'react'

import { compose }          from 'redux'
import { connect }          from 'react-redux'


import withUserLoadSetting  from 'app/page/graphql/userLoadSetting'

import go                   from 'app/story/go'

import menuReducer          from 'app/menu/reducer'
import userReducer          from 'app/user/reducer'
import Loading              from 'app/page/web/PageHome/LoadingHomePage'

//загрузка выбранных пользователем параметров из сервера
class UserLoadSetting extends Component {
  componentWillReceiveProps(nextProps) {
    const {
      dispatch,

      dataProvider,
      orderProvider,

      userSetting = {},
      userInfo = {},
      userLoadSetting,

      errors = {},
    } = nextProps

    const { code } = errors //message

    if (code === 1 && userLoadSetting === false) {
      return go('/login')
    }

    const {
      goodsCategory = '',
      pointFormat   = '',
      period        = '',
      dateBegin     = '',
      dateEnd       = '',
      typeParameter = '',
    } = userSetting

    const { user, userName, provider, providerName } = userInfo

    if (nextProps.userLoadSetting !== this.props.userLoadSetting) {

      if (!_.isEmpty(userSetting)) { //если настройки пользователя не пустой, то сохраням их
        let providerSelected = ''

        if (orderProvider.length > 0) { //выбор поставщика
          orderProvider.forEach((index) => {
            const { id, selected } = dataProvider[index]

            if (selected) providerSelected = id
          })

          if (!providerSelected) { //если поставщик не выбран, то назначаем первого из списка
            const { id } = dataProvider[orderProvider[0]] || {}

            providerSelected = id
          }
        }

        dispatch({
          type: menuReducer.types.SET_LOAD_SETTING,
          payload: {
            categoryId: goodsCategory === 0 ? null : goodsCategory,
            periodId: period === 0 ? null: period,
            dateBegin: dateBegin === 0 ? null : dateBegin,
            dateEnd: dateEnd === 0 ? null: dateEnd,
            parameterId: typeParameter === 0 ? null : typeParameter,
            pointFormatId: pointFormat === null ? null : pointFormat,
            providerList: providerSelected,
            selectedStatus: true,
          }
        })
      }

      if (!_.isEmpty(userInfo)) {
        dispatch({
          type: userReducer.types.USER_SUCCESS,
          payload: {
            userId: user,
            session: '',
            provider,
            providerName,
            userName,
            isProvider: !!providerName
          },
        })
      }

      go('/')
    }
  }

  render() {
    return (
      <Loading userLoadSetting={ true } />
    )
  }
}

export default compose(
  connect(),
  withUserLoadSetting()
)(UserLoadSetting)
