import { injectGlobal }     from 'styled-components'


import Helvetica            from 'app/theme/font/HelveticaRegular.ttf'
import HelveticaLight       from 'app/theme/font/HelveticaLight.ttf'
import HelveticaItalic      from 'app/theme/font/HelveticaItalic.ttf'
import HelveticaBold        from 'app/theme/font/HelveticaBold.ttf'


export default injectGlobal`
  body {
    width: 100%;
    height: 100%;
    margin: 0;
    overflow: hidden;
    @font-face {
      font-family: 'HelveticaRegular';
      src: url(${ Helvetica }) format('truetype');
    }
    @font-face {
      font-family: 'HelveticaLight';
      src: url(${ HelveticaLight }) format('truetype');
    }
    @font-face {
      font-family: 'HelveticaItalic';
      src: url(${ HelveticaItalic }) format('truetype');
    }
    @font-face {
      font-family: 'HelveticaBold';
      src: url(${ HelveticaBold }) format('truetype');
    },
  },
   input,
  select,
  textarea {
    border: none;
    outline: none;
    -webkit-appearance: none;
  }
`
