import React, { Component } from 'react'

import styled, { css }      from 'styled-components'


import injectGlobal         from './injectGlobal'

import Header               from 'app/page/web/Header/Header'
import Aside                from 'app/page/web/Aside/Container'
import Feedback             from 'app/feedback/web/Container'

import Map                  from 'es6-map'

require('es5-shim')
require('es6-shim')

const DivContainer = styled.div`
  display: flex;
	flex-direction: column;
	width: 100vw;
	height: 100vh;
	overflow: hidden;
	background-color: #f5f5f5;
`

const DivLayoutHeader = styled.div`
  background-color: #4c9595
`

const DivLayoutContent = styled.div`
  display: flex;
  height: calc(100% - 50px);
`

const DivContent = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-content: flex-start;
  height: calc(100% - 30px);
  width: calc(100% - 10px);
  box-sizing: border-box;
  margin: 15px 5px;
  overflow-x: hidden;
  transition: margin-right 0.1s;
`

const DivContentActive = DivContent.extend`
  ${ props=>props.active && css`
    width: calc(100% - 320px);
    transition: margin-right 0.1s;
  `}
`

class App extends Component {
  constructor() {
    super()

    this.state = {
      shift:  false,
      resize: false,
      openFeedback: false,
    }
  }

  onResizeByTimeout = () => {
    setTimeout(() => {
      this.setState({ resize: false })
    }, 500)
  }

  onShiftAside = () => {
    this.setState({ resize: true, shift: !this.state.shift })

    this.onResizeByTimeout()
  }

  onCloseAside = () => {
    this.setState({ resize: true, shift: !this.state.shift })

    this.onResizeByTimeout()
  }

  onClickFeedback = () => this.setState({ openFeedback: !this.state.openFeedback })

  render() {
    const { shift, openFeedback, resize } = this.state
    const { title } = this.props

    if (title === 'Login') {
      return (
        <DivContainer>
        { this.props.children }
        </DivContainer>
      )
    }

    return (
      <DivContainer>
        <DivLayoutHeader>
          <Header
            title={ title }
            active={ shift }
            onContentShift={ this.onShiftAside }
          />
        </DivLayoutHeader>
        <DivLayoutContent>
          <Aside
            open={ shift }

            onClickFeedback={ this.onClickFeedback }
            onClose={ this.onCloseAside }
          />
          <DivContentActive active={ shift }>
            { resize ? null : this.props.children }
          </DivContentActive>
          <Feedback
            openFeedback={ openFeedback }
            onCloseFeedback={ this.onClickFeedback }
          />
        </DivLayoutContent>
      </DivContainer>
     )
  }
}

export default App


