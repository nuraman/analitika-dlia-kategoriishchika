import React, { Component } from 'react'


import Discount             from 'app/discount/web/Discount'
import DiscountAppendStatus from 'app/discount/web/DiscountAppendStatus'


class DiscountHandle extends Component {
  constructor() {
    super()

    this.state = {
      openDiscountAppend: false,  //открыть сообщение из сервера о результате оформления акции
      codeDiscountAppend: '',     //код из сервера об оформлении акции, 0 - удачно, 1 - неудачно
    }
  }

  onCloseDiscount = (code) => {
    const { openDiscountAppend } = this.state

    const { onClose } = this.props

    onClose()

    this.setState({
      openDiscountAppend: !openDiscountAppend,
      codeDiscountAppend: code,
    })
  }

  onCloseDiscountAppendStatus = () => this.setState({ openDiscountAppend: !this.state.openDiscountAppend })

  render() {
    const { open } = this.props
    const { openDiscountAppend, codeDiscountAppend } = this.state

    if (!open) return null

    return (
      <div>
        <Discount
          open={ open }
          onClose={ this.onCloseDiscount }
        />
        <DiscountAppendStatus
          open={ openDiscountAppend }
          code={ codeDiscountAppend }

          onClose={ this.onCloseDiscountAppendStatus }
        />
      </div>
    )
  }
}

export default DiscountHandle
