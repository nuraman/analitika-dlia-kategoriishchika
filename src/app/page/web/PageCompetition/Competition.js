import Paper                from 'material-ui/Paper'

import React, { Component } from 'react'

import { connect }          from 'react-redux'
import { compose }          from 'redux'

import { Responsive,
  WidthProvider }           from 'react-grid-layout'

import  styled              from 'styled-components'


import go                   from 'app/story/go'

import CheckComparisonChart from 'app/widget/checkComparison/web/Container'
import
  CategoryConsumptionChart  from 'app/widget/grf06/web/Grf06Logic'
import RfmAnalyserChart     from 'app/widget/rfmAnalyseWidget/web/Container'
import CategoryLoyalty      from 'app/widget/categoryLoyalty/Container'

import Discount             from './DiscountHandle'

import gridLayout           from './gridLayout'

import withData             from 'app/page/web/PageCustomer/hoc/withData'


import CategorySelect       from 'app/widget/checkComparison/web/CategorySelect/Container'
import ModalCheck           from 'app/widget/checkContent/Modal'

import reducer              from 'app/widget/checkComparison/reducer'


const ResponsiveReactGridLayout = WidthProvider(Responsive)




const Container = styled.div`
  width: 100%;
  height: inherit;
`

class Competition extends Component {
  constructor(props) {
    super(props)

    const { title } = props

    this.state = {
      categoryList: [],

      height: '',
      layouts: {},

      openDiscount: false,        //открыть окно оформления акции
      openCategorySelect: false,
      openModal: false,

      title: title,

      width: '1300px',
    }
  }

  componentWillMount() {
    const { isAuth, width, height } = this.props

    if (!isAuth) go('/')

    if (width !== 0 && height !== 0)
      this.onSetLayout({ height, width })
  }

  componentWillReceiveProps(nextProps) {
    const { width, height } = nextProps

    if (width !== 0  && height !== 0)
      this.onSetLayout({ height, width })
  }

  onHandleDiscountOpen = () => this.setState({ openDiscount: !this.state.openDiscount, openModal: false })
  onHandleOpenModal = () => this.setState({ openModal: !this.state.openModal })

  onGetCategoryList = (categoryList) =>  this.setState({ categoryList, openCategorySelect: !this.state.openCategorySelect })
  onCloseCategorySelect = () => this.setState({ openCategorySelect: false })
  onSelectCategoryList = ({ categorySelectedList, valueBigger, valueDifference }) => {
    const { dispatch } = this.props

    dispatch({
      type: reducer.types.SET_CATEGORY_LIST,
      payload: {
        categorySelectedList,
        valueBigger,
        valueDifference,
      }
    })

    this.setState({ openCategorySelect: !this.state.openCategorySelect })
  }

  onSetLayout = ({ height, width }) => {
    const layouts = gridLayout(height)

    this.setState({ width, layouts })
  }

//width={ width } height={ height }
  render() {
    const { categoryList, layouts, openDiscount, openCategorySelect, openModal, width } = this.state
    const { classes } = this.props

    return (
      <Container>
        <ResponsiveReactGridLayout
          width={ width }
          margin={[0, 0]}
          measureBeforeMount={ true }
          className='layout'
          rowHeight={ 20 }
          isDraggable={ false }
          layouts={ layouts }
          breakpoints={{ lg: 1300, md: 960, sm: 768, xs: 568 }}
          cols={{ lg: 12, md: 10, sm: 6, xs: 4 }}
        >
          <div key='a'>
            <Paper className={ classes.paperA }>
              <CategoryConsumptionChart onSelect={ this.onHandleDiscountOpen } />
            </Paper>
          </div>
          <div key='b'>
            <Paper className={ classes.paperB }>
              <CategoryLoyalty onSelectDiscont={ this.onHandleDiscountOpen } />
            </Paper>
          </div>
          <div key='c'>
            <Paper className={ classes.paperC }>
              <RfmAnalyserChart
                onSelectDiscont={ this.onHandleDiscountOpen }
                onOpenModal={ this.onHandleOpenModal }
              />
            </Paper>
          </div>
          <div key='d'>
            <Paper  className={ classes.paperD }>
              <CheckComparisonChart
                onGetCategoryList={ this.onGetCategoryList }
                onSelect={ this.onHandleDiscountOpen }
              />
            </Paper>
          </div>
        </ResponsiveReactGridLayout>
        <Discount
          open={ openDiscount }
          onClose={ this.onHandleDiscountOpen }
        />
        <CategorySelect
          categoryList={ categoryList }
          open={ openCategorySelect }

          onSelect={ this.onSelectCategoryList }
          onClose={ this.onCloseCategorySelect }
        />
        <ModalCheck open={ openModal } onClose={ this.onHandleOpenModal } onDiscountOpen={ this.onHandleDiscountOpen } />
      </Container>
    )
  }
}

export default compose(
  connect(),
  withData,
)(Competition)
