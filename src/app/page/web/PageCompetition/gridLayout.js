export default (height) => {
  const firstRowHeight = height * 0.4
  const secondRowHeight = height * 0.6

  const layout1 = [
    { i: 'a', x: 0, y: 0, h: (firstRowHeight)/20, w: 6 },
    { i: 'b', x: 6, y: 0, h: (firstRowHeight)/20, w: 6 },
    { i: 'c', x: 0, y: 0, h: (secondRowHeight)/20, w: 6 },
    { i: 'd', x: 6, y: 0, h: (secondRowHeight)/20, w: 6 },
  ]

  const layout2 = [
    { i: 'a', x: 0, y: 0, h: (firstRowHeight)/20, w: 5, },
    { i: 'b', x: 5, y: 0, h: (firstRowHeight)/20, w: 5, },
    { i: 'c', x: 0, y: 0, h: (secondRowHeight)/20, w: 5, },
    { i: 'd', x: 5, y: 0, h: (secondRowHeight)/20, w: 5, },
  ]

  const layout3 = [
    { i: 'a', x: 0, y: 0, h: (firstRowHeight)/20, w: 3, },
    { i: 'b', x: 3, y: 0, h: (firstRowHeight)/20, w: 3, },
    { i: 'c', x: 0, y: 0, h: (secondRowHeight)/20, w: 3 },
    { i: 'd', x: 3, y: 0, h: (secondRowHeight)/20, w: 3 },
  ]

  const layouts = {
    lg: layout1,
    md: layout2,
    sm: layout3
  }

  return layouts
}

