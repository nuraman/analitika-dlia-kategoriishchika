import React                from 'react'


import routeName            from 'app/story/routeNames'

import Competition          from './Competition'


const title = routeName.Competition.Title

export default {
  path: '/competition',

  action() {
    return {
      title,
      component: (
       <Competition title={ title } />
      )
    }
  },
}


