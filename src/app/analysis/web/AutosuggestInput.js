import _                    from 'lodash'

import { withStyles }       from 'material-ui/styles'

import React, { Component } from 'react'

import Autosuggest          from 'react-autosuggest'

import styled               from 'styled-components'


const Input = styled.input`
  background-color: rgba(214, 219, 226, 0);
  border: 1px solid rgb(158, 158, 158);
  height: 34px;
  width: calc(100% - 10px);
  z-index: 76;
  
  &:focus {
    outline: none;
  }
`

const DivSuggestedItem = styled.div`
  display: block;
  margin-top: 10px;
  margin-bottom: 10px;
  text-align: justify;
  &:hover {
    background-color: #616161;
  }
`

const DivLink = styled.a`
  width: 100%;
  height: 100%;
  text-decoration: none;
  cursor: pointer;
`

const styles = theme => ({
  emptyContainer: {
    flexGrow: 1,
    position: 'relative',
  },
  fullContainer: {
    width: 'calc(100% - 10px)',
    position: 'relative',
    flexGrow: 1,
    height: '150px',
    overflow: 'hidden',
  },
  suggestionsContainerOpen: {
    position: 'absolute',
    backgroundColor: 'rgba(214, 219, 226, 0)',
    borderBottom: '1px solid rgb(158, 158, 158)',
    borderLeft: '1px solid rgb(158, 158, 158)',
    borderRight: '1px solid rgb(158, 158, 158)',
    height: '110px',
    width: 'calc(100% - 10px)',
    overflow: 'auto',
    '&::-webkit-scrollbar': {
      width: '9px',
    },
    '&::-webkit-scrollbar-track':
    {
      background: '#73c7c5',
      backgroundClip: 'content-box',
    },
    '&::-webkit-scrollbar-thumb':
    {
      background: '#484848',
      height: '35px',
      borderRadius: '5px solid #484848',
      border: '1px solid #484848',
    }
  },
  suggestion: {
    display: 'block',
    fontSize: '0.875em',
  },
  suggestionsList: {
    margin: 0,
    listStyleType: 'none',
    fontSize: '0.875em',
    color: 'rgb(255,255,255)',
    paddingLeft: '2px',
    height: '150px',
  },
  textField: {
    width: '80%',
    left: '10%',
    right: '10%'
  },
  input: {
    fontFamily: 'HelveticaLight',
    fontSize: '18px',
    color: 'rgb(255,255,255)'
  },
})

const getSuggestions = ({ value, dataList }) => {
  const inputValue = value.trim().toLowerCase()
  const inputLength = inputValue.length

  return inputLength === 0 ? [] : dataList.filter(x => isContain(x.name, inputValue))
}

const isContain = (name, inputValue) => {
  //const contained = name.toLowerCase().includes(inputValue)

  const inputList = _.words(inputValue)

  const matchList = inputList.reduce((matchList, item) => {
    if (name.toLowerCase().includes(item)) matchList.push(item)

    return matchList
  }, [])

  if ( matchList.length >= inputList.length ) return name

  return null
}

const getSuggestionValue = suggestion => suggestion.name

const renderInput = inputProps => {
  const { classes, home, value, ref, onChange, ...other } = inputProps

  return (
    <Input
      type='text'
      value={ value }
      className={ classes.input }
      onChange={ onChange }
      { ...other }
    />
  )
}

const renderSuggestionsContainer = options => {
  const { containerProps, children } = options

  return (
    <div { ...containerProps }>
      { children }
    </div>
  )
}

const renderSuggestion = (suggestion,{ query, isHighlighted }) => {
  return (
    <DivSuggestedItem>
      <DivLink>
      { suggestion.name }
      </DivLink>
    </DivSuggestedItem>
  )
 }

class AutosuggestInput extends Component {
  constructor(props) {
    super(props)

    const { value = '' } = this.props

    this.state = {
      value: value === null? '' : value,
      suggestions: [],
      selected: false,
      selecting: false,
    }
  }

  onChange = (event, { newValue }) => {
    this.setState({ value: newValue })
  }


  onSuggestionsFetchRequested = ({ value }) =>  {
    const { dataList = [], } = this.props

    this.setState({
      suggestions: getSuggestions({ value, dataList }),
      selected: false,
      selecting: true,
    })
  }

  onSuggestionsClearRequested = () => this.setState({ suggestions: [] })

  onSuggestionSelected =(event, { suggestion, suggestionValue, suggestionIndex, sectionIndex, method }) => {
    const { onSelect } = this.props

    onSelect && onSelect({ id: suggestion.id, value: suggestionValue })

    this.setState({ selected: true, selecting: false })
  }

  componentWillReceiveProps(nextProps) {
    const { value = '' } = nextProps

    this.setState({ value })
  }

  componentWillMount() {
    const { value } = this.props

    if (value) this.setState({ selected: false, selecting: false })
  }

  render() {
    const { value, suggestions } = this.state
    const { classes } = this.props

    const inputProps = {
      autoFocus: false,
      classes,
      placeholder: 'Не выбрано',
      value: value === null? '' : value,
      onChange: this.onChange
    }

    return (
      <Autosuggest
        theme={{
          container: suggestions.length === 0 ? classes.emptyContainer : classes.fullContainer,
          input: classes.input,
          inputOpen: classes.inputOpen,
          suggestionsContainerOpen: classes.suggestionsContainerOpen,
          suggestionsList: classes.suggestionsList,
          suggestion: classes.suggestion,
        }}
        focusInputOnSuggestionClick = { false }
        renderInputComponent={ renderInput }
        suggestions={ suggestions }
        onSuggestionsFetchRequested={ this.onSuggestionsFetchRequested }
        onSuggestionsClearRequested={ this.onSuggestionsClearRequested }
        onSuggestionSelected={ this.onSuggestionSelected }
        renderSuggestionsContainer={ renderSuggestionsContainer }
        getSuggestionValue={ getSuggestionValue }
        renderSuggestion={ renderSuggestion }
        inputProps={ inputProps }
      />
    )
  }
}

export default withStyles(styles)(AutosuggestInput)
