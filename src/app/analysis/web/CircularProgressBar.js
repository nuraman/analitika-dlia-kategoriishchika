import { CircularProgress } from 'material-ui/Progress'
import { withStyles }       from 'material-ui/styles'

import PropTypes            from 'prop-types'

import React                from 'react'


const styleSheet = theme => ({
  progress: {
    margin: `0 ${ theme.spacing.unit * 2 }px`,
  },
})

function CircularProgressBar(props) {
  const classes = props.classes
  return (
   <CircularProgress color='primary' className={ classes.progress } />
  )
}

CircularProgressBar.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styleSheet)(CircularProgressBar)
