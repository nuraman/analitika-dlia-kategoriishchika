import React, { Component } from 'react'

import styled               from 'styled-components'


const DivContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  height: 200px;
  overflow-x: hidden;
  border: 1px solid rgb(158, 158, 158);
  
  &::-webkit-scrollbar
  {
    width: 9px;
  }

  &::-webkit-scrollbar-track
  {
    background: #73c7c5;
    background-clip: content-box;
  }

  &::-webkit-scrollbar-thumb
  {
    background: #484848;
    height: 35px;
    border-radius: 5px solid #484848;
    border: 1px solid #484848;
  }
`

const DivSelectOption = styled.div`
  display: flex;
  align-items: center;
  padding: 5px;
`

const Input = styled.input`
  display: none;
  
  &:checked {
    display: block; 
  }
`

const LabelNewCheck = styled.label`
  position: relative;
  width: 18px;
  min-width: 18px;
  height: 18px;
  margin-right: 10px;
  border: 1px solid #48b9bf;
  cursor: pointer;
  
  &:before {
    display: ${ props=>props.checked ? 'block' : 'none' };
    content: "";
    position: absolute;
    width: 12px;
    height: 12px;
    top: 3px;
    left: 3px;
    background-color: #48b9bf;
  }
`

const Label = styled.label`
  margin-bottom: -2px;
  font-weight: 300;
  font-size: 16px;
  font-family: HelveticaLight;
  color: #b3fffb;
  cursor: pointer;
`

class EntitySelectCheckBox extends Component {
  constructor() {
    super()

    this.state = {
      checkedList: [],
    }
  }

  onChange = (id) => {
    const { checkedList } = this.state

    const currentIndex = checkedList.indexOf(id)

    if (currentIndex === -1) {
      checkedList.push(id)
    } else {
      checkedList.splice(currentIndex, 1)
    }

    const { onSelect } = this.props

    this.setState({ checkedList })

    onSelect && onSelect({ id })
  }

  componentWillMount() {
    const { providerSelectedList, data, order } = this.props
    let newCheckedList = []

    if (providerSelectedList.length > 0) {
      newCheckedList = providerSelectedList.reduce((newCheckedList, item) => {
        return [ ...newCheckedList, item ]
      }, [])
    }

    this.setState({ checkedList: newCheckedList, data, order })
  }

  render() {
    const { checkedList, data, order } = this.state

    return(
      <DivContainer>
        {
          order.map((index) => {
            const { value = 0, id, selected } = data[index]

            const isChecked = checkedList.indexOf(id) !== -1

            return (
              <DivSelectOption key={ index }>
                <Input type='checkbox' id={ `item${ index }` } name='' value='' />
                <LabelNewCheck
                  checked={ isChecked }
                  onClick = { () => this.onChange(id) }
                />
                <Label
                  onClick = { () => this.onChange(id) }
                >
                  { value }
                </Label>
              </DivSelectOption>
            )
          })
        }
      </DivContainer>
    )
  }
}

export default EntitySelectCheckBox
