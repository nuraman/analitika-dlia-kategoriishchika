import React, { Component } from 'react'

import styled               from 'styled-components'


const DivContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  height: 200px;
  overflow-x: hidden;
  border: 1px solid rgb(158, 158, 158);
  
  &::-webkit-scrollbar
  {
    width: 9px;
  }

  &::-webkit-scrollbar-track
  {
    background: #73c7c5;
    background-clip: content-box;
  }

  &::-webkit-scrollbar-thumb
  {
    background: #484848;
    height: 35px;
    border-radius: 5px solid #484848;
    border: 1px solid #484848;
  }
`

const DivSelectOption = styled.div`
  display: flex;
  align-items: center;
  padding: 5px;
`

const Input = styled.input`
  display: none;
  
  &:checked {
    display: block; 
  }
`

const LabelNewCheck = styled.label`
  position: relative;
  width: 18px;
  min-width: 18px;
  height: 18px;
  margin-right: 10px;
  border: 1px solid #48b9bf;
  cursor: pointer;
  
  &:before {
    display: ${ props=>props.checked ? 'block' : 'none' };
    content: "";
    position: absolute;
    width: 12px;
    height: 12px;
    top: 3px;
    left: 3px;
    background-color: #48b9bf;
  }
`

const Label = styled.label`
  margin-bottom: -2px;
  font-weight: 300;
  font-size: 16px;
  font-family: HelveticaLight;
  color: #b3fffb;
  cursor: pointer;
`


class EntitySelectMultiple extends Component {
  constructor() {
    super()

    this.state = {
      checkedList: [],
      data: {},
      order: []
    }
  }

  isCheckedItem = ({ checkedId, checkedList }) => {
    let checkedIndex = -1

    checkedList.forEach((obj, index) => {
      const { id } = obj

      if (checkedId === +id) checkedIndex = index
    })

    return checkedIndex
  }

  onChange = ({ id, value }) => {
    const { onSelect } = this.props

    const { checkedList } = this.state

    let index = this.isCheckedItem({ checkedId: id, checkedList })

    if (index === -1) {
      checkedList.push({ id, value })
    } else {
      checkedList.splice(index, 1)
    }

    this.setState({ checkedList })

    onSelect && onSelect(checkedList)
  }

  componentWillMount() {
    const { data = {}, order = [], checkedList = [] } = this.props

    this.setState({ data, order, checkedList })
  }

  render() {
    const { data = {}, order = [], checkedList = [] } = this.state

    return (
      <DivContainer>
        {
          order.map((item, index) => {
            const { id, value } = data[item]

            const isChecked = this.isCheckedItem({ checkedId: id, checkedList }) !== -1

            return (
              <DivSelectOption key={ index }>
                <Input type='checkbox' name='' value='' />
                <LabelNewCheck
                  checked={ isChecked }
                  onClick = { () => this.onChange({ id, value }) }
                />
                <Label
                  onClick = { () => this.onChange({ id, value }) }
                >
                  { value }
                </Label>
              </DivSelectOption>
            )

          })
        }
      </DivContainer>
    )
  }
}

export default EntitySelectMultiple
