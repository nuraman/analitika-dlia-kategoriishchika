import Divider              from 'material-ui/Divider'
import { CircularProgress } from 'material-ui/Progress'
import { withStyles }       from 'material-ui/styles'

import styled               from 'styled-components'

import PropTypes            from 'prop-types'

import React                 from 'react'


const DivContainer = styled.div`
  width: 100%;
  height: 100%;
`

const DivHeader = styled.div`
  height: 60px;
  width: 100%;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`

const DivTitle = styled.div`
  padding-left: 20px;
  font-family: HelveticaRegular;
  font-size: 22px;
`

const DivContent = styled.div`
  width: 100%;
  height: calc(100% - 60px);
  display: flex;
  align-items: center;
  justify-content: space-around;
`

const styleSheet = theme => ({
  progress: {
    margin: `0 ${ theme.spacing.unit * 2 }px`,
  },
})

function WidgetProgressBar(props) {
  const classes = props.classes
  const { title } = props

  return (
    <DivContainer>
      <DivHeader>
        <DivTitle>
          { title }
        </DivTitle>
      </DivHeader>
      <Divider />
      <DivContent>
        <CircularProgress color='primary' className={ classes.progress } />
      </DivContent>
    </DivContainer>
  )
}

WidgetProgressBar.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styleSheet)(WidgetProgressBar)
