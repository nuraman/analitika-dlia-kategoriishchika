import React                from 'react'

import styled               from 'styled-components'


const Container = styled.div`
  margin: 0 -20px 0 -70px;
  background-color: #383838;
  max-height: 200px;
  overflow: auto;
  
  &::-webkit-scrollbar
  {
    width: 9px;
  }

  &::-webkit-scrollbar-track
  {
    background: #73c7c5;
    background-clip: content-box;
  }

  &::-webkit-scrollbar-thumb
  {
    background: #484848;
    height: 35px;
    border-radius: 5px solid #484848;
    border: 1px solid #484848;
  }
`

const List = styled.div`
  box-sizing: border-box;
  padding: 8px 20px 8px 70px;
  font-size: 18px;
  font-family: HelveticaLight;
  cursor: pointer;
  color: #fff;
  &:hover {
    color: #b3fffb;
    background-color: #314545;
  }
`

export default function EntitySelect(props) {
  const onChange = ({ id, value }) => {
    const { onSelect } = props

    onSelect && onSelect({ id, value })
  }

  const { data = {}, order = [], } = props

  return (
    <Container>
      {
        order.map((item) => {
          const { id, value = 0 } = data[item] || {}

          return (
            <List
              key={ id }
              onClick={ () => onChange({ id, value }) }
            >
              { value }
            </List>
          )
        })
      }
    </Container>
  )
}
