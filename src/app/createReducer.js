import { combineReducers }  from 'redux'

import discount             from 'app/discount/reducer'

import menu                 from 'app/menu/reducer'

import provider             from 'app/page/web/Aside/Provider/reducer'

import grf04                from 'app/widget/grf04/reducer'
import rfmAnaylyse          from 'app/widget/rfmAnalyseWidget/reducer'
import checkComparison      from 'app/widget/checkComparison/reducer'
import checkContent         from 'app/widget/checkContent/reducer'

import consumptionPeriod    from 'app/widget/consumptionPeriodSku/reducer'

import user                 from 'app/user/reducer'



export default function createReducer(payload) {
  const { client } = payload

  return combineReducers({
    discount: discount.reducer,

    menu: menu.reducer,

    provider: provider.reducer,

    user: user.reducer,

    grf04: grf04.reducer,
    rfmAnalyse: rfmAnaylyse.reducer,
    checkComparison: checkComparison.reducer,
    checkContent: checkContent.reducer,
    consumptionPeriod: consumptionPeriod.reducer,

    apollo: client.reducer(),
  })
}
