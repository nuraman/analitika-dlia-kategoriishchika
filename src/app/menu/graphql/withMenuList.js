import { graphql }          from 'react-apollo'

import queryGenerate        from 'engine/graphql/generate/create/query'

import config               from 'app/menu/config'

import dataMap              from './dataMap'


export default (payload) => {
  const { queryMap } = config

  const { queryName } = payload

  const query = queryGenerate({ queryMap, name: queryName })

  return graphql(query, {
    skip: (ownProps = {}) => {
      const { options } = ownProps

      const { isAuth } = options

      if (!isAuth) return true

      return false
    },
    props: ({ data }) => {
      return dataMap({ queryName, data })
    }
  })
}
