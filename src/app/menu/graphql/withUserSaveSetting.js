import { graphql }          from 'react-apollo'


import config               from 'app/menu/config'

import generateMutationQuery  from 'engine/graphql/generate/create/mutation'


const userSaveSetting = () => {
  const { mutationMap, mutationNameMap } = config

  const mutationQuery = generateMutationQuery({ mutationMap, name: mutationNameMap.USER_SAVE_SETTING })

  return graphql(mutationQuery, {
    props: ({ mutate }) => ({
      userSaveSetting: (variables) => mutate({variables})
    })
  })
}

export default userSaveSetting

