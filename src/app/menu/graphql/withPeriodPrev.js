import { graphql }          from 'react-apollo'


import config               from 'app/menu/config'

import generateQuery        from 'engine/graphql/generate/create/query'


export default (payload) => {
  const { options } = payload || {}

  const { queryMap, queryNameMap } = config

  const query = generateQuery({ queryMap, name: queryNameMap.PERIOD_PREV })

  return graphql(query, {
    options: (ownProps) => ({ ...options, ...(ownProps.options || {}) }),
    skip: (ownProps) => {
      const { variables } = ownProps.options || {}

      const { dateBegin, dateEnd } = variables || {}

      return !dateBegin
    },
    props: ({ data }) => {
      const { periodPrev = {} } = data || {}

      if (periodPrev && periodPrev.errors && !periodPrev.errors.length) {
        const { errors, result } = periodPrev
        const { code, message } = errors

        const { dateBegin, dateEnd, canPeriodPrev, canPeriodNext } = result || {}

        return { dateBegin, dateEnd, canPeriodPrev, canPeriodNext }
      }
    }
  })
}
