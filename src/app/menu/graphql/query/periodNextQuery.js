import config               from 'app/menu/config'

import queryPeriod          from 'engine/graphql/generate/create/queryPeriodPrev'

export default () => {
  const { queryMap, queryNameMap } = config

  const query = queryPeriod({ queryMap, name: queryNameMap.PERIOD_NEXT })

  return query
}
