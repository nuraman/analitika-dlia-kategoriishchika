import config               from 'app/menu/config'

import queryPeriodPrev      from 'engine/graphql/generate/create/queryPeriodPrev'

export default () => {
  const { queryMap, queryNameMap } = config

  const query = queryPeriodPrev({ queryMap, name: queryNameMap.PERIOD_PREV })

  return query
}
