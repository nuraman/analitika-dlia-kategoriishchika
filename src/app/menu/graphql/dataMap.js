import _                    from 'lodash'


import config               from 'app/menu/config'


export default (payload) => {
  const { data, queryName } = payload

  const { queryNameMap } = config

  switch (queryName) {
    case queryNameMap.GOODS_CATEGORY_LIST: {
      const { goodsCategorieList = {}, loading } = data || {}

      if (goodsCategorieList && goodsCategorieList.errors && !goodsCategorieList.errors.length) {
        const { errors, result = {} } = goodsCategorieList
        const { code } = errors

        if (code === 0 && result !== null) {
          let dataGoodsCategory = {}

          for( let i = 0; i < result.length; i++ ) {
            const { id, value } = result[i]

            _.merge( dataGoodsCategory, { [i]: { id, value } })
          }

          const orderGoodsCategory = Object.keys(dataGoodsCategory)

          return {
            orderGoodsCategory,
            dataGoodsCategory
          }
        }
      }

      return { loading }
    }
    case queryNameMap.PARAMETER_LIST: {
      const { parameterList = {}, loading } = data || {}

      if (parameterList && parameterList.errors && !parameterList.errors.length) {
        const { errors, result = {} } = parameterList
        const { code } = errors

        if (code === 0) {
          const dataParameter = result.reduce((dataParameter, item) => {
            const { id, value } = item

            return { ...dataParameter, [id.toString()]: { id: id, value } }
          }, {})

          const orderParameter = Object.keys(dataParameter)

          return { orderParameter, dataParameter, loading }
        }
      }

      return { loading }
    }
    case queryNameMap.PERIOD_LIST: {
      const { periodList = {}, loading } = data || {}

      if (periodList && periodList.errors && !periodList.errors.length) {
        const { errors, result } = periodList
        const { code } = errors

        if (code === 0) {
          const dataPeriod = result.reduce((dataPeriod, item) => {
            const { id, value } = item

            return { ...dataPeriod, [id]: { id, value } }
          }, 0)

          const orderPeriod = Object.keys(dataPeriod)

          return { orderPeriod, dataPeriod }
        }
      }

      return { loading }
    }
    case queryNameMap.POINT_FORMAT_LIST: {
      const { pointFormatList = {}, loading } = data || {}

      if (pointFormatList && pointFormatList.errors && !pointFormatList.errors.length) {
        const { errors, result } = pointFormatList
        const { code } = errors

        if (code === 0) {
          const dataPointFormat = result.reduce((dataPointFormat, item) => {
            const { id, value } = item

            return { ...dataPointFormat, [id]: { id, value } }
          }, {})

          const orderPointFormat = Object.keys(dataPointFormat)

          return { orderPointFormat, dataPointFormat }
        }
      }

      return { loading }
    }
    case queryNameMap.POINT_LIST: {
      const { pointList = {}, loading } = data || {}

      if (pointList && pointList.errors && !pointList.errors.length) {
        const { errors, result } = pointList
        const { code } = errors

        if (code === 0) {
          const dataPoint = result.reduce((dataPoint, item) => {
            const { id, value } = item

            return { ...dataPoint, [id]: { id, value } }
          }, {})

          const orderPoint = Object.keys(dataPoint)

          return { orderPoint, dataPoint }
        }
      }

      return { loading }
    }
    case queryNameMap.POINT_GROUP_LIST: {
      const { pointGroupList = {}, loading } = data || {}

      if (pointGroupList && pointGroupList.errors && !pointGroupList.errors.length) {
        const { errors, result = [] } = pointGroupList
        const { code } = errors

        if (code === 0 && result !== null) {
          const dataPointGroup = result.reduce((dataPointGroup, item) => {
            const { id, value } = item

            return { ...dataPointGroup, [id]: { id, value } }
          }, {})

          const orderPointGroup = Object.keys(dataPointGroup)

          return { orderPointGroup, dataPointGroup }
        }
      }

      return { loading }
    }
    default: return {}
  }
}
