import { graphql }          from 'react-apollo'


import config               from 'app/menu/config'

import generateQuery        from 'engine/graphql/generate/create/query'


export default (payload) => {
  const { options } = payload || {}

  const { queryMap, queryNameMap } = config

  const query = generateQuery({ queryMap, name: queryNameMap.PERIOD_NEXT })

  return graphql(query, {
    options: (ownProps) => ({ ...options, ...(ownProps.options || {}) }),
    skip: (ownProps) => {
        const { variables } = ownProps.options || {}

        const { dateBegin, dateEnd } = variables || {}

        return !dateBegin
      },
    props: ({ data }) => {
        const { periodNext = {} } = data || {}

        if (periodNext && periodNext.errors && !periodNext.errors.length) {
          const { errors, result } = periodNext
          const { code, message } = errors

          if (result !== null) {
            const { dateBegin, dateEnd, canPeriodPrev, canPeriodNext } = result

            return { dateBegin, dateEnd, canPeriodPrev, canPeriodNext }
          }

          return { errors }
        }
      }
    }
  )
}
