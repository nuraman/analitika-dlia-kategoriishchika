import { graphql }          from 'react-apollo'


import config               from 'app/menu/config'

import generateQuery        from 'engine/graphql/generate/create/query'


export default (payload) => {
  const { options } = payload || {}

  const { queryMap, queryNameMap } = config

  const query = generateQuery({ queryMap, name: queryNameMap.PERIOD_LAST })

  return graphql(query, {
    options: (ownProps) => ({ ...options, ...(ownProps.options || {}) }),
    skip: (ownProps) => {
      const { options } = ownProps || {}
      const { variables } = options || {}

      const { period } = variables || {}

      return !period
    },
    props: ({ data }) => {
      const { periodLast = {}, loading } = data || {}

      if (periodLast && periodLast.errors && !periodLast.errors.length && loading === false) {
        const { result } = periodLast //  { errors } = periodList

        const { dateBegin, dateEnd, canPeriodPrev, canPeriodNext } = result || {}

        return { dateBegin, dateEnd, canPeriodPrev, canPeriodNext, loading }
      }

      return { loading }
    }
  })
}
