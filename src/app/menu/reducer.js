import _update              from 'immutability-helper'


const name = 'menu'

const types = {
  SET_LOAD_SETTING: `${ name }/SET_LOAD_SETTING`,
  SET_SETTING:      `${ name }/SET_SETTING`,
  SET_SELECTED:     `${ name }/SET_SELECTED`,
  SET_PERIOD_DATE:  `${ name }/SET_PERIOD_DATE`,
  SET_PARAMETER:    `${ name }/SET_PATAMETER`,
  SET_PERIOD:       `${ name }/SET_PERIOD`,
  SET_CATEGORY:     `${ name }/SET_CATEGORY`,
  SET_POINT_FORMAT: `${ name }/SET_POINT_FORMAT`,
  RESET_MENU:       `${ name }/RESET_MENU`,
}

const STATE = {
  period: {
    id:             0,
    dateBegin:      0,
    dateEnd:        0,
    canPeriodPrev:  0,
    canPeriodNext:  0,
    value:          0,
  },
  parameter:        { id: '', value: '' },
  pointFormat:      { id: '', value: '' },
  category:         { id: '', value: '' },
  providerList:     '',

  selected:         false,
}

const reducer = (state = STATE, action) => {
  const { type, payload } = action
  const { category, parameter, period, pointFormat, selected } = state

  switch (type) {
    case types.SET_LOAD_SETTING: {
      const {
        categoryId = {},
        dateBegin,
        dateEnd,
        periodId = {},
        parameterId = {},
        pointFormatId,
        providerList,
        selectedStatus
      } = payload

      const newPeriod = _update(period, {
        id:         { $set: periodId },
        dateBegin:  { $set: dateBegin },
        dateEnd:    { $set: dateEnd },
      })

      const newParameter    = _update(parameter,    { id: { $set: parameterId } })
      const newCategory     = _update(category,     { id: { $set: categoryId } })
      const newPointFormat  = _update(pointFormat,  { id: { $set: pointFormatId } })
      const newSelected     = _update(selected,     { $set: selectedStatus })

      return {
        ...state,
        category:     newCategory,
        period:       newPeriod,
        parameter:    newParameter,
        pointFormat:  newPointFormat,
        providerList: providerList,
        selected:     newSelected,
      }
    }
    case types.SET_SETTING: {
      const {
        newCategory,
        newParameter,
        newPeriod,
        newPointFormat,
        newProviderList,

        selected
      } = payload

      return {
        ...state,
        category:     _update(category, { id: { $set: newCategory.id }, value: { $set: newCategory.value } }),
        parameter:    _update(parameter, { id: { $set: newParameter.id }, value: { $set: newParameter.value } }),
        period:       _update(period, {
          id: { $set: newPeriod.id },
          value: { $set: newPeriod.value },
        }),
        pointFormat:  _update(pointFormat, { id: { $set: newPointFormat.id }, value: { $set: newPointFormat.value } }),
        providerList: newProviderList,
        selected:     selected
      }

    }
    case types.SET_SELECTED: {
      const { value } = payload
      const newSelected = _update(selected, { $set: value })

      return {
        ...state,
        selected: newSelected,
      }
    }
    case types.SET_PERIOD_DATE: {
      const { dateBegin, dateEnd, canPeriodNext = '', canPeriodPrev = ''} = payload

      const newPeriod = _update(period, {
        dateBegin: { $set: dateBegin },
        dateEnd: { $set: dateEnd },
        canPeriodNext: { $set: canPeriodNext },
        canPeriodPrev: { $set: canPeriodPrev },
      })

      return {
        ...state,
        period: newPeriod
      }
    }
    case types.SET_CATEGORY: {
      const { id, value } = payload

      const newCategory = _update(category, { id: { $set: id }, value: { $set: value } })

      return {
        ...state,
        category: newCategory,
        providerList: '',
      }
    }
    case types.SET_PARAMETER: {
      const { id, value } = payload

      const newParameter = _update(parameter, { id: { $set: id }, value: { $set: value } })

      return {
        ...state,
        parameter: newParameter,
      }
    }
    case types.RESET_MENU: {
      return {
        ...state,
        category:     _update(category, { id: { $set: '' }, value: { $set: '' } }),
        parameter:    _update(parameter, { id: { $set: '' }, value: { $set: '' } }),
        period:       _update(period, { id: { $set: '' }, value: { $set: '' } }),
        pointFormat:  _update(pointFormat, { id: { $set: '' }, value: { $set: '' } }),
        providerList: '',
        selected:     false
      }
    }
    default: return {
        ...state
      }
  }
}

export default {
  types: types,
  reducer: reducer
}

