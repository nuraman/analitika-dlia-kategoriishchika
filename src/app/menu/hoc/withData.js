import React, { Component } from 'react'

import { connect }          from 'react-redux'

import { compose }          from 'redux'


import withParam            from 'app/menu/graphql/withMenuList'


export default (WrappedComponent, payload) => {
  class DataList extends Component {
    render() {
      return (<WrappedComponent { ...this.props } />)
    }
  }

  function mapStateToProps(state) {
    const { menu, user } = state

    const { parameter: { id } } = menu

    const { userLogin: { order } } = user

    const isAuth = order.length !== 0

    return {
      selectedId: id === null ? '' : id,
      options: {
        isAuth,
      }
    }
  }

  const { queryName } = payload

  return compose(
    connect(mapStateToProps),
    withParam({ queryName })
  )(DataList)
}

