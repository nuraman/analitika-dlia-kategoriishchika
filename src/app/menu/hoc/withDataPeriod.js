import React                from 'react'


import { connect }          from 'react-redux'
import { compose }          from 'redux'

import withPeriodLast       from 'app/menu/graphql/withPeriodLast'


function withDataPeriod(WrappedComponent) {
  function Period(props) {
    return (<WrappedComponent { ...props } />)
  }

  function mapStateToProps(state) {
    const { user, menu } = state
    const { userLogin: { order } } = user
    const isAuth = order.length > 0

    const { selected, period } = menu

    const { dateBegin, dateEnd } = period

    return {
      isAuth,
      selected,
      currentDateBegin: dateBegin,
      currentDateEnd: dateEnd,
      options: {
        variables: {
          period: period.id,
        }
      },
    }
  }

  return compose(
    connect(mapStateToProps),
    withPeriodLast(),
  )(Period)
}

export default withDataPeriod
