import PropTypes            from 'prop-types'

import React, { Component } from 'react'

import styled               from 'styled-components'

import CircularProgressBar  from 'app/analysis/web/CircularProgressBar'

import withData             from 'app/menu/hoc/withData'

import config               from 'app/menu/config'


const DivContainer = styled.div`
  margin: 0 -20px 0 -70px;
  background-color: #383838;
  max-height: 200px;
  overflow: auto;
  
  &::-webkit-scrollbar
  {
    width: 9px;
  }

  &::-webkit-scrollbar-track
  {
    background: #73c7c5;
    background-clip: content-box;
  }

  &::-webkit-scrollbar-thumb
  {
    background: #484848;
    height: 35px;
    border-radius: 5px solid #484848;
    border: 1px solid #484848;
  }
`

const DivSelectOption = styled.div`
  align-items: center;
  box-sizing: border-box;
  display: flex;
 
  padding: 8px 20px 8px 70px;
  
  color: #fff;
  font-family: HelveticaRegular;
  font-size: 18px;

  &:hover {
    color: #b3fffb;
    background-color: #314545;
  }
`

const DivInputCheck = styled.input`
  display: none;
  
  &:checked {
    display: block; 
  }
`

const LabelNewCheck = styled.label`
  position: relative;
  width: 18px;
  min-width: 18px;
  height: 18px;
  margin-right: 10px;
  border: 1px solid #48b9bf;
  cursor: pointer;
  
  &:before {
    display: ${ props=>props.checked ? 'block' : 'none' };
    content: "";
    position: absolute;
    width: 12px;
    height: 12px;
    top: 3px;
    left: 3px;
    background-color: #48b9bf;
  }
`

const Label = styled.label`
  margin-bottom: -2px;
  font-weight: 300;
  font-size: 18px;
  font-family: HelveticaRegular;
  color: #b3fffb;
  cursor: pointer;
`

class PointFormatList extends Component {
  static propTypes = {
    dataPointFormat: PropTypes.object,
    orderPointFormat: PropTypes.array,
    pointSelectedList: PropTypes.array,
  }

  constructor() {
    super()

    this.state = {
      checkedList: [],
      dataPointFormatList: {},
      orderPointFormatList: [],
    }
  }

  onChange = ({ id, value }) => {
    const { onSelect } = this.props

    const { checkedList } = this.state

    onSelect && onSelect({ id, value })

    let index = checkedList.indexOf(id)

    if (index === -1) {
      checkedList.push(id)
    } else {
      checkedList.splice(index, 1)
    }

    this.setState({ checkedList })
  }

  componentWillReceiveProps(nextProps) {
    const { dataPointFormat = {}, orderPointFormat = [], pointSelectedList = [] } = nextProps

    const checkedList = []

    if (pointSelectedList.length > 0) {
      //проверяем id на 0 - все форматы
      let isAllPoint = false

      pointSelectedList.forEach((item) => {
        const { id } = item

        if (id == 0) isAllPoint = true
      })

      if (isAllPoint) {
        orderPointFormat.forEach((item) => {
          const { id } = dataPointFormat[item]

          checkedList.push(id.toString())
        })
      } else {
        pointSelectedList.forEach((item) => {
          const {id} = item

          checkedList.push(id)
        })
      }
    }

    this.setState({ dataPointFormat, orderPointFormat, checkedList })
  }

  render() {
    const { dataPointFormat = {}, orderPointFormat = [], checkedList } = this.state

    const { loading } = this.props

    if (loading === true) {
      return (
        <CircularProgressBar/>
      )
    }

    return (
      <DivContainer>
        {
          orderPointFormat.map((item) => {
            const { id, value = 0 } = dataPointFormat[item]

            const checked = checkedList.indexOf(id.toString()) !== -1

            return (
              <DivSelectOption
                key={ id }
                onClick={ () => this.onChange({ id: id.toString(), value }) }
              >
                <DivInputCheck type='checkbox' name='' value='' />
                <LabelNewCheck
                  checked = { checked }
                />
                <Label> { value } </Label>
              </DivSelectOption>
            )
          })
        }
      </DivContainer>
     )
  }
}

export default withData(PointFormatList, { queryName: config.queryNameMap.POINT_FORMAT_LIST })


