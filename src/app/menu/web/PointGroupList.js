import PropTypes            from 'prop-types'

import React, { Component } from 'react'


import CircularProgressBar  from 'app/analysis/web/CircularProgressBar'
import CustomAutoSuggest    from 'app/menu/web/AutoSuggestInput'

import config               from 'app/menu/config'
import withData             from 'app/menu/hoc/withData'


class PointGroupList extends Component {
  static propTypes = {
    data: PropTypes.object,
    order: PropTypes.array
  }

  constructor() {
    super()

    this.state = {
      selectedValue: '',
      dataList: [],
    }
  }

  onSelectCallback = ({ id, value }) => {
    const { onSelect } = this.props

    if (id !== undefined || id !== null) {
      onSelect && onSelect({ id, value })
    }

    this.setState({ selectedValue: value })
  }

  componentWillReceiveProps(nextProps) {
    const { selectedValue } = nextProps

    this.setState({ selectedValue })
  }

  componentWillMount() {
    const { dataPointGroup = {}, orderPointGroup = [], selectedValue } = this.props

    if (orderPointGroup.length === 0) return null

    const dataList = orderPointGroup.reduce((dataList, item) => {
      const { id, value } = dataPointGroup[item] || {}

      return [ ...dataList, { name: value, id } ]
    }, [])

    this.setState({ dataList, selectedValue })
  }

  render() {
    const { dataList = [], selectedValue } = this.state

    if (dataList.length === 0) {
      return (
        <CircularProgressBar/>
      )
    }

    return (
      <CustomAutoSuggest
        pointList={ dataList }
        selectedValue={ selectedValue }

        onSelect={ this.onSelectCallback }
      />
    )
  }
}

export default withData(PointGroupList, { queryName: config.queryNameMap.POINT_GROUP_LIST })


