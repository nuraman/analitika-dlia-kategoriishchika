import _                    from 'lodash'

import React, { Component } from 'react'

import styled               from 'styled-components'


const Container = styled.div`
  overflow: hidden;
  border: 1px solid #7d7d7d;

  &.in-popup {
    height: 150px;
  }
  
  input::-webkit-input-placeholder {
    opacity: 1;
    font-weight: 300;
    font-size: 14px;
    color: #fff;
    transition: opacity 0.2s ease;
  }
   input::-moz-placeholder {
    opacity: 1;
    font-weight: 300;
    font-size: 14px;
    color: #fff;
    transition: opacity 0.2s ease;
  }
  
  input:-moz-placeholder {
    opacity: 1;
    font-weight: 300;
    font-size: 14px;
    color: #fff;
    transition: opacity 0.2s ease;
  }
  
  input:-ms-input-placeholder {
    opacity: 1;
    font-weight: 300;
    font-size: 14px;
    color: #fff;
    transition: opacity 0.2s ease;
  }
   
  input:focus::-webkit-input-placeholder {
    opacity: 0;
    transition: opacity 0.2s ease;
  }
  
  input:focus::-moz-placeholder {
    opacity: 0;
    transition: opacity 0.2s ease;
  }
  
  input:focus:-moz-placeholder {
    opacity: 0;
    transition: opacity 0.2s ease;
  }
  
  input:focus:-ms-input-placeholder {
    opacity: 0;
    transition: opacity 0.2s ease;
  }
`

const Header = styled.div`
  display: flex;
  align-items: center;
  border-bottom: 1px solid #7d7d7d;
`

const InputSearch = styled.input`
  margin-top: 1px;
  box-sizing: border-box;
  padding: 10px;
  width: 100%;
  height: 40px;
  background-color: transparent;
  border: none;
  outline: none;
  color: #fff;
  font-family: HelveticaRegular;
  font-size: 14px;
`

const Body = styled.div`
  overflow: auto;

  &.in-popup {
    height: 111px;
  }
  
  &::-webkit-scrollbar
  {
    width: 9px;
  }
  
  &::-webkit-scrollbar-track
  {
    background: #7d7d7d;
    border: 3px solid transparent;
    background-clip: content-box;
  }
  
  &::-webkit-scrollbar-thumb
  {
    background: #73c7c5;
    height: 35px;
    border-radius: 5px solid #73c7c5;
    border: 1px solid #73c7c5;
  }
`

const DivContent = styled.div`
  display: block;
  overflow: auto;
  &::-webkit-scrollbar
  {
    width: 9px;
  }
  
  &::-webkit-scrollbar-track
  {
    background: #7d7d7d;
    border: 3px solid transparent;
    background-clip: content-box;
  }
  
  &::-webkit-scrollbar-thumb
  {
    background: #73c7c5;
    height: 35px;
    border-radius: 5px solid #73c7c5;
    border: 1px solid #73c7c5;
  }
`

const DivSelectWrapp = styled.div`
  display: flex;
  flex-wrap: wrap;
`

const DivSelectOption = styled.div`
  display: flex;
  align-items: center;
  padding: 5px;
  font-size: 14px;
  color: #fff;
  font-family: HelveticaRegular;
`

const DivInputCheck = styled.input`
  display: none;
  
  &:checked {
    display: block; 
  }
`

const LabelNewCheck = styled.label`
  position: relative;
  width: 18px;
  min-width: 18px;
  height: 18px;
  margin-right: 10px;
  border: 1px solid #48b9bf;
  cursor: pointer;
  
  &:before {
    display: ${ props=>props.checked ? 'block' : 'none' };
    content: "";
    position: absolute;
    width: 12px;
    height: 12px;
    top: 3px;
    left: 3px;
    background-color: #48b9bf;
  }
`

const Label = styled.label`
  font-weight: 300;
  font-size: 14px;
  font-family: HelveticaRegular;
  color: #b3fffb;
  cursor: pointer;
`

class AutoSuggestInput extends Component {
  constructor() {
    super()

    this.state = {
      suggestionList: [],
      isCheckAllPoint: false,

      dataList: [],
      pointCheckedList: [] //{ id, name }
    }
  }

  getIndexPointById = ({ pointCheckedList = [], id }) => {
    let index = -1

    pointCheckedList.forEach((item, idx) => {
      if (item.id === id) index = idx
    })

    return index
  }

  onChangeInputCheck = ({ id, value }) => {
    const { onSelect } = this.props

    const { pointCheckedList } = this.state

    let index = this.getIndexPointById({ pointCheckedList, id })

    if (index === -1) {
      pointCheckedList.push({ id, value })
    } else {
      pointCheckedList.splice(index, 1)
    }

    onSelect && onSelect(pointCheckedList)

    this.setState({ pointCheckedList })
  }

  onChangeAllPoint = () => {
    const { isCheckAllPoint, dataList, pointCheckedList } = this.state
    const { onSelect } = this.props

    if (isCheckAllPoint === false) {
      onSelect(dataList)

      return this.setState({ pointCheckedList: dataList, isCheckAllPoint: !isCheckAllPoint })
    }

    if (isCheckAllPoint === true && pointCheckedList.length > 0) {
      onSelect([])

      return this.setState({ pointCheckedList: [], isCheckAllPoint: !isCheckAllPoint })
    }
  }

  onChangePoint = ({ currentTarget: { value } }) => {
    const { dataList } = this.state

    const suggestionList = this.onGetSuggestions(value, dataList)

    this.setState({ pointName: value, suggestionList })
  }

  onGetSuggestions = (pointName, dataList) => {
    const inputValue = pointName.trim().toLowerCase()
    const inputLength = pointName.length

    return inputLength === 0 ? dataList : dataList.filter(x => this.onIsContain(x.value, inputValue))
  }

  onIsContain = (name, inputValue) => {
    const inputList = _.words(inputValue)

    const matchList = inputList.reduce((matchList, item) => {
      if (name.toLowerCase().includes(item)) matchList.push(item)

      return matchList
    }, [])

    if ( matchList.length >= inputList.length ) return name

    return null
  }

  componentWillReceiveProps(nextProps) {
    const { dataList = [], pointSelectedList = [] } = nextProps
    let pointCheckedList = []

    if (pointSelectedList.length > 0) {
      pointCheckedList = pointSelectedList.reduce((pointCheckedList, item) => {
        const { id, value } = item

        pointCheckedList.push({ id, value })

        return pointCheckedList
      }, [])
    }

    const isCheckAllPoint = dataList.length === pointSelectedList.length && pointSelectedList.length > 0

    this.setState({
      dataList,
      suggestionList: dataList,
      pointCheckedList,
      isCheckAllPoint
    })
  }

  componentWillMount() {
    const { dataList, pointSelectedList = [] } = this.props

    let pointCheckedList = []

    if (pointSelectedList.length > 0) {
      pointCheckedList = pointSelectedList.reduce((pointCheckedList, item) => {
        const { id, value } = item

        pointCheckedList.push({ id, value })

        return pointCheckedList
      }, [])
    }

    const isCheckAllPoint = dataList.length === pointSelectedList.length && pointSelectedList.length > 0

    this.setState({
      dataList,
      suggestionList: dataList,
      pointCheckedList,
      isCheckAllPoint
    })
  }

  render() {
    const { suggestionList, pointName, pointCheckedList, isCheckAllPoint } = this.state

    return(
      <Container className='in-popup'>
        <Header>
          <DivSelectOption>
            <DivInputCheck type='checkbox' name='' value='' />
            <LabelNewCheck
              checked={ isCheckAllPoint }
              onClick={ this.onChangeAllPoint }
            />
            <Label
              onClick={ this.onChangeAllPoint }
            >
              Все
            </Label>
          </DivSelectOption>
          <InputSearch
            type='text'
            name=''
            id=''
            placeholder='Введите адрес'
            value={ pointName }

            onChange={ this.onChangePoint }
          />
        </Header>
        <Body className='in-popup'>
          <DivContent>
            <DivSelectWrapp>
              {
                suggestionList.map((item) => {
                  const { value, id } = item

                  let checked = this.getIndexPointById({ pointCheckedList, id }) !== -1

                  return (
                    <DivSelectOption key={ id }>
                      <DivInputCheck type='checkbox' name='' value='' />
                      <LabelNewCheck
                        checked={ checked }
                        onClick={ () => this.onChangeInputCheck({ id: id.toString(), value }) }
                      />
                      <Label
                        onClick={ () => this.onChangeInputCheck({ id: id.toString(), value }) }
                      >
                        { value }
                      </Label>
                    </DivSelectOption>
                  )
                })
              }
            </DivSelectWrapp>
          </DivContent>
        </Body>
      </Container>
    )
  }
}

export default AutoSuggestInput
