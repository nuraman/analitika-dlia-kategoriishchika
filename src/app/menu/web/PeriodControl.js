import moment               from 'moment'

import React, { Component } from 'react'
import { compose }          from 'redux'

import styled               from 'styled-components'


import reducer              from 'app/menu/reducer'

import withDataPeriod       from 'app/menu/hoc/withDataPeriod'


const Container = styled.div`
  flex-grow: 1;
  font-size: 18px;
  font-family: HelveticaLight;
  color: #fff;
  text-overflow: ellipsis;
  white-space: nowrap;
  
  @media(max-width: 1400px) {
    font-size: 16px;
  }
  @media(max-width: 1200px) {
    font-size: 14px;
  }
  @media(max-width: 900px) {
    font-size: 12px;
  }
`

class PeriodControl extends Component {
  constructor() {
    super()

    this.state = {
      dateBegin: '',
      dateEnd: '',
      isPeriodNext: false,
      isPeriodPrev: false,
    }
  }

  componentWillReceiveProps(nextProps) {
    const { dispatch, dateBegin, dateEnd, canPeriodNext, canPeriodPrev, currentDateBegin = '', currentDateEnd = ''} = nextProps

    if (nextProps.dateBegin !== this.props.dateBegin && nextProps.dateEnd !== this.props.dateEnd) {
      dispatch({
        type: reducer.types.SET_PERIOD_DATE,
        payload: { dateBegin, dateEnd, canPeriodNext, canPeriodPrev }
      })

      this.setState({
        dateBegin: moment(dateBegin, 'DD.MM.YYYY').format('DD/MM/YYYY'),
        dateEnd: moment(dateEnd, 'DD.MM.YYYY').format('DD/MM/YYYY')
      })
    }

    if (currentDateBegin && currentDateEnd) {
      return this.setState({
        dateBegin: moment(currentDateBegin, 'DD.MM.YYYY').format('DD/MM/YYYY'),
        dateEnd: moment(currentDateEnd, 'DD.MM.YYYY').format('DD/MM/YYYY')
      })
    }
  }

  render() {
    const { isAuth } = this.props

    if (isAuth === false) return null

    const { dateBegin, dateEnd } = this.state

    if (!dateBegin && !dateEnd) return null

    const datePeriodText=`c ${ dateBegin } по ${ dateEnd }`

    return (
      <Container>
        { datePeriodText }
      </Container>
    )
  }
}

export default compose(
  withDataPeriod
)(PeriodControl)
