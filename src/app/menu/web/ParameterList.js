import PropTypes            from 'prop-types'

import React, { Component } from 'react'


import EntitySelect         from 'app/analysis/web/EntitySelect'

import withData             from 'app/menu/hoc/withData'

import config               from 'app/menu/config'


class ParameterList extends Component {
  static propTypes = {
    data: PropTypes.object,
    order: PropTypes.array
  }

  onChange = ({ id, value }) => {
    const { onSelect } = this.props

    onSelect && onSelect({ id, value })
  }

  render() {
    const { dataParameter, orderParameter = [], selectedId, } = this.props

    if (orderParameter.length === 0) return null

    return (
      <EntitySelect
        data={ dataParameter }
        order={ orderParameter }
        value={ selectedId.toString() }

        onSelect={ this.onChange }
      />
    )
  }
}


export default withData(ParameterList, { queryName: config.queryNameMap.PARAMETER_LIST })



