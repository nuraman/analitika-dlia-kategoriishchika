import React, { Component } from 'react'


import EntitySelect         from 'app/analysis/web/EntitySelect'

import CircularProgressBar  from 'app/analysis/web/CircularProgressBar'

import config               from 'app/menu/config'

import withData             from 'app/menu/hoc/withData'


function PeriodList(props) {
  const { onSelect, dataPeriod, orderPeriod, id } = props

  const onChange = ({ id, value }) => onSelect && onSelect({ id, value })

  if (!orderPeriod.length) {
    return (
      <CircularProgressBar/>
    )
  }

  return (
    <EntitySelect
      data={ dataPeriod }
      order={ orderPeriod }
      value={ id }

      onSelect={ onChange }
    />
  )
}

export default withData(PeriodList, { queryName: config.queryNameMap.PERIOD_LIST })
