import PropTypes            from 'prop-types'

import React, { Component } from 'react'


import AutosuggestInput     from 'app/menu/web/AutoSuggestInput'
import CircularProgressBar  from 'app/analysis/web/CircularProgressBar'


class PointList extends Component {
  static propTypes = {
    data: PropTypes.object,
    order: PropTypes.array
  }

  constructor() {
    super()

    this.state = {
      dataList: [],
      pointList: [],
    }
  }

  onSelect = (pointCheckedList) => {
    const { onSelect } = this.props

    onSelect(pointCheckedList)

  }

  componentWillReceiveProps(nextProps) {
    const { data = {}, order = [], pointList } = nextProps

    if (order.length === 0) return null

    const dataList = order.reduce((dataList, item) => {
      const { id, value } = data[item] || {}

      return [ ...dataList, { value, id: id.toString() } ]
    }, [])

    this.setState({ dataList, pointList })
  }


  componentWillMount() {
    const { data = {}, order = [], pointList } = this.props

    if (order.length === 0) return null

    const dataList = order.reduce((dataList, item) => {
      const { id, value } = data[item] || {}

      return [ ...dataList, { value, id: id.toString() } ]
    }, [])

    this.setState({ dataList, pointList })
  }

  render() {
    const { dataList = [], pointList } = this.state

    if (dataList.length === 0) {
      return (
        <CircularProgressBar/>
      )
    }

    return (
      <AutosuggestInput
        dataList={ dataList }
        pointSelectedList={ pointList }

        onSelect={ this.onSelect }
      />
    )
  }
}

export default PointList

