import React, { Component } from 'react'


import EntitySelect         from 'app/analysis/web/EntitySelect'

//import EntitySelect         from 'app/analysis/web/EntitySelectMultiple'

import reducerProvider      from 'app/page/web/Aside/Provider/reducer'

import config               from 'app/menu/config'

import withData             from 'app/menu/hoc/withData'


class GoodsCategoryList extends Component {
  constructor(props) {
    super(props)

    const { selectedValue } = this.props

    this.state = {
      selectedValue
    }
  }

  onChange = ({ id, value }) => {
    const { onSelect, dispatch } = this.props

    this.setState({ open: false, selectedValue: value })

    onSelect && onSelect({ id, value })

    dispatch({
      type: reducerProvider.types.SET_CATEGORY,
      payload: { id },
    })
  }

  render() {
    const { selectedValue } = this.state
    const { dataGoodsCategory = {}, orderGoodsCategory = [], } = this.props || {}

    if (orderGoodsCategory.length === 0) return null

    return (
      <EntitySelect
        data={ dataGoodsCategory }
        order={ orderGoodsCategory }
        value={ selectedValue }

        onSelect={ this.onChange }
      />
    )
  }
}

export default withData( GoodsCategoryList, { queryName: config.queryNameMap.GOODS_CATEGORY_LIST })
