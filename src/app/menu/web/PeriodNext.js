import React, { Component } from 'react'

import { withApollo }       from 'react-apollo'

import gql                  from 'graphql-tag'

import { connect }          from 'react-redux'
import { compose }          from 'redux'


import styled               from 'styled-components'


import reducer              from 'app/menu/reducer'

import queryPeriodNext      from 'app/menu/graphql/query/periodNextQuery'


const Div = styled.div`
  display: flex;
  align-items: center;
  padding-left: 7px;
  padding-right: 7px;
  color: #b3fffb;
  cursor: pointer;
`

const DivBlockIco = styled.div`
  width: 11px;
  height: 21px;
  margin-top: 15px;
  margin-bottom: 15px;
  fill: #9e9e9e;
  
  background-image: url(assets/periodRight.svg);
  background-size: cover;
`


class PeriodNext extends Component {
  onClick = () => {
    const { dispatch, currPeriodId, currDateBegin, currDateEnd } = this.props

    this.props.client.query({
      query: gql`${ queryPeriodNext() }`,
      variables: {
        period: currPeriodId,
        dateBegin: currDateBegin,
        dateEnd: currDateEnd,
      }
    }).then(((data) => {
      const { periodNext } = data.data

      const { errors = {}, result } = periodNext

      const { code } = errors

      if (code === 0) {
        const { dateBegin, dateEnd, canPeriodPrev, canPeriodNext } = result || {}

        if (dateBegin && dateEnd) {
          dispatch({
            type: reducer.types.SET_PERIOD_DATE,
            payload: { dateBegin, dateEnd, canPeriodPrev, canPeriodNext }
          })
        }
      }
    }))
  }

  render() {
    const {  currDateBegin, currDateEnd } = this.props

    if (!currDateBegin && !currDateEnd) return null

    return (
      <Div
        onClick={ this.onClick }
      >
        <DivBlockIco />
      </Div>
    )
  }
}

const mapStateToProps = (state) => {
  const { menu } = state
  const { period } = menu
  const { id, dateBegin, dateEnd, canPeriodNext } = period

  return {
    currPeriodNext: canPeriodNext,
    currPeriodId: id,
    currDateBegin: dateBegin,
    currDateEnd: dateEnd,
  }
}

const withApolloPeriodNext = withApollo(PeriodNext)

export default compose(
  connect(mapStateToProps),
)(withApolloPeriodNext)
