import { withApollo }       from 'react-apollo'
import gql                  from 'graphql-tag'

import React, { Component } from 'react'

import { connect }          from 'react-redux'
import { compose }          from 'redux'

import styled               from 'styled-components'


import queryPeriodPrev      from 'app/menu/graphql/query/periodPrevQuery'

import reducer              from 'app/menu/reducer'


const Div = styled.div`
  display: flex;
  align-items: center;
  padding-left: 7px;
  padding-right: 7px;
  color: #b3fffb;
  cursor: pointer;
`

const DivBlockIco = styled.div`
  width: 11px;
  height: 21px;
  margin-top: 15px;
  margin-bottom: 15px;
  fill: #9e9e9e;
  
  background-image: url(assets/periodLeft.svg);
  background-size: cover;
`


class PeriodPrev extends Component {
  onClick = () => {
    const { dispatch, currDateBegin, currDateEnd, currPeriodId } = this.props

    this.props.client.query({
      query: gql`${queryPeriodPrev()}`,
      variables: {
        period: currPeriodId,
        dateBegin: currDateBegin,
        dateEnd: currDateEnd,
      }
    }).then(((data) => {
      const { periodPrev } = data.data

      const { errors = {}, result } = periodPrev || {}
      if (errors !== null) {
        const {code} = errors

        if (code === 0) {
          const {dateBegin, dateEnd, canPeriodPrev, canPeriodNext} = result || {}

          if (dateBegin && dateEnd) {
            dispatch({
              type: reducer.types.SET_PERIOD_DATE,
              payload: {dateBegin, dateEnd, canPeriodPrev, canPeriodNext}
            })
          }
        }
      }
    }))
  }

  render() {
    const { currDateEnd, currDateBegin } = this.props

    if(!currDateBegin && !currDateEnd) return null

    return (
      <Div
        onClick={ this.onClick }
      >
        <DivBlockIco />
      </Div>
    )
  }
}

const mapStateToProps = (state) => {
  const { menu } = state
  const { period } = menu
  const { id, dateBegin, dateEnd, canPeriodPrev } = period

  return {
    currCanPeriodPrev: canPeriodPrev,
    currPeriodId: id,
    currDateBegin: dateBegin,
    currDateEnd: dateEnd,
  }
}

const withApolloPeriodPrev = withApollo(PeriodPrev)

export default compose(
  connect(mapStateToProps),
)(withApolloPeriodPrev)
