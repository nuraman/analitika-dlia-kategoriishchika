import React, { Component } from 'react'

import { connect }          from 'react-redux'
import { compose }          from 'react-apollo'

import EntitySelect         from 'app/analysis/web/EntitySelectCheckBox'

import CircularProgressBar  from 'app/analysis/web/CircularProgressBar'

import withLoadSetting      from 'app/page/graphql/userLoadSetting'


class ProviderList extends Component {
  constructor() {
    super()

    this.state = {
      dataProvider: {},
      orderProvider: [],

      providerSelectedList: [],
    }
  }

  onChange = ({ id }) => {
    const { onSelect } = this.props

    onSelect && onSelect({ id })
  }

  componentWillReceiveProps(nextProps) {
    const { providerSelectedList, dataProvider, orderProvider } = nextProps

    this.setState({ providerSelectedList })
  }

  componentWillMount() {
    const { dataProvider, orderProvider, providerSelectedList } = this.props

    this.setState({ dataProvider, orderProvider, providerSelectedList })
  }

  render() {
    const { dataProvider, orderProvider, providerSelectedList } = this.state

    if (orderProvider && !orderProvider.length) {
      return (
        <CircularProgressBar />
      )
    }

    return (
      <EntitySelect
        data={ dataProvider }
        order={ orderProvider }
        providerSelectedList={ providerSelectedList }

        onSelect={ this.onChange }
      />
    )
  }
}

export default ProviderList
