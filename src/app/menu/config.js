import types                from 'engine/types'

const inputNameMap = {
  DATE_BEGIN:                         'dateBegin',
  DATE_END:                           'dateEnd',
  GOODS_CATEGORY:                     'goodsCategory',
  PERIOD:                             'period',
  POINT_GROUP:                        'pointGroup',
  POINT_FORMAT:                       'pointFormat',
  POINT:                              'point',
  PROVIDER_LIST:                      'providerList',
  TYPE_PARAMETER:                     'typeParameter',
}

const queryNameMap = {
  PERIOD_LIST:                        'periodList',
  PERIOD_LAST:                        'periodLast',
  PERIOD_PREV:                        'periodPrev',
  PERIOD_NEXT:                        'periodNext',
  PARAMETER_LIST:                     'parameterList',
  POINT_FORMAT_LIST:                  'pointFormatList',
  POINT_LIST:                         'pointList',
  POINT_GROUP_LIST:                   'pointGroupList',
  GOODS_CATEGORY_LIST:                'goodsCategorieList',
 }

 const mutationNameMap = {
  USER_SAVE_SETTING:                  'userSaveSettings'
 }

const resultNameMap = {
  ID:                                 'id',
  VALUE:                              'value',
  DATE_BEGIN:                         'dateBegin',
  DATE_END:                           'dateEnd',
  CAN_PERIOD_NEXT:                    'canPeriodNext',
  CAN_PERIOD_PREV:                    'canPeriodPrev',
 }

const queryMap = [
  {
    queryName: queryNameMap.PERIOD_LIST,
    inputList: [],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        }
      ],
      result: [
        {
          name: [resultNameMap.ID],
          type: types.Int,
        },
        {
          name: [resultNameMap.VALUE],
          type: types.String,
        }
      ]
    }
  },

  {
    queryName: queryNameMap.PERIOD_LAST,
    inputList: [
      {
        name: inputNameMap.PERIOD,
        type: types.Int,
      }
    ],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        },
      ],
      result: [
        {
          name: resultNameMap.DATE_BEGIN,
          type: types.String,
        },
        {
          name: resultNameMap.DATE_END,
          type: types.String,
        },
        {
          name: resultNameMap.CAN_PERIOD_PREV,
          type: types.Boolean,
        },
        {
          name: resultNameMap.CAN_PERIOD_NEXT,
          type: types.Boolean,
        }
      ]
    }
  },

  {
    queryName: queryNameMap.PERIOD_PREV,
    inputList: [
      {
        name: inputNameMap.PERIOD,
        type: types.Int
      },
      {
        name: inputNameMap.DATE_BEGIN,
        type: types.String,
      },
      {
        name: inputNameMap.DATE_END,
        type: types.String,
      },
    ],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        },
      ],
      result: [
        {
          name: resultNameMap.DATE_BEGIN,
          type: types.String,
        },
        {
          name: resultNameMap.DATE_END,
          type: types.String,
        },
        {
          name: resultNameMap.CAN_PERIOD_PREV,
          type: types.Boolean,
        },
        {
          name: resultNameMap.CAN_PERIOD_NEXT,
          type: types.Boolean,
        }
      ]
    }
  },

  {
    queryName: queryNameMap.PERIOD_NEXT,
    inputList: [
      {
        name: inputNameMap.PERIOD,
        type: types.Int
      },
      {
        name: inputNameMap.DATE_BEGIN,
        type: types.String,
      },
      {
        name: inputNameMap.DATE_END,
        type: types.String,
      },
    ],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        },
      ],
      result: [
        {
          name: resultNameMap.DATE_BEGIN,
          type: types.String,
        },
        {
          name: resultNameMap.DATE_END,
          type: types.String,
        },
        {
          name: resultNameMap.CAN_PERIOD_PREV,
          type: types.Boolean,
        },
        {
          name: resultNameMap.CAN_PERIOD_NEXT,
          type: types.Boolean,
        }
      ]
    }
  },

  {
    queryName: queryNameMap.GOODS_CATEGORY_LIST,
    inputList: [],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        },
      ],
      result: [
        {
          name: 'id',
          type: types.Int
        },
        {
          name: 'value',
          type: types.String,
        }
      ]
    }
  },

  {
    queryName: queryNameMap.PARAMETER_LIST,
    inputList: [],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        },
      ],
      result: [
        {
          name: 'id',
          type: types.Int,
        },
        {
          name: 'value',
          type: types.String,
        }
      ]
    }
  },

  {
    queryName: queryNameMap.POINT_LIST,
    inputList: [],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        },
      ],
      result: [
        {
          name: resultNameMap.ID,
          type: types.Int,
        },
        {
          name: resultNameMap.VALUE,
          type: types.String,
        }
      ]
    }
  },

  {
    queryName: queryNameMap.POINT_FORMAT_LIST,
    inputList: [],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        },
      ],
      result: [
        {
          name: resultNameMap.ID,
          type: types.String,
        },
        {
          name: resultNameMap.VALUE,
          type: types.String
        }
      ]
    }
  },

  {
    queryName: queryNameMap.POINT_GROUP_LIST,
    inputList: [],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        },
      ],
      result: [
        {
          name: resultNameMap.ID,
          type: types.Int,
        },
        {
          name: resultNameMap.VALUE,
          type: types.String,
        }
      ]
    }
  }
]

const mutationMap = [
  {
    mutationName: mutationNameMap.USER_SAVE_SETTING,
    inputList: [
      {
        name: [inputNameMap.PERIOD],
        type: types.Int,
      },
      {
        name: [inputNameMap.DATE_BEGIN],
        type: types.String,
      },
      {
        name: [inputNameMap.DATE_END],
        type: types.String,
      },
      /*{
        name: [inputNameMap.POINT],
        type: types.String,
      },*/
      {
        name: [inputNameMap.POINT_FORMAT],
        type: types.String,
      },
      /*{
        name: [inputNameMap.POINT_GROUP],
        type: types.String,
      },*/
      {
        name: [inputNameMap.PROVIDER_LIST],
        type: types.String,
      },
      {
        name: [inputNameMap.GOODS_CATEGORY],
        type: types.String,
      },
      {
        name: [inputNameMap.TYPE_PARAMETER],
        type: types.Int,
      }

    ],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        }
      ],
    }
  }
]

export default {
  inputNameMap,
  queryMap,
  queryNameMap,
  mutationMap,
  mutationNameMap,
}
