import React, { Component } from 'react'
import { connect }          from 'react-redux'
import { compose }          from 'redux'


import withDiscountPrepare  from 'app/discount/graphql/withDiscountPrepare'
import withDiscountAppend   from 'app/discount/graphql/withDiscountAppend'


function withData(ComposedComponent) {
  class Widget extends Component {
    render() {
      return (
        <ComposedComponent {...this.props } />
      )
    }
  }

  function mapSateToProps(state) {
    const { menu, discount } = state

    const { period, category, pointFormat } = menu
    const { id } = period

    const pointFormatId = pointFormat.id === ''? null : pointFormat.id

    const { widget, param1, param2 } = discount

    return {
      options: {
        variables: {
          goodsCategory: category.id,
          pointGroup: null,
          pointFormat: pointFormatId,
          point: null,
          period: id,
          vidget: widget,
          param1: param1,
          param2: param2
        }
      }
    }
  }

  return compose(
    connect(mapSateToProps),
    withDiscountPrepare(),
    withDiscountAppend(),
  )(Widget)
}

export default withData
