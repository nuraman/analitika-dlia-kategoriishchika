const name = `discount`

const types = {
  SET_SUCCESS: `${ name }/SET_SUCCESS`
}

const STATE = {
  widget: '',
  param1: '',
  param2: '',
}

const reducer = ( state = STATE, action ) => {
  const { type, payload } = action

  switch(type) {
    case types.SET_SUCCESS: {
      const { widget, param1, param2 } = payload

      return {
        widget,
        param1,
        param2,
      }
    }
    default: {
      return state
    }
  }
}

export default {
  types,
  reducer,
}
