import { graphql }          from 'react-apollo'


import config               from 'app/discount/config'

import
  generateMutationQuery     from 'engine/graphql/generate/create/mutation'


const discountAppend = () => {
  const { mutationMap, mutationName } = config

  const mutationQuery = generateMutationQuery({ mutationMap, name: mutationName })

  return graphql(mutationQuery, {
    props: ({ mutate }) => ({
      [mutationName]: (variables) => mutate({variables})
    })
  })
}

export default discountAppend

