import { graphql }          from 'react-apollo'

import config               from 'app/discount/config'

import queryGenerate        from 'engine/graphql/generate/create/queryDiscount'


const getData = (result) => {
  const data = result.reduce((data, item) => {
    const { id, name } = item

    return { ...data, [id]: { id, name } }
  }, {})

  return data
}


export default (payload) => {
  const { options } = payload || {}

  const { queryMap, name } = config

  const query = queryGenerate({ queryMap, name })

  return graphql(query, {
    options: (ownProps) => ({ ...options, ...(ownProps.options || {}) }),
    skip: (ownProps) => {
      const { options } = ownProps || {}
      const { variables } = options
      const { vidget, param1, param2 } = variables

      if(!vidget && !param1 && !param2) return true

      return false
    },
    props: ({ data }) => {
      const { actionPrepare = {} } = data || {}


      if (actionPrepare && actionPrepare.errors && !actionPrepare.errors.length) {
        const { errors, resultGoods = [], resultPoints = [], resultOther = {} } = actionPrepare
        const { code } = errors

        if (code === 0 && resultGoods !== null) {
          const dataGoods = getData(resultGoods)
          const orderGoods = Object.keys(dataGoods)

          const dataPoint = getData(resultPoints)
          const orderPoint = Object.keys(dataPoint)

          const { countCustomers } = resultOther //Количество участников

          return {
            dataGoods,
            orderGoods,
            dataPoint,
            orderPoint,
            customerCount: countCustomers,
          }
        }
      }
    }
  })
}
