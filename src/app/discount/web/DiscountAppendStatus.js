import React, { Component } from 'react'

import styled               from 'styled-components'


const DivDiscountAppend = styled.div`
 position: absolute;
 display: ${ props=>props.open ? 'block' : 'none' };
 top: 25%;
 left: 50%;
 width: 368px;
 height: 109px;
 background-color: #484848;
`

const DivHeader = styled.div`
  display: table;
  width: 100%;
  height: 30px;
`

const DivRight = styled.div`
  display: table-cell;
  text-align: right;
  vertical-align: middle;
  height: 100%;
`

const DivInline = styled.div`
  display: inline-flex;
  align-items: center;
  justify-content: space-around;
  height: 100%;
`

const DivClose = styled.div`
  box-sizing: border-box;
  
  height: 17px;
  min-width: 30px;
  width: 30px;
  
  cursor: pointer;
  
  fill: #fff;
  
  background-image: url(assets/discount/closeWight.svg);
  background-size: cover;
`

const DivBody = styled.div`
  margin-bottom: 20px;
`

const DivRow = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  height: 30px;
`

const DivTextThank = styled.div`
  font-family: HelveticaLight;
  font-size: 22px;
  color: #fff;
  margin-left: 10px;
`

const DivIcoThank = styled.div`
  min-width: 50px;
  width: 50px;
  height: 32px;
  
  background-image: url(assets/discount/thank.svg);
  background-size: cover;
`

const DivTextStatus = styled.div`
  font-family: HelveticaLight;
  font-size: 14px;
  color: #fff;
  padding-left: 60px;
`


class DiscountAppendStatus extends Component {
  constructor() {
    super()

    this.state = {
      open: false,
      code: ''
    }
  }

  componentWillReceiveProps(nextProps) {
    const { open, code, onClose } = nextProps

    this.setState({ open, code })

    if (open === true) {
      setTimeout(() => {
        this.setState({ open: !open })
        onClose()
      }, 3000)
    }
  }

  onHandleClick = () => {
    this.setState({ open: !this.state.open })

    const { onClose } = this.props

    onClose()
  }

  render() {
    const { open, code } = this.state
    let message = ''

    if (code === 0) {
      message = 'Заявка на акцию успешно отправлена'
    }

    if (code === 1) {
      message = 'Ошибка при оформлении акции'
    }

    if (code === 6) return null

    return (
      <DivDiscountAppend open={ open }>
        <DivHeader>
          <DivRight>
            <DivInline>
              <DivClose
                onClick={ this.onHandleClick }
              />
            </DivInline>
          </DivRight>
        </DivHeader>
        <DivBody>
          <DivRow>
            <DivIcoThank />
            <DivTextThank>
              Спасибо
            </DivTextThank>
          </DivRow>
          <DivRow>
            <DivTextStatus>
              { message }
            </DivTextStatus>
          </DivRow>
        </DivBody>
      </DivDiscountAppend>
    )
  }
}

export default DiscountAppendStatus
