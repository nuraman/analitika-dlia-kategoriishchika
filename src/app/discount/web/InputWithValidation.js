import React                from 'react'

import styled               from 'styled-components'

import Formsy               from 'formsy-react'


const PgaBlockInput = styled.input`
  box-sizing: border-box;
  width: 100%;
  padding: 10px 30px;
  border: 1px solid #7d7d7d;
  background-color: transparent;
  outline: none;
  font-family: HelveticaRegular;
  font-size: 14px;
  color: #fff;
`

export default  React.createClass({
  mixins: [Formsy.Mixin],

  changeValue(event) {
    this.setValue(event.currentTarget.value);
  },

  componentWillReceiveProps(nextProps) {
    const { value = '' } = nextProps

    this.setState({ value })
  },

  render() {
    const className = this.showRequired() ? 'required' : this.showError() ? 'error' : null

    const errorMessage = this.getErrorMessage()

    //const value=this.getValue()

    //if (value === undefined || value === '') errorMessage='Ведите название акции'

    return (
      <div className={ className }>
        <PgaBlockInput
          type='text'
          placeholder='Введите имя'
          onChange={ this.changeValue }
          value={ this.getValue() }
        />
        <span>{ errorMessage }</span>
      </div>
    )
  }
})

