import React, { Component } from 'react'

import styled               from 'styled-components'


import ProductSelect        from './ProductSelect'

import withData             from 'app/menu/hoc/withData'
import config               from 'app/menu/config'


const DivBlock = styled.div`
  display: flex;
  align-items: flex-start;
  margin: 10px 0 18px 0;
  
  & textarea {
    resize: vertical;
    min-height: 115px;
  }
`

const DivIco = styled.div`
  min-width: 20px;
  width: 20px;
  height: 17px;
  margin: 3px 15px 0 15px;
  opacity: 0.8;
  fill: #fff;
  
  background-image: url(${ props=>props.path });
  background-size: cover;
`

const DivContent = styled.div`
  width: 100%;
`

const DivName = styled.div`
  margin: 3px 0;
  font-size: 14px;
  color: #fff;
  font-family: HelveticaRegular;
`

const DivSelect = styled.div`
  overflow: auto;
  border: 1px solid #7d7d7d;

  &.in-popup {
    margin: 10px 0 0 -35px;
    height: 210px;
  }
  
  &::-webkit-scrollbar
  {
    width: 9px;
  }
  
  &::-webkit-scrollbar-track
  {
    background: #7d7d7d;
    border: 3px solid transparent;
    background-clip: content-box;
  }
  
  &::-webkit-scrollbar-thumb
  {
    background: #73c7c5;
    height: 35px;
    border-radius: 5px solid #73c7c5;
    border: 1px solid #73c7c5;
  }
`

const SpanError = styled.span`
  font-family: HelveticaRegular;
  font-size: 14px;
  color: orange;
`

class BlockGoods extends Component {
  constructor() {
    super()

    this.state = {
      data: {},
      order: [],

      productList: [],
      countProductList: 0,

      error: ''
    }
  }



  onSelect = ({ id }) => {
    const { productList } = this.state

    const currentIndex = productList.indexOf(id)

    if (currentIndex === -1) {
      productList.push(id)
    } else {
      productList.splice(currentIndex, 1)
    }

    const { onSelect } = this.props

    onSelect(productList)

    this.setState({
      productList,
      countProductList: productList.length,
      error: '',
    })
  }

  componentWillReceiveProps(nextProps) {
    const { data, order, error } = nextProps

    if (order.length > 0) this.setState({ data, order, error })
  }

  render() {
    const { data, order, error } = this.state

    return (
      <DivBlock>
        <DivIco path='assets/product.svg' />
        <DivContent>
          <DivName>
            Выберите товары, участвующие в акции
          </DivName>
          <DivSelect className='in-popup'>
            <ProductSelect
              data={ data }
              order={ order }

              onSelect={ this.onSelect }
            />
          </DivSelect>
          <SpanError>{ error }</SpanError>
        </DivContent>
      </DivBlock>
    )
  }
}

export default withData(BlockGoods, { queryName: config.queryNameMap.GOODS_CATEGORY_LIST })
