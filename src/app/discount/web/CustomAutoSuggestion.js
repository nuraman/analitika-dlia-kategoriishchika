import _                    from 'lodash'

import React, { Component } from 'react'

import styled               from 'styled-components'


const DivPgaFullSelectPoint = styled.div`
  overflow: hidden;
  border: 1px solid #7d7d7d;

  &.in-popup {
    margin: 10px 0 0 -35px;
    height: 210px;
  }
`

const DivPgaFullSelectCurrentRow = styled.div`
  display: flex;
  align-items: center;
  border-bottom: 1px solid #7d7d7d;
`

const DivFlexCheckAll = styled.div`
  display: flex;
  height: 30px;
  flex-direction: row;
  align-items: center;
  padding: 5px;
`

const DivPgaFullSelectInputCheck = styled.input`
  display: none;
  
  &:checked {
    display: block; 
  }
`

const InputPgaSelectSearch = styled.input`
  box-sizing: border-box;
  width: 100%;
  padding: 10px;
  height: 40px;
  background-color: transparent;
  border: none;
  outline: none;
  color: #fff;
  font-family: HelveticaRegular;
  font-size: 14px;
`

const DivPgaFullSelectWithScroll = styled.div`
  overflow: auto;

  &.in-popup {
    height: 210px;
  }
  
  &::-webkit-scrollbar
  {
    width: 9px;
  }
  
  &::-webkit-scrollbar-track
  {
    background: #7d7d7d;
    border: 3px solid transparent;
    background-clip: content-box;
  }
  
  &::-webkit-scrollbar-thumb
  {
    background: #73c7c5;
    height: 35px;
    border-radius: 5px solid #73c7c5;
    border: 1px solid #73c7c5;
  }
`

const DivPgaFullSelect = styled.div`
  display: block;
  overflow: auto;
  &::-webkit-scrollbar
  {
    width: 9px;
  }
  
  &::-webkit-scrollbar-track
  {
    background: #7d7d7d;
    border: 3px solid transparent;
    background-clip: content-box;
  }
  
  &::-webkit-scrollbar-thumb
  {
    background: #73c7c5;
    height: 35px;
    border-radius: 5px solid #73c7c5;
    border: 1px solid #73c7c5;
  }
`

const DivPgaFullSelectContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
`

const DivPgaFullSelectOption = styled.div`
  display: flex;
  align-items: center;
  padding: 5px;
`

const LabelNewCheck = styled.label`
  position: relative;
  width: 18px;
  min-width: 18px;
  height: 18px;
  margin-right: 10px;
  border: 1px solid #48b9bf;
  cursor: pointer;
  
  &:before {
    display: ${ props=>props.checked ? 'block' : 'none' };
    content: "";
    position: absolute;
    width: 12px;
    height: 12px;
    top: 3px;
    left: 3px;
    background-color: #48b9bf;
  }
`

const Label = styled.label`
  margin-bottom: -2px;
  font-weight: 300;
  font-size: 16px;
  font-family: HelveticaRegular;
  color: #b3fffb;
  cursor: pointer;
`

class CustomAutoSuggestion extends Component {
  constructor() {
    super()

    this.state = {
      checkedList: [],
      suggestionList: [],
      checkedAllPoint: false,

      pointList: [],
      pointName: '',
    }
  }

  onChangeInputCheck = (id) => {
    const { checkedList } = this.state

    const currentIndex = checkedList.indexOf(id)

    if (currentIndex === -1) {
      checkedList.push(id)
    } else {
      checkedList.splice(currentIndex, 1)
    }
  
    const { onSelect } = this.props

    onSelect && onSelect({ checkedList })

    this.setState({ checkedList })
  }

  onChangeAllPoint = () => {
    const { checkedAllPoint, suggestionList, pointList } = this.state
    const { onSelect } = this.props

    if (checkedAllPoint === false && suggestionList.length === 0) {
      const checkedList = pointList.reduce((checkedList, item) => {
        const { id } = item

        return [...checkedList, id]
      }, [])

     onSelect({ checkedList })

     return this.setState({ suggestionList: pointList, checkedList, checkedAllPoint: !checkedAllPoint })
    }

    if (checkedAllPoint === true && suggestionList.length > 0) {
      onSelect({ checkedList: [] })

      return this.setState({ checkedList: [], checkedAllPoint: !checkedAllPoint })
    }

    if (checkedAllPoint === false && suggestionList.length > 0) {
      const checkedList = suggestionList.reduce((checkedList, item) => {
        const { id } = item

        return [...checkedList, id]
      }, [])

      onSelect({ checkedList })

      return this.setState({ checkedList, checkedAllPoint: !checkedAllPoint })
    }
  }

  onChangePoint = ({ currentTarget: { value } }) => {

    const { pointList } = this.state

    const suggestionList = this.onGetSuggestions(value, pointList)

    this.setState({ pointName: value, suggestionList })
  }

  onIsContain = (name, inputValue) => {
    const inputList = _.words(inputValue)

    const matchList = inputList.reduce((matchList, item) => {
      if (name.toLowerCase().includes(item)) matchList.push(item)

      return matchList
    }, [])

    if ( matchList.length >= inputList.length ) return name

    return null
  }

  onGetSuggestions = (pointName, pointList) => {
    const inputValue = pointName.trim().toLowerCase()
    const inputLength = pointName.length

    return inputLength === 0 ? [] : pointList.filter(x => this.onIsContain(x.name, inputValue))
  }

  componentWillReceiveProps(nextProps) {
    const { pointList } = nextProps

    this.setState({ pointList })
  }

  componentWillMount() {
    const { pointList } = this.props

    this.setState({ pointList })
  }

  render() {
    const { suggestionList, checkedList, checkedAllPoint, pointName } = this.state

    return(
      <DivPgaFullSelectPoint className='in-popup'>
        <DivPgaFullSelectCurrentRow>
          <DivFlexCheckAll>
            <DivPgaFullSelectInputCheck type='checkbox' name='' value='' />
            <LabelNewCheck
              checked={ checkedAllPoint }
              onClick={ this.onChangeAllPoint }
            />
          </DivFlexCheckAll>
          <InputPgaSelectSearch
            type='text'
            name=''
            id=''
            placeholder='Введите адрес магазина'
            value={ pointName }

            onChange={ this.onChangePoint }
          />
        </DivPgaFullSelectCurrentRow>
        <DivPgaFullSelectWithScroll className='in-popup'>
          <DivPgaFullSelect>
            <DivPgaFullSelectContainer>
              {
                suggestionList.map((item) => {
                  const { name = 0, id } = item
                  const isChecked = checkedList.indexOf(id) !== -1

                  return (
                    <DivPgaFullSelectOption key={ id }>
                      <DivPgaFullSelectInputCheck type='checkbox' name='' value='' />
                      <LabelNewCheck
                        checked={ isChecked }
                        onClick={ () => this.onChangeInputCheck(id) }
                      />
                      <Label
                        onClick={ () => this.onChangeInputCheck(id) }
                      >
                        { name }
                      </Label>
                    </DivPgaFullSelectOption>
                  )
                })
              }
            </DivPgaFullSelectContainer>
          </DivPgaFullSelect>
        </DivPgaFullSelectWithScroll>
      </DivPgaFullSelectPoint>
    )
  }
}

export default CustomAutoSuggestion
