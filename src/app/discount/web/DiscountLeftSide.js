import React, { Component } from 'react'

import styled               from 'styled-components'


import BlockDiscountName    from './BlockDiscountName'
import BlockCountCustomer   from './BlockCountCustomer'
import BlockCountGoodList   from './BlockCountGoodList'
import BlockCountPoint      from './BlockCountPoint'
import BlockDatePicker      from './BlockDatePicker'
import BlockDescription     from './BlockDiscountDescription'


const DivContainer = styled.div`
  width: 50%;
  color: #fff;
`

const DivHeader = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  box-sizing: border-box;
  height: 60px;
  padding: 10px 40px;
  font-size: 22px;
  font-family: HelveticaRegular;
  color: #fff;
  border-bottom: 1px solid #7d7d7d;
`

const DivContent = styled.div`
  padding: 20px 40px 20px 25px;
`


class DiscountLeftSide extends Component {
  constructor() {
    super()

    this.state = {
      customerCount: 0,
      goodCount: 0,
      pointCount: 0,

      errorMessageDiscountName: '',
      errorMessageDescription: '',
      errorMessageDate: ''
    }
  }

  onSelectDiscountName = (value) => {
    const { onSelectName } = this.props

    onSelectName(value)
  }

  onSelectDiscountDescription = (value) => {
    const { onSelectDescription } = this.props

    onSelectDescription(value)
  }

  componentWillReceiveProps(nextProps) {
    const { customerCount, goodCount, pointCount } = nextProps

    this.setState({
      customerCount,
      goodCount,
      pointCount
    })
  }

  render() {
    const {
      customerCount,
      goodCount,
      pointCount,

      errorMessageDiscountName = '',
      errorMessageDate = '',
      errorMessageDescription = ''
    } = this.state

    return (
      <DivContainer>
        <DivHeader>
          Заявка на проведение акции
        </DivHeader>
        <DivContent>
          <BlockCountCustomer customerCount={ customerCount } />
          <BlockDiscountName
            error={ errorMessageDiscountName }

            onSelect={ this.onSelectDiscountName }
          />
          <BlockCountGoodList goodCount={ goodCount } />
          <BlockDatePicker error={ errorMessageDate } />
          <BlockCountPoint pointCount={ pointCount }/>
          <BlockDescription
            error={ errorMessageDescription }

            onSelect={ this.onSelectDiscountDescription }
          />
        </DivContent>
      </DivContainer>
    )
  }
}

export default DiscountLeftSide
