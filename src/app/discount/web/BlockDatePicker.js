import { withStyles }       from 'material-ui/styles'

import React, { Component } from 'react'

import DatePicker           from 'react-datepicker'

import styled               from 'styled-components'

import 'react-datepicker/dist/react-datepicker.css'


const DivBlock = styled.div`
 display: flex;
  align-items: flex-start;
  margin: 10px 0 18px 0;
  
  & textarea {
    resize: vertical;
    min-height: 115px;
  }
`

const DivIco = styled.div`
  min-width: 20px;
  width: 20px;
  height: 17px;
  margin: 3px 15px 0 15px;
  opacity: 0.8;
  fill: #fff;
  
  background-image: url(${ props=>props.path });
  background-size: cover;
`

const DivContent = styled.div`
  width: 100%;
`

const DivName = styled.div`
  margin: 3px 0;
  font-size: 14px;
  color: #fff;
  font-family: HelveticaRegular;
`

const DivWrap = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin: 10px 0 0 -35px;
`

const DivWrapDate = DivWrap.extend`
 position: relative;
 
 &:before {
  position: absolute;
  content: "";
  top: 50%;
  left: 50%;
  height: 2px;
  width: 10px;
  transform: translate(-50%, -50%);
  background-color: #48b9bf;
 }
`

const SpanError = styled.span`
  font-family: HelveticaRegular;
  font-size: 14px;
  color: orange;
`

const style = {
  datePickerStart: {
    boxSizing: 'border-box',
    padding: '10px 30px',
    border: '1px solid #7d7d7d',
    backgroundColor: 'transparent',
    width: '85%',
    fontFamily: 'HelveticaRegular',
    fontSize: '14px',
    color: '#fff',
    outline: 'none',
  },
  datePickerEnd: {
    fontFamily: 'HelveticaRegular',
    color: '#fff',
    boxSizing: 'border-box',
    padding: '10px 30px',
    marginLeft: '15%',
    border: '1px solid #7d7d7d',
    backgroundColor: 'transparent',
    fontSize: '14px',
    outline: 'none',
    width: '85%',
  },
}

class BlockDatePicker extends Component {
  constructor() {
    super()

    this.state = {
      dataBegin: '',
      dateEnd: '',

      error: ''
    }
  }

  onChangeDateBegin = (date) => this.setState({ dateBegin: date })
  onChangeDateEnd   = (date) => this.setState({ dateEnd: date })

  componentWillReceiveProps(nextProps) {
    const { error } = nextProps

    this.setState({ error })
  }

  render() {
    const { classes } = this.props

    const { dateBegin, dateEnd, error } = this.state

    return(
      <DivBlock>
        <DivIco path='assets/discount/calendarWight.svg' />
        <DivContent>
          <DivName>
            Период акции
          </DivName>
          <DivWrapDate>
            <DatePicker
              className={ classes.datePickerStart }
              selected={ dateBegin }

              onChange={ this.onChangeDateBegin }
            />
            <DatePicker
              className={ classes.datePickerEnd }
              selected={ dateEnd }

              onChange={ this.onChangeDateEnd }
            />
          </DivWrapDate>
          <SpanError>{ error }</SpanError>
        </DivContent>
      </DivBlock>
    )
  }
}

export default withStyles(style)(BlockDatePicker)
