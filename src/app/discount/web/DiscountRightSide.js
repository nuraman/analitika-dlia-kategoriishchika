import React, { Component } from 'react'

import styled               from 'styled-components'


import BlockGoods           from './BlockGoods'
import BlockPoint           from './BlockPoint'


const DivContainer = styled.div`
  width: 50%;
  color: #fff;
`

const DivHeader = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  box-sizing: border-box;
  height: 60px;
  padding: 10px 40px;
  font-size: 22px;
  font-family: HelveticaRegular;
  color: #fff;
  border-bottom: 1px solid #7d7d7d;
`

const DivClose = styled.div`
  position: absolute;
  right: 0;
  display: block;
  box-sizing: border-box;
  width: 60px;
  height: 60px;
  padding: 10px;
  border-left: 1px solid #7d7d7d;
  cursor: pointer;
  
  &:hover:before, :hover:after {
    opacity: 0.6;
  }
  &:before, &:after {
    content: "";
    position: absolute;
    top: 50%;
    left: 15px;
    width: 30px;
    height: 1px;
    opacity: 0.9;
    background-color: #fff;
    cursor: pointer;
  }
  &:before {
    transform: translateY(-50%) rotate(45deg);
  }
  &:after {
    transform: translateY(-50%) rotate(-45deg);
  }
`

const DivContent = styled.div`
  padding: 20px 40px 20px 25px;
`

const Button = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  box-sizing: border-box;
  width: 100%;
  height: 50px;
  text-align: center;
  font-weight: 300;
  font-size: 18px;
  font-family: HelveticaRegular;
  text-decoration: none;
  border: none;
  outline: none;
  color: #fff;
  background-color: #48b9bf;
  width: calc(100% - 15px);
  margin-left: 15px;
  cursor: pointer;
`

class DiscountRightSide extends Component {
  constructor() {
    super()

    this.state = {
      dataGoods: {},
      orderGoods: [],

      dataPoint: {},
      orderPoint: [],

      pointList: [],
      goodList: [],

      errorMessageGoodList: '',
    }
  }

  onSelectPointList = (pointList) => this.setState({ pointList })

  onSelectGoodList = (goodList) => this.setState({ goodList })

  onCloseDiscount = () => {
    const { onClose } = this.props

    onClose()
  }

  onHandleClick = () => {
    const { pointList, goodList } = this.state

    const { onSelect } = this.props

    onSelect({ pointList, goodList })
  }

  componentWillReceiveProps(nextProps) {
    const { dataGoods, orderGoods, dataPoint, orderPoint, errorMessageGoodList, errorMessagePoint } = nextProps

    this.setState({
      dataGoods,
      dataPoint,

      orderGoods,
      orderPoint,

      errorMessageGoodList,
      errorMessagePoint,
    })
  }

  render() {
    const {
      dataGoods,
      dataPoint,
      orderGoods,
      orderPoint,
      errorMessageGoodList,
      errorMessagePoint
    } = this.state

    return (
      <DivContainer>
        <DivHeader>
          <DivClose
            onClick={ this.onCloseDiscount }
          />
        </DivHeader>
        <DivContent>
          <BlockGoods
            data={ dataGoods }
            order={ orderGoods }

            error={ errorMessageGoodList }

            onSelect={ this.onSelectGoodList }
          />
          <BlockPoint
            dataPoint={ dataPoint }
            orderPoint={ orderPoint }

            error={ errorMessagePoint }

            onSelect={ this.onSelectPointList }
          />
          <Button
            onClick={ this.onHandleClick }
          >
            Отправить
          </Button>
        </DivContent>
      </DivContainer>
    )
  }
}

export default DiscountRightSide
