import React, { Component } from 'react'

import styled               from 'styled-components'

import DiscountLeftSide     from './DiscountLeftSide'
import DiscountRightSide    from './DiscountRightSide'

import withData             from 'app/discount/hoc/withData'


const DivContainer = styled.div`
  display: flex;
  width: 880px;
  box-sizing: border-box;
  position: absolute;
  top: 0;
  left: 100px;
  
  > div:first-child {
    background-color: #484848;
  }
  > div:last-child {
    background-color: #404040;
  }
`

class DiscountSide extends Component {
  constructor() {
    super()

    this.state = {
      customerCount: 0,
      dataGoods: {},
      dataPointList: [],
      description:'',
      name: '',
      goodList: 0,
      orderGoods: [],
      pointList: 0,
    }
  }

  onStartValidation = () => {
  }

  onSelect = ({ pointList, goodList }) => {
    this.onStartValidation()

    this.setState({ pointList, goodList })
  }
  onSelectName = (value) => this.setState({ name: value })
  onSelectDescription = (value) => this.setState({ description: value })
  onCloseDiscount = () => {
    const { onClose } = this.props

    onClose()
  }

  componentWillReceiveProps(nextProps) {
    const {
      customerCount,
      dataGoods = {},
      dataPoint = {},
      open,
      orderGoods = [],
      orderPoint = [],
    } = nextProps

    this.setState({ open: open })

    if (orderGoods.length > 0) {
      this.setState({ dataGoods, orderGoods, dataPoint, orderPoint, customerCount })
    }
  }

  render() {
    const {
      customerCount,
      dataGoods = {},
      orderGoods = [],
      dataPoint = {},
      orderPoint = [],
      pointList = [],
      goodList = [],
    } = this.state

    const { open } = this.props

    if (!open) return null

    return (
      <DivContainer>
        <DiscountLeftSide
          customerCount={ customerCount }
          pointCount={ pointList.length }
          goodCount={ goodList.length }

          onSelectName={ this.onSelectName }
          onSelectDescription={ this.onSelectDescription }
        />
        <DiscountRightSide
          dataGoods={ dataGoods }
          orderGoods={ orderGoods }

          dataPoint={ dataPoint }
          orderPoint={ orderPoint }

          onClose={ this.onCloseDiscount }
          onSelect={ this.onSelect }
        />
      </DivContainer>
    )
  }
}

export default withData(DiscountSide)
