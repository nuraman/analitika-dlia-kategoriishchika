import React, { Component } from 'react'

import styled               from 'styled-components'


const DivPgaFullSelectContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
`

const DivPgaFullSelectOption = styled.div`
  display: flex;
  align-items: center;
  padding: 5px;
`

const DivPgaFullSelectInputCheck = styled.input`
  display: none;
  
  &:checked {
    display: block; 
  }
`

const LabelNewCheck = styled.label`
  position: relative;
  width: 18px;
  min-width: 18px;
  height: 18px;
  margin-right: 10px;
  border: 1px solid #48b9bf;
  cursor: pointer;
  
  &:before {
    display: ${ props=>props.checked ? 'block' : 'none' };
    content: "";
    position: absolute;
    width: 12px;
    height: 12px;
    top: 3px;
    left: 3px;
    background-color: #48b9bf;
  }
`

const Label = styled.label`
  margin-bottom: -2px;
  font-weight: 300;
  font-size: 16px;
  font-family: HelveticaRegular;
  color: #b3fffb;
  cursor: pointer;
`

class ProductSelect extends Component {
  constructor() {
    super()

    this.state = {
      checkedList: [],
    }
  }

  onChange = (id) => {
    const { checkedList } = this.state

    const currentIndex = checkedList.indexOf(id)

    if (currentIndex === -1) {
      checkedList.push(id)
    } else {
      checkedList.splice(currentIndex, 1)
    }

    const { onSelect } = this.props

    onSelect && onSelect({ id })

    this.setState({ checkedList })
  }

  render() {
    const { data, order } = this.props

    if (order.length === 0) return( <DivPgaFullSelectContainer />)

    const { checkedList } = this.state

    return(
      <DivPgaFullSelectContainer>
        {
          order.map((id) => {
            const { name = 0 } = data[id]
            const isChecked = checkedList.indexOf(id) !== -1
            return (
              <DivPgaFullSelectOption key={ id }>
                <DivPgaFullSelectInputCheck type='checkbox' id={ `item${ id }` } name='' value='' />
                <LabelNewCheck
                  checked = { isChecked }
                  onClick = { () => this.onChange(id) }
                />
                <Label
                  onClick = { () => this.onChange(id) }
                >
                  { name }
                </Label>
              </DivPgaFullSelectOption>
            )
          })
        }
      </DivPgaFullSelectContainer>
    )
  }
}

export default ProductSelect
