import React, { Component } from 'react'

import styled               from 'styled-components'


import theme                from 'app/discount/theme'


const DivBlock = styled.div`
  display: flex;
  align-items: flex-start;
  margin: 10px 0 18px 0;
  
  & textarea {
    resize: vertical;
    min-height: 115px;
  }
`

const DivIco = styled.div`
  font-family: HelveticaLight;
  font-size: 12px;
  margin-top: 4px;
  color: ${ theme.colorMap.COLOR_FONT_AZ};
  height: 12px;
`

const DivContent = styled.div`
  width: 100%;
`

const DivName = styled.div`
  margin: 3px 0 0 50px;
  font-size: 14px;
  color: #fff;
  font-family: HelveticaRegular;
`

const DivWrapInput= styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin: 10px 0 0 15px;
`

const Textarea = styled.textarea`
  box-sizing: border-box;
  width: 100%;
  padding: 10px 30px;
  border: 1px solid #7d7d7d;
  background-color: transparent;
  font-family: HelveticaRegular;
  font-size: 14px;
  color: #fff;
  outline: none;
  font-family: HelveticaRegular;
  font-size: 14px;
  color: #fff;
`

const SpanError = styled.span`
  font-family: HelveticaRegular;
  font-size: 14px;
  color: orange;
`


class BlockDiscountDescription extends Component {
  constructor() {
    super()

    this.state = {
      value: '',
      error: '',
    }
  }

  onChange = ({ currentTarget: { value } }) => {
    const { onSelect } = this.props

    onSelect(value)

    this.setState({ value, error: '' })
  }

  render() {
    const { value, error } = this.state

    return (
      <DivBlock>
        <DivIco />
        <DivContent>
          <DivName>
            Описание акции
          </DivName>
          <DivWrapInput>
            <Textarea
              value={ value }
              name=''
              id=''

              onChange={ this.onChange }
            />
          </DivWrapInput>
          <SpanError>{ error }</SpanError>
        </DivContent>
      </DivBlock>
    )
  }
}

export default BlockDiscountDescription
