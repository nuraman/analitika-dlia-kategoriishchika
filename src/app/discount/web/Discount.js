import { withStyles }       from 'material-ui/styles'

import React, { Component } from 'react'

import DatePicker           from 'react-datepicker'

import styled               from 'styled-components'


import theme                from 'app/discount/theme'
import config               from 'app/discount/config'

import ProductSelect        from './ProductSelect'
import CustomAutoSuggestion from './CustomAutoSuggestion'

import withData             from 'app/discount/hoc/withData'

import 'react-datepicker/dist/react-datepicker.css'


const DivPopupCasting = styled.div`
  display: flex;
  width: 880px;
  box-sizing: border-box;
  position: absolute;
  top: 0px;
  left: 100px;
  
  > div:first-child {
    background-color: #484848;
  }
  > div:last-child {
    background-color: #404040;
  }
  
  input::-webkit-input-placeholder {
    opacity: 1;
    font-weight: 300;
    font-size: 18px;
    color: #fff;
    transition: opacity 0.2s ease;
  }

  input::-moz-placeholder {
    opacity: 1;
    font-weight: 300;
    font-size: 18px;
    color: #fff;
    transition: opacity 0.2s ease;
  }
  
  input:-moz-placeholder {
    opacity: 1;
    font-weight: 300;
    font-size: 18px;
    color: #fff;
    transition: opacity 0.2s ease;
  }
  
  input:-ms-input-placeholder {
    opacity: 1;
    font-weight: 300;
    font-size: 18px;
    color: #fff;
    transition: opacity 0.2s ease;
  }
  
  input:focus::-webkit-input-placeholder {
    opacity: 0;
    transition: opacity 0.2s ease;
  }
  
  input:focus::-moz-placeholder {
    opacity: 0;
    transition: opacity 0.2s ease;
  }
  
  input:focus:-moz-placeholder {
    opacity: 0;
    transition: opacity 0.2s ease;
  }
  
  input:focus:-ms-input-placeholder {
    opacity: 0;
    transition: opacity 0.2s ease;
  }
`

const DivPopupCastingSide = styled.div`
  width: 50%;
  color: #fff;
`

const DivPopupCastingHeadrow = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  box-sizing: border-box;
  height: 60px;
  padding: 10px 40px;
  font-size: 22px;
  font-family: HelveticaRegular;
  color: #fff;
  border-bottom: 1px solid #7d7d7d;
`

const DivPopupCastingContainer = styled.div`
  padding: 20px 40px 20px 25px;
`

const DivPgaBlock = styled.div`
  display: flex;
  align-items: flex-start;
  margin: 10px 0 18px 0;
  
  & textarea {
    resize: vertical;
    min-height: 115px;
  }
`

const DivPgaBlockIco = styled.div`
  min-width: 20px;
  width: 20px;
  height: 20px;
  margin: 0 15px 0 15px;
  opacity: 0.8;
  fill: #fff;
`

const DivPgaBlockContent = styled.div`
  width: 100%;
`

const DivPgaBlockName = styled.div`
  margin: 3px 0;
  font-size: 14px;
  color: #fff;
  font-family: HelveticaRegular;
`

const DivPgaBlockValue = styled.div`
  font-size: 18px;
  color: #b3fffb;
  font-family: HelveticaRegular;
`

const DivPgaBlockWrapValue = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin: 10px 0 0 -35px;
`

const PgaBlockInput = styled.input`
  height: 38px;
  display: flex;
  align-items: center;
  box-sizing: border-box;
  width: 100%;
  padding: 10px 30px;
  border: 1px solid #7d7d7d;
  background-color: transparent;
  outline: none;
  font-family: HelveticaRegular;
  font-size: 14px;
  color: #fff;
`

const DivPgaBlockWrapValueDate = DivPgaBlockWrapValue.extend`
 position: relative;
 
 &:before {
  position: absolute;
  content: "";
  top: 50%;
  left: 50%;
  height: 2px;
  width: 10px;
  transform: translate(-50%, -50%);
  background-color: #48b9bf;
 }
`

const Textarea = styled.textarea`
  box-sizing: border-box;
  width: 100%;
  padding: 10px 30px;
  border: 1px solid #7d7d7d;
  background-color: transparent;
  font-family: HelveticaRegular;
  font-size: 14px;
  color: #fff;
  outline: none;
  font-family: HelveticaRegular;
  font-size: 14px;
  color: #fff;
`

const DivPopupCastingClose = styled.div`
  position: absolute;
  right: 0;
  display: block;
  box-sizing: border-box;
  width: 60px;
  height: 60px;
  padding: 10px;
  border-left: 1px solid #7d7d7d;
  cursor: pointer;
  
  &:hover:before, :hover:after {
    opacity: 0.6;
  }
  &:before, &:after {
    content: "";
    position: absolute;
    top: 50%;
    left: 15px;
    width: 30px;
    height: 1px;
    opacity: 0.9;
    background-color: #fff;
    cursor: pointer;
  }
  &:before {
    transform: translateY(-50%) rotate(45deg);
  }
  &:after {
    transform: translateY(-50%) rotate(-45deg);
  }
`

const DivPgaFullSelect = styled.div`
  overflow: auto;
  border: 1px solid #7d7d7d;

  &.in-popup {
    margin: 10px 0 0 -35px;
    height: 210px;
  }
  
  &::-webkit-scrollbar
  {
    width: 9px;
  }
  
  &::-webkit-scrollbar-track
  {
    background: #7d7d7d;
    border: 3px solid transparent;
    background-clip: content-box;
  }
  
  &::-webkit-scrollbar-thumb
  {
    background: #73c7c5;
    height: 35px;
    border-radius: 5px solid #73c7c5;
    border: 1px solid #73c7c5;
  }
`

const DivIconAz = styled.div`
  font-family: HelveticaLight;
  font-size: 12px;
  margin-top: 4px;
  color: ${ theme.colorMap.COLOR_FONT_AZ};
  height: 12px;
`

const LinkCastingBtn = styled.a`
  display: flex;
  align-items: center;
  justify-content: center;
  box-sizing: border-box;
  width: 100%;
  height: 50px;
  text-align: center;
  font-weight: 300;
  font-size: 18px;
  font-family: HelveticaRegular;
  text-decoration: none;
  border: none;
  outline: none;
  color: #fff;
  background-color: #48b9bf;
  width: calc(100% - 15px);
  margin-left: 15px;
  cursor: pointer;
`

const SpanError = styled.span`
  font-family: HelveticaRegular;
  font-size: 14px;
  color: orange;
`

const DivPgaBlockIcon = styled.div`
  min-width: 20px;
  width: 20px;
  height: 17px;
  margin: 3px 15px 0 15px;
  opacity: 0.8;
  fill: #fff;
  
  background-image: url(${ props=>props.path });
  background-size: cover;
`

const DivPgaBlockIconPoint = styled.div`
  min-width: 20px;
  width: 20px;
  height: 17px;
  margin: 5px 15px 0 15px;
  
  background-image: url(assets/discount/point.svg);
  background-size: cover;
`


const style = {
  datePickerStart: {
    boxSizing: 'border-box',
    padding: '10px 30px',
    border: '1px solid #7d7d7d',
    backgroundColor: 'transparent',
    width: '85%',
    fontFamily: 'HelveticaRegular',
    fontSize: '14px',
    color: '#fff',
    outline: 'none',
  },
  datePickerEnd: {
    fontFamily: 'HelveticaRegular',
    color: '#fff',
    boxSizing: 'border-box',
    padding: '10px 30px',
    marginLeft: '15%',
    border: '1px solid #7d7d7d',
    backgroundColor: 'transparent',
    fontSize: '14px',
    outline: 'none',
    width: '85%',
  },
  button: {
    backgroundColor: `${ theme.colorMap.BACKGROUND_COLOR_BUTTON }`,
    width: '366px',
    height: '49px',
    zIndex: '81',
    fontFamily: 'HelveticaLight',
    fontSize: '18px',
    color: `${ theme.colorMap.COLOR_FONT }`,
    textTransform: 'capitalize',
  },
}

class Discount  extends Component {
  constructor(props) {
    super(props)

    const { open } = props

    this.state = {
      discountName: '',
      discountDescription: '',
      dateBegin: '',
      dateEnd: '',

      pointName: '',

      dataGoods: {},                //прокукты
      orderGoods: [],

      dataPointList: [],            //список магазинов

      dataPointListSelected: [],    //список выбранных магазинов
      countPointListSelected: '',   //количество выбранных магазинов
      dataGoodListSelected: [],     //список выбранных продуктов
      countGoodListSeleced: '',     //количество выбранных продуктов

      customerCount: '',            //количество участников

      checkedAllPoint: false,       //выбрать все магазины

      errorMessageDiscountName: '',
      errorMessageDate: '',
      errorMessageDescription: '',
      errorMessagePointList: '',
      errorMessageGoodList: '',

      isDiscountAppendSuccess: false, //Статус подачи заявки на акцию
      messageDiscountAppend: '',      //Сообщение об успешном отправке заявки или ошибки

      open: open
    }
  }

  onChangeDiscountName  = ({ currentTarget: { value } }) => this.setState({ discountName: value,  errorMessageDiscountName: '' })
  onChangeDiscountDescription = ({ currentTarget: { value } }) => this.setState({ discountDescription: value, errorMessageDescription: '' })
  onChangeDateBegin     = (date) => this.setState({ dateBegin: date })
  onChangeDateEnd       = (date) => this.setState({ dateEnd: date })

  onSelectGoodCallback = ({ id }) => {
    const { dataGoodListSelected } = this.state

    const currentIndex = dataGoodListSelected.indexOf(id)

    if (currentIndex === -1) {
      dataGoodListSelected.push(id)
    } else {
      dataGoodListSelected.splice(currentIndex, 1)
    }

    this.setState({
      dataGoodListSelected,
      countGoodListSeleced: dataGoodListSelected.length,
      errorMessageGoodList: '',
    })
  }
  onSelectPointCallback = ({ checkedList = [] }) => {
    const dataPointListSelected = checkedList.reduce((dataPointListSelected, item) => {
      return [...dataPointListSelected, item]
    }, [])

    this.setState({
      dataPointListSelected,
      countPointListSelected: dataPointListSelected.length,
      errorMessagePointList: ''
    })
  }

  isValidDiscountName = ({ discountName }) => {
    if (discountName === '' || discountName === undefined) {
      this.setState({ errorMessageDiscountName: 'Введите название акции' })

      return false
    }

    this.setState({ errorMessageDiscountName: '' })

    return true
  }
  isValidDiscountDescription = ({ discountDescription }) => {
    if (discountDescription === '' || discountDescription === undefined) {
      this.setState({ errorMessageDescription: 'Введите описание акции' })

      return false
    }

    this.setState({ errorMessageDescription: '' })

    return true
  }
  isValidPeriod = ({ dateBegin, dateEnd }) => {
    if (dateBegin === '' || dateEnd === '') {
      this.setState({ errorMessageDate: 'Заполните дату' })
      return false;
    }

    if (dateBegin > dateEnd) {
      this.setState({ errorMessageDate: 'Начальная дата должна быть меньше конечной' })
      return false;
    }

    this.setState({ errorMessageDate: '' })
    return true
  }
  isValidPointList = ({ dataPointListSelected }) => {
    if (dataPointListSelected.length > 0) {
      this.setState({ errorMessagePointList: '' })
      return true
    }

    this.setState({ errorMessagePointList: 'Выберите торговые точки' })

    return false
  }
  isValidGoodListSelected = ({ dataGoodListSelected }) => {
    if (dataGoodListSelected.length > 0) {
      this.setState({ errorMessageGoodList: '' })
      return true
    }

    this.setState({ errorMessageGoodList: 'Выберите продукты' })

    return false
  }

  onValidateForm = ({ discountName, discountDescription, dateBegin, dateEnd, dataPointListSelected, dataGoodListSelected }) => {
    const isValidName = this.isValidDiscountName({ discountName })
    const isValidDesc = this.isValidDiscountDescription({ discountDescription })
    const isValidPeriod = this.isValidPeriod({ dateBegin, dateEnd })
    const isValidPointList = this.isValidPointList({ dataPointListSelected })
    const isValidGoodList = this.isValidGoodListSelected({ dataGoodListSelected })

    if (isValidName && isValidDesc && isValidPeriod && isValidGoodList && isValidPointList) return true

    return false
  }

  onHandleClickDiscount = (data) => {
    const { discountName, discountDescription, dateBegin, dateEnd, dataPointListSelected, dataGoodListSelected } = this.state

    const isValid = this.onValidateForm({ discountName, discountDescription, dateBegin, dateEnd, dataPointListSelected, dataGoodListSelected })

    if (isValid === true) {
      const {mutationInputNameMap} = config

      const pointListInString = dataPointListSelected.reduce((pointListInString, item, index) => {

        if (index === dataPointListSelected.length - 1) {
          return pointListInString = pointListInString + item
        }

        pointListInString = pointListInString + item + ','

        return pointListInString
      }, '')

      const goodListInString = dataGoodListSelected.reduce((goodListInString, item, index) => {
        if (index === dataGoodListSelected.length - 1) {
          return goodListInString = goodListInString + item
        }

        goodListInString = goodListInString + item + ','

        return goodListInString
      }, '')

      const { options: { variables } } = this.props

      const { goodsCategory, pointGroup, pointFormat, point, vidget, period, param1, param2 } = variables

      if (discountName) {
        this.props.actionAppend({
          [mutationInputNameMap.DISCOUNT_NAME]: discountName,
          [mutationInputNameMap.DESCRIPTION]: discountDescription,
          [mutationInputNameMap.DATA_BEGIN]: dateBegin,
          [mutationInputNameMap.DATE_END]: dateEnd,
          [mutationInputNameMap.POINT]: point,
          [mutationInputNameMap.POINT_GROUP]: pointGroup,
          [mutationInputNameMap.POINT_FORMAT]: pointFormat,
          [mutationInputNameMap.POINT_SELECTED]: pointListInString,
          [mutationInputNameMap.PERIOD]: period,
          [mutationInputNameMap.GOODS]: goodListInString,
          [mutationInputNameMap.GOODS_CATEGORY]: goodsCategory,
          [mutationInputNameMap.WIDGET]: vidget,
          [mutationInputNameMap.PARAM1]: param1,
          [mutationInputNameMap.PARAM2]: param2,
        }).then(({ data }) => {
          const { actionAppend } = data
          const { errors } = actionAppend

          const { code } = errors

          this.onCloseDiscount(code)
        })
      }
    }
  }

  onCloseDiscount = (code) => {
    const { onClose } = this.props

    this.setState({
      open: !this.state.open,
      countGoodListSeleced: '',
      countPointListSelected: '',
      errorMessageDiscountName: '',
      errorMessageDate: '',
      errorMessageDescription: '',
      errorMessagePointList: '',
      errorMessageGoodList: '',
      dataPointListSelected: [],
      dataGoodListSelected: [],

      isDiscoundAppend: false,
      messageDiscountAppend: ''
    })

    onClose(code)
  }

  getDataList = (data, order) => {
    const dataList = order.reduce((dataList, item) => {
      const { id, name } = data[item] || {}

      return [ ...dataList, { name, id } ]
    }, [])

    return dataList
  }

  componentWillReceiveProps(nextProps) {
    const { open, dataGoods = {}, orderGoods = [], dataPoint = {}, orderPoint = [], customerCount, pageX } = nextProps

    this.setState({ open: open, pageX })

    if (orderGoods.length > 0) {
      const dataPointList = this.getDataList(dataPoint, orderPoint)

      this.setState({ dataGoods, orderGoods, dataPointList, customerCount })
    }
  }

  componentWillMount() {
    this.setState({ errorMessageDiscountName: '' })
  }

  render() {
    const {
      open,
      customerCount,
      discountName,
      discountDescription,
      dateBegin,
      dateEnd,
      dataGoods,
      dataPointList,
      orderGoods,

      countGoodListSeleced,
      countPointListSelected,

      pageX,

      errorMessageDiscountName,
      errorMessageDate,
      errorMessageDescription,
      errorMessageGoodList,
      errorMessagePointList,
    } = this.state

    const { classes } = this.props

    if (!open) return null

    return (
       <DivPopupCasting>
        <DivPopupCastingSide>
          <DivPopupCastingHeadrow>
            Заявка на проведение акции
          </DivPopupCastingHeadrow>
          <DivPopupCastingContainer>
            <DivPgaBlock>
              <DivPgaBlockIcon path='assets/person.svg' />
              <DivPgaBlockContent>
                <DivPgaBlockName>
                  Участники
                </DivPgaBlockName>
                <DivPgaBlockValue>
                  { customerCount }
                </DivPgaBlockValue>
              </DivPgaBlockContent>
            </DivPgaBlock>
            <DivPgaBlock>
              <DivPgaBlockIco>
                <DivIconAz>
                  Az.
                </DivIconAz>
              </DivPgaBlockIco>
              <DivPgaBlockContent>
                <DivPgaBlockName>
                  Название акции
                </DivPgaBlockName>
                <DivPgaBlockWrapValue>
                  <PgaBlockInput
                    type='text'
                    placeholder='Введите имя'
                    onChange={ this.onChangeDiscountName }
                    value={ discountName }
                  />
                </DivPgaBlockWrapValue>
                <SpanError>{ errorMessageDiscountName }</SpanError>
              </DivPgaBlockContent>
            </DivPgaBlock>
            <DivPgaBlock>
              <DivPgaBlockIcon path='assets/setting.svg' />
              <DivPgaBlockContent>
                <DivPgaBlockName>
                  SKU акции
                </DivPgaBlockName>
                <DivPgaBlockValue>
                  { countGoodListSeleced ? countGoodListSeleced : 0 } SKU
                </DivPgaBlockValue>
              </DivPgaBlockContent>
            </DivPgaBlock>
            <DivPgaBlock>
              <DivPgaBlockIcon path='assets/discount/calendarWight.svg' />
              <DivPgaBlockContent>
                <DivPgaBlockName>
                  Период акции
                </DivPgaBlockName>
                <DivPgaBlockWrapValueDate>
                  <DatePicker
                    className={ classes.datePickerStart }
                    selected={ dateBegin }

                    onChange={ this.onChangeDateBegin }
                  />
                  <DatePicker
                    className={ classes.datePickerEnd }
                    selected={ dateEnd }

                    onChange={ this.onChangeDateEnd }
                  />
                </DivPgaBlockWrapValueDate>
                <SpanError>{ errorMessageDate }</SpanError>
              </DivPgaBlockContent>
            </DivPgaBlock>
            <DivPgaBlock>
              <DivPgaBlockIcon path='assets/outlet.svg' />
              <DivPgaBlockContent>
                <DivPgaBlockName>
                  География акции
                </DivPgaBlockName>
                <DivPgaBlockValue>
                  { countPointListSelected ? countPointListSelected : 0 } торговые точки
                </DivPgaBlockValue>
              </DivPgaBlockContent>
            </DivPgaBlock>
            <DivPgaBlock>
              <DivPgaBlockIco />
              <DivPgaBlockContent>
                <DivPgaBlockName>
                  Описание акции
                </DivPgaBlockName>
                <DivPgaBlockWrapValue>
                  <Textarea
                    value={ discountDescription }
                    name=''
                    id=''

                    onChange={ this.onChangeDiscountDescription }
                  />
                </DivPgaBlockWrapValue>
                <SpanError>{ errorMessageDescription }</SpanError>
              </DivPgaBlockContent>
            </DivPgaBlock>
          </DivPopupCastingContainer>
        </DivPopupCastingSide>
        <DivPopupCastingSide>
          <DivPopupCastingHeadrow>
            <DivPopupCastingClose
              onClick={ () => this.onCloseDiscount(6) }
            />
          </DivPopupCastingHeadrow>
          <DivPopupCastingContainer>
            <DivPgaBlock>
              <DivPgaBlockIcon path='assets/product.svg' />
              <DivPgaBlockContent>
                <DivPgaBlockName>
                  Выберите товары, участвующие в акции
                </DivPgaBlockName>
                <DivPgaFullSelect className='in-popup'>
                  <ProductSelect
                    data={ dataGoods }
                    order={ orderGoods }

                    onSelect={ this.onSelectGoodCallback }
                  />
                </DivPgaFullSelect>
                <SpanError>{ errorMessageGoodList }</SpanError>
              </DivPgaBlockContent>
            </DivPgaBlock>
            <DivPgaBlock>
              <DivPgaBlockIconPoint />
                <DivPgaBlockContent>
                  <DivPgaBlockName>
                    Магазины, учавствующие в акции
                  </DivPgaBlockName>
                  <CustomAutoSuggestion
                    pointList={ dataPointList }

                    onSelect={ this.onSelectPointCallback }
                  />
                  <SpanError>{ errorMessagePointList }</SpanError>
              </DivPgaBlockContent>
            </DivPgaBlock>
            <LinkCastingBtn
              onClick={ this.onHandleClickDiscount }
            >
              Отправить
            </LinkCastingBtn>
          </DivPopupCastingContainer>
        </DivPopupCastingSide>
      </DivPopupCasting>
    )
  }
}

const WithDataDiscount = withData(Discount)

export default withStyles(style)(WithDataDiscount)
