import React                from 'react'

import styled               from 'styled-components'


const DivBlock = styled.div`
  display: flex;
  align-items: flex-start;
  margin: 10px 0 18px 0;
  
  & textarea {
    resize: vertical;
    min-height: 115px;
  }
`

const DivIco = styled.div`
  min-width: 20px;
  width: 20px;
  height: 17px;
  margin: 3px 15px 0 15px;
  opacity: 0.8;
  fill: #fff;
  
  background-image: url(${ props=>props.path });
  background-size: cover;
`

const DivContent = styled.div`
  width: 100%;
`

const DivName = styled.div`
  margin: 3px 0;
  font-size: 14px;
  color: #fff;
  font-family: HelveticaRegular;
`

const DivValue = styled.div`
  font-size: 18px;
  color: #b3fffb;
  font-family: HelveticaRegular;
`


export default (props) => {
  const { customerCount } = props

  return(
    <DivBlock>
      <DivIco path='assets/person.svg' />
      <DivContent>
        <DivName>
          Участники
        </DivName>
        <DivValue>
          { customerCount }
        </DivValue>
      </DivContent>
    </DivBlock>
  )
}
