import React, { Component } from 'react'

import styled               from 'styled-components'


import AutoSuggestion       from './CustomAutoSuggestion'


const DivBlock = styled.div`
  display: flex;
  align-items: flex-start;
  margin: 10px 0 18px 0;
  
  & textarea {
    resize: vertical;
    min-height: 115px;
  }
`

const DivIcon = styled.div`
  min-width: 20px;
  width: 20px;
  height: 17px;
  margin: 5px 15px 0 15px;
  
  background-image: url(assets/discount/point.svg);
  background-size: cover;
`

const DivContent = styled.div`
  width: 100%;
`

const DivName = styled.div`
  margin: 3px 0;
  font-size: 14px;
  color: #fff;
  font-family: HelveticaRegular;
`

const SpanError = styled.span`
  font-family: HelveticaRegular;
  font-size: 14px;
  color: orange;
`

class BlockPoint extends Component {
  constructor() {
    super()

    this.state = {
      checkedList: [],
      pointList: [],
      error: ''
    }
  }

  onSelectPoint = ({ checkedList = [] }) => {
    this.setState({ checkedList })

    const { onSelect } = this.props

    onSelect && onSelect({ pointList: checkedList })
  }


  componentWillReceiveProps(nextProps) {
    const { dataPoint, orderPoint, error } = nextProps
    let pointList = []

    if (orderPoint.length > 0) {
      pointList = orderPoint.reduce((pointList, item) => {
        const { id, name } = dataPoint[item]

        return [...pointList, { id, name }]
      }, [])
    }

    this.setState({ pointList, error })
  }

  render() {
    const { pointList, error } = this.state

    return (
      <DivBlock>
        <DivIcon />
        <DivContent>
          <DivName>
            Магазины, учавствующие в акции
          </DivName>
          <AutoSuggestion
            pointList={ pointList }

            onSelect={ this.onSelectPoint }
          />
          <SpanError>{ error }</SpanError>
        </DivContent>
      </DivBlock>
    )
  }
}

export default BlockPoint
