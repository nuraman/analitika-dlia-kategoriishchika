import types                from 'engine/types'


const queryNameMap = {
  ACTION_PREPARE:           'actionPrepare'
}

const mutationName = 'actionAppend'


const inputNameMap = {
  POINT_GROUP:              'pointGroup',
  POINT_FORMAT:             'pointFormat',
  POINT:                    'point',
  PERIOD:                   'period',
  GOODS_CATEGORY:           'goodsCategory',
  WIDGET:                   'vidget',
  PARAM1:                   'param1',
  PARAM2:                   'param2',
}

const mutationInputNameMap = {
  DATA_BEGIN:                         'dateStart',
  DATE_END:                           'dateEnd',
  DESCRIPTION:                        'descriptionAction',
  GOODS:                              'goodsList',
  POINT_SELECTED:                     'pointList',
  GOODS_CATEGORY:                     'goodsCategory',
  DISCOUNT_NAME:                      'nameAction',
  PARAM1:                             'param1',
  PARAM2:                             'param2',
  POINT:                              'point',
  POINT_GROUP:                        'pointGroup',
  POINT_FORMAT:                       'pointFormat',
  PERIOD:                             'period',
  WIDGET:                             `vidget`,
}

const resultNameMap = {
  ID:                       'id',
  NAME:                     'name',
  COUNT_CUSTOMER:           'countCustomers',
}

const queryMap = [
  {
    queryName: queryNameMap.ACTION_PREPARE,
    inputList: [
      {
        name: inputNameMap.GOODS_CATEGORY,
        type: types.String,
      },
      {
        name: inputNameMap.PARAM1,
        type: types.String,
      },
      {
        name: inputNameMap.PARAM2,
        type: types.String,
      },
      {
        name: inputNameMap.POINT,
        type: types.String,
      },
      {
        name: inputNameMap.POINT_GROUP,
        type: types.String,
      },
      {
        name: inputNameMap.POINT_FORMAT,
        type: types.String,
      },
      {
        name: inputNameMap.PERIOD,
        type: types.Int,
      },
      {
        name: inputNameMap.WIDGET,
        type: types.Int,
      },
    ],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        }
      ],
      resultGoods: [
        {
          name: resultNameMap.ID,
          type: types.Int,
        },
        {
          name: resultNameMap.NAME,
          type: types.String,
        }
      ],
      resultPoints: [
        {
          name: resultNameMap.ID,
          type: types.Int,
        },
        {
          name: resultNameMap.NAME,
          type: types.String,
        }
      ],
      resultOther: [{
        name: resultNameMap.COUNT_CUSTOMER,
        type: types.Int,
      }]
    }
  },
]

const mutationMap = [
  {
    mutationName: mutationName,
    inputList: [
      {
        name: [mutationInputNameMap.DATA_BEGIN],
        type: types.String,
      },
      {
        name: [mutationInputNameMap.DATE_END],
        type: types.String,
      },
      {
        name: [mutationInputNameMap.DESCRIPTION],
        type: types.String,
      },
      {
        name: [mutationInputNameMap.GOODS],
        type: types.String,
      },
      {
        name: [mutationInputNameMap.GOODS_CATEGORY],
        type: types.Int,
      },
      {
        name: [mutationInputNameMap.DISCOUNT_NAME],
        type: types.String,
      },
      {
        name: [mutationInputNameMap.PARAM1],
        type: types.String,
      },
      {
        name: [mutationInputNameMap.PARAM2],
        type: types.String,
      },
      {
        name: [mutationInputNameMap.POINT],
        type: types.String,
      },
      {
        name: [mutationInputNameMap.POINT_GROUP],
        type: types.String,
      },
      {
        name: [mutationInputNameMap.POINT_FORMAT],
        type: types.String,
      },
      {
        name: [mutationInputNameMap.PERIOD],
        type: types.Int,
      },
      {
        name: [mutationInputNameMap.WIDGET],
        type: types.Int,
      },
      {
        name: [mutationInputNameMap.POINT_SELECTED],
        type: types.String,
      }
    ],
    output: {
      errors: [
        {
          name: 'code',
          type: types.Int,
        },
        {
          name: 'message',
          type: types.String,
        }
      ],
    }
  }
]

export default {
  queryMap,
  name: queryNameMap.ACTION_PREPARE,
  mutationMap,
  mutationName,
  mutationInputNameMap
}

