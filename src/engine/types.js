export default {
  Boolean:  'Boolean',
  Date:     'Date',
  Int:      'Int',
  ID:       'ID',
  Float:    'Float',
  String:   'String',
}
