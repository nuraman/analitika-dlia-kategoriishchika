const inputNameMap = {
  DATE_BEGIN:                         'dateBegin',
  DATE_END:                           'dateEnd',
  GOODS_CATEGORY:                     'goodsCategory',
  PERIOD:                             'period',
  POINT_GROUP:                        'pointGroup',
  POINT_FORMAT:                       'pointFormat',
  POINT:                              'point',
  TYPE_PARAMETER:                     'typeParameter',
}

const queryNameMap = {
  PERIOD_LIST:                        'periodList',
  PERIOD_LAST:                        'periodLast',
  PERIOD_PREV:                        'periodPrev',
  PERIOD_NEXT:                        'periodNext',
  POINT_FORMAT_LIST:                  'pointFormatList',
  PARAMETER_LIST:                     'parameterList',
  POINT_LIST:                         'pointList',
  POINT_GROUP_LIST:                   'pointGroupList',
  GOODS_CATEGORY_LIST:                'goodsCategorieList',
  GRF01:                              'grf01',
  GRF02:                              'grf02',
  GRF03:                              'grf03',
  GRF04:                              'grf04',
}

const resultNameMap = {
  ID:                                 'id',
  VALUE:                              'value',
  DATE_BEGIN:                         'dateBegin',
  DATE_END:                           'dateEnd',
  CAN_PERIOD_NEXT:                    'canPeriodNext',
  CAN_PERIOD_PREV:                    'canPeriodPrev',
  PROVIDER:                           'provider',
  PROVIDER_NAME:                      'providerName',
  VALUE:                              'value',
  MONTH:                              'month',
  VALUE_PREV_PERIOD:                  'valuePrevPeriod',
  VALUE_PREV_YEAR:                    'valuePrevYear',
  VALUE_AMOUNT:                       'valueAmount',
  VALUE_COUNT:                        'valueCount',
  VALUE_QUANTITY:                     'valueQuantity',
}

export default {
  inputNameMap,
  queryNameMap,
  resultNameMap,
}
