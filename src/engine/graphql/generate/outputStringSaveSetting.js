export default function({ name, output, config }) {
  const { errors = [], } = output || {}

  const errorStr = errors.reduce((errorStr, item) => {
    const { name } = item
    return errorStr = errorStr + `${ name } `
  },``)

  return `
    errors { ${ errorStr } }
    `
}
