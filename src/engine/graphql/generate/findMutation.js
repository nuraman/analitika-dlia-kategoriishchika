import _                    from 'lodash'

export default function findMutation (payload) {
  const { mutationMap, name } = payload

  const mutation = _.find(mutationMap, { mutationName: name })

  return mutation
}
