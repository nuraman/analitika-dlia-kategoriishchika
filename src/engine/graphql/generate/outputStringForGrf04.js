export default function({ name, output }) {
  const { errors = [], resultSale = [], resultDiscount = [], resultDiscountOther, resultGoods = [], title } = output || {}

  const errorStr = errors.reduce((errorStr, item) => {
    const { name } = item
    return errorStr = errorStr + `${ name } `
  },``)

  const resultSaleStr = resultSale.reduce((resultSaleStr, item) => {
    const { name } = item

    return resultSaleStr = resultSaleStr + `${ name } `
  },``)

  const resultDiscountStr = resultDiscount.reduce((resultDiscountStr, item) => {
    const { name } = item

    return resultDiscountStr = resultDiscountStr + `${ name } `
  },``)

  const resultDiscountOtherStr = resultDiscountOther.reduce((resultDiscountOtherStr, item) => {
    const { name } = item

    return resultDiscountOtherStr = resultDiscountOtherStr + `${ name } `
  },``)

  const resultGoodsStr = resultGoods.reduce((resultGoodsStr, item) => {
    const { name } = item

    return resultGoodsStr = resultGoodsStr + `${ name } `
  },``)

  const titleStr = title.reduce((titleStr, item) => {
    const { name } = item

    return titleStr = titleStr + `${ name } `
  },``)

  return `
    errors { ${ errorStr } }
    resultSale { ${ resultSaleStr } }
    resultDiscount { ${ resultDiscountStr } }
    resultOther { ${ resultDiscountOtherStr } }
    resultGoods { ${ resultGoodsStr } }
    title { ${ titleStr } }
   `
}
