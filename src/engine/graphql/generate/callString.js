export default function(payload) {
  const { name = '', inputList = [] } = payload

  if (inputList.length === 0) {
    return `${ name }`
  }
  let params = ``
  inputList.map((item) => {
    const { name }= item
    return params = params + `${ name }: $${ name } `
  }).join(`\n`)

  const header = `${ name } (${ params })`

  return header
}

