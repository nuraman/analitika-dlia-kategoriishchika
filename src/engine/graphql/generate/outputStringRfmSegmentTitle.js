export default function({ name, output }) {
  const { errors = [], title = [] } = output || {}

  const errorStr = errors.reduce((errorStr, item) => {
    const { name } = item
    return errorStr = errorStr + `${ name } `
  },``)

  const titleStr = title.reduce((titleStr, item) => {
    const { name } = item

    return titleStr = titleStr + `${ name } `
  },``)

  return `
    errors { ${ errorStr } }
    title { ${ titleStr } }
    `
}
