export default function({ name, output, config }) {
  const { errors = [], result = [] } = output || {}

  const errorStr = errors.reduce((errorStr, item) => {
    const { name } = item
    return errorStr = errorStr + `${ name } `
  },``)

  const resultStr = result.reduce((resultStr, item) => {
    const { name } = item

    return resultStr = resultStr + `${ name } `
  },``)

  return `
    errors { ${ errorStr } }
    result { ${ resultStr } }
    `
}
