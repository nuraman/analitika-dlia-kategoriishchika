export default function({ name, output }) {
  const { errors = [], result = [], pie = [], title } = output || {}

  const errorStr = errors.reduce((errorStr, item) => {
    const { name } = item
    return errorStr = errorStr + `${ name } `
  },``)

  const resultStr = result.reduce((resultStr, item) => {
    const { name } = item

    return resultStr = resultStr + `${ name } `
  },``)

  const pieStr = pie.reduce((pieStr, item) => {
    const { name } = item

    return pieStr = pieStr + `${ name } `
  },``)

  const titleStr = title.reduce((titleStr, item) => {
    const { name } = item

    return titleStr = titleStr + `${ name } `
  },``)

  return `
    errors { ${ errorStr } }
    result { ${ resultStr } }
    pie { ${ pieStr } }
    title { ${ titleStr } }
   `
}
