export default function(payload) {
  const { name = '', inputList = [] } = payload

  if (inputList.length === 0) {
    return `${ name }`
  }

  const paramList = inputList.reduce((header, item) => {
    const { name, type }= item
    return header = header + `$${ name }: ${ type }, `
  },``)

  const header = `${ name } (${ paramList })`

  return header
}


