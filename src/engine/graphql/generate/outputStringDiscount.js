export default function({ name, output }) {
  const { errors = [], resultGoods, resultPoints, resultOther = [] } = output || {}

  const errorStr = errors.reduce((errorStr, item) => {
    const { name } = item
    return errorStr = errorStr + `${ name } `
  },``)

  const resultGoodsStr = resultGoods.reduce((resultGoodsStr, item) => {
    const { name } = item

    return resultGoodsStr = resultGoodsStr + `${ name } `
  },``)

  const resultPointsStr = resultPoints.reduce((resultPointsStr, item) => {
    const { name } = item

    return resultPointsStr = resultPointsStr + `${ name } `
  },``)

  const resultOtherStr = resultOther.reduce((resultOtherStr, item) => {
    const { name } = item

    return resultOtherStr = resultOtherStr + `${ name }`
  }, ``)

  return `
    errors { ${ errorStr } }
    resultGoods { ${ resultGoodsStr } },
    resultPoints { ${ resultPointsStr } },
    resultOther { ${ resultOtherStr } }
   `
}
