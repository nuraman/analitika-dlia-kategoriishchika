export default function({ name, output }) {
  const { errors = [], result, providerList } = output || {}

  const errorStr = errors.reduce((errorStr, item) => {
    const { name } = item
    return errorStr = errorStr + `${ name } `
  },``)

  const resultStr = result.reduce((resultStr, item) => {
    const { name } = item

    return resultStr = resultStr + `${ name } `
  },``)

  const providerStr = providerList.reduce((providerStr, item) => {
    const { name } = item

    return providerStr = providerStr + `${ name } `
  },``)


  return `
    errors { ${ errorStr } }
    result { ${ resultStr } }
    providerList { ${ providerStr } }
   `
}
