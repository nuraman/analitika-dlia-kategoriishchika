export default function({ name, output }) {
  const { errors = [], result = [], title = [] } = output || {}

  const errorStr = errors.reduce((errorStr, item) => {
    const { name } = item
    return errorStr = errorStr + `${ name } `
  },``)

  const resultStr = result.reduce((resultStr, item) => {
    const { name } = item

    return resultStr = resultStr + `${ name } `
  },``)

  const titleStr = title.reduce((titleStr, item) => {
    const { name } = item

    return titleStr = titleStr + `${ name } `
  },``)

  return `
    errors { ${ errorStr } }
    result { ${ resultStr } }
    title { ${ titleStr } }
    `
}
