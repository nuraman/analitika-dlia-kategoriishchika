import gql                  from 'graphql-tag'

import findMutation         from 'engine/graphql/generate/findMutation'

import generateHeaderString from 'engine/graphql/generate/headerString'
import generateCallString   from 'engine/graphql/generate/callString'
import generateOutputString from 'engine/graphql/generate/outputStringSaveSetting'

export default function generateMutationCreate(payload) {
  const { mutationMap, name } = payload
  const mutationDescription = findMutation({ mutationMap, name })

  const { inputList, output } = mutationDescription || {}

  const mutation = `mutation ${ generateHeaderString({ name, inputList }) }
  { 
    ${ generateCallString({ name, inputList }) }
    {
      ${ generateOutputString({ name, output }) }
    }
  }`

  return gql`${ mutation }`
}
