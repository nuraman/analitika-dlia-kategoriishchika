import gql                  from 'graphql-tag'

import findQuery            from 'engine/graphql/generate/findQuery'

import generateHeaderString from 'engine/graphql/generate/headerString'
import generateCallString   from 'engine/graphql/generate/callString'
import
  generateOutputStringGrf04 from 'engine/graphql/generate/outputStringForGrf04'

export default function generateQueryCreate(payload) {
  const { queryMap, name } = payload
  const queryDescription = findQuery({ queryMap, name })

  const { inputList, output } = queryDescription || {}

  const query = `query ${ generateHeaderString({ name, inputList }) }
  { 
    ${ generateCallString({ name, inputList }) }
    {
      ${ generateOutputStringGrf04({ name, output }) }
    }
  }`

  return gql`${ query }`
}
