import gql                  from 'graphql-tag'

import findQuery            from 'engine/graphql/generate/findQuery'

import generateHeaderString from 'engine/graphql/generate/headerString'
import generateCallString   from 'engine/graphql/generate/callString'
import
  generateOutputString from 'engine/graphql/generate/outputStringTitle'

export default function generateQueryCreate(payload) {
  const { queryMap, name } = payload
  const queryDescription = findQuery({ queryMap, name })

  const { inputList, output } = queryDescription || {}

  const query = `query ${ generateHeaderString({ name, inputList }) }
  { 
    ${ generateCallString({ name, inputList }) }
    {
      ${ generateOutputString({ name, output }) }
    }
  }`

  return gql`${ query }`
}
