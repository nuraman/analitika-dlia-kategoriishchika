import _                    from 'lodash'

export default function findQuery (payload) {
  const { queryMap, name } = payload

  const query = _.find(queryMap, { queryName: name })

  return query
}
