import es6                  from 'es6-shim'
import es5                  from 'es5-shim'

import injectTapEventPlugin from 'react-tap-event-plugin'

import React                from 'react'
import ReactDOM             from 'react-dom'

import { ApolloProvider }   from 'react-apollo'
import { ApolloClient  }    from 'react-apollo'
import {
  createNetworkInterface,
}                           from 'react-apollo'

import queryString          from 'query-string'
import Router               from 'universal-router'

import routes               from 'app/page/web/routes'

import store                from 'app/store'

import App                  from 'app/page/web/App'

import history              from 'engine/history'

require('weakmap-polyfill')

injectTapEventPlugin()


const container = document.getElementById('root')

const router = new Router(routes)
let currentLocation = history.location

const url = 'http://mindcloud.pro/kpl/graphql.php'

const networkInterface = createNetworkInterface({
  uri: url,
  opts: {
    credentials: 'include',
  },
  transportBatching: true,
})

//same-origin

networkInterface.use([{
  applyMiddleware(req, next) {
    if (!req.options.headers) {
      req.options.headers = {}
    }
    const token = localStorage.getItem('sessionID')
    req.options.headers['Authorization'] = token ? token : null
    next()
  }
}])


const client = new ApolloClient({
  networkInterface,
})

const storeWithApollo = store({ client })

async function onLocationChange(location, action) {
  currentLocation = location
  try {
    const route = await router.resolve({
      path: location.pathname,
      query: queryString.parse(location.search)
    })
    if (currentLocation.key !== location.key) return

    if (route.redirect) {
      history.replace(route.redirect)
      return
    }

    ReactDOM.render(
      <ApolloProvider  client={ client } store={ storeWithApollo }>
        <App title={ route.title }>
          { route.component }
        </App>
      </ApolloProvider>,
      container
    )
  } catch (error) {
    console.error(error)
  }
}

history.listen(onLocationChange)
onLocationChange(currentLocation)
